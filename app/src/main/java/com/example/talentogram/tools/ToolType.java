package com.example.talentogram.tools;

public enum ToolType {
    BRUSH,
    TEXT,
    ERASER,
    FILTER,
    EMOJI,
    STICKER
}
