package com.example.talentogram;

import android.app.Application;
import android.content.ContextWrapper;

import com.example.talentogram.others.MyConstants;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.pixplicity.easyprefs.library.Prefs;

public class TalentogramApplication extends Application {
    String userAgent = null;
    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        DatabaseReference recentRef = FirebaseDatabase.getInstance().getReference(MyConstants.TBL_RECENT);
        recentRef.keepSynced(true);

        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference(MyConstants.TBL_USER);
        usersRef.keepSynced(true);

        DatabaseReference messagesRef = FirebaseDatabase.getInstance().getReference(MyConstants.TBL_MESSAGES);
        messagesRef.keepSynced(true);

        DatabaseReference typingRef = FirebaseDatabase.getInstance().getReference(MyConstants.TBL_TYPING);
        typingRef.keepSynced(true);

        userAgent = Util.getUserAgent(this, "Talentogram");

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }
}
