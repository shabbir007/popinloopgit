package com.example.talentogram.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.talentogram.R;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class RegisterActivity extends AppCompatActivity {

    public TextView tvLogin;
    public Button btnSignup;
    private CircularImageView ivUserImage;
    private EditText edtfname;
    private EditText edtlname;
    private EditText edtemail;
    private EditText edtpwd,edtconfirm_pwd,tvDob;
    private RadioButton rdmale, rdfemale, rdOther;
    private EditText edtupi;
    private RelativeLayout rlImageSelect;
    private RadioGroup rgGender;
    private String dateStr = "";
    int age = 0;

    private String text = "Already have an Account? <font color=\\\"#ff5722\\\">LOGIN</font>";
    public Uri selectedUri;
    private String filePathFirst = "", fname = "", lname = "", email = "", password = "", edtdob = "", upi = "",confirm_pass;
    private File file;
    RadioButton radioButtonValue;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    boolean isShowText = true;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        bindReference();
        bindClick();
        StartFirebaseLogin();
        edtpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        tvLogin.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
    }

    private void bindReference() {
        tvLogin = findViewById(R.id.tvLogin);
        btnSignup = findViewById(R.id.btnSignup);
        ivUserImage = findViewById(R.id.imageView);
        edtfname = findViewById(R.id.edtfname);
        edtlname = findViewById(R.id.edtlname);
        edtemail = findViewById(R.id.edtemail);
        edtpwd = findViewById(R.id.edtpwd);
        rdmale = findViewById(R.id.rdmale);
        rdfemale = findViewById(R.id.rdfemale);
        rdOther = findViewById(R.id.rdOther);
        edtupi = findViewById(R.id.edtupi);
        rlImageSelect = findViewById(R.id.rlImageSelect);
        rgGender = findViewById(R.id.rgGender);
        tvDob = findViewById(R.id.tvDob);
        edtconfirm_pwd = findViewById(R.id.edtconfirm_pwd);

    }


    private void bindClick() {

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isValid();
            }
        });

        rlImageSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(RegisterActivity.this,
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(RegisterActivity.this,
                            new String[]{Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
                } else
                    CropImage.startPickImageActivity(RegisterActivity.this);
            }
        });

        tvDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickDate(tvDob, -1L, -1L);
            }
        });

    }

    private void pickDate(final TextView tv_date, Long minDate, Long maxDate) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        dateStr = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year + " ";
                        age =getAge(year, monthOfYear, dayOfMonth);
                        tv_date.setText(dateStr);
                    }
                }, mYear, mMonth, mDay);


        c.add(Calendar.DATE, 1);
        if (minDate != 1980) {
            datePickerDialog.getDatePicker().setMinDate(minDate);
        }
        c.add(Calendar.DATE, 90);
        if (maxDate != -1L)
            datePickerDialog.getDatePicker().setMaxDate(maxDate);
        datePickerDialog.show();

    }

    public int getAge(int _year, int _month, int _day) {
        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if (a < 0)
            throw new IllegalArgumentException("Age < 0");
        return a;
    }

    @Override
    public void onBackPressed() {
      startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
      finish();

    }
    public void onClickPasswordShowHide(View view){
        if(isShowText){
            isShowText = false;
            edtpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }else{
            isShowText = true;
            edtpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    private void isValid() {
        fname = edtfname.getText().toString().trim();
        lname = edtlname.getText().toString().trim();
        email = edtemail.getText().toString().trim();
        password = edtpwd.getText().toString().trim();
        edtdob = tvDob.getText().toString().trim();
        upi = edtupi.getText().toString().trim();
        confirm_pass = edtconfirm_pwd.getText().toString().trim();

        int selectedId = rgGender.getCheckedRadioButtonId();
        radioButtonValue =  findViewById(selectedId);


        if (TextUtils.isEmpty(fname)) {
            edtfname.requestFocus();
            edtfname.setError(getString(R.string.blank_validation_first_name));
        } else if (TextUtils.isEmpty(email)) {
            edtemail.requestFocus();
            edtemail.setError(getString(R.string.blank_validation_email));
        } else if (TextUtils.isEmpty(password) || password.length() < 8) {
            edtpwd.requestFocus();
            edtpwd.setError(getString(R.string.blank_validation_password_digit));
        } else if (TextUtils.isEmpty(confirm_pass) || !password.equals(confirm_pass)) {
            edtconfirm_pwd.requestFocus();
            edtconfirm_pwd.setError(getString(R.string.blank_validation_password_match));
        }else if (TextUtils.isEmpty(edtdob)) {
            tvDob.requestFocus();
            tvDob.setError(getString(R.string.blank_validation_age));
        } else if (age <= 13 || age >= 100) {
            Utils.showWarn(RegisterActivity.this, getString(R.string.blank_validation_underage));
        } else {
            if (email.matches(emailPattern)) {
                doRegister();
            } else if (android.util.Patterns.PHONE.matcher(email).matches()) {
                String phn = "+91"+email;
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        phn,                     // Phone number to verify
                        60,                           // Timeout duration
                        TimeUnit.SECONDS,                // Unit of timeout
                        RegisterActivity.this,        // Activity (for callback binding)
                        mCallback);

            }
        }

    }

    private void StartFirebaseLogin() {

        auth = FirebaseAuth.getInstance();
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Utils.showWarn(RegisterActivity.this,"Please Check your number.");

            }
            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);

                new AlertDialog.Builder(RegisterActivity.this)
                        .setMessage("OTP Sent Succesfully")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent mIntent = new Intent(RegisterActivity.this, OtpViewActivity.class);
                                Bundle extras = new Bundle();
                                extras.putString("FirstName", fname);
                                extras.putString("LastName", lname);
                                extras.putString("MobileNumber", email);
                                extras.putString("Password", password);
                                extras.putString("Password_confirmation", confirm_pass);
                                extras.putString("Age", String.valueOf(age));
                                extras.putString("Gender", radioButtonValue.getText().toString());
                                extras.putString("ProfilePic", String.valueOf(file));
                                extras.putString("verificationCode", s);
                                mIntent.putExtras(extras);
                                startActivity(mIntent);
                                finish();
                                dialog.dismiss();
                            }
                        })
                        .show();

            }
        };
    }

    private void doRegister() {
        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("FirstName", fname);
        userdata.put("LastName", lname);
        userdata.put("Email", email);
        userdata.put("Password", password);
        userdata.put("Password_confirmation", confirm_pass);
        userdata.put("Age", String.valueOf(age));
        userdata.put("Gender", radioButtonValue.getText().toString());
        userdata.put("MobileNumber", "");
        //userdata.put("UPI",upi);

        HashMap<String, File> postFile = new HashMap<>();
        postFile.put("ProfilePic", file);

        new NetworkCall(RegisterActivity.this, Services.REGISTER, userdata, postFile, true, NetworkCall.REQ_POST, false, multiPartRequest);
    }

    NetworkCall.MultiPartRequest multiPartRequest = new NetworkCall.MultiPartRequest() {

        @Override
        public void myResponseMethod(String response) {
            try {
                if (!response.isEmpty()) {
                    JSONObject obj = new JSONObject(response);
                    boolean Success = obj.getBoolean("Success");
                    String message = obj.getString("message");
                    // String email = message.getString("Email");
                    if (Success) {
                        try {
                            new AlertDialog.Builder(RegisterActivity.this)
                                    .setMessage(message + "\n " + getString(R.string.blank_email_check_validation))
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                            finish();
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        new AlertDialog.Builder(RegisterActivity.this)
                                .setMessage(message)
                                .show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start(RegisterActivity.this);
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(RegisterActivity.this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(RegisterActivity.this, imageUri)) {
                selectedUri = imageUri;
                filePathFirst = selectedUri.getPath().toString();
                file = new File(filePathFirst);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivUserImage.setImageURI(result.getUri());
                selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath().toString();
                file = new File(filePathFirst);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }
    }
}
