package com.example.talentogram.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.talentogram.R;
import com.example.talentogram.adapter.CommentAdapter;
import com.example.talentogram.chat.model.Users;
import com.example.talentogram.custom_views.CustomEditText;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.helper.OnCommentEditListener;
import com.example.talentogram.model.CommentModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommentActivity extends AppCompatActivity {
    private RecyclerView rvComments;
    private ImageView ivBack, ivSend;
    public String postId;
    private CustomEditText etComment;
    private ArrayList<CommentModel> commentList;
    private int commentId = 0, position;
    CommentAdapter commentAdapter;
    private String comment;
    LinearLayout llNoData;
    private LinearLayout llback;
    private Users otherUser;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Intent intent = getIntent();
        postId = intent.getStringExtra("postId");

        initView();
        tvTitle.setText(getResources().getString(R.string.txt_comments));
        bindWidget();
        getComments();


    }

    private void getComments() {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(postId));

        new NetworkCall(CommentActivity.this, Services.COMMENT_LIST, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        // String message = obj.getString("message");
                        if (Success) {
                            rvComments.setVisibility(View.VISIBLE);
                            llNoData.setVisibility(View.GONE);
                            commentList = new ArrayList<>();
                            JSONArray jPostData = obj.getJSONArray("data");
                            commentList = (ArrayList<CommentModel>)
                                    new Gson().fromJson(jPostData.toString(),
                                            new TypeToken<List<CommentModel>>() {
                                            }.getType());
                            commentsListView();

                        } else {
                            rvComments.setVisibility(View.GONE);
                            llNoData.setVisibility(View.VISIBLE);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    private void bindWidget() {
        llback.setOnClickListener(v -> onBackPressed());
        ivSend.setOnClickListener(v -> {
            if (TextUtils.isEmpty(etComment.getText().toString())) {
                Utils.showWarn(CommentActivity.this, "Please add your comment", 3);
            }else
            { Utils.hideSoftKeyboard(CommentActivity.this);
                if (commentId == 0) {
                    addComment();
                } else {
                    editComment(commentId, etComment.getText().toString().trim(), position);
                }
            }
        });
    }

    private void initView() {
        rvComments = findViewById(R.id.rvComments);
        ivBack = findViewById(R.id.ivBack);
        ivSend = findViewById(R.id.ivSend);
        etComment = findViewById(R.id.etComment);
        llNoData = findViewById(R.id.llNoData);
        llback = findViewById(R.id.llBack);
        tvTitle =findViewById(R.id.tvTitle);
    }

    private void addComment() {
        Utils.hideSoftKeyboard(CommentActivity.this);
        comment = etComment.getText().toString().trim();
        if (TextUtils.isEmpty(comment)) {
            Utils.showWarn(CommentActivity.this, "Please add your comment", 3);
        } else {
            HashMap<String, String> userData = new HashMap<>();
            userData.put("PostId", String.valueOf(postId));
            userData.put("Comment", comment);

            new NetworkCall(CommentActivity.this, Services.COMMENT_POST, userData,
                    true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            etComment.getText().clear();
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            JSONArray jPostData = obj.getJSONArray("data");
                            String message = obj.getString("message");
                            if (Success) {
                                Toast.makeText(CommentActivity.this, message, Toast.LENGTH_SHORT).show();
                                getComments();
                            } else {
                                Toast.makeText(CommentActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
        }

    }

    private void commentsListView() {
        rvComments.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        commentAdapter = new CommentAdapter(CommentActivity.this, commentList, new OnCommentEditListener() {
            @Override
            public void onClick(int id, String comment, int Position) {
                etComment.setText(comment);
                commentId = id;
                position = Position;
            }
        });
        rvComments.setHasFixedSize(true);
        rvComments.setItemViewCacheSize(20);
        rvComments.setDrawingCacheEnabled(true);
        rvComments.setAdapter(commentAdapter);
    }

    private void editComment(int id, String comment, int Position) {
        String auth = new MySharedPref(CommentActivity.this).getString(CommentActivity.this, MySharedPref.USER_AUTH, "");
        HashMap<String, String> userData = new HashMap<>();
        userData.put("CommentId", String.valueOf(id));
        userData.put("Comment", comment);

        AndroidNetworking.upload(Services.EDIT_COMMENT)
                .addHeaders("X-Requested-With", "XMLHttpRequest")
                .addHeaders("Authorization", "Bearer " + auth)
                .addMultipartParameter("CommentId", String.valueOf(id))
                .addMultipartParameter("Comment", comment)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean Success = response.getBoolean("Success");
                            String message = response.getString("message");
                            if (Success) {
                                commentList.get(Position).setCommentMessage(comment);
                                commentAdapter.notifyDataSetChanged();
                                etComment.getText().clear();
                                Toast.makeText(CommentActivity.this, message, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(CommentActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError ===>", String.valueOf(anError));
                    }
                });

    }
}
