package com.example.talentogram.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.talentogram.R;
import com.example.talentogram.network.Services;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OtpView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class OtpViewActivity extends AppCompatActivity {
    private String fname = "", lname = "", phn = "", password = "", password_confirmation = "", edtdob = "", gender = "", file = "";
    private Button btnSignIn;
    private OtpView otp_view;
    private FirebaseAuth auth;
    private String otp;
    private String verificationCode;
    private File fileUpload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_view);
        auth = FirebaseAuth.getInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fname = bundle.getString("FirstName");
            lname = bundle.getString("LastName");
            phn = bundle.getString("MobileNumber");
            password = bundle.getString("Password");
            password_confirmation = bundle.getString("Password_confirmation");
            edtdob = bundle.getString("Age");
            gender = bundle.getString("Gender");
            file = bundle.getString("ProfilePic");
            verificationCode = bundle.getString("verificationCode");
        }
        btnSignIn = findViewById(R.id.btn_verify);
        otp_view = findViewById(R.id.otp_view);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = otp_view.getText().toString();
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, otp);
                SigninWithPhone(credential);
            }
        });


    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(OtpViewActivity.this, RegisterActivity.class));
        finish();
    }

    private void SigninWithPhone(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //Toast.makeText(OtpViewActivity.this, "Verified OTP", Toast.LENGTH_SHORT).show();
                           doRegister();
                        } else {
                            Toast.makeText(OtpViewActivity.this, "Incorrect OTP", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void doRegister() {
        if (file != null && !file.isEmpty() && !file.equals("null"))
        {
            fileUpload = new File(file);
            AndroidNetworking.upload(Services.REGISTER)
                    .addHeaders("X-Requested-With", "XMLHttpRequest")
                    .addMultipartParameter("FirstName", fname)
                    .addMultipartParameter("LastName", lname)
                    .addMultipartParameter("MobileNumber", phn)
                    .addMultipartParameter("Password", password)
                    .addMultipartParameter("Password_confirmation", password_confirmation)
                    .addMultipartParameter("Age", edtdob)
                    .addMultipartParameter("Gender", gender)
                    .addMultipartParameter("Email", "")
                    .addMultipartFile("ProfilePic", fileUpload)
                    .setTag("uploadTest")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                boolean Success = response.getBoolean("Success");
                                String message = response.getString("message");
                                if (Success) {
                                    new AlertDialog.Builder(OtpViewActivity.this)
                                            .setMessage(message)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent i = new Intent(OtpViewActivity.this,LoginActivity.class);
                                                    startActivity(i);
                                                    dialog.dismiss();
                                                }
                                            })
                                            .show();
                                } else {
                                    Toast.makeText(OtpViewActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("anError ===>", String.valueOf(anError));
                        }
                    });
        }
        else {
            AndroidNetworking.upload(Services.REGISTER)
                    .addHeaders("X-Requested-With", "XMLHttpRequest")
                    .addMultipartParameter("FirstName", fname)
                    .addMultipartParameter("LastName", lname)
                    .addMultipartParameter("MobileNumber", phn)
                    .addMultipartParameter("Password", password)
                    .addMultipartParameter("Password_confirmation", password_confirmation)
                    .addMultipartParameter("Age", edtdob)
                    .addMultipartParameter("Gender", gender)
                    .setTag("uploadTest")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                boolean Success = response.getBoolean("Success");
                                String message = response.getString("message");
                                if (Success) {
                                    new AlertDialog.Builder(OtpViewActivity.this)
                                            .setMessage(message)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent i = new Intent(OtpViewActivity.this,LoginActivity.class);
                                                    startActivity(i);
                                                    dialog.dismiss();
                                                }
                                            })
                                            .show();
                                } else {
                                    Toast.makeText(OtpViewActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("anError ===>", String.valueOf(anError));
                        }
                    });
        }

    }

}


