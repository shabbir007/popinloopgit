package com.example.talentogram.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.talentogram.R;
import com.example.talentogram.adapter.JoinMembershipAdapter;

public class JoinMembershipActivity extends AppCompatActivity {

    public static ViewPager vp;
    public static JoinMembershipAdapter jMemberShipAdapter;
    private ImageView ivBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_membership);


        vp = findViewById(R.id.vp_joinmembership);
        ivBack = findViewById(R.id.ivBack);

        jMemberShipAdapter = new JoinMembershipAdapter(getSupportFragmentManager());

        vp.setAdapter(jMemberShipAdapter);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    public void moveToNext() {
        int currentPos = vp.getCurrentItem();
        if (currentPos < 2) {
            vp.setCurrentItem(++currentPos);
        }
    }

    public void moveToPrevious() {
        int currentPos = vp.getCurrentItem();
        if (currentPos > 0) {
            vp.setCurrentItem(--currentPos);
        }
    }
}
