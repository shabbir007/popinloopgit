package com.example.talentogram.activity;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.adapter.WalletHistoryAdapter;
import com.example.talentogram.model.WalletHistoryModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.retrofit.ApiClient;
import com.example.talentogram.retrofit.ApiInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletHistoryActivity extends AppCompatActivity {

    private Button btnAddMoney;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rvHistory)
    RecyclerView rvHistory;

    WalletHistoryAdapter adapter;
    ArrayList<WalletHistoryModel.History>model = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_history);
        ButterKnife.bind(this);
        tvTitle.setText(R.string.title_wallet_history);
        getWalleHistory();


    }

    private void getWalleHistory() {
        Utils.showProgressDialog(WalletHistoryActivity.this);
        ApiInterface apiInterface = ApiClient.getClient(WalletHistoryActivity.this).create(ApiInterface.class);

        Call<WalletHistoryModel> call = apiInterface.getHistory();
        call.enqueue(new Callback<WalletHistoryModel>() {
            @Override
            public void onResponse(Call<WalletHistoryModel> call, Response<WalletHistoryModel> response) {
                Utils.hideProgressDialog();
                if (response.body().isSuccess()) {
                    model = response.body().getHistory();
                    setData();
                }
            }

            @Override
            public void onFailure(Call<WalletHistoryModel> call, Throwable t) {
                Utils.hideProgressDialog();
                Utils.showWarn(WalletHistoryActivity.this, "No internet Conection");
            }
        });
    }


    private void setData() {
        adapter = new WalletHistoryAdapter(WalletHistoryActivity.this, model);
        LinearLayoutManager layoutManager = new LinearLayoutManager(WalletHistoryActivity.this, RecyclerView.VERTICAL,false);
        rvHistory.setLayoutManager(layoutManager);
        rvHistory.setAdapter(adapter);
    }

    @OnClick(R.id.llBack)
    void backPress() {
        onBackPressed();
    }

    private void addMoneyDialog() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.money_added_succesfully, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        promptsView.findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
