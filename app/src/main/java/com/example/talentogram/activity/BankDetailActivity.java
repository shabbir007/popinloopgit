package com.example.talentogram.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;

import com.example.talentogram.helper.MySharedPref;
import com.pixplicity.easyprefs.library.Prefs;
import com.skydoves.elasticviews.ElasticFloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BankDetailActivity extends AppCompatActivity {

    @BindView(R.id.fab)
    ElasticFloatingActionButton fab;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.txtHolderName)
    TextView txtHolderName;

    @BindView(R.id.txtAccountNumber)
    TextView txtAccountNumber;

    @BindView(R.id.txtIfscCode)
    TextView txtIfscCode;

    @BindView(R.id.llAccountDetail)
    LinearLayout llAccountDetail;

    @BindView(R.id.rlNoBankDetailNote)
    RelativeLayout rlNoBankDetailNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_detail);
        ButterKnife.bind(this);
        tvTitle.setText("Bank Detail");

        setData();


    }

    private void setData() {
        if (TextUtils.isEmpty(Prefs.getString(MySharedPref.BANK_ACCOUNT_NAME, ""))) {
            llAccountDetail.setVisibility(View.GONE);
            rlNoBankDetailNote.setVisibility(View.VISIBLE);
        } else {
            llAccountDetail.setVisibility(View.VISIBLE);
            rlNoBankDetailNote.setVisibility(View.GONE);
            txtHolderName.setText(Prefs.getString(MySharedPref.BANK_ACCOUNT_NAME, ""));
            txtAccountNumber.setText(Prefs.getString(MySharedPref.ACCOUNT_NUMBER, ""));
            txtIfscCode.setText(Prefs.getString(MySharedPref.IFSCCODE, ""));
        }
    }

    @OnClick(R.id.ivBack)
    void clickBack() {
        finish();
    }

    @OnClick(R.id.imgEdit)
    void clickEditBank() {
        Intent i = new Intent(BankDetailActivity.this, EditBankDetailActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.fab)
    void clickBankDetail() {
        Intent i = new Intent(BankDetailActivity.this, AddBankDetailActivity.class);
        startActivity(i);
    }
}
