package com.example.talentogram.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.adapter.FollowersAdapter;
import com.example.talentogram.model.FollowersModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FollowersActivity extends AppCompatActivity {

    private ArrayList<FollowersModel> followersList = new ArrayList<>();
    private FollowersAdapter followersAdapter;
    private RecyclerView rvFollowers;
    private ImageView ivBack;
    private LinearLayout llNoFollowers;
    private LinearLayout llback;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers);

        bindWidgetReference();
        followersListView();
    }

    private void bindWidgetReference() {
        rvFollowers = findViewById(R.id.rvFollowers);
        llNoFollowers = findViewById(R.id.llNoFollowers);
        ivBack = findViewById(R.id.ivBack);
        llback = findViewById(R.id.llBack);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getResources().getString(R.string.txt_followers));
        llback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void followersListView() {

        new NetworkCall(this, Services.FOLLOWER_LIST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");
                    if (Success) {
                        JSONArray jfollowersList = jsonObject.getJSONArray("data");
                        followersList.clear();
                        followersList = (ArrayList<FollowersModel>)
                                new Gson().fromJson(jsonObject.get("data").toString(),
                                        new TypeToken<ArrayList<FollowersModel>>() {
                                        }.getType());

                        followersAdapter = new FollowersAdapter(FollowersActivity.this, followersList);
                        rvFollowers.setAdapter(followersAdapter);
                        rvFollowers.setLayoutManager(new LinearLayoutManager(FollowersActivity.this,
                                LinearLayoutManager.VERTICAL, false));
                        if (followersList.size() == 0) {
                            llNoFollowers.setVisibility(View.VISIBLE);
                            rvFollowers.setVisibility(View.GONE);
                        }
                    } else {
                        llNoFollowers.setVisibility(View.VISIBLE);
                        rvFollowers.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

    }
}
