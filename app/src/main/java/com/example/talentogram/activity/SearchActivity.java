package com.example.talentogram.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.adapter.CatagoriesWiseVideoAdapter;
import com.example.talentogram.adapter.TrendingAdapter;
import com.example.talentogram.model.CatagoryWiseVideoModel;
import com.example.talentogram.model.TrendingModel;
import com.example.talentogram.others.CenteredImageSpan;
import com.greenfrvr.hashtagview.HashtagView;

import java.util.ArrayList;

import static com.example.talentogram.R.color;
import static com.example.talentogram.R.dimen;
import static com.example.talentogram.R.drawable;
import static com.example.talentogram.R.id;
import static com.example.talentogram.R.layout;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView rlTrending,rvCatagotieswise;
    private TrendingAdapter trendingAdapter;
    private CatagoriesWiseVideoAdapter catagoriesWiseVideoAdapter;
    private  ArrayList<CatagoryWiseVideoModel> catagoryWiseVideoModelArrayList = new ArrayList<>();
    private HashtagView hashTagCategory;
    private ArrayList<TrendingModel> trendingModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_search);

        bindReference();
        addDataTrending();
        addDataCatagorieWise();
        setHasTag();
    }

    private void setHasTag() {
        trendingModels.add(new TrendingModel("1", "KabirSingh", "https://cdn.shopify.com/s/files/1/1061/1924/files/Shyly_Smiling_Face_Emoji.png?8026536574188759287"));
        trendingModels.add(new TrendingModel("1", "Bollywood", "1"));
        trendingModels.add(new TrendingModel("1", "Hollywood", "1"));
        trendingModels.add(new TrendingModel("1", "Attitude", "1"));
        trendingModels.add(new TrendingModel("1", "Religion", "1"));

        trendingAdapter.notifyDataSetChanged();
        hashTagCategory.setData(trendingModels, new HashtagView.DataTransform<TrendingModel>() {
            @Override
            public CharSequence prepare(TrendingModel item) {
                String label = item.getTrendingName();
                SpannableString spannableString = new SpannableString(label);
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(color.white)), 1,
                        spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                Drawable myIcon = getResources().getDrawable(drawable.smile);
                int size = getApplication().getResources().getDimensionPixelSize(dimen._15sdp);
                myIcon.setBounds(0, 0, size, size);
                CenteredImageSpan btnFeedback = new CenteredImageSpan(myIcon, ImageSpan.ALIGN_BASELINE);
                spannableString.setSpan(
                        btnFeedback,
                        label.length() - 1,
                        label.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                return spannableString;
            }
        });
    }

    private void bindReference() {
        hashTagCategory = findViewById(id.hashTagCategory);
        rlTrending = findViewById(id.rlTrending);
        rvCatagotieswise = findViewById(id.rvCatagotieswise);
    }

    private void addDataTrending() {
        rlTrending.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        trendingAdapter = new TrendingAdapter(this, trendingModels);
        rlTrending.setAdapter(trendingAdapter);
    }

    private void addDataCatagorieWise() {
        catagoryWiseVideoModelArrayList.clear();

        for (int i = 0; i < 5; i++) {
            CatagoryWiseVideoModel catagoryWiseVideoModel = new CatagoryWiseVideoModel();
            catagoryWiseVideoModel.setCtTitle("Bollywood Songs");
            catagoryWiseVideoModel.setVideoCount("7458 Videos");
            catagoryWiseVideoModel.setfImgUrl("https://upload.wikimedia.org/wikipedia/en/d/dc/Kabir_Singh.jpg");
            catagoryWiseVideoModel.setsImgUrl("https://c.ndtvimg.com/2019-08/nsrpbl9g_friendship-day_625x300_04_August_19.jpg");
            catagoryWiseVideoModelArrayList.add(catagoryWiseVideoModel);
        }

        rvCatagotieswise.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        catagoriesWiseVideoAdapter = new CatagoriesWiseVideoAdapter(this, catagoryWiseVideoModelArrayList);
        rvCatagotieswise.setAdapter(catagoriesWiseVideoAdapter);

    }


}



