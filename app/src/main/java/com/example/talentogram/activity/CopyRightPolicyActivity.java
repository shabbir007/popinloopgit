package com.example.talentogram.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;

public class CopyRightPolicyActivity extends AppCompatActivity {

    private LinearLayout llBack;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copy_right_policy);
        initView();
        tvTitle.setText(getResources().getString(R.string.txt_copy_right_policy));
        bindClick();
    }

    private void bindClick() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initView() {
        llBack =findViewById(R.id.llBack);
        tvTitle =findViewById(R.id.tvTitle);
    }
}
