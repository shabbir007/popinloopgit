package com.example.talentogram.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;

public class HelpCenterActivity extends AppCompatActivity {

    private LinearLayout llBack;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_center);
        initView();
        bindClick();
        tvTitle.setText(getResources().getString(R.string.txt_help_center));
    }

    private void bindClick() {
        llBack.setOnClickListener(view -> onBackPressed());
    }

    private void initView() {
        llBack = findViewById(R.id.llBack);
        tvTitle = findViewById(R.id.tvTitle);
    }
}
