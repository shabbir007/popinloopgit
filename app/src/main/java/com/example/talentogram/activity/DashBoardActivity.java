package com.example.talentogram.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.adapter.BottomAdapter;
import com.example.talentogram.adapter.DrawerAdapter;
import com.example.talentogram.chat.activity.RecentChatActivity;
import com.example.talentogram.contest.Contest_Uploads_Fragment;
import com.example.talentogram.fragment.NotificationFragment;
import com.example.talentogram.fragment.PostFragment;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.profile.ProfileFragment;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.material.navigation.NavigationView;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashBoardActivity extends AppCompatActivity {
    @BindView(R.id.ivUserImage)
    CircularImageView ivUserImg;
    @BindView(R.id.tvUsername)
    TextView tvUsername;


    public static RecyclerView rvBottom;
    public static Toolbar toolbar;
    public static ActionBarDrawerToggle actionBarDrawerToggle;
    private FrameLayout frame;
    public static FragmentManager manager;
    public static ActionBar actionBar;
    public static DrawerLayout drawer;
    private RecyclerView rvNavDrawerB;
    private NavigationView nvView;
    private ImageView imgChat;
    public boolean doubleBackToExitPressedOnce = false;
    BottomAdapter bottomAdapter;
    public static final int RUNTIME_PERMISSION_CODE = 7;
    public static final String EXTRA_TYPE = "type";
    private String TAG="Notification";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        ButterKnife.bind(this);

        bindReference();
        initView();
        AndroidRuntimePermission();
        rvBottom = findViewById(R.id.rvBottom);
        getUserProfile();
        setBottomView(2);
        rvNavDrawerB.setHasFixedSize(true);
        rvNavDrawerB.setLayoutManager(new LinearLayoutManager(DashBoardActivity.this));

        setSideData();

        imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashBoardActivity.this, RecentChatActivity.class));
            }
        });

    }

    private void setBottomView(int pos) {
        rvBottom.setHasFixedSize(true);
        rvBottom.setLayoutManager(new GridLayoutManager(DashBoardActivity.this, 5));
        bottomAdapter = new BottomAdapter(DashBoardActivity.this, pos, rvBottom);
        rvBottom.setAdapter(bottomAdapter);
    }


    public void setSideData() {
        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Integer> iconList = new ArrayList<>();

        nameList.add("Home");
        iconList.add(R.drawable.ic_home_icon);

        nameList.add("Push Notification");
        iconList.add(R.drawable.ic_notification);

        nameList.add("Report a Problem");
        iconList.add(R.drawable.ic_report);

        nameList.add("Setting");
        iconList.add(R.drawable.ic_setting);

        nameList.add("Terms of Use");
        iconList.add(R.drawable.ic_terms_and);

        nameList.add("About Us");
        iconList.add(R.drawable.ic_user);

        nameList.add("Privacy Policy");
        iconList.add(R.drawable.ic_privacy);

        nameList.add("Bank Account Detail");
        iconList.add(R.drawable.ic_bank_building);

        nameList.add("Wallet");
        iconList.add(R.drawable.ic_wallet_icon);

        nameList.add("CopyRight Policy");
        iconList.add(R.drawable.ic_copyright);

        nameList.add("Help Center");
        iconList.add(R.drawable.ic_help);

        nameList.add("Log Out");
        iconList.add(R.drawable.ic_logout);


        DrawerAdapter drawerAdapter = new DrawerAdapter
                (DashBoardActivity.this, nameList, iconList, drawer);
        rvNavDrawerB.setAdapter(drawerAdapter);
    }

    private void bindReference() {
        drawer = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        frame = findViewById(R.id.frame);
        nvView = findViewById(R.id.nvView);
        rvNavDrawerB = findViewById(R.id.rvNavDrawer);
        imgChat = findViewById(R.id.imgChat);
    }

    private void initView() {
        manager = getSupportFragmentManager();
        actionBar = getSupportActionBar();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        toolbar.setTitle("");
        initDrawer();
    }

    private void initDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        changeFragment(this, new PostFragment(), true);

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.d_ic_menu, getTheme());
        actionBarDrawerToggle.setHomeAsUpIndicator(drawable);

        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

    }

    public static void changeFragment(Context context, Fragment fragment, boolean doAdd) {
        FragmentManager manager = ((DashBoardActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        if (doAdd) {
            transaction.addToBackStack(fragment.getClass().getName());
        }
        transaction.commit();
    }

    public static void changeFragmentWithBundle(Context context, Fragment fragment, boolean doAdd,Bundle bundle) {
        FragmentManager manager = ((DashBoardActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        fragment.setArguments(bundle);
        if (doAdd)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();

    }

    private Context getActivity() {
        return this;
    }

    public void setCurrentFragment() {
        Fragment myFragment = getSupportFragmentManager().findFragmentById(R.id.frame);

        if (myFragment instanceof Contest_Uploads_Fragment) {
            setBottomView(3);
        } else if (myFragment instanceof PostFragment) {
            setBottomView(2);
        } else if (myFragment instanceof NotificationFragment) {
            setBottomView(1);
        } else if (myFragment instanceof ProfileFragment) {
            setBottomView(4);
        }
    }

    @Override
    public void onBackPressed() {
        toolbar.setVisibility(View.VISIBLE);
        FragmentManager manager = getSupportFragmentManager();

        if (manager.getBackStackEntryCount() == 1) {
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            manager.popBackStack();
            initDrawer();
            setBottomView(2);
            if (doubleBackToExitPressedOnce) {
                createDialog();
                initDialogComponents();
            }
            this.doubleBackToExitPressedOnce = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        } else if (manager.getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStackImmediate();
            setCurrentFragment();
            setBottomView(2);

        } else  {

        }

    }

    public static Dialog dialog;

    private void createDialog() {
        dialog = new Dialog(DashBoardActivity.this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_exit);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        window.setAttributes(lp);
        dialog.show();
    }


    private void initDialogComponents() {
        Button btnYes, btnNo;
        btnNo = dialog.findViewById(R.id.btnNo);
        btnYes = dialog.findViewById(R.id.btnYes);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
                moveTaskToBack(true);
            }
        });


        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void getUserProfile() {
        String userId = new MySharedPref(getApplicationContext()).getLoggedInUserID();
        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("UserProfileId", userId);

        new NetworkCall(DashBoardActivity.this, Services.GETUSERPROFILE,
                userdata, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");

                        if (Success) {
                            JSONObject user_data = obj.getJSONObject("user_data");
                            String FirstName = user_data.getString("FirstName");
                            String LastName = user_data.getString("LastName");
                            String Profile = user_data.getString("ProfilePic");

                            MySharedPref.setString(DashBoardActivity.this, MySharedPref.USER_FIRSTNAME, FirstName);
                            MySharedPref.setString(DashBoardActivity.this, MySharedPref.USER_LASTNAME, LastName);
                            MySharedPref.setString(DashBoardActivity.this, MySharedPref.USER_PROFILE_PIC, Profile);

                            Glide.with(DashBoardActivity.this)
                                    .load(Profile)
                                    .centerCrop()
                                    .placeholder(R.drawable.user)
                                    .into(ivUserImg);

                            tvUsername.setText(FirstName+" "+ LastName);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentFragment();
        getUserProfile();
    }

    // Creating Runtime permission function.
    public void AndroidRuntimePermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    android.app.AlertDialog.Builder alert_builder = new android.app.AlertDialog.Builder(DashBoardActivity.this);
                    alert_builder.setMessage("External Storage Permission is Required.");
                    alert_builder.setTitle("Please Grant Permission.");
                    alert_builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            ActivityCompat.requestPermissions(
                                    DashBoardActivity.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    RUNTIME_PERMISSION_CODE

                            );
                        }
                    });

                    alert_builder.setNeutralButton("Cancel", null);

                    android.app.AlertDialog dialog = alert_builder.create();

                    dialog.show();

                } else {

                    ActivityCompat.requestPermissions(
                            DashBoardActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            RUNTIME_PERMISSION_CODE
                    );
                }
            } else {

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case RUNTIME_PERMISSION_CODE: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
            }
        }
    }

}

