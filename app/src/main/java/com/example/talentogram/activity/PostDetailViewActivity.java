package com.example.talentogram.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.danikula.videocache.HttpProxyCacheServer;
import com.example.talentogram.R;
import com.example.talentogram.SegmentProgress.Variables;
import com.example.talentogram.contest.AudioDialogActivity;
import com.example.talentogram.model.PostModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostDetailViewActivity extends AppCompatActivity implements Player.EventListener {

    public String user_id_encrypt;
    public String appLinkAction;
    public Uri appLinkData;

    @BindView(R.id.ivUserProfile)
    ImageView ivUserProfile;

    @BindView(R.id.ivPostImage)
    ImageView ivPostImage;

    @BindView(R.id.ivPostAudio)
    ImageView ivPostAudio;

    @BindView(R.id.tvPosttitle)
    TextView tvPosttitle;

    @BindView(R.id.tvTime)
    TextView tvTime;

    @BindView(R.id.tvLike)
    TextView tvLike;

    @BindView(R.id.flImage)
    FrameLayout flImage;

    @BindView(R.id.flAudio)
    FrameLayout flAudio;

    @BindView(R.id.flVideo)
    FrameLayout flVideo;

    @BindView(R.id.flWriting)
    FrameLayout flWriting;

    @BindView(R.id.llWritingView)
    LinearLayout cvWritingView;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.tvWriting)
    TextView tvWriting;
    LinearLayoutManager layoutManager;
    public SimpleExoPlayer privious_player;
    boolean is_visible_to_user;
    boolean is_user_stop_video = false;
    private ArrayList<PostModel.ImageUrl> imageList;
    private ArrayList<PostModel.AudioUrl> audioList;
    private ArrayList<PostModel.VideoUrl> videoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail_view);
        ButterKnife.bind(this);

        appLinkAction = getIntent().getAction();
        appLinkData = getIntent().getData();
        layoutManager = new LinearLayoutManager(PostDetailViewActivity.this);

        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
            String recipeId = appLinkData.getLastPathSegment();
            user_id_encrypt = recipeId;
        }
        getPostDetail(user_id_encrypt);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getPostDetail(String user_id_encrypt) {
        new NetworkCall(this, Services.GET_POST_DETAIL + user_id_encrypt,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");
                    if (Success) {
                        JSONObject jData = jsonObject.getJSONObject("data");
                        JSONObject jUserData = jData.getJSONObject("userdata");
                        JSONObject jPostDetail = jData.getJSONObject("post_detail");
                        String ProfilePics = jUserData.getString("ProfilePics");
                        String ImageArray = jData.getString("ImageUrl");
                        String AudioUrl = jData.getString("AudioUrl");
                        String VideoUrl = jData.getString("VideoUrl");

                        imageList = new ArrayList<>();
                        imageList = (ArrayList<PostModel.ImageUrl>) new Gson().fromJson(ImageArray,
                                new TypeToken<ArrayList<PostModel.ImageUrl>>() {
                                }.getType());

                        audioList = (ArrayList<PostModel.AudioUrl>) new Gson().fromJson(AudioUrl,
                                new TypeToken<ArrayList<PostModel.AudioUrl>>() {
                                }.getType());

                        videoList = (ArrayList<PostModel.VideoUrl>) new Gson().fromJson(VideoUrl,
                                new TypeToken<ArrayList<PostModel.VideoUrl>>() {
                                }.getType());

                        Glide.with(PostDetailViewActivity.this)
                                .load(ProfilePics)
                                .placeholder(R.drawable.noimage)
                                .into(ivUserProfile);
                        tvPosttitle.setText(jUserData.getString("FirstName") + "" + jUserData.getString("LastName"));
                        tvTime.setText(Utils.getdate(jUserData.getString("created_at")));
                        tvLike.setText(jPostDetail.getString("LikeCount") + " Likes");

                        int PostType = jData.getInt("PostType");

                        switch (PostType) {
                            //audio
                            case 0:
                                flImage.setVisibility(View.GONE);
                                flAudio.setVisibility(View.VISIBLE);
                                flWriting.setVisibility(View.GONE);
                                flVideo.setVisibility(View.GONE);

                                try {
                                    Glide.with(PostDetailViewActivity.this)
                                            .load(R.drawable.audio_visualizer)
                                            .thumbnail(Glide.with(PostDetailViewActivity.this).load(R.drawable.audio_visualizer))
                                            .into(ivPostAudio);

                                } catch (Exception e) {
                                    e.printStackTrace();

                                }
                                flAudio.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String url = null;
                                        String title = null;
                                        try {
                                            url = audioList.get(0).getAudioUrl();
                                            title = jPostDetail.getString("Title");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Intent i = new Intent(PostDetailViewActivity.this, AudioDialogActivity.class);
                                        i.putExtra("AudioUrl", url);
                                        i.putExtra("title", title);
                                        startActivity(i);
                                    }
                                });
                                break;
                            //video
                            case 1:
                                flImage.setVisibility(View.GONE);
                                flAudio.setVisibility(View.GONE);
                                flVideo.setVisibility(View.VISIBLE);
                                flWriting.setVisibility(View.GONE);
                                String videourl = videoList.get(0).getVideoUrl();
                                Set_Player(videourl);

                                break;
                            //image
                            case 2:
                                flImage.setVisibility(View.VISIBLE);
                                flAudio.setVisibility(View.GONE);
                                flWriting.setVisibility(View.GONE);
                                flVideo.setVisibility(View.GONE);

                                Glide.with(PostDetailViewActivity.this)
                                        .load(imageList.get(0).getVideoUrl())
                                        .thumbnail(Glide.with(PostDetailViewActivity.this).load(R.drawable.noimage))
                                        .error(R.drawable.noimage)
                                        .into(ivPostImage);

                                break;
                            //writing
                            case 3:
                                flImage.setVisibility(View.GONE);
                                flAudio.setVisibility(View.GONE);
                                flVideo.setVisibility(View.GONE);
                                flWriting.setVisibility(View.VISIBLE);
                                tvWriting.setText(jPostDetail.getString("Description"));
                                if (!TextUtils.isEmpty(jData.getString("Color"))) {
                                    String color_ = "#" + jData.getString("Color");
                                    cvWritingView.setBackgroundColor(Color.parseColor(color_));
                                } else {
                                    cvWritingView.setBackgroundColor(R.color.DarkGray);
                                }
                                if (!TextUtils.isEmpty(jData.getString("Font"))) {
                                    String fontName = jData.getString("Font");
                                    Typeface font = Typeface.createFromAsset(getAssets(),
                                            "fonts/" + fontName);
                                    tvWriting.setTypeface(font);
                                }
                                break;
                        }

                    } else {
                        Toast.makeText(PostDetailViewActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private HttpProxyCacheServer proxy;

    public HttpProxyCacheServer getProxy(Context context) {
        return proxy == null ? (proxy = newProxy()) : proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(PostDetailViewActivity.this)
                .maxCacheSize(1024 * 1024 * 1024)
                .maxCacheFilesCount(20)
                .build();
    }

    public void Set_Player(String videourl) {

        HttpProxyCacheServer proxy = getProxy(PostDetailViewActivity.this);
        String proxyUrl = proxy.getProxyUrl(videourl);

        DefaultLoadControl loadControl = new DefaultLoadControl.Builder().
                setBufferDurationsMs(1 * 1024, 1 * 1024,
                        500, 1024).createDefaultLoadControl();

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(PostDetailViewActivity.this, trackSelector, loadControl);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(PostDetailViewActivity.this,
                Util.getUserAgent(PostDetailViewActivity.this,
                        PostDetailViewActivity.this.getResources().getString(R.string.app_name)));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(proxyUrl));

        Log.d(Variables.tag, videourl);
        Log.d(Variables.tag, proxyUrl);

        player.prepare(videoSource);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.addListener(PostDetailViewActivity.this);
        final PlayerView playerView;
        if (layoutManager != null) {
            playerView = findViewById(R.id.playerview);
            playerView.setPlayer(player);
            player.setPlayWhenReady(is_visible_to_user);
            privious_player = player;

            playerView.setOnTouchListener(new View.OnTouchListener() {
                private GestureDetector gestureDetector = new GestureDetector(PostDetailViewActivity.this, new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                        super.onFling(e1, e2, velocityX, velocityY);
                        float diffY = e2.getY() - e1.getY();
                        float diffX = e2.getX() - e1.getX();
                        if (Math.abs(diffX) > Math.abs(diffY)) {
                            if (Math.abs(diffX) > 100 && Math.abs(velocityX) > 1000) {
                                if (diffX > 0) {
                                    if (player.getPlayWhenReady()) {
                                        is_user_stop_video = true;
                                        privious_player.setPlayWhenReady(false);
                                    }
                                }
                            }
                        }
                        return true;
                    }

                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent e) {
                        if (!player.getPlayWhenReady()) {
                            is_user_stop_video = false;
                            privious_player.setPlayWhenReady(true);

                        } else {
                            is_user_stop_video = true;
                            privious_player.setPlayWhenReady(false);
                        }
                        return super.onSingleTapConfirmed(e);
                    }

                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        super.onSingleTapUp(e);
                        return true;
                    }

                    @Override
                    public void onLongPress(MotionEvent e) {
                        super.onLongPress(e);

                    }

                });

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    gestureDetector.onTouchEvent(event);
                    return true;
                }
            });


        }
    }
}