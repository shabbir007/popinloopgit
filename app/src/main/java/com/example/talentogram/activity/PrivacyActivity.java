package com.example.talentogram.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.talentogram.R;
import com.example.talentogram.others.MyConstants;

public class PrivacyActivity extends AppCompatActivity {

    private String data,description;
    private ImageView ivBack, ivPublic, ivonlyme, ivFriends;
    private LinearLayout llPublic, llFriends, llonlyme;
    private TextView tvTitle;
    private Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        initView();
        if (MyConstants.visible_me) {
            ivonlyme.setVisibility(View.VISIBLE);
            ivFriends.setVisibility(View.GONE);
            ivPublic.setVisibility(View.GONE);
        } else if (MyConstants.visible_friends) {
            ivonlyme.setVisibility(View.GONE);
            ivFriends.setVisibility(View.VISIBLE);
            ivPublic.setVisibility(View.GONE);
        } else {
            ivonlyme.setVisibility(View.GONE);
            ivFriends.setVisibility(View.GONE);
            ivPublic.setVisibility(View.VISIBLE);
        }
        bindClick();

    }
    private void bindClick() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });


        llPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConstants.visible_me = false;
                MyConstants.visible_friends = false;
                MyConstants.visible_public = true;
                ivPublic.setVisibility(View.VISIBLE);
                ivFriends.setVisibility(View.GONE);
                ivonlyme.setVisibility(View.GONE);

            }
        });

        llFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConstants.visible_me = false;
                MyConstants.visible_friends = true;
                MyConstants.visible_public = false;
                ivPublic.setVisibility(View.GONE);
                ivFriends.setVisibility(View.VISIBLE);
                ivonlyme.setVisibility(View.GONE);


            }
        });

        llonlyme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConstants.visible_me = true;
                MyConstants.visible_friends = false;
                MyConstants.visible_public = false;
                ivonlyme.setVisibility(View.VISIBLE);
                ivFriends.setVisibility(View.GONE);
                ivPublic.setVisibility(View.GONE);


            }
        });

    }
    private void initView() {
        ivBack = (ImageView) findViewById(R.id.ivBack);
        llPublic = (LinearLayout) findViewById(R.id.llPublic);
        ivPublic = (ImageView) findViewById(R.id.ivPublic);
        llFriends = (LinearLayout) findViewById(R.id.llFriends);
        ivFriends = (ImageView) findViewById(R.id.ivFriends);
        llonlyme = (LinearLayout) findViewById(R.id.llonlyme);
        ivonlyme = (ImageView) findViewById(R.id.ivonlyme);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

    }
}