package com.example.talentogram.activity;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.gallery.adapter.AudioAdapter;
import com.example.talentogram.helper.CustomTouchListener;
import com.example.talentogram.helper.MediaPlayerService;
import com.example.talentogram.helper.StorageUtil;
import com.example.talentogram.helper.onItemClickListener;
import com.example.talentogram.model.AudioModel;

import java.util.ArrayList;

import static com.example.talentogram.others.MyConstants.Broadcast_PLAY_NEW_AUDIO;

public class MediaActivity extends AppCompatActivity {
    private RecyclerView rvAudio;
    private MediaPlayerService player;
    boolean serviceBound = false;
    private ArrayList<AudioModel> audioModelList;
    private ImageView ivBack;
    private LinearLayout llback;
    private TextView tvTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);

        bindReference();
        GetAllMediaMp3Files();
        initRecyclerView();
    }

    private void bindReference() {
        rvAudio = findViewById(R.id.rvAudio);
        ivBack = findViewById(R.id.ivBack);
        llback = findViewById(R.id.llBack);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getResources().getString(R.string.txt_media));
        llback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initRecyclerView() {
        if (audioModelList != null && audioModelList.size() > 0) {
            AudioAdapter audioAdapter = new AudioAdapter(audioModelList, MediaActivity.this);
            rvAudio.setAdapter(audioAdapter);
            rvAudio.setLayoutManager(new LinearLayoutManager(MediaActivity.this));
            rvAudio.addOnItemTouchListener(new CustomTouchListener(MediaActivity.this, new onItemClickListener() {
                @Override
                public void onClick(View view, int index) {
                    playAudio(index);
                }
            }));
        }
    }
    public void GetAllMediaMp3Files() {

        ContentResolver contentResolver = getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";

        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            audioModelList = new ArrayList<>();
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                audioModelList.add(new AudioModel(data, title, album, artist,duration));
            }
        }
        if (cursor != null)
            cursor.close();
    }

    private void playAudio(int audioIndex) {
        //Check is service is active
        if (!serviceBound) {
            //Store Serializable audioModelList to SharedPreferences
            StorageUtil storage = new StorageUtil(MediaActivity.this);
            storage.storeAudio(audioModelList);
            storage.storeAudioIndex(audioIndex);
            startService(new Intent(MediaActivity.this, MediaPlayerService.class));
            bindService(new Intent(MediaActivity.this,MediaPlayerService.class),
                    serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            //Store the new audioIndex to SharedPreferences
            StorageUtil storage = new StorageUtil(MediaActivity.this);
            storage.storeAudioIndex(audioIndex);
            //Service is active
            //Send a broadcast to the service -> PLAY_NEW_AUDIO
            Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
            sendBroadcast(broadcastIntent);
        }
    }

    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };
}
