package com.example.talentogram.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;
import com.example.talentogram.contest.activity.ApplyForFreeWritingContestActivity;
import com.example.talentogram.contest.activity.ApplyFreeContestActivity;
import com.example.talentogram.contest.model.ContestPaidModel;
import com.example.talentogram.model.SuccessModel;
import com.example.talentogram.model.WalletHistoryModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.retrofit.ApiClient;
import com.example.talentogram.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WithdrawAmtActivity extends AppCompatActivity {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.edtAmt)
    EditText edtAmt;
    @BindView(R.id.tvbalance)
    TextView tvbalance;
    String amount = "", availBal = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_amt);
        ButterKnife.bind(this);

        tvTitle.setText(R.string.txt_withdraw);
        getWalleHistory();
    }

    @OnClick(R.id.llBack)
    void backPress() {
        onBackPressed();
    }

    private void getWalleHistory() {
        Utils.showProgressDialog(WithdrawAmtActivity.this);
        ApiInterface apiInterface = ApiClient.getClient(WithdrawAmtActivity.this).create(ApiInterface.class);

        Call<WalletHistoryModel> call = apiInterface.getHistory();
        call.enqueue(new Callback<WalletHistoryModel>() {
            @Override
            public void onResponse(Call<WalletHistoryModel> call, Response<WalletHistoryModel> response) {
                Utils.hideProgressDialog();
                if (response.body().isSuccess()) {
                    availBal = response.body().getAvail_balance();
                    tvbalance.setText("₹"+availBal);
                }
            }

            @Override
            public void onFailure(Call<WalletHistoryModel> call, Throwable t) {
                Utils.hideProgressDialog();
                Utils.showWarn(WithdrawAmtActivity.this, "No internet Conection");
            }
        });
    }

    @OnClick(R.id.btnWithDraw)
    void transferAmtToBank() {
        amount = edtAmt.getText().toString().trim();
        if (TextUtils.isEmpty(amount)) {
            edtAmt.requestFocus();
            edtAmt.setError(getString(R.string.alert_amt_empty));
        } else if (Integer.parseInt(amount) > Integer.parseInt(availBal)) {
            edtAmt.requestFocus();
            edtAmt.setError(getString(R.string.alert_balance_check));
        } else {
            Utils.showProgressDialog(WithdrawAmtActivity.this);
            ApiInterface apiInterface = ApiClient.getClient(WithdrawAmtActivity.this).create(ApiInterface.class);

            Call<SuccessModel> call = apiInterface.transferAmtToBank(amount);
            call.enqueue(new Callback<SuccessModel>() {
                @Override
                public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                    Utils.hideProgressDialog();
                    if (response.body().isSuccess()) {
                        new SweetAlertDialog(WithdrawAmtActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setContentText(response.body().getMessage())
                                .setConfirmText("Ok")
                                .showCancelButton(true)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                }).show();
                    } else {
                        new SweetAlertDialog(WithdrawAmtActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setContentText(response.body().getMessage())
                                .setConfirmText("Ok")
                                .showCancelButton(true)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();

                                    }
                                }).show();
                    }
                }

                @Override
                public void onFailure(Call<SuccessModel> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showWarn(WithdrawAmtActivity.this, "No internet Conection");
                }
            });
        }
    }
}

