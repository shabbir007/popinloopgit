package com.example.talentogram.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.talentogram.R;
import com.example.talentogram.chat.model.Recent;
import com.example.talentogram.chat.model.Users;
import com.example.talentogram.helper.JsonParserUniversal;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.model.LoginModel;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.example.talentogram.retrofit.ApiClient;
import com.example.talentogram.retrofit.ApiInterface;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btnRegister;
    private Button btnLogin;
    private EditText edtUserName;
    private EditText edtPassword;
    private TextView tvForgotpwd;
    private ImageView ivFacebook;
    private ImageView ivGoogle;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 1;
    private String TAG = "LOGIN";

    public JsonParserUniversal jParser;
    public MySharedPref mShared;
    public LoginActivity instance;
    public GoogleSignInOptions gso;
    public SignInButton googleSignInbutton;
    public CallbackManager callbackManager;
    boolean isShowText = true;

    private GoogleSignInClient mGoogleSignInClient;
    FirebaseAuth mAuth;
    FirebaseUser firebaseUser;
    private String FCMToken;

    private DatabaseReference mDatabase;

    final String password = "Talento_2020";
    final String mobile_Email = "@gmail.com";

    LoginModel.UserData loginModel;

    @Override
    public void onStart() {
        super.onStart();
        signOut();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        instance = this;
        jParser = new JsonParserUniversal();
        mShared = new MySharedPref(LoginActivity.this);
        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();

        if (new MySharedPref(LoginActivity.this).isLogin()) {
            Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            //To do//
                            return;
                        }
                        FCMToken = task.getResult().getToken();
                        Prefs.putString(MySharedPref.FCM_TOKEN, FCMToken);
                        String msg = getString(R.string.fcm_token, FCMToken);
                        Log.d(TAG, msg);

                    }
                });
        bindReference();
        bindClick();

    }

    private Context getActivity() {
        return this;
    }

    private void bindReference() {
        setUpGoogle();
        btnRegister = findViewById(R.id.btnnewaccount);
        btnLogin = findViewById(R.id.btnlogin);
        edtUserName = findViewById(R.id.edtusername);
        edtPassword = findViewById(R.id.edtpwd);
        tvForgotpwd = findViewById(R.id.txtforgotpwd);
        ivFacebook = findViewById(R.id.ivFacebook);
        ivGoogle = findViewById(R.id.ivGoogle);

    }

    private void bindClick() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLoginCall();
            }
        });

        tvForgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgetPassActivity.class));
            }
        });
        ivGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInGoogle();
            }
        });

    }

    private void setUpGoogle() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        googleSignInbutton = findViewById(R.id.btnGoogle_loginOriginal);
    }

    public void onClickPasswordShowHide(View view) {
        if (isShowText) {
            isShowText = false;
            edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            isShowText = true;
            edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    private void doLoginCall() {
        final String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String email = edtUserName.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            edtUserName.requestFocus();
            edtUserName.setError(getString(R.string.blank_validation_email));
        } else if (TextUtils.isEmpty(password)) {
            edtPassword.requestFocus();
            edtPassword.setError(getString(R.string.blank_validation_password));

        } else {
            Utils.showProgressDialog(LoginActivity.this);
            HashMap<String, Object> map = new HashMap<>();
            map.put("username", email);
            map.put("password", password);
            map.put("device_token", FCMToken);
            map.put("imei", "9632587410");
            map.put("device_name", "Android");
            map.put("os_version", String.valueOf(android.os.Build.VERSION.SDK_INT));

            ApiInterface apiInterface = ApiClient.getClient(LoginActivity.this).create(ApiInterface.class);
            Call<LoginModel> call = apiInterface.doLogin(map);

            call.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    Utils.hideProgressDialog();
                    if (response.code() == 401 || response.code() == 400) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            String userMessage = jsonObject.getString("message");
                            new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setContentText(userMessage)
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();

                                        }
                                    }).show();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (response.body() != null && response.body().getMessage().equals("UnAuthenticate.")) {
                            finish();
                        } else {
                            if (response.body() != null && response.body().isSuccess()) {
                                loginModel = response.body().getUserData();
                                MySharedPref.setString(LoginActivity.this, "Update", "Yes");

                                MySharedPref.setString(LoginActivity.this, MySharedPref.USER_ID, String.valueOf(loginModel.getId()));
                                MySharedPref.setString(LoginActivity.this, MySharedPref.USER_EMAIL, loginModel.getEmail());
                                MySharedPref.setString(LoginActivity.this, MySharedPref.USER_PROFILE_PIC, loginModel.getProfilePic());
                                MySharedPref.setString(LoginActivity.this, MySharedPref.USER_AUTH, response.body().getAccessToken());

                                Prefs.putString(MySharedPref.USER_ID, String.valueOf(loginModel.getId()));
                                Prefs.putString(MySharedPref.USER_FIRSTNAME, loginModel.getFirstName());
                                Prefs.putString(MySharedPref.USER_LASTNAME, loginModel.getLastName());
                                Prefs.putString(MySharedPref.USER_PROFILE_PIC, loginModel.getProfilePic());
                                Prefs.putString(MySharedPref.USER_GENDER, loginModel.getGender());
                                Prefs.putString(MySharedPref.USER_ACCESS_TOKEN, response.body().getAccessToken());

                                Prefs.putString(MySharedPref.BANK_ACCOUNT_NAME, loginModel.getBankAccount().getBank_holder_name());
                                Prefs.putString(MySharedPref.IFSCCODE, loginModel.getBankAccount().getIfsc_code());
                                Prefs.putString(MySharedPref.ACCOUNT_NUMBER, loginModel.getBankAccount().getBank_account());
                                Prefs.putString(MySharedPref.BRANCH_NAME, loginModel.getBankAccount().getBranch_name());

                                boolean isNumber = Utils.isNumeric(edtUserName.getText().toString().trim());
                                if (isNumber) {
                                    doLogin(edtUserName.getText().toString().trim(), "login");
                                } else {
                                    doLogin(edtUserName.getText().toString().trim(), "gmail");
                                }
                            } else {
                                Utils.showWarn(LoginActivity.this, "Unauthorized", 3);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    Utils.showWarn(LoginActivity.this, "Error --- >");
                    Utils.hideProgressDialog();
                }
            });
            /*HashMap<String, String> userdata = new HashMap<>();
            userdata.put("username", email);
            userdata.put("password", password);
            userdata.put("device_token", FCMToken);
            userdata.put("imei",Utils.getDeviceId(LoginActivity.this));
            userdata.put("device_name", Build.MODEL);
            userdata.put("os_version", String.valueOf(android.os.Build.VERSION.SDK_INT));
            new NetworkCall(LoginActivity.this, Services.LOGIN,
                    userdata, true, NetworkCall.REQ_POST,
                    false, multiPartRequest);*/
        }
    }

    private void login() {

    }

    NetworkCall.MultiPartRequest multiPartRequest = new NetworkCall.MultiPartRequest() {
        @Override
        public void myResponseMethod(String response) {
            try {
                if (!response.isEmpty()) {
                    JSONObject obj = new JSONObject(response);
                    boolean Success = obj.getBoolean("Success");
                    String message = obj.getString("message");
                    if (Success) {
                        String access_token = obj.getString("access_token");
                        JSONObject user_data = obj.getJSONObject("user_data");
                        String user_id = user_data.getString("id");
                        String user_email = user_data.getString("email");
                        String user_Fname = user_data.getString("FirstName");
                        String user_Lname = user_data.getString("LastName");
                        JSONObject bank_account = user_data.getJSONObject("bank_account");


                        MySharedPref.setString(LoginActivity.this, "Update", "Yes");

                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_ID, user_id);
                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_EMAIL, user_email);
                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_PROFILE_PIC, user_data.getString("ProfilePic"));
                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_AUTH, obj.getString("access_token"));

                        Prefs.putString(MySharedPref.USER_ID, user_id);
                        Prefs.putString(MySharedPref.USER_FIRSTNAME, user_Fname);
                        Prefs.putString(MySharedPref.USER_LASTNAME, user_Lname);
                        Prefs.putString(MySharedPref.USER_PROFILE_PIC, user_data.optString("ProfilePic"));
                        Prefs.putString(MySharedPref.USER_GENDER, user_data.optString("Gender"));
                        Prefs.putString(MySharedPref.USER_ACCESS_TOKEN, access_token);

                        Prefs.putString(MySharedPref.BANK_ACCOUNT_NAME, bank_account.optString("bank_holder_name"));
                        Prefs.putString(MySharedPref.IFSCCODE, bank_account.optString("ifsc_code"));
                        Prefs.putString(MySharedPref.ACCOUNT_NUMBER, bank_account.optString("bank_account"));
                        Prefs.putString(MySharedPref.BRANCH_NAME, bank_account.optString("branch_name"));

                        boolean isNumber = Utils.isNumeric(edtUserName.getText().toString().trim());
                        if (isNumber) {
                            doLogin(edtUserName.getText().toString().trim(), "login");
                        } else {
                            doLogin(edtUserName.getText().toString().trim(), "gmail");
                        }


                    } else {
                        Utils.showWarn(LoginActivity.this, message, 1);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void signInGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        handleGoogleSignInResult(null);
                    }
                });
    }

    public void handleGoogleSignInResult(@Nullable GoogleSignInAccount account) {

        if (account != null) {
            String email = account.getEmail();
            String name = account.getDisplayName();
            String socialId = account.getId();
            String profilePictureUrl = account.getPhotoUrl() == null ? "" : account.getPhotoUrl().toString();
            String[] parts = name.split("\\s+");

            if (parts.length == 2) {
                String firstname = parts[0];
                String lastname = parts[1];

                loginSocialCall(email, socialId, firstname, lastname, profilePictureUrl, false);
            } else if (parts.length == 3) {
                String firstname = parts[0];
                String middlename = parts[1];
                String lastname = parts[2];
                loginSocialCall(email, socialId, firstname, lastname, profilePictureUrl, false);

            }

            System.out.println("Photo URL  --> " + profilePictureUrl);

        }

    }

    public void loginSocialCall(String email, String socialID, String firstname, String lastname,
                                String profilePictureURL, boolean isFacebook) {
        HashMap<String, String> postData = new HashMap<>();
        postData.put("email", email);
        postData.put("device_token", FCMToken);
        postData.put("imei", Utils.getDeviceId(LoginActivity.this));
        postData.put("device_name", "android");
        postData.put("os_version", Utils.getOSVersion());
        postData.put("social_id", socialID);
        postData.put("social_flag", String.valueOf(1));
        postData.put("first_name", firstname);
        postData.put("last_name", lastname);
        postData.put("profile_url", profilePictureURL);
        System.out.println(postData + "UserGmailLoginData");

        new NetworkCall(LoginActivity.this, Services.SOCIAL_LOGIN,
                postData, true, NetworkCall.REQ_POST,
                false, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    Log.i("Response From Server :", response);
                    JSONObject jsonObject = new JSONObject(response);
                    Log.i("Response From Server :", jsonObject.toString());
                    boolean status = jsonObject.getBoolean("Success");
                    String access_token = jsonObject.getString("access_token");

                    if (status) {
                        JSONObject user_data = jsonObject.getJSONObject("user_data");
                        String user_id = user_data.getString("id");
                        String user_Fname = user_data.getString("FirstName");
                        String user_Lname = user_data.getString("LastName");
                        String user_email = user_data.getString("email");

                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_ID, user_id);
                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_EMAIL, user_email);
                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_FIRSTNAME, user_Fname);
                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_LASTNAME, user_Lname);
                        MySharedPref.setString(LoginActivity.this, MySharedPref.DEVICE_TOKEN, FCMToken);
                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_PROFILE_PIC, user_data.getString("ProfilePic"));
                        MySharedPref.setString(LoginActivity.this, MySharedPref.USER_AUTH, jsonObject.getString("access_token"));
                        Utils.hideSoftKeyboard(LoginActivity.this);

                        Prefs.putString(MySharedPref.USER_ID, user_id);
                        Prefs.putString(MySharedPref.USER_FIRSTNAME, user_Fname);
                        Prefs.putString(MySharedPref.USER_LASTNAME, user_Lname);
                        Prefs.putString(MySharedPref.USER_PROFILE_PIC, user_data.optString("ProfilePic"));
                        Prefs.putString(MySharedPref.USER_GENDER, user_data.optString("Gender"));
                        Prefs.putString(MySharedPref.USER_ACCESS_TOKEN, access_token);

                        doLogin(user_email, "gmail");

                    } else {

                        Utils.showWarn(LoginActivity.this, getString(R.string.blank_something_wrong));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void parseGoogleButton() {
        googleSignInbutton.callOnClick();
        googleSignInbutton.setPressed(true);
        googleSignInbutton.invalidate();

        googleSignInbutton.setPressed(false);
        googleSignInbutton.invalidate();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            handleGoogleSignInResult(account);
        } catch (ApiException e) {
            handleGoogleSignInResult(null);
        }
    }
    /*Firebase login using email and password*/

    private void doLogin(String emailOrPhone, String singnup_type) {

        boolean isNumber = Utils.isNumeric(emailOrPhone);
        if (singnup_type.equals("gmail") || !isNumber) {
            mAuth.signInWithEmailAndPassword(emailOrPhone, password)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                Log.v("FireBase Login Sucess", ": Success");
                                updateUserData("", emailOrPhone);

                            } else {
                                Log.v("FirebaseAuth", "FirebaseAuth: " + task.getException());
                                doSignUp(emailOrPhone);

                            }
                        }
                    });
        } else {

            String first5Digit = emailOrPhone.substring(0, 4);
            String last5Digit = emailOrPhone.substring(5, 9);

            String email = "telento" + first5Digit + "gram" + last5Digit + mobile_Email;
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                Log.v("FireBase Login Sucess", ": Success");
                                updateUserData("", email);

                            } else {
                                Log.v("FirebaseAuth", "FirebaseAuth: " + task.getException());
                                doSignUp(email);

                            }
                        }
                    });
        }


    }

    /*Firebase signup using email and password*/

    private void doSignUp(String emailOrMobile) {
        mAuth.createUserWithEmailAndPassword(emailOrMobile, password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    Log.e("onSuccess: Success=", task.getResult().toString());
                    addUserData(emailOrMobile);

                } else {
                    Log.e("onComplete: Failed=", task.getException().getMessage());

                    finish();
                    startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                }

            }
        });
    }


    public void updateUserData(String picturePath, String email) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(MyConstants.TBL_USER).child(Prefs.getString(MySharedPref.USER_ID, ""))
                .child("name").setValue(
                Prefs.getString(MySharedPref.USER_FIRSTNAME, "") + " " + Prefs.getString(MySharedPref.USER_LASTNAME, ""));
        if (!Prefs.getString(MyConstants.USER_NAME, "")
                .equals(Prefs.getString(MySharedPref.USER_FIRSTNAME, "") + " " + Prefs.getString(MySharedPref.USER_LASTNAME, ""))) {
            Prefs.putString(MyConstants.USER_NAME,
                    Prefs.getString(MySharedPref.USER_FIRSTNAME, "") + " " + Prefs.getString(MySharedPref.USER_LASTNAME, ""));
        }
        if (!picturePath.equals("")) {
            Prefs.putString(MyConstants.USER_PROFILEIMAGE, picturePath);
            mDatabase.child(MyConstants.TBL_USER).child(Prefs.getString(MySharedPref.USER_ID, "")).child("picture").setValue(picturePath);
        }
        updateDataRecentTable(email);
    }

    /*Update user's profile data in firebase's recent table*/

    public void updateDataRecentTable(String email) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            final String strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            Query query = mDatabase.child(MyConstants.TBL_RECENT).orderByChild("member2").equalTo(Prefs.getString(MySharedPref.USER_ID, ""));
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            Recent recent = singleSnapshot.getValue(Recent.class);
                            mDatabase.child(MyConstants.TBL_RECENT).child(recent.getObjectId()).child("description").setValue(Prefs.getString(MySharedPref.USER_FIRSTNAME, "") + " " + Prefs.getString(MySharedPref.USER_LASTNAME, ""));
                            mDatabase.child(MyConstants.TBL_RECENT).child(recent.getObjectId()).child("memberImage").setValue(Prefs.getString(MySharedPref.USER_PROFILE_PIC, ""));
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            getUserDetail(email);
        }
    }

    /*get user's details from firebase account*/

    public void getUserDetail(String email) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            final String strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            final DatabaseReference database = FirebaseDatabase.getInstance().getReference();
            Query query = database.child(MyConstants.TBL_USER).orderByChild("userId").equalTo(Prefs.getString(MySharedPref.USER_ID, ""));
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    if (dataSnapshot.exists()) {
                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            Users user = singleSnapshot.getValue(Users.class);

                            Prefs.putString(MyConstants.USERID, user.getUserId());
                            Prefs.putString(MyConstants.USER_NAME, user.getName());
                            Prefs.putString(MyConstants.USER_EMAIL, user.getEmail());


                            finish();
                            startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);


                            break;
                        }
                    } else {
                        addUserData(email);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {


                }
            });
        }
    }

    /*add user's profile data in to firebase account*/

    public void addUserData(String email) {
        FirebaseUser user = mAuth.getCurrentUser();
        String userId = user.getUid();

        Prefs.putString(MyConstants.USERID, userId);
        Prefs.putString(MyConstants.USER_EMAIL, email);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        String user_id = Prefs.getString(MySharedPref.USER_ID, "");

        mDatabase.child(MyConstants.TBL_USER).child(user_id).child("userId").setValue(user_id);
        mDatabase.child(MyConstants.TBL_USER).child(user_id).child("name").setValue(Prefs.getString(MySharedPref.USER_FIRSTNAME, "") + " " + Prefs.getString(MySharedPref.USER_LASTNAME, ""));
        mDatabase.child(MyConstants.TBL_USER).child(user_id).child("email").setValue(email);

        mDatabase.child(MyConstants.TBL_USER).child(user_id).child("online").setValue(false);

        mDatabase.child(MyConstants.TBL_USER).child(user_id).child("fcmtoken").setValue(Prefs.getString(MySharedPref.DEVICE_TOKEN, ""));
        mDatabase.child(MyConstants.TBL_USER).child(user_id).child("badge").setValue(0);
        mDatabase.child(MyConstants.TBL_USER).child(user_id).child("picture").setValue(Prefs.getString(MySharedPref.USER_PROFILE_PIC, ""));
        mDatabase.child(MyConstants.TBL_USER).child(user_id).child("currentChatWith").setValue("");

        Prefs.putString(MyConstants.USERID, userId);
        Prefs.putString(MyConstants.USER_NAME, Prefs.getString(MySharedPref.USER_FIRSTNAME, "") + "" + Prefs.getString(MySharedPref.USER_LASTNAME, ""));
        Prefs.putString(MyConstants.USER_EMAIL, user.getEmail());


        finish();
        startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

}
