package com.example.talentogram.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.talentogram.R;
import com.github.glomadrian.grav.GravView;

public class SplashActivity extends AppCompatActivity {
    private GravView grav;
    private ImageView iv;

    private static int SPLASH_TIME_OUT = 2000;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        iv = findViewById(R.id.imageView);

        if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CAMERA)
                + ContextCompat.checkSelfPermission(
                SplashActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(
                SplashActivity.this, Manifest.permission.RECORD_AUDIO)
                + ContextCompat.checkSelfPermission(
                SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            checkPermission();

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }

    }


    protected void checkPermission() {

        // Do something, when permissions not granted
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                SplashActivity.this, Manifest.permission.CAMERA)
                || ActivityCompat.shouldShowRequestPermissionRationale(
                SplashActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(
                SplashActivity.this, Manifest.permission.RECORD_AUDIO)
                || ActivityCompat.shouldShowRequestPermissionRationale(
                SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(
                SplashActivity.this, Manifest.permission.READ_PHONE_STATE)
        ) {
            // If we should give explanation of requested permissions
            ActivityCompat.requestPermissions(
                    SplashActivity.this,
                    new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_PHONE_STATE
                    },
                    MY_PERMISSIONS_REQUEST_CODE
            );

        } else {
            // Directly request for required permissions, without explanation
            ActivityCompat.requestPermissions(
                    SplashActivity.this,
                    new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_PHONE_STATE
                    },
                    MY_PERMISSIONS_REQUEST_CODE
            );
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CODE: {
                // When request is cancelled, the results array are empty
                if (
                        (grantResults.length > 0) &&
                                (grantResults[0]
                                        + grantResults[1]
                                        + grantResults[2]
                                        == PackageManager.PERMISSION_GRANTED
                                )
                ) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }, SPLASH_TIME_OUT);
                } else {
                    // Permissions are denied
                    Toast.makeText(SplashActivity.this, "Permissions denied.", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }, SPLASH_TIME_OUT);
                }
                return;
            }
        }
    }


}


