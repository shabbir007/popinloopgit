package com.example.talentogram.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.Video_recording.activity.PostVideoActivity;

public class WhoViewActivity extends AppCompatActivity {

    private String data,description;
    private ImageView ivBack, ivPublic, ivPrivate, ivFollowers;
    private LinearLayout llPublic, llFollowers, llPrivate;
    private TextView tvTitle;

    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_who_view);
        initView();

        description = getIntent().getStringExtra("description");
        data = getIntent().getStringExtra("filename");
        tvTitle.setText(getResources().getString(R.string.who_can_view_this_video));


        if (MyConstants.public_post) {
            ivPublic.setVisibility(View.VISIBLE);
            ivFollowers.setVisibility(View.GONE);
            ivPrivate.setVisibility(View.GONE);
        } else if (MyConstants.followers_post) {
            ivPublic.setVisibility(View.GONE);
            ivFollowers.setVisibility(View.VISIBLE);
            ivPrivate.setVisibility(View.GONE);
        } else {
            ivPublic.setVisibility(View.GONE);
            ivFollowers.setVisibility(View.GONE);
            ivPrivate.setVisibility(View.VISIBLE);
        }
        bindClick();

        i = (new Intent(WhoViewActivity.this, PostVideoActivity.class)
                .putExtra("filename", data).putExtra("description", description));
    }

    @Override
    public void onBackPressed() {
        startActivity(i);
        finish();

    }

    private void bindClick() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(i);
                finish();
            }
        });


        llPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConstants.public_post = true;
                MyConstants.followers_post = false;
                MyConstants.private_post = false;
                ivPublic.setVisibility(View.VISIBLE);
                ivFollowers.setVisibility(View.GONE);
                ivPrivate.setVisibility(View.GONE);
               startActivity(i);
               finish();


            }
        });

        llFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConstants.public_post = false;
                MyConstants.followers_post = true;
                MyConstants.private_post = false;
                ivPublic.setVisibility(View.GONE);
                ivFollowers.setVisibility(View.VISIBLE);
                ivPrivate.setVisibility(View.GONE);
                startActivity(i);
                finish();

            }
        });

        llPrivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConstants.public_post = false;
                MyConstants.followers_post = false;
                MyConstants.private_post = true;
                ivPublic.setVisibility(View.GONE);
                ivFollowers.setVisibility(View.GONE);
                ivPrivate.setVisibility(View.VISIBLE);
                startActivity(i);
                finish();


            }
        });

    }

    private void initView() {
        ivBack = (ImageView) findViewById(R.id.ivBack);
        llPublic = (LinearLayout) findViewById(R.id.llPublic);
        ivPublic = (ImageView) findViewById(R.id.ivPublic);
        llFollowers = (LinearLayout) findViewById(R.id.llFollowers);
        ivFollowers = (ImageView) findViewById(R.id.ivFollowers);
        llPrivate = (LinearLayout) findViewById(R.id.llPrivate);
        ivPrivate = (ImageView) findViewById(R.id.ivPrivate);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

    }
}
