package com.example.talentogram.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talentogram.R;
import com.example.talentogram.contest.activity.ApplyForFreeWritingContestActivity;
import com.example.talentogram.contest.activity.ApplyFreeContestActivity;
import com.example.talentogram.contest.model.ContestPaidModel;
import com.example.talentogram.model.SuccessModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.retrofit.ApiClient;
import com.example.talentogram.retrofit.ApiInterface;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;


import org.json.JSONObject;
import org.w3c.dom.Text;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RazorPayActivity extends Activity implements PaymentResultListener {

    private static final String TAG = RazorPayActivity.class.getSimpleName();
    public String amount,razorpayPaymentId ;
    ContestPaidModel item ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_razor_pay);

        Checkout.preload(getApplicationContext());
        // checkout.setKeyID("<YOUR_KEY_ID>")
        startPayment();
        amount = getIntent().getExtras().getString("amt", "");

        item = (ContestPaidModel) getIntent().getSerializableExtra("item");

    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "RazorPay Crop");
            options.put("description", "Demoing Charges");
            options.put("send_sms_hash",true);
            options.put("allow_rotation", true);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", "100");


            JSONObject preFill = new JSONObject();
            preFill.put("email", "test@razorpay.com");
            preFill.put("contact", "9876543210");

            options.put("preFill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            System.out.println(razorpayPaymentID+"564566666666666664");
            razorpayPaymentId = razorpayPaymentID ;
            creditWallet(razorpayPaymentID);
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    private void creditWallet(String razorpayPaymentID) {
        Utils.showProgressDialog(RazorPayActivity.this);
        ApiInterface apiInterface = ApiClient.getClient(RazorPayActivity.this).create(ApiInterface.class);

        Call<SuccessModel> call = apiInterface.creditWallet(amount,razorpayPaymentID);
        call.enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                Utils.hideProgressDialog();
                if (response.body().isSuccess()) {
                    new SweetAlertDialog(RazorPayActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText(response.body().getMessage())
                            .setConfirmText("Ok")
                            .showCancelButton(true)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    // 0-audio,1-video,2-image,3-writing
                                    //for Free contest isFree = 0 AND For Paid IsFree = 1
                                    if (item.getContestType() == 0 || item.getContestType() == 1 || item.getContestType() == 2) {
                                        Intent i = new Intent(RazorPayActivity.this, ApplyFreeContestActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("isFree", 1);
                                        bundle.putInt("ContestId", item.getId());
                                        bundle.putInt("ContestType", item.getContestType());
                                        bundle.putInt("TimeSlotId", item.getTimeSlotId());
                                        bundle.putString("amt", String.valueOf(amount));
                                        bundle.putString("razorpayPaymentId", razorpayPaymentId);
                                        i.putExtras(bundle);
                                        startActivity(i);
                                    } else if (item.getContestType() == 3) {
                                        Intent i = new Intent(RazorPayActivity.this, ApplyForFreeWritingContestActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("isFree", 1);
                                        bundle.putInt("ContestId", item.getId());
                                        bundle.putInt("ContestType", item.getContestType());
                                        bundle.putInt("TimeSlotId", item.getTimeSlotId());
                                        bundle.putString("amt", String.valueOf(amount));
                                        bundle.putString("razorpayPaymentId", razorpayPaymentId);
                                        i.putExtras(bundle);
                                        startActivity(i);
                                    }
                                }
                            }).show();

                } else {
                    new SweetAlertDialog(RazorPayActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setContentText(response.body().getMessage())
                            .setConfirmText("Ok")
                            .showCancelButton(true)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            }).show();
                }
            }

            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {
                Utils.hideProgressDialog();
                Utils.showWarn(RazorPayActivity.this, "No internet Conection");
            }
        });   }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }
}
