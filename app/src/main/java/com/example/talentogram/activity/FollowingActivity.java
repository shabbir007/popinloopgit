package com.example.talentogram.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.adapter.FollowingAdapter;
import com.example.talentogram.model.FollowingModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FollowingActivity extends AppCompatActivity {
    private RecyclerView rvFollowing;
    private ArrayList<FollowingModel> followingList = new ArrayList<>();
    private ImageView ivBack;
    private FollowingAdapter followingAdapter;
    private LinearLayout llNoFollowers;
    private LinearLayout llback;
    private TextView tvTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);

        bindWidgetReference();
        followingListView();
    }


    private void bindWidgetReference() {
        rvFollowing = findViewById(R.id.rvFollowing);
        llNoFollowers = findViewById(R.id.llNoFollowers);
        ivBack = findViewById(R.id.ivBack);
        llback = findViewById(R.id.llBack);
        tvTitle = findViewById(R.id.tvTitle);

        tvTitle.setText(getResources().getString(R.string.following));
        llback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void followingListView() {

        new NetworkCall(this, Services.FOLLOWING_LIST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");
                    if(Success) {
                        JSONArray jContestList = jsonObject.getJSONArray("data");
                        followingList.clear();
                        followingList = (ArrayList<FollowingModel>)
                                new Gson().fromJson(jsonObject.get("data").toString(),
                                        new TypeToken<ArrayList<FollowingModel>>() {
                                        }.getType());

                        followingAdapter = new FollowingAdapter(FollowingActivity.this, followingList);
                        rvFollowing.setAdapter(followingAdapter);
                        rvFollowing.setLayoutManager(new LinearLayoutManager(FollowingActivity.this,
                                LinearLayoutManager.VERTICAL, false));

                        if (followingList.size() == 0) {
                            llNoFollowers.setVisibility(View.VISIBLE);
                            rvFollowing.setVisibility(View.GONE);

                        }
                    }
                    else
                    {
                        llNoFollowers.setVisibility(View.VISIBLE);
                        rvFollowing.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
