package com.example.talentogram.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;

public class TermsOfUseActivity extends AppCompatActivity {

    private ImageView ivBack;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_use);
        initView();
        tvTitle.setText(getResources().getString(R.string.txt_terms_of_use));
        bindClick();
    }

    private void bindClick() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initView() {
        ivBack = (ImageView) findViewById(R.id.ivBack);
        tvTitle =findViewById(R.id.tvTitle);
    }
}
