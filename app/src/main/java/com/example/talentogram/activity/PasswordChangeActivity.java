package com.example.talentogram.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talentogram.R;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PasswordChangeActivity extends AppCompatActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.edtOldpwd)
    EditText edtOldpwd;

    @BindView(R.id.edtNewpwd)
    EditText edtNewpwd;

    @BindView(R.id.edtReEnterPwd)
    EditText edtReEnterPwd;

    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private String oldPass = "";
    private String newPass = "";
    private String reEnterPass = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        ButterKnife.bind(this);
        bindWidget();
        tvTitle.setText(getResources().getString(R.string.password_change));
    }


    private void bindWidget() {
        ivBack.setOnClickListener(v -> onBackPressed());

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    changePassword();
                }
            }
        });

    }

    private void changePassword() {
        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("old_password", oldPass);
        userdata.put("new_password", newPass);
        userdata.put("password_confirmation", reEnterPass);

        new NetworkCall(PasswordChangeActivity.this, Services.CHANGE_PASSWORD,
                userdata, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {

                            new AlertDialog.Builder(PasswordChangeActivity.this)
                                    .setMessage(message+"\n"+"Do Login Again.")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            logoutUser();
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(PasswordChangeActivity.this, "Error->" + message, Toast.LENGTH_SHORT).show();

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
    private void logoutUser() {

        new NetworkCall(PasswordChangeActivity.this, Services.LOGOUT,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            new MySharedPref(PasswordChangeActivity.this).sharedPrefClear(PasswordChangeActivity.this);
                            Intent intent = new Intent(PasswordChangeActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            ActivityCompat.finishAffinity((Activity) PasswordChangeActivity.this);
                            ((Activity) PasswordChangeActivity.this).finish();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private boolean checkValidation() {
        boolean isError = false;
        oldPass = edtOldpwd.getText().toString().trim();
        newPass = edtNewpwd.getText().toString().trim();
        reEnterPass = edtReEnterPwd.getText().toString().trim();

        if (TextUtils.isEmpty(oldPass)) {
            isError = true;
            edtOldpwd.requestFocus();
            edtOldpwd.setError(getString(R.string.blank_empty_oldpass));
        } else if (TextUtils.isEmpty(newPass)) {
            isError = true;
            edtNewpwd.requestFocus();
            edtNewpwd.setError(getString(R.string.blank_empty_newpass));
        } else if (TextUtils.isEmpty(reEnterPass)) {
            isError = true;
            edtReEnterPwd.requestFocus();
            edtReEnterPwd.setError(getString(R.string.blank_empty_reenter_pass));
        } else if (!newPass.equals(reEnterPass)) {
            isError = true;
            edtReEnterPwd.requestFocus();
            Utils.showWarn(PasswordChangeActivity.this, getString(R.string.blank_pass_donomatch));
        }
        return !isError;
    }
}