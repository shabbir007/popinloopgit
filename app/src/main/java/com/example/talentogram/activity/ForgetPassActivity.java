package com.example.talentogram.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;
import com.example.talentogram.custom_views.CustomBoldTextView;
import com.example.talentogram.custom_views.CustomButton;
import com.example.talentogram.custom_views.CustomEditText;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import org.json.JSONObject;

import java.util.HashMap;

public class ForgetPassActivity extends AppCompatActivity {

    private ImageView poopinloop;
    private CustomEditText etEmail;
    private CustomButton btnSend;
    private CustomBoldTextView tvLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
        bindReference();
        bindClick();
    }

    private void bindReference() {
        poopinloop = (ImageView) findViewById(R.id.poopinloop);
        etEmail = (CustomEditText) findViewById(R.id.etEmail);
        btnSend = (CustomButton) findViewById(R.id.btnSend);
        tvLogin = (CustomBoldTextView) findViewById(R.id.tvLogin);
    }

    private void bindClick() {
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ForgetPassActivity.this,LoginActivity.class));
                finish();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doForgetPassCall();
            }
        });
    }

    private void doForgetPassCall() {
        String email = etEmail.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (TextUtils.isEmpty(email) || !Utils.isValidEmail(email) || !email.matches(emailPattern)) {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.blank_email_check));
        }else {

            HashMap<String,String> userdata = new HashMap<>();
            userdata.put("email",email);

            new NetworkCall(ForgetPassActivity.this, Services.FORGET_PASS,
                    userdata,true,NetworkCall.REQ_POST,
                    false, multiPartRequest);

        }
    }

    NetworkCall.MultiPartRequest multiPartRequest = new NetworkCall.MultiPartRequest() {

        @Override
        public void myResponseMethod(String response) {
            try {
                if (!response.isEmpty()) {
                    JSONObject obj = new JSONObject(response);
                    boolean Success = obj.getBoolean("Success");
                    String message = obj.getString("message");
                    if (Success){
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ForgetPassActivity.this);
                        alertDialog.setTitle("Success");
                        alertDialog.setMessage(message);
                        alertDialog.setPositiveButton("ok", (dialog, which) -> {
                            startActivity(new Intent(ForgetPassActivity.this, LoginActivity.class));
                            finish();
                        });
                        alertDialog.show();
                    }else{

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ForgetPassActivity.this);
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage(message);
                        alertDialog.show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
