package com.example.talentogram.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.custom_views.CustomBoldTextView;
import com.example.talentogram.custom_views.CustomEditText;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class AddImagesPostActivity extends AppCompatActivity {
    int IsCheckedForDownload = 1;
    private ImageView ivBack, selectedImage;
    private CircularImageView IvProfile;
    private CustomBoldTextView tvUserName;
    private Button btnPost;
    private CustomEditText etDescription, etHashtag;
    private String ImagePath, hashTag = "", description = "", toggleUpload;
    private ArrayList<String> list;
    private String[] image_list;
    private CheckBox chAllowDownload;
    private TextView tvTitle;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_post);
        init();
        bindWidget();
        tvTitle.setText("Go to gallery");

    }

    private void init() {
        ivBack = findViewById(R.id.ivBack);
        selectedImage = findViewById(R.id.selectedImage);
        IvProfile = findViewById(R.id.IvProfile);
        tvUserName = findViewById(R.id.tvUserName);
        etDescription = findViewById(R.id.etDescription);
        btnPost = findViewById(R.id.btnPost);
        etHashtag = findViewById(R.id.etHashtag);
        chAllowDownload = findViewById(R.id.chAllowDownload);
        tvTitle =findViewById(R.id.tvTitle);
        chAllowDownload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsCheckedForDownload = 1;
                } else {
                    IsCheckedForDownload = 0;
                }
            }
        });

        tvUserName.setText(MySharedPref.getString(AddImagesPostActivity.this, MySharedPref.USER_FIRSTNAME + " " + MySharedPref.USER_LASTNAME, ""));
        String userProfile = MySharedPref.getString(AddImagesPostActivity.this, MySharedPref.USER_PROFILE_PIC, "");

        if (!TextUtils.isEmpty(userProfile)) {
            Glide.with(AddImagesPostActivity.this).load(userProfile).placeholder(R.drawable.noimage).error(R.drawable.noimage).into(IvProfile);
        } else
            IvProfile.setImageResource(R.drawable.noimage);

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPost();
            }
        });
    }

    private void bindWidget() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        ImagePath = intent.getStringExtra("image");

        Glide.with(AddImagesPostActivity.this)
                .load(ImagePath)
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .into(selectedImage);


    }

    private void addPost() {

        hashTag = etHashtag.getText().toString().trim();
        description = etDescription.getText().toString().trim();

        if (TextUtils.isEmpty(hashTag)) {
            etHashtag.requestFocus();
            etHashtag.setError(getString(R.string.blank_validation_hashtag));
        } else if (TextUtils.isEmpty(description)) {
            etDescription.requestFocus();
            etDescription.setError(getString(R.string.blank_validation_desc));
        } else {

            HashMap<String, String> userdata = new HashMap<>();
            userdata.put("Title", "Galllery_Image");
            userdata.put("PostType", String.valueOf(2));
            userdata.put("PostHashTagIds", "");
            userdata.put("NewHashtags", hashTag);
            userdata.put("PostVisibleType", String.valueOf(0));
            userdata.put("Description", description);
            userdata.put("isDownload", String.valueOf(IsCheckedForDownload));
            userdata.put("AdminSongId", String.valueOf(1));

            HashMap<String, File> userFile = new HashMap<>();
            File file = new File(ImagePath);
            userFile.put("PostUrl[]", file);

            new NetworkCall(AddImagesPostActivity.this, Services.ADDPOST,
                    userdata, userFile, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {

                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            String message = obj.getString("message");
                            if (Success) {
                                new AlertDialog.Builder(AddImagesPostActivity.this)
                                        .setMessage(message)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                startActivity(new Intent(AddImagesPostActivity.this, DashBoardActivity.class));
                                                finish();
                                                dialog.dismiss();
                                            }
                                        })
                                        .show();
                            } else {
                                Utils.showWarn(AddImagesPostActivity.this, message, 1);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        String tvUserFirstName = MySharedPref.getString(AddImagesPostActivity.this, MySharedPref.USER_FIRSTNAME, "");
        String tvUserLastName = MySharedPref.getString(AddImagesPostActivity.this, MySharedPref.USER_LASTNAME, "");

        tvUserName.setText(tvUserFirstName + " " + tvUserLastName);
        Glide.with(AddImagesPostActivity.this)
                .load(MySharedPref.getString(AddImagesPostActivity.this, MySharedPref.USER_PROFILE_PIC, ""))
                .thumbnail(Glide.with(AddImagesPostActivity.this).load(R.drawable.noimage))
                .error(R.drawable.noimage)
                .into(IvProfile);

    }

}
