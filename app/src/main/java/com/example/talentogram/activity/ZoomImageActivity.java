package com.example.talentogram.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.others.TouchImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ZoomImageActivity extends AppCompatActivity {
    @BindView(R.id.ivBtnClose)
    ImageView mIvBtnClose;

    @BindView(R.id.ivPicture)
    TouchImageView mIvPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image);
        ButterKnife.bind(this);


        if (getIntent().hasExtra("imageurl")) {
            String picture = getIntent().getStringExtra("imageurl");
            if (!TextUtils.isEmpty(picture)) {
                Glide.with(ZoomImageActivity.this).load(picture).placeholder(R.drawable.noimage).into(mIvPicture);
            }
        }
        mIvBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}