package com.example.talentogram.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WalletActivity extends AppCompatActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        tvTitle.setText(R.string.title_wallet);

    }

    @OnClick(R.id.llBack)
    void backPress() {
        onBackPressed();
    }

    @OnClick(R.id.llWalletHistory)
    void walletHistoryClick() {
        Intent i = new Intent(WalletActivity.this, WalletHistoryActivity.class);
        startActivity(i);
    }
    @OnClick(R.id.llWithdrawBal)
    void withdrawBalClick() {
        Intent i = new Intent(WalletActivity.this, WithdrawAmtActivity.class);
        startActivity(i);
    }
}
