package com.example.talentogram.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.talentogram.R;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.model.SuccessModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.retrofit.ApiClient;
import com.example.talentogram.retrofit.ApiInterface;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditBankDetailActivity extends AppCompatActivity {
    @BindView(R.id.btnAdd)
    Button btnAdd;

    @BindView(R.id.edtHolderName)
    EditText edtHolderName;

    @BindView(R.id.edtAccountNumber)
    EditText edtAccountNumber;

    @BindView(R.id.edtReAccountNumber)
    EditText edtReAccountNumber;

    @BindView(R.id.edtIfsc)
    EditText edtIfsc;

    @BindView(R.id.edtBranchName)
    EditText edtBranchName;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    String holderName = "", accountNumber = "", reAccountNumber = "", ifscCode = "", branchName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_bank_detail);
        ButterKnife.bind(this);
        tvTitle.setText(R.string.title_update_bank);

        setData();
    }

    private void setData() {
        edtHolderName.setText(Prefs.getString(MySharedPref.BANK_ACCOUNT_NAME, ""));
        edtAccountNumber.setText(Prefs.getString(MySharedPref.ACCOUNT_NUMBER, ""));
        edtIfsc.setText(Prefs.getString(MySharedPref.IFSCCODE, ""));
        edtBranchName.setText(Prefs.getString(MySharedPref.BRANCH_NAME, ""));
    }

    @OnClick(R.id.ivBack)
    void clickBack() {
        finish();
    }

    @OnClick(R.id.btnAdd)
    void clickAdd() {
        if (checkValidation()) {
            updateAccount();
        }
    }

    public boolean checkValidation() {
        boolean isError = false;

        holderName = edtHolderName.getText().toString().trim();
        accountNumber = edtAccountNumber.getText().toString().trim();
        reAccountNumber = edtReAccountNumber.getText().toString().trim();
        ifscCode = edtIfsc.getText().toString().trim();
        branchName = edtBranchName.getText().toString().trim();

        if (TextUtils.isEmpty(holderName)) {
            isError = true;
            edtHolderName.requestFocus();
            edtHolderName.setError(getString(R.string.alert_holder_name));
        } else if (TextUtils.isEmpty(accountNumber)) {
            isError = true;
            edtAccountNumber.requestFocus();
            edtAccountNumber.setError(getString(R.string.alert_account_number));
        } else if (TextUtils.isEmpty(reAccountNumber)) {
            isError = true;
            edtReAccountNumber.requestFocus();
            edtReAccountNumber.setError(getString(R.string.alert_reAc_number));
        } else if (!accountNumber.matches(reAccountNumber)) {
            isError = true;
            edtReAccountNumber.requestFocus();
            edtReAccountNumber.setError(getString(R.string.alert_re_account_number));
        } else if (TextUtils.isEmpty(ifscCode)) {
            isError = true;
            edtIfsc.requestFocus();
            edtIfsc.setError(getString(R.string.alert_ifsc));
        } else if (TextUtils.isEmpty(branchName)) {
            isError = true;
            edtBranchName.requestFocus();
            edtBranchName.setError(getString(R.string.alert_branch));
        }
        return !isError;
    }

    private void updateAccount() {
        Utils.showProgressDialog(EditBankDetailActivity.this);
        HashMap<String, String> map = new HashMap<>();
        map.put("ifsc_code", ifscCode);
        map.put("bank_account", accountNumber);
        map.put("bank_holder_name", holderName);
        map.put("branch_name", branchName);

        ApiInterface apiInterface = ApiClient.getClient(EditBankDetailActivity.this).create(ApiInterface.class);

        Call<SuccessModel> call = apiInterface.addBankAccount(map);
        call.enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                Utils.hideProgressDialog();
                if (response.body().isSuccess()) {
                    new SweetAlertDialog(EditBankDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText(response.body().getMessage())
                            .setConfirmText("Ok")
                            .showCancelButton(true)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    Prefs.putString(MySharedPref.BANK_ACCOUNT_NAME, holderName);
                                    Prefs.putString(MySharedPref.IFSCCODE, ifscCode);
                                    Prefs.putString(MySharedPref.ACCOUNT_NUMBER, accountNumber);
                                    Prefs.putString(MySharedPref.BRANCH_NAME, branchName);
                                }
                            }).show();
                } else {
                    new SweetAlertDialog(EditBankDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setContentText(response.body().getMessage())
                            .setConfirmText("Ok")
                            .showCancelButton(true)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                }
                            }).show();
                }
            }

            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {
                Utils.hideProgressDialog();
                Utils.showWarn(EditBankDetailActivity.this, "No internet Conection");
            }
        });
    }
}
