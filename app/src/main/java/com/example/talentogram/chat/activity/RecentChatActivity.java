package com.example.talentogram.chat.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.SegmentProgress.Fragment_Callback;
import com.example.talentogram.chat.adapter.RecentChatListAdapter;
import com.example.talentogram.chat.helper.OnRecyelerItemClick;
import com.example.talentogram.chat.model.Recent;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RecentChatActivity extends AppCompatActivity implements OnRecyelerItemClick {

    @SuppressLint("StaticFieldLeak")
    public static RecentChatActivity instance = null;

    public RecentChatActivity() {
        instance = this;
    }

    public static synchronized RecentChatActivity getInstance() {
        if (instance == null) {
            instance = new RecentChatActivity();
        }
        return instance;
    }

    @BindView(R.id.recylerview)
    public RecyclerView mRvUserList;

    @BindView(R.id.no_data_layout)
    public RelativeLayout mNoDataLayout;

    @BindView(R.id.search_edit)
    public EditText search_edit;

    @BindView(R.id.ivsearch)
    ImageView mIvsearch;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    private Unbinder unbinder;
    private RecentChatListAdapter mAdapter;
    public ArrayList<Recent> arrRecentChat;
    private DatabaseReference mDatabase;

    Context context;
    String user_id;

    Fragment_Callback fragment_callback;
    private CircularImageView ivUserProfile;

    @SuppressLint("ValidFragment")
    public RecentChatActivity(Fragment_Callback fragment_callback) {
        this.fragment_callback = fragment_callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_chat);

        context = RecentChatActivity.this;

        Intent intent = getIntent();
        user_id = intent.getStringExtra("id");
        unbinder = ButterKnife.bind(this);
        ivUserProfile = findViewById(R.id.ivUserImg);
        if (MySharedPref.getString(RecentChatActivity.this, MySharedPref.USER_PROFILE_PIC, "") != null) {
            Glide.with(RecentChatActivity.this)
                    .load(MySharedPref.getString(RecentChatActivity.this, MySharedPref.USER_PROFILE_PIC, ""))
                    .placeholder(R.drawable.user)
                    .error(R.drawable.noimage)
                    .into(ivUserProfile);
        } else {
            ivUserProfile.setImageResource(R.drawable.noimage);
        }

        arrRecentChat = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mAdapter = new RecentChatListAdapter(RecentChatActivity.this, arrRecentChat, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RecentChatActivity.this.getApplicationContext());
        mRvUserList.setLayoutManager(mLayoutManager);
        mRvUserList.setItemAnimator(new DefaultItemAnimator());
        mRvUserList.setAdapter(mAdapter);

        if (arrRecentChat.size() == 0) {
            mNoDataLayout.setVisibility(View.VISIBLE);
            mRvUserList.setVisibility(View.GONE);
        } else {
            mNoDataLayout.setVisibility(View.GONE);
            mRvUserList.setVisibility(View.VISIBLE);
        }

        llBack.setOnClickListener(v -> finish());
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // filter(s.toString());
                if (search_edit.getText().toString().length() > 0) {
                    mIvsearch.setVisibility(View.VISIBLE);
                } else {
                    mIvsearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        getRecentChatList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public void getRecentChatList() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            final String strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();

            Query query = mDatabase.child(MyConstants.TBL_RECENT).
                    orderByChild("userId").equalTo(MySharedPref.getString(RecentChatActivity.this, MySharedPref.USER_ID, ""));
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    arrRecentChat.clear();
                    if (dataSnapshot.exists()) {
                        if (mNoDataLayout == null) {

                        } else {
                            mNoDataLayout.setVisibility(View.GONE);
                        }
                        if (mRvUserList == null) {

                        } else {
                            mRvUserList.setVisibility(View.VISIBLE);
                        }

                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            Recent recent = singleSnapshot.getValue(Recent.class);
                            arrRecentChat.add(recent);
                            Utils.sortRecentChatList(arrRecentChat);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {

        }
    }

    @Override
    public void onItemClick(int pos) {
        int unread_count = MyConstants.COUNT_MESSAGE - arrRecentChat.get(pos).getCounter();
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("userId", arrRecentChat.get(pos).getMember2());
        intent.putExtra("pos", pos + "");
        intent.putExtra("from recent", "from recent");
        intent.putExtra("isRecentChat", "yes");
        intent.putExtra("unread_msg_count", unread_count);
        intent.putExtra("groupId", arrRecentChat.get(pos).getGroupId());
        startActivity(intent);
    }

}
