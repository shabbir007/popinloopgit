package com.example.talentogram.chat.model;

public class Users {
    public String fcmtoken = "";
    public int badge = 0;
    public String email = "";
    public String name = "";
    public String userId = "";
    public String planetId = "";
    public String picture = "";
    public boolean online;
    public String currentChatWith= "";

    public String getFcmtoken() {
        return fcmtoken;
    }

    public int getBadge() {
        return badge;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getUserId() {
        return userId;
    }

    public String getPicture() {
        return picture;
    }

    public boolean isOnline() {
        return online;
    }

    public String getPlanetId() {
        return planetId;
    }

    public String getCurrentChatWith() {
        return currentChatWith;
    }
}
