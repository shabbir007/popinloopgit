package com.example.talentogram.chat.model;

public class Recent {

    public int counter;
    public boolean active;
    public boolean online;
    public long createdAt;
    public String description = "";
    public String groupId = "";
    public String lastMessage = "";
    public long lastMessageDate;
    public String userId = "";
    public String objectId = "";
    public String member1="";
    public String member2="";
    public String memberImage="";
    public String planetId="";
    public String userId_planetId="";

    public int getCounter() {
        return counter;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isOnline() {
        return online;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String getDescription() {
        return description;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public long getLastMessageDate() {
        return lastMessageDate;
    }

    public String getUserId() {
        return userId;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getMember1() {
        return member1;
    }

    public String getMember2() {
        return member2;
    }

    public String getMemberImage() {
        return memberImage;
    }

    public String getPlanetId() {
        return planetId;
    }

    public String getUserId_planetId() {
        return userId_planetId;
    }
}
