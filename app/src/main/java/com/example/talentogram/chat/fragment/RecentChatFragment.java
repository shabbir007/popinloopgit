package com.example.talentogram.chat.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.talentogram.R;
import com.example.talentogram.SegmentProgress.Fragment_Callback;
import com.example.talentogram.chat.activity.ChatActivity;
import com.example.talentogram.chat.adapter.RecentChatListAdapter;
import com.example.talentogram.chat.helper.OnRecyelerItemClick;
import com.example.talentogram.chat.model.Recent;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecentChatFragment extends Fragment implements OnRecyelerItemClick {

    public static RecentChatFragment instance = null;

    public RecentChatFragment() {
        instance = this;
    }

    public static synchronized RecentChatFragment getInstance() {
        if (instance == null) {
            instance = new RecentChatFragment();
        }
        return instance;
    }

    @BindView(R.id.recylerview)
   public RecyclerView mRvUserList;

    @BindView(R.id.no_data_layout)
    public RelativeLayout mNoDataLayout;

    @BindView(R.id.search_edit)
    public EditText search_edit;

    @BindView(R.id.ivsearch)
    ImageView mIvsearch;

    private Unbinder unbinder;
    private RecentChatListAdapter mAdapter;
    public ArrayList<Recent> arrRecentChat;
    private DatabaseReference mDatabase;

    Context context;
    String user_id;

    Fragment_Callback fragment_callback;

    @SuppressLint("ValidFragment")
    public RecentChatFragment(Fragment_Callback fragment_callback) {
        this.fragment_callback = fragment_callback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        context = getContext();
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            user_id = bundle.getString("id");
        }
        unbinder = ButterKnife.bind(this, view);

        arrRecentChat = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mAdapter = new RecentChatListAdapter(getActivity(), arrRecentChat, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRvUserList.setLayoutManager(mLayoutManager);
        mRvUserList.setItemAnimator(new DefaultItemAnimator());
        mRvUserList.setAdapter(mAdapter);

        if (arrRecentChat.size() == 0) {
            mNoDataLayout.setVisibility(View.VISIBLE);
            mRvUserList.setVisibility(View.GONE);
        } else {
            mNoDataLayout.setVisibility(View.GONE);
            mRvUserList.setVisibility(View.VISIBLE);
        }


        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // filter(s.toString());
                if (search_edit.getText().toString().length() > 0) {
                    mIvsearch.setVisibility(View.VISIBLE);
                } else {
                    mIvsearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }


    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (getView() != null && menuVisible) {
            search_edit.getText().clear();
            if (getActivity().getCurrentFocus() != null) {
                getActivity().getCurrentFocus().clearFocus();
            }

        }
    }

    @Override
    public void onDetach() {

        if (fragment_callback != null)
            fragment_callback.Responce(new Bundle());

        super.onDetach();
    }
    @Override
    public void onResume() {

        super.onResume();
        getRecentChatList();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    public void getRecentChatList() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            final String strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();

            Query query = mDatabase.child(MyConstants.TBL_RECENT).
                    orderByChild("userId").equalTo(MySharedPref.getString(getContext(),MySharedPref.USER_ID, ""));
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    arrRecentChat.clear();if (dataSnapshot.exists()) {
                        if (mNoDataLayout == null) {

                        } else {
                            mNoDataLayout.setVisibility(View.GONE);
                        }
                        if (mRvUserList == null) {

                        } else {
                            mRvUserList.setVisibility(View.VISIBLE);
                        }

                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                         Recent recent = singleSnapshot.getValue(Recent.class);
                            arrRecentChat.add(recent);
                            Utils.sortRecentChatList(arrRecentChat);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {

        }
    }

    @Override
    public void onItemClick(int pos) {

        int unread_count = MyConstants.COUNT_MESSAGE - arrRecentChat.get(pos).getCounter();

        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra("userId", arrRecentChat.get(pos).getMember2());
        intent.putExtra("pos", pos + "");
        intent.putExtra("from recent", "from recent");
        intent.putExtra("isRecentChat", "yes");
        intent.putExtra("unread_msg_count", unread_count);
        intent.putExtra("groupId", arrRecentChat.get(pos).getGroupId());
        startActivity(intent);
    }
}
