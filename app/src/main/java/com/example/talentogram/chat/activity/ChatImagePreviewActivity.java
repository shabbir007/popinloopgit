package com.example.talentogram.chat.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.talentogram.R;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChatImagePreviewActivity extends AppCompatActivity implements View.OnClickListener{

    private Unbinder unbinder;

    @BindView(R.id.ivChatPreview)
    ImageView mIvChatPreview;

    @BindView(R.id.tvCancel)
    TextView mTvCancel;

    @BindView(R.id.tvSend)
    TextView mTvSend;

    private Uri fileUri;
    private String picturePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_image_preview);

        unbinder = ButterKnife.bind(this);

        if(getIntent().getBooleanExtra("gallery",false)){
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, MyConstants.REQUEST_PICK_IMAGE);
        }else{
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            captureImage();
        }

        mTvCancel.setOnClickListener(this);
        mTvSend.setOnClickListener(this);
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = Uri.fromFile(Utils.getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, MyConstants.REQUEST_CAMERA);// start the image capture Intent

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Utils.checkValidResult(ChatImagePreviewActivity.this, resultCode)) {
            if (requestCode == MyConstants.REQUEST_CAMERA) {
                if (fileUri != null) {
                    picturePath = new File(fileUri.getPath()).getAbsolutePath();

                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.noimage)
                            .error(R.drawable.noimage);

                    Glide.with(ChatImagePreviewActivity.this).load(new File(picturePath)).apply(options).into(mIvChatPreview);
                }
            } else if (requestCode ==MyConstants. REQUEST_PICK_IMAGE) {
                Uri selectedImage = data.getData();
                String filePath[] = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);// c.getColumnIndex(filePath[0]);
                picturePath = c.getString(columnIndex);

                RequestOptions options = new RequestOptions()
                        .placeholder(R.drawable.noimage)
                        .error(R.drawable.noimage);

                Glide.with(ChatImagePreviewActivity.this).load(new File(picturePath)).apply(options).into(mIvChatPreview);

                c.close();
            }
        }else{
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvCancel:
                finish();
                break;
            case R.id.tvSend:
                if(getIntent().getBooleanExtra("gallery",false)) {
                    Intent intent = new Intent();
                    intent.putExtra("picture", picturePath);
                    setResult(RESULT_OK, intent);
                    finish();
                }else{
                    Intent intent = new Intent();
                    intent.putExtra("picture", picturePath);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
        }
    }
}

