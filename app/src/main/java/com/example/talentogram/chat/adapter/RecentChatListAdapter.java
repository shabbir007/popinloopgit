package com.example.talentogram.chat.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.talentogram.R;
import com.example.talentogram.chat.fragment.RecentChatFragment;
import com.example.talentogram.chat.helper.OnRecyelerItemClick;
import com.example.talentogram.chat.model.Messages;
import com.example.talentogram.chat.model.Recent;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;


import java.util.ArrayList;

public class RecentChatListAdapter extends RecyclerView.Adapter<RecentChatListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Recent> arrRecentChat;
    private OnRecyelerItemClick onRecyelerItemClick;

    private DatabaseReference mDatabase;

    public RecentChatListAdapter(Context mContext, ArrayList<Recent> arrRecentChat, OnRecyelerItemClick onRecyelerItemClick) {
        this.mContext = mContext;
        this.arrRecentChat = arrRecentChat;
        this.onRecyelerItemClick = onRecyelerItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_chatlist, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvName.setText(arrRecentChat.get(position).getDescription());

        mDatabase = FirebaseDatabase.getInstance().getReference();
        holder.ivUserImage.setImageResource(R.drawable.noimage);

        if (!Utils.isStringNull(arrRecentChat.get(position).getMemberImage())) {
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.noimage)
                    .error(R.drawable.noimage);

            Glide.with(mContext)
                    .load(arrRecentChat.get(position).getMemberImage())
                    .apply(options)
                    .into(holder.ivUserImage);

        }
        String createdTime = Utils.getMessageTimeInChat(arrRecentChat.get(position).getCreatedAt());

        holder.tvTime.setText(createdTime.toLowerCase());
        holder.tvUser_last_msg.setText(arrRecentChat.get(position).getLastMessage());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRecyelerItemClick.onItemClick(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrRecentChat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvTime,tvUser_last_msg;

        CircularImageView ivUserImage;

        public ViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvUserName);

            tvTime = (TextView) view.findViewById(R.id.tvTime);

            tvUser_last_msg = (TextView) view.findViewById(R.id.tvUser_last_msg);

            ivUserImage = (CircularImageView) view.findViewById(R.id.ivUserImage);

        }
    }
}
