package com.example.talentogram.chat.model;

public class Messages {

    boolean callAudioDuration = false;
    long audioDuration = 0;

    long createdAt;
    String groupId="";
    String objectId="";
    String senderId="";
    String senderName="";
    String picture="";
    String video="";
    String audio="";
    String videoThumb="";
    String contactName="";
    String contactNumber="";
    String type="";
    String text ="";
    int deletedBy = 0 ;
    double latitude=0.0;
    double longitude=0.0;

    public long getAudioDuration() {
        return audioDuration;
    }

    public void setAudioDuration(long audioDuration) {
        this.audioDuration = audioDuration;
    }

    public boolean isCallAudioDuration() {
        return callAudioDuration;
    }

    public void setCallAudioDuration(boolean callAudioDuration) {
        this.callAudioDuration = callAudioDuration;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getPicture() {
        return picture;
    }

    public String getVideo() {
        return video;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public int getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(int deletedBy) {
        this.deletedBy = deletedBy;
    }
}
