package com.example.talentogram.chat.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.activity.LoginActivity;
import com.example.talentogram.chat.adapter.ChatAdapter;
import com.example.talentogram.chat.helper.DialogAlert;
import com.example.talentogram.chat.helper.OnItemClick;
import com.example.talentogram.chat.model.Messages;
import com.example.talentogram.chat.model.Recent;
import com.example.talentogram.chat.model.Users;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    public static ChatActivity instance = null;

    public ChatActivity() {
        instance = this;
    }

    public static synchronized ChatActivity getInstance() {
        if (instance == null) {
            instance = new ChatActivity();
        }
        return instance;
    }

    @BindView(R.id.tvTitle)
    TextView mTvTitle;

    @BindView(R.id.rvChatList)
    RecyclerView mRvChatList;

    @BindView(R.id.etChat)
    EditText mEtText;

    @BindView(R.id.ivPicture)
    ImageView ivPicture;

    @BindView(R.id.ivCamera)
    ImageView ivCamera;

    @BindView(R.id.ivSend)
    ImageView mIvBtnSend;

    @BindView(R.id.rlProgress)
    RelativeLayout mRlProgress;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.ivUserImg)
    ImageView ivUserImg;

    @BindView(R.id.ivAudio)
    ImageView ivAudio;

    private Dialog dialog;

    private Unbinder unbinder;
    private ChatAdapter mAdapter;
    private ArrayList<Messages> arrMessages;
    private DatabaseReference mDatabase;
    private Users otherUser;
    public int badge = 0;
    String otherUserId = "", GROUP_ID = "";
    private Timer timer = new Timer();
    private final long DELAY = 3000;
    private boolean isActiveUser = false;
    private boolean isOnline = false;
    private boolean isTyping = false;
    private ArrayList<Users> arrUsers = new ArrayList<>();
    private ArrayList<Recent> arrRecentChatBadge = new ArrayList<>();
    private Recent recentChatBadge;

    String audioID;
    Uri MyUri = Uri.EMPTY;
    private String picturePath = "";
    private String pos = "", isRecentChat = "";
    public boolean isFirst = false, mReturningWithResult = false;

    private String foldername = MyConstants.root;
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private String fileName = null;
    private int lastProgress = 0;
    LinearLayoutManager mLayoutManager;
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    public boolean isNotification = false;


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
            case MyConstants.REQUEST_TAKE_AUDIO:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    DialogAlert.show_dialog_dismiss_ok(ChatActivity.this, getResources().getString(R.string.record_permission_message), new OnItemClick() {
                        @Override
                        public void OnItemClickListner(int position) {

                        }
                    });
                }
                break;
            case MyConstants.REQUEST_CAMERA:
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    DialogAlert.show_dialog_dismiss_ok(ChatActivity.this, getResources().getString(R.string.camera_permission_message), new OnItemClick() {
                        @Override
                        public void OnItemClickListner(int position) {

                        }
                    });
                }
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);

        unbinder = ButterKnife.bind(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mTvTitle.setVisibility(View.VISIBLE);
        ivPicture.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        llBack.setOnClickListener(this);
        ivAudio.setOnClickListener(this);

        arrMessages = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (Prefs.getString(MySharedPref.USER_ID, "").equals("")) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            if (getIntent().hasExtra("userId")) {
                GROUP_ID = getIntent().getStringExtra("groupId");
                otherUserId = getIntent().getStringExtra("userId");
                MyConstants.otherUserId = otherUserId;
                MyConstants.GROUP_ID = GROUP_ID;
                pos = getIntent().getStringExtra("pos");
                isRecentChat = getIntent().getStringExtra("isRecentChat");

            }
            Prefs.putString(MySharedPref.USER_ID, Prefs.getString(MySharedPref.USER_ID, ""));
        }
        if (getIntent().hasExtra("isNotification")) {
            isNotification = true;
        } else {
            isNotification = false;
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        mRlProgress.setVisibility(View.VISIBLE);
        getUserProfile();
        setAdapterData();
        bindWidget();

    }

    private void setAdapterData() {
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);

        mAdapter = new ChatAdapter(ChatActivity.this, arrMessages);
        mRvChatList.setLayoutManager(mLayoutManager);
        mRvChatList.setItemAnimator(new DefaultItemAnimator());
        mRvChatList.setAdapter(mAdapter);
    }

    private void bindWidget() {
        mIvBtnSend.setOnClickListener(view -> {
            if (Utils.isConnectingToInternet(ChatActivity.this)) {
                String msg = mEtText.getText().toString().trim();
                if (!msg.equals("")) {
                    updateTypingData(false);
                    isTyping = false;
                    if (timer != null) {
                        timer.cancel();
                        timer = null;
                    }
                    mRvChatList.smoothScrollToPosition(0);

                    try {
                        sendMessage(mEtText.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mEtText.setText("");
                }
            }
        });

        mEtText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!isTyping) {
                    Log.v("typing", "typing");
                    updateTypingData(true);
                    isTyping = true;
                    if (timer != null) {
                        timer.cancel();
                        timer = null;
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (isTyping) {
                            Log.v("test", "From type Stop");
                            isTyping = false;
                            updateTypingData(false);
                        }
                    }

                }, DELAY);

            }
        });
    }

    private void addRecent(final long time, final String message) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            final String strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUserId);
            Log.v("groupId", groupId + " group ID");
            Query query = mDatabase.child(MyConstants.TBL_RECENT).orderByChild("groupId").equalTo(groupId)/*.orderByChild("userId").equalTo(strMyUDID)*/;
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()) {

                        Map<String, Object> valuesMap = new HashMap<>();
                        valuesMap.put("objectId", Prefs.getString(MySharedPref.USER_ID, "") + "-" + otherUserId);
                        valuesMap.put("description", otherUser.getName());
                        valuesMap.put("memberImage", otherUser.getPicture());
                        valuesMap.put("counter", 0);
                        valuesMap.put("createdAt", time);
                        valuesMap.put("groupId", groupId);
                        valuesMap.put("lastMessage", message);
                        valuesMap.put("lastMessageDate", time);
                        valuesMap.put("userId", Prefs.getString(MySharedPref.USER_ID, ""));
                        valuesMap.put("member1", Prefs.getString(MySharedPref.USER_ID, ""));
                        valuesMap.put("member2", otherUserId);

                        mDatabase.child(MyConstants.TBL_RECENT).child(Prefs.getString(MySharedPref.USER_ID, "") + "-" + otherUserId).setValue(valuesMap);

                        Map<String, Object> valuesMap2 = new HashMap<>();
                        valuesMap2.put("objectId", otherUserId + "-" + Prefs.getString(MySharedPref.USER_ID, ""));
                        valuesMap2.put("description", Prefs.getString(MySharedPref.USER_FIRSTNAME, "")+" "+ Prefs.getString(MySharedPref.USER_LASTNAME, ""));
                        valuesMap2.put("memberImage", Prefs.getString(MySharedPref.USER_PROFILE_PIC, ""));
                        valuesMap2.put("counter", 1);
                        valuesMap2.put("createdAt", time);
                        valuesMap2.put("groupId", groupId);
                        valuesMap2.put("lastMessage", message);
                        valuesMap2.put("lastMessageDate", time);
                        valuesMap2.put("userId", otherUserId);
                        valuesMap2.put("member1", otherUserId);
                        valuesMap2.put("member2", Prefs.getString(MySharedPref.USER_ID, ""));

                        mDatabase.child(MyConstants.TBL_RECENT).child(otherUserId + "-" + Prefs.getString(MySharedPref.USER_ID, "")).setValue(valuesMap2);

                        getBadgeCount(message);

                    } else {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private void getUserProfile() {
        Query query = mDatabase.child(MyConstants.TBL_USER).orderByChild("userId").equalTo(otherUserId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshott) {

                if (mRlProgress != null) {
                    mRlProgress.setVisibility(View.GONE);
                }
                if (dataSnapshott.exists()) {
                    for (DataSnapshot singleSnapshot : dataSnapshott.getChildren()) {
                        otherUser = singleSnapshot.getValue(Users.class);

                        Glide.with(ChatActivity.this)
                                .load(otherUser.getPicture())
                                .placeholder(R.drawable.user)
                                .error(R.drawable.noimage)
                                .into(ivUserImg);

                        ivUserImg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popUpForProfile(otherUser.getPicture());
                            }
                        });

                        if (mTvTitle == null) {

                        } else {
                            mTvTitle.setText(otherUser.getName());
                            isOnline = otherUser.isOnline();

                        }
                        break;
                    }
                    getMessages();
                    isActiveUser();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void popUpForProfile(String otherProfilePic) {
        LayoutInflater li = LayoutInflater.from(ChatActivity.this);
        View promptsView = li.inflate(R.layout.profile_zoom, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChatActivity.this);
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        final ImageView ivProfile =  promptsView.findViewById(R.id.ivProfile);
        Glide.with(ChatActivity.this)
                .load(otherProfilePic)
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .into(ivProfile);
        alertDialog.show();
    }
    public void getMessages() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            if (otherUser != null) {
                final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""),
                        otherUser.getUserId());
                Query query = mDatabase.child(MyConstants.TBL_MESSAGES).orderByChild("groupId").equalTo(groupId);
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        arrMessages.clear();
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                                Messages messages = singleSnapshot.getValue(Messages.class);

                                try {
                                    if (messages.getDeletedBy() != 0 && messages.getDeletedBy() != Integer.valueOf(Prefs.getString(MySharedPref.USER_ID, ""))) {
                                        arrMessages.add(messages);
                                        Utils.sortMessageConversation(arrMessages);
                                    } else if (messages.getDeletedBy() == 0) {
                                        arrMessages.add(messages);
                                        Utils.sortMessageConversation(arrMessages);
                                    }
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                        mAdapter.notifyDataSetChanged();
                        if (arrMessages.size() > 0) {
                            if (mRvChatList != null) {
                                mRvChatList.scrollToPosition(0);
                            }

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    private Runnable mOnActivityResultTask;

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (Utils.checkValidResult(ChatActivity.this, resultCode)) {
            if (requestCode == MyConstants.REQUEST_CAMERA) {
                picturePath = data.getStringExtra("picture");
                if (Utils.isConnectingToInternet(ChatActivity.this)) {
                    uploadPicture();
                }

            } else if (requestCode == MyConstants.REQUEST_PICK_IMAGE) {
                picturePath = data.getStringExtra("picture");
                if (Utils.isConnectingToInternet(ChatActivity.this)) {
                    uploadPicture();
                }

            }
            else if (requestCode == MyConstants.REQUEST_TAKE_AUDIO ) {
                if ((data != null) && (data.getData() != null)) {
                    audioID = data.getDataString();
                    String filePath[] = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(data.getData(), filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    uploadAudio(c.getString(columnIndex));
                }
            }
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (mReturningWithResult) {

            if (mOnActivityResultTask != null) {
                mOnActivityResultTask.run();
                mOnActivityResultTask = null;
            }
        }

        mReturningWithResult = false;
    }

    // Send Text Message
    private void sendMessage(String message) throws JSONException {

        String objectId = Utils.randomString();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            if (otherUser != null) {
                final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUser.getUserId());
                Calendar calendar = Calendar.getInstance();
                long time = calendar.getTimeInMillis();

                Map<String, Object> valuesMap = new HashMap<>();
                valuesMap.put("objectId", objectId);
                valuesMap.put("createdAt", time);
                valuesMap.put("groupId", groupId);
                valuesMap.put("type", "text");
                valuesMap.put("senderId", Prefs.getString(MySharedPref.USER_ID, ""));
                valuesMap.put("senderName", Prefs.getString(MySharedPref.USER_FIRSTNAME, "")+" "+Prefs.getString(MySharedPref.USER_LASTNAME, ""));
                valuesMap.put("text", message);
                valuesMap.put("picture", Prefs.getString(MySharedPref.USER_PROFILE_PIC,""));
                valuesMap.put("deletedBy", 0);
                mDatabase.child(MyConstants.TBL_MESSAGES).child(objectId).setValue(valuesMap);

                updateRecentMessage(time, message);

                makeRequest(Prefs.getString(MySharedPref.DEVICE_TOKEN,""),Prefs.getString(MySharedPref.USER_FIRSTNAME, "")+" "+Prefs.getString(MySharedPref.USER_LASTNAME, "") +"  :  "+message);

            }
        }
    }

    private void updateTypingData(boolean isTypeing) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            if (otherUser != null) {
                final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUser.getUserId());
                mDatabase.child(MyConstants.TBL_TYPING).child(groupId).child(Prefs.getString(MySharedPref.USER_ID, "")).setValue(isTypeing);
            }
        }
    }
    // Send Picture

    private boolean checkPermissionForPicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(ChatActivity.this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(ChatActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(ChatActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MyConstants.REQUEST_CAMERA);
                return false;

            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void uploadPicture() {
        StorageReference mStorageRef;
        mStorageRef = FirebaseStorage.getInstance().getReference();
        File originalFile = new File(picturePath);
        if (Utils.getMimeExtension(picturePath).equals("jpeg") || Utils.getMimeExtension(picturePath).equals("jpg") || Utils.getMimeExtension(picturePath).equals("png")) {
            File compressFile = Utils.getThumbImage(Calendar.getInstance(), picturePath);

            if (!Utils.isStringNull(picturePath)) {
                Uri file;
                if (compressFile != null) {
                    file = Uri.fromFile(compressFile);
                } else {
                    file = Uri.fromFile(originalFile);
                }
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    final String strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    final String picture = MyConstants.STORAGE_CHAT_PICTURE + Prefs.getString(MySharedPref.USER_ID, "") + "_" + Calendar.getInstance().getTimeInMillis() + ".jpg";
                    final StorageReference riversRef = mStorageRef.child(picture);

                    if (mRlProgress != null) {
                        mRlProgress.setVisibility(View.VISIBLE);
                    }
                    riversRef.putFile(file)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                    if (mRlProgress != null)
                                        mRlProgress.setVisibility(View.GONE);

                                    riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            Uri downloadUrl = uri;
                                            //Do what you want with the url

                                            picturePath = "";
                                            if (Utils.isConnectingToInternet(ChatActivity.this)) {
                                                try {
                                                    sendPicture(downloadUrl.toString());
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });


                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {

                                    if (mRlProgress != null)
                                        mRlProgress.setVisibility(View.GONE);
                                    Log.e("Image Upload Fail", exception.getMessage());
                                }
                            });
                }
            }
        } else {
            DialogAlert.show_dialog_ok(ChatActivity.this, "Select_another_picture", new OnItemClick() {
                @Override
                public void OnItemClickListner(int position) {

                }
            });
        }
    }

    private void sendPicture(String picturePath) throws JSONException {
        String objectId = Utils.randomString();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            if (otherUser != null) {
                final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUser.getUserId());
                Calendar calendar = Calendar.getInstance();
                long time = calendar.getTimeInMillis();


                Map<String, Object> valuesMap = new HashMap<>();
                valuesMap.put("objectId", objectId);
                valuesMap.put("createdAt", time);
                valuesMap.put("groupId", groupId);
                valuesMap.put("type", "picture");
                valuesMap.put("senderId", Prefs.getString(MySharedPref.USER_ID, ""));
                valuesMap.put("senderName", Prefs.getString(MySharedPref.USER_FIRSTNAME, "")+" "+Prefs.getString(MySharedPref.USER_LASTNAME, ""));
                valuesMap.put("text", "Picture");
                valuesMap.put("picture", picturePath);
                valuesMap.put("deletedBy", 0);
                mDatabase.child(MyConstants.TBL_MESSAGES).child(objectId).setValue(valuesMap);

                updateRecentMessage(time, "Picture");

                makeRequest(Prefs.getString(MySharedPref.DEVICE_TOKEN,""),Prefs.getString(MySharedPref.USER_FIRSTNAME, "")+" "+Prefs.getString(MySharedPref.USER_LASTNAME, "") +"  "+"Sent Picture");
            }
        }
    }


    // Send Video
    private boolean checkPermissionForVideo() {

        if (checkPermissionAudio()) {
            if (checkPermissionForPicture()) {
                return true;
            } else {
                return false;
            }
        } else {
            if (checkPermissionForPicture()) {
                return true;
            } else {
                return false;
            }
        }

    }

    private boolean checkPermissionAudio() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                        MyConstants.REQUEST_TAKE_AUDIO);

                return false;

            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    /*Upload Audio*/
    private void uploadAudio(String audioPath) {
        StorageReference mStorageRef;
        mStorageRef = FirebaseStorage.getInstance().getReference();
        if (audioPath != null) {
            File originalFile = new File(audioPath);
            if (!Utils.isStringNull(audioPath)) {
                Uri file = Uri.fromFile(originalFile);
                String fileExtension
                        = "." + MimeTypeMap.getFileExtensionFromUrl(file.toString());
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    final String strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    final String video = MyConstants.STORAGE_CHAT_AUDIOS + Prefs.getString(MySharedPref.USER_ID, "") + "_" + Calendar.getInstance().getTimeInMillis() + fileExtension;
                    final StorageReference riversRef = mStorageRef.child(video);
                    //final ProgressDialog dialog = ProgressDialog.show(DouguaChatActivity.this, null, getResources().getString(R.string.please_wait));
                    if (mRlProgress != null)
                        mRlProgress.setVisibility(View.VISIBLE);
                    riversRef.putFile(file)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                    if (mRlProgress != null)
                                        mRlProgress.setVisibility(View.GONE);

                                    riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            Uri downloadUrl = uri;

                                            if (Utils.isConnectingToInternet(ChatActivity.this)) {
                                                try {
                                                    sendAudio(downloadUrl.toString());
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    if (mRlProgress != null)
                                        mRlProgress.setVisibility(View.GONE);
                                    Log.e("Audio Upload Fail", exception.getMessage());
                                }
                            });
                }

            }
        }

    }

    private void sendAudio(String audioPath) throws JSONException {
        String objectId = Utils.randomString();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            if (otherUser != null) {
                final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUser.getUserId());
                Calendar calendar = Calendar.getInstance();
                long time = calendar.getTimeInMillis();

                Map<String, Object> valuesMap = new HashMap<>();
                valuesMap.put("objectId", objectId);
                valuesMap.put("createdAt", time);
                valuesMap.put("groupId", groupId);
                valuesMap.put("type", "audio");
                valuesMap.put("senderId", Prefs.getString(MySharedPref.USER_ID, ""));
                valuesMap.put("senderName", Prefs.getString(MySharedPref.USER_FIRSTNAME, "")+""+ Prefs.getString(MySharedPref.USER_LASTNAME, ""));
                valuesMap.put("text", "Audio");
                valuesMap.put("picture", "");
                valuesMap.put("video", "");
                valuesMap.put("videoThumb", "");
                valuesMap.put("audio", audioPath);
                valuesMap.put("deletedBy", 0);
                mDatabase.child(MyConstants.TBL_MESSAGES).child(objectId).setValue(valuesMap);
                updateRecentMessage(time, "Audio");

                makeRequest(Prefs.getString(MySharedPref.DEVICE_TOKEN,""),Prefs.getString(MySharedPref.USER_FIRSTNAME, "")+" "+Prefs.getString(MySharedPref.USER_LASTNAME, "") +"  "+"Sent Audio");

            }
        }
    }

    private void getBadgeCount(final String message) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null && otherUser.getUserId() != null) {
            final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUser.getUserId());
            Log.v("groupId", groupId + " ID");
            Query query = mDatabase.child(MyConstants.TBL_RECENT).orderByChild("userId").equalTo(otherUser.getUserId());
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            Recent recent = singleSnapshot.getValue(Recent.class);

                            if (recent.getCounter() != 0) {
                                arrRecentChatBadge.add(recent);
                                badge = badge + recent.getCounter();
                                recentChatBadge = recent;
                            }

                        }
                        if (arrRecentChatBadge != null) {

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }

    // Send Notification
    public void makeRequest(String id,String message) throws JSONException {
        JSONObject json = new JSONObject();
        JSONObject info = new JSONObject();
        info.put("title", "Talentogram");   // Notification title
        info.put("message", message); // Notification body
        info.put("sound", "mySound"); // Notification sound

        json.put("data", info);
        json.put("to",otherUser.getFcmtoken());

        Log.e("deviceidkey==> ",id+"");
        Log.e("jsonn==> ",json.toString());

        AndroidNetworking.post("https://fcm.googleapis.com/fcm/send")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Authorization", "Bearer "+ MyConstants.FIREBASE_AUTH)
                .addJSONObjectBody(json)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("Success11");
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError ===>", String.valueOf(anError));
                        System.out.println("fail11");
                    }
                });

    }


    private void updateRecentMessage(final long time, final String message) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null && otherUser != null) {

            Log.v("Other USer ID", otherUser.getUserId() + " ID");

            final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUser.getUserId());
            Log.v("groupId", groupId + " ID");
            Query query = mDatabase.child(MyConstants.TBL_RECENT).orderByChild("groupId").equalTo(groupId);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {

                        int loop = 0;

                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            Recent recent = singleSnapshot.getValue(Recent.class);
                            loop++;

                            mDatabase.child(MyConstants.TBL_RECENT).child(recent.getObjectId()).child("createdAt").setValue(time);
                            mDatabase.child(MyConstants.TBL_RECENT).child(recent.getObjectId()).child("lastMessage").setValue(message);
                            mDatabase.child(MyConstants.TBL_RECENT).child(recent.getObjectId()).child("lastMessageDate").setValue(time);

                            int counter = 0;
                            if (recent.getUserId().equals(otherUser.getUserId())) {
                                if (!isActiveUser) {
                                    counter = recent.getCounter();
                                    counter++;
                                    mDatabase.child(MyConstants.TBL_RECENT).child(recent.getObjectId()).child("counter").setValue(counter);
                                    getBadgeCount(message);
                                }
                            } else {

                            }

                        }
                        if (loop == 1) {

                            Map<String, Object> valuesMap = new HashMap<>();
                            valuesMap.put("objectId", Prefs.getString(MySharedPref.USER_ID, "") + "-" + otherUserId);
                            valuesMap.put("description", otherUser.getName());
                            valuesMap.put("memberImage", otherUser.getPicture());
                            valuesMap.put("counter", 0);
                            valuesMap.put("createdAt", time);
                            valuesMap.put("groupId", groupId);
                            valuesMap.put("lastMessage", message);
                            valuesMap.put("lastMessageDate", time);
                            valuesMap.put("userId", Prefs.getString(MySharedPref.USER_ID, ""));
                            valuesMap.put("member1", Prefs.getString(MySharedPref.USER_ID, ""));
                            valuesMap.put("member2", otherUserId);

                            mDatabase.child(MyConstants.TBL_RECENT).child(Prefs.getString(MySharedPref.USER_ID, "") + "-" + otherUserId).setValue(valuesMap);

                            Map<String, Object> valuesMap2 = new HashMap<>();
                            valuesMap2.put("objectId", otherUserId + "-" + Prefs.getString(MySharedPref.USER_ID, ""));
                            valuesMap2.put("description", Prefs.getString(MySharedPref.USER_FIRSTNAME, "")+" "+ Prefs.getString(MySharedPref.USER_LASTNAME, ""));
                            valuesMap2.put("memberImage", Prefs.getString(MySharedPref.USER_PROFILE_PIC, ""));
                            valuesMap2.put("counter", 1);
                            valuesMap2.put("createdAt", time);
                            valuesMap2.put("groupId", groupId);
                            valuesMap2.put("lastMessage", message);
                            valuesMap2.put("lastMessageDate", time);
                            valuesMap2.put("userId", otherUserId);
                            valuesMap2.put("member1", otherUserId);
                            valuesMap2.put("member2", Prefs.getString(MySharedPref.USER_ID, ""));

                            mDatabase.child(MyConstants.TBL_RECENT).child(otherUserId + "-" + Prefs.getString(MySharedPref.USER_ID, "")).setValue(valuesMap2);

                        }
                    } else {
                        Log.v("Not Exits?", "Not");

                        Log.v("Time", time + "");
                        Log.v("message", message + "");

                        isFirst = true;
                        if (isRecentChat.equals("no")) {
                            addRecent(time, message);

                        }

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void isActiveUser() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null && otherUser != null) {

            final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUser.getUserId());
            Query query = mDatabase.child(MyConstants.TBL_RECENT).orderByChild("groupId").equalTo(groupId);
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            Recent recent = singleSnapshot.getValue(Recent.class);
                            if (recent.getUserId().equals(otherUser.getUserId())) {
                                isActiveUser = recent.isActive();
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void goOnline(final boolean isActive) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            final String strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            final String groupId = Utils.createGroupId(Prefs.getString(MySharedPref.USER_ID, ""), otherUserId);
            Query query = mDatabase.child(MyConstants.TBL_RECENT).orderByChild("groupId").equalTo(groupId);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            Recent recent = singleSnapshot.getValue(Recent.class);
                            if (recent.getUserId().equals(Prefs.getString(MySharedPref.USER_ID, ""))) {
                                mDatabase.child(MyConstants.TBL_RECENT).child(recent.getObjectId()).child("active").setValue(isActive);
                                if (isActive) {
                                    mDatabase.child(MyConstants.TBL_RECENT).child(recent.getObjectId()).child("counter").setValue(0);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        goOnline(true);

    }

    @Override
    protected void onPause() {
        super.onPause();
        goOnline(false);
        if (mAdapter != null) {
            mAdapter.setActivityPause(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        if (mAdapter != null) {
            mAdapter.setActivityDestroy(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isNotification) {
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivPicture:
                if (checkPermissionForPicture()) {
                    startActivityForResult(new Intent(ChatActivity.this,
                            ChatImagePreviewActivity.class).putExtra("gallery", true), MyConstants.REQUEST_PICK_IMAGE);
                }
                break;
            case R.id.ivCamera:
                if (checkPermissionForPicture()) {
                    startActivityForResult(new Intent(ChatActivity.this,
                            ChatImagePreviewActivity.class).putExtra("gallery", false), MyConstants.REQUEST_CAMERA);
                }
                break;
                case R.id.llBack:
                onBackPressed();
                break;
            case R.id.ivAudio:
                if (checkPermissionForPicture()) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, MyConstants.REQUEST_TAKE_AUDIO);
                }
                break;
            default:

        }
    }
}
