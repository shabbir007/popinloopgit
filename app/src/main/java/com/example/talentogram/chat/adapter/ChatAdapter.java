package com.example.talentogram.chat.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.talentogram.R;
import com.example.talentogram.chat.activity.ActivityPictureFullView;
import com.example.talentogram.chat.helper.DialogAlert;
import com.example.talentogram.chat.helper.ImageLoader;
import com.example.talentogram.chat.model.Messages;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static ChatAdapter instance = null;

    public ChatAdapter() {
        instance = this;
    }

    public static synchronized ChatAdapter getInstance() {
        if (instance == null) {
            instance = new ChatAdapter();
        }
        return instance;
    }

    private Context mContext;
    private ArrayList<Messages> arrMessages;
    public String strMyUDID;

    public ImageLoader imageLoader;
    Bitmap bitmap;
    private DownloadManager downloadManager;
    private RecyclerView.ViewHolder holderRowAudio;
    private SeekBarRunnable seekBarRunnable;
    boolean isPreparedMedia = false;
    long mPlaylingPositionAudioDuration = 0;

    private long refid;

    private boolean isPlaying = false;

    private static final int NOT_PLAYING = -1;

    private static final String TAG = "";

    public MediaPlayer mPlayer = new MediaPlayer();
    private int mPlayingPosition = NOT_PLAYING, mLastPlayingPosition = NOT_PLAYING;
    private Handler mHandler = new Handler();

    private PlaybackUpdater mProgressUpdater = new PlaybackUpdater();

    public ChatAdapter(Context mContext, ArrayList<Messages> arrMessages) {
        this.mContext = mContext;
        this.arrMessages = arrMessages;
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            strMyUDID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        }
        seekBarRunnable = new SeekBarRunnable();
        downloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        imageLoader = new ImageLoader(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_sender, parent, false);
                return new SenderViewHolder(view);
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_receiver, parent, false);
                return new ReceiverViewHolder(view);
        }
        return null;


    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final Messages messages = arrMessages.get(position);
        if (holder instanceof SenderViewHolder) {
            ((SenderViewHolder) holder).llSenderText.setVisibility(View.GONE);
            ((SenderViewHolder) holder).llSenderPicture.setVisibility(View.GONE);
            ((SenderViewHolder) holder).llSenderVideo.setVisibility(View.GONE);
            ((SenderViewHolder) holder).llSenderAudio.setVisibility(View.GONE);
            ((SenderViewHolder) holder).tvMsgTime.setVisibility(View.GONE);
            ((SenderViewHolder) holder).mVoiceProgressBar.setVisibility(View.GONE);
            ((SenderViewHolder) holder).seekBar.setProgress(0);

            if (position != 0) {
                if (messages.getSenderId().equals(arrMessages.get(position - 1).getSenderId())) {
                    String currentMesTime = Utils.convertTimestampToString(messages.getCreatedAt(), "dd/MM/yyyy hh:mm");
                    String nextMesTime = Utils.convertTimestampToString(arrMessages.get(position - 1).getCreatedAt(), "dd/MM/yyyy hh:mm");
                    if (currentMesTime.equals(nextMesTime)) {
                        ((SenderViewHolder) holder).tvMsgTime.setVisibility(View.GONE);
                    } else {
                        ((SenderViewHolder) holder).tvMsgTime.setVisibility(View.VISIBLE);
                    }
                } else {
                    ((SenderViewHolder) holder).tvMsgTime.setVisibility(View.VISIBLE);
                }
            } else {
                ((SenderViewHolder) holder).tvMsgTime.setVisibility(View.VISIBLE);
            }
            ((SenderViewHolder) holder).tvMsgTime.setText(Utils.getMessageTimeInChat(messages.getCreatedAt()).toLowerCase());

            if (position == mPlayingPosition) {
                holderRowAudio = ((SenderViewHolder) holder);
                if (mPlayer != null) {
                    String v = String.format("%02d", mPlayer.getCurrentPosition() / 60000);
                    int va = (int) ((mPlayer.getCurrentPosition() % 60000) / 1000);
                    ((SenderViewHolder) holder).mTvDuration.setText(v + ":" + String.format("%02d", va));
                    ((SenderViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                    ((SenderViewHolder) holder).seekBar.setMax(mPlayer.getDuration());
                }
            } else {
                pauseOrStopPlayer(holder, true);
                if (messages.getAudioDuration() > 0) {
                    String v = String.format("%02d", messages.getAudioDuration() / 60000);
                    int va = (int) ((messages.getAudioDuration() % 60000) / 1000);
                    ((SenderViewHolder) holder).mTvDuration.setText(v + ":" + String.format("%02d", va));
                } else {
                    ((SenderViewHolder) holder).mTvDuration.setText("00:00");
                }
            }

            ((SenderViewHolder) holder).tvMsgTime.setText(Utils.getMessageTimeInChat(messages.getCreatedAt()).toLowerCase());
            if (messages.getType().equals("text")) {
                if (!messages.getText().equals("")) {
                    ((SenderViewHolder) holder).llSenderText.setVisibility(View.VISIBLE);
                    ((SenderViewHolder) holder).tvSenderMsg.setText(messages.getText());
                }
            } else if (messages.getType().equals("picture")) {
                ((SenderViewHolder) holder).llSenderPicture.setVisibility(View.VISIBLE);

                Log.v("picture", messages.getPicture());

                ContextWrapper cw = new ContextWrapper(mContext);

                try {
                    File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                    File f = new File(directory, messages.getPicture().hashCode() + ".jpg");

                    if (!f.exists()) {
                        ImageView image = ((SenderViewHolder) holder).ivSenderPic;
                        if (image != null)
                            bitmap = Utils.getBitmapFromURL(messages.getPicture());
                        image.setImageBitmap(bitmap);
                        Utils.saveToInternalStorage(mContext, bitmap, messages.getPicture());
                    } else {

                        RequestOptions options = new RequestOptions()
                                .placeholder(R.drawable.noimage)
                                .error(R.drawable.noimage);

                        Glide.with(mContext).load("file://" + f.getPath()).apply(options).into(((SenderViewHolder) holder).ivSenderPic);
                    }
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    DialogAlert.show_alert_dialog(mContext,mContext.getResources().getString(R.string.memory_msg));
                }

            }  else if (messages.getType().equals("video")) {
                ((SenderViewHolder) holder).llSenderVideo.setVisibility(View.VISIBLE);

                try {
                    ContextWrapper cw1 = new ContextWrapper(mContext);

                    File directory1 = cw1.getDir("videoDir", Context.MODE_PRIVATE);
                    File ff = new File(directory1, messages.getVideo().hashCode() + ".Mp4");
                    if (!ff.exists()) {
                        Utils.downloadFile(messages.getVideo(), directory1.getAbsolutePath());
                    }

                    try {

                        ImageView imageThumb = ((SenderViewHolder) holder).ivSenderVideoThumb;

                        try {
                            File thumb = new File(mContext.getCacheDir(), "thumb_" + ff.getPath().hashCode() + ".jpg");
                            if (thumb.exists()) {

                                RequestOptions options = new RequestOptions()
                                        .placeholder(R.drawable.noimage)
                                        .error(R.drawable.noimage);

                                Glide.with(mContext).load(thumb).apply(options).into(imageThumb);
                            } else {
                                File videoThumb = Utils.getThumb(mContext, ff.getPath());

                                if (videoThumb.exists()) {

                                    RequestOptions options = new RequestOptions()
                                            .placeholder(R.drawable.noimage)
                                            .error(R.drawable.noimage);

                                    Glide.with(mContext).load(videoThumb).apply(options).into(imageThumb);

                                }
                            }
                        } catch (OutOfMemoryError e) {
                            e.printStackTrace();
                            DialogAlert.show_alert_dialog(mContext,mContext.getResources().getString(R.string.memory_msg));
                        }

                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }  else if (messages.getType().equals("audio")) {
                ((SenderViewHolder) holder).llSenderAudio.setVisibility(View.VISIBLE);
                Log.v("Audio", messages.getAudio());
                try {
                    ContextWrapper cw1 = new ContextWrapper(mContext);

                    File directory1 = cw1.getDir("audioDir", Context.MODE_PRIVATE);
                    File ff = new File(directory1, messages.getAudio().hashCode() + ".mp3");
                    if (!ff.exists()) {
                        Utils.downloadAudio(messages.getAudio(), directory1.getAbsolutePath());
                    }

                    ContextWrapper cw = new ContextWrapper(mContext);

                    File directory = cw.getDir("audioDir", Context.MODE_PRIVATE);
                    String filename = String.valueOf(directory + "/" + messages.getAudio().hashCode()) + ".mp3";

                    if (!messages.isCallAudioDuration()) {
                        final MediaPlayer mediaPlayer1 = new MediaPlayer();

                        mediaPlayer1.setDataSource(mContext, Uri.parse(filename));

                        mediaPlayer1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                String v = String.format("%02d", mediaPlayer1.getDuration() / 60000);
                                int va = (int) ((mediaPlayer1.getDuration() % 60000) / 1000);
                                va = va +1;
                                messages.setAudioDuration(mediaPlayer1.getDuration());
                                ((SenderViewHolder) holder).mTvDuration.setText(v + ":" + String.format("%02d", va));
                                ((SenderViewHolder) holder).mVoiceProgressBar.setVisibility(View.GONE);
                            }
                        });
                        mediaPlayer1.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                            @Override
                            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                                ((SenderViewHolder) holder).mTvDuration.setText("00:00");
                                ((SenderViewHolder) holder).mVoiceProgressBar.setVisibility(View.GONE);
                                return false;
                            }
                        });
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    mediaPlayer1.prepare();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                        ((SenderViewHolder) holder).mVoiceProgressBar.setVisibility(View.VISIBLE);
                        messages.setCallAudioDuration(true);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    DialogAlert.show_alert_dialog(mContext,mContext.getResources().getString(R.string.memory_msg));
                }
            }

            ((SenderViewHolder) holder).seekBar.setClickable(false);

            if (position == mPlayingPosition) {
                if (mPlayer != null)
                    ((SenderViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());

            } else {
                ((SenderViewHolder) holder).seekBar.setProgress(0);
                ((SenderViewHolder) holder).ivPlay.setImageResource(R.drawable.ic_playvideo);
                ((SenderViewHolder) holder).chronometerTimer.setBase(SystemClock.elapsedRealtime());
                ((SenderViewHolder) holder).chronometerTimer.stop();

            }

            ((SenderViewHolder) holder).ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        ContextWrapper cw = new ContextWrapper(mContext);
                        File directory = cw.getDir("audioDir", Context.MODE_PRIVATE);
                        String filename = String.valueOf(directory + "/" + messages.getAudio().hashCode()) + ".mp3";

                        final String filepath = filename;

                        if (position == mPlayingPosition) {
                            if (mPlayer != null) {
                                if (mPlayer.isPlaying()) {
                                    ((SenderViewHolder) holder).mVoiceProgressBar.setVisibility(View.GONE);
                                    mPlayer.pause();
                                } else {
                                    if (isPreparedMedia)
                                        mPlayer.start();
                                }
                                startPlayer("");
                            }
                        } else {
                            if (mPlayingPosition != -1)
                                mLastPlayingPosition = mPlayingPosition;
                            mPlayingPosition = position;
                            mPlaylingPositionAudioDuration = messages.getAudioDuration();
                            if (mPlayer != null) {
                                mPlayer.release();
                                if (holderRowAudio != null) {
                                    pauseOrStopPlayer(holderRowAudio, false, mLastPlayingPosition);

                                }
                            }
                            if (holderRowAudio != null && holderRowAudio instanceof ReceiverViewHolder) {
                                ((ReceiverViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                            } else if (holderRowAudio != null && holderRowAudio instanceof SenderViewHolder) {
                                ((SenderViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                            }
                            holderRowAudio = ((SenderViewHolder) holder);
                            ((SenderViewHolder) holder).mVoiceProgressBar.setVisibility(View.VISIBLE);
                            startPlayer(filepath);
                        }

                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        DialogAlert.show_alert_dialog(mContext,mContext.getResources().getString(R.string.memory_msg));
                    }
                }
            });

            ((SenderViewHolder) holder).ivSenderPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(mContext, ActivityPictureFullView.class);
                    intent.putExtra("picture", messages.getPicture());
                    intent.putExtra("chat", "yes");
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((Activity) mContext,
                                    ((SenderViewHolder) holder).ivSenderPic,
                                    ViewCompat.getTransitionName(((SenderViewHolder) holder).ivSenderPic));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        mContext.startActivity(intent, options.toBundle());
                    } else {
                        mContext.startActivity(intent);
                    }

                }
            });
        } else {
            ((ReceiverViewHolder) holder).llReceiverText.setVisibility(View.GONE);
            ((ReceiverViewHolder) holder).llReceiverPicture.setVisibility(View.GONE);
            ((ReceiverViewHolder) holder).llReceiverAudio.setVisibility(View.GONE);

            ((ReceiverViewHolder) holder).llReceiverVideo.setVisibility(View.GONE);

            ((ReceiverViewHolder) holder).tvMsgTime.setVisibility(View.GONE);
            ((ReceiverViewHolder) holder).mVoiceProgressBar.setVisibility(View.GONE);
            ((ReceiverViewHolder) holder).seekBar.setProgress(0);

            if (position == mPlayingPosition) {
                holderRowAudio = ((ReceiverViewHolder) holder);
                if (mPlayer != null) {
                    String v = String.format("%02d", mPlayer.getCurrentPosition() / 60000);
                    int va = (int) ((mPlayer.getCurrentPosition() % 60000) / 1000);
                    ((ReceiverViewHolder) holder).mTvDuration.setText(v + ":" + String.format("%02d", va));
                    ((ReceiverViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                    ((ReceiverViewHolder) holder).seekBar.setMax(mPlayer.getDuration());
                }
            } else {
                pauseOrStopPlayer(holder, true);
                if (messages.getAudioDuration() > 0) {
                    String v = String.format("%02d", messages.getAudioDuration() / 60000);
                    int va = (int) ((messages.getAudioDuration() % 60000) / 1000);
                    ((ReceiverViewHolder) holder).mTvDuration.setText(v + ":" + String.format("%02d", va));
                } else {
                    ((ReceiverViewHolder) holder).mTvDuration.setText("00:00");
                }

            }

            if (position != 0) {
                if (messages.getSenderId().equals(arrMessages.get(position - 1).getSenderId())) {
                    String currentMesTime = Utils.convertTimestampToString(messages.getCreatedAt(), "dd/mm/yyyy hh:mm");
                    String nextMesTime = Utils.convertTimestampToString(arrMessages.get(position - 1).getCreatedAt(), "dd/mm/yyyy hh:mm");
                    if (currentMesTime.equals(nextMesTime)) {
                        ((ReceiverViewHolder) holder).tvMsgTime.setVisibility(View.GONE);
                    } else {
                        ((ReceiverViewHolder) holder).tvMsgTime.setVisibility(View.VISIBLE);
                    }
                } else {
                    ((ReceiverViewHolder) holder).tvMsgTime.setVisibility(View.VISIBLE);
                }
            } else {
                ((ReceiverViewHolder) holder).tvMsgTime.setVisibility(View.VISIBLE);
            }

            ((ReceiverViewHolder) holder).tvMsgTime.setText(Utils.getMessageTimeInChat(messages.getCreatedAt()).toLowerCase());

            if (messages.getType().equals("text")) {
                if (!messages.getText().equals("")) {
                    ((ReceiverViewHolder) holder).llReceiverText.setVisibility(View.VISIBLE);
                    ((ReceiverViewHolder) holder).tvReceiverMsg.setText(messages.getText());
                }
            } else if (messages.getType().equals("picture")) {
                ((ReceiverViewHolder) holder).llReceiverPicture.setVisibility(View.VISIBLE);


                ContextWrapper cw = new ContextWrapper(mContext);

                try {
                    File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                    File f = new File(directory, messages.getPicture().hashCode() + ".jpg");

                    if (!f.exists()) {
                        ImageView image = ((ReceiverViewHolder) holder).ivReceiverPic;
                        if (image != null)
                            bitmap = Utils.getBitmapFromURL(messages.getPicture());


                        image.setImageBitmap(bitmap);
                        Utils.saveToInternalStorage(mContext, bitmap, messages.getPicture());
                    } else {

                        RequestOptions options = new RequestOptions()
                                .placeholder(R.drawable.noimage)
                                .error(R.drawable.noimage);

                        Glide.with(mContext).load("file://" + f.getPath()).apply(options).into(((ReceiverViewHolder) holder).ivReceiverPic);
                    }
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    DialogAlert.show_alert_dialog(mContext,mContext.getResources().getString(R.string.memory_msg));
                }

            } else if (messages.getType().equals("video")) {
                ((ReceiverViewHolder) holder).llReceiverVideo.setVisibility(View.VISIBLE);

                try {
                    ContextWrapper cw1 = new ContextWrapper(mContext);

                    File directory1 = cw1.getDir("videoDir", Context.MODE_PRIVATE);
                    File ff = new File(directory1, messages.getVideo().hashCode() + ".Mp4");
                    if (!ff.exists()) {
                        Utils.downloadFile(messages.getVideo(), directory1.getAbsolutePath());
                    }

                    try {

                        ImageView imageThumb = ((ReceiverViewHolder) holder).ivReceiverVideoThumb;
                        File thumb = new File(mContext.getCacheDir(), "thumb_" + ff.getPath().hashCode() + ".jpg");
                        if (thumb.exists()) {

                            RequestOptions options = new RequestOptions()
                                    .placeholder(R.drawable.noimage)
                                    .error(R.drawable.noimage);

                            Glide.with(mContext).load(thumb).apply(options).into(imageThumb);
                        } else {
                            File videoThumb = Utils.getThumb(mContext, ff.getPath());

                            if (videoThumb.exists()) {

                                RequestOptions options = new RequestOptions()
                                        .placeholder(R.drawable.noimage)
                                        .error(R.drawable.noimage);

                                Glide.with(mContext).load(videoThumb).apply(options).into(imageThumb);

                            }
                        }

                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    DialogAlert.show_alert_dialog(mContext,mContext.getResources().getString(R.string.memory_msg));
                }

            } else if (messages.getType().equals("audio")) {
                ((ReceiverViewHolder) holder).llReceiverAudio.setVisibility(View.VISIBLE);
                Log.v("Audio", messages.getAudio());
                try {
                    ContextWrapper cw1 = new ContextWrapper(mContext);

                    File directory1 = cw1.getDir("audioDir", Context.MODE_PRIVATE);
                    File ff = new File(directory1, messages.getAudio().hashCode() + ".mp3");
                    if (!ff.exists()) {
                        Utils.downloadAudio(messages.getAudio(), directory1.getAbsolutePath());
                    }

                    ContextWrapper cw = new ContextWrapper(mContext);
                    File directory = cw.getDir("audioDir", Context.MODE_PRIVATE);
                    String filename = String.valueOf(directory + "/" + messages.getAudio().hashCode()) + ".mp3";

                    if (!messages.isCallAudioDuration()) {
                        final MediaPlayer mediaPlayer1 = new MediaPlayer();

                        mediaPlayer1.setDataSource(mContext, Uri.parse(filename));

                        mediaPlayer1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                String v = String.format("%02d", mediaPlayer1.getDuration() / 60000);
                                int va = (int) ((mediaPlayer1.getDuration() % 60000) / 1000);
                                va = va +1;
                                messages.setAudioDuration(mediaPlayer1.getDuration());
                                ((ReceiverViewHolder) holder).mTvDuration.setText(v + ":" + String.format("%02d", va));
                                ((ReceiverViewHolder) holder).mVoiceProgressBar.setVisibility(View.GONE);
                            }
                        });
                        mediaPlayer1.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                            @Override
                            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                                ((ReceiverViewHolder) holder).mTvDuration.setText("00:00");
                                ((ReceiverViewHolder) holder).mVoiceProgressBar.setVisibility(View.GONE);
                                return false;
                            }
                        });
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    mediaPlayer1.prepare();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                        ((ReceiverViewHolder) holder).mVoiceProgressBar.setVisibility(View.VISIBLE);
                        messages.setCallAudioDuration(true);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    DialogAlert.show_alert_dialog(mContext,mContext.getResources().getString(R.string.memory_msg));
                }
            }
            ((ReceiverViewHolder) holder).seekBar.setClickable(false);

            if (position == mPlayingPosition) {
                if (mPlayer != null)
                    ((ReceiverViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());

            } else {

                ((ReceiverViewHolder) holder).seekBar.setProgress(0);
                ((ReceiverViewHolder) holder).ivPlay.setImageResource(R.drawable.ic_playvideo);
                ((ReceiverViewHolder) holder).chronometerTimer.setBase(SystemClock.elapsedRealtime());
                ((ReceiverViewHolder) holder).chronometerTimer.stop();

            }

            ((ReceiverViewHolder) holder).ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        ContextWrapper cw = new ContextWrapper(mContext);
                        File directory = cw.getDir("audioDir", Context.MODE_PRIVATE);
                        String filename = String.valueOf(directory + "/" + messages.getAudio().hashCode()) + ".mp3";

                        final String filepath = filename;

                        if (position == mPlayingPosition) {
                            if (mPlayer != null) {
                                if (mPlayer.isPlaying()) {
                                    ((ReceiverViewHolder) holder).mVoiceProgressBar.setVisibility(View.GONE);
                                    mPlayer.pause();
                                } else {
                                    if (isPreparedMedia)
                                        mPlayer.start();
                                }
                                startPlayer("");
                            }
                        } else {
                            if (mPlayingPosition != -1)
                                mLastPlayingPosition = mPlayingPosition;
                            mPlayingPosition = position;
                            mPlaylingPositionAudioDuration = messages.getAudioDuration();
                            if (mPlayer != null) {
                                mPlayer.release();
                                if (holderRowAudio != null) {

                                    pauseOrStopPlayer(holderRowAudio, false, mLastPlayingPosition);
                                }
                            }
                            if (holderRowAudio != null && holderRowAudio instanceof ReceiverViewHolder) {
                                ((ReceiverViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                            } else if (holderRowAudio != null && holderRowAudio instanceof SenderViewHolder) {
                                ((SenderViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                            }
                            holderRowAudio = ((ReceiverViewHolder) holder);
                            ((ReceiverViewHolder) holder).mVoiceProgressBar.setVisibility(View.VISIBLE);
                            startPlayer(filepath);
                        }

                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        DialogAlert.show_alert_dialog(mContext,mContext.getResources().getString(R.string.memory_msg));
                    }
                }
            });
            ((ReceiverViewHolder) holder).ivReceiverPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ActivityPictureFullView.class);
                    intent.putExtra("picture", messages.getPicture());
                    intent.putExtra("chat", "yes");
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((Activity) mContext,
                                    ((ReceiverViewHolder) holder).ivReceiverPic,
                                    ViewCompat.getTransitionName(((ReceiverViewHolder) holder).ivReceiverPic));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        mContext.startActivity(intent, options.toBundle());
                    } else {
                        mContext.startActivity(intent);
                    }

                }
            });



        }
    }

    @Override
    public int getItemCount() {
        return arrMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (arrMessages != null) {
            Messages object = arrMessages.get(position);
            if (object != null) {
                if (object.getSenderId().equals(Prefs.getString(MySharedPref.USER_ID, ""))) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
        return 0;
    }

    public boolean checkCallPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                ((Activity) mContext).requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                        101);
                return false;

            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public class ReceiverViewHolder extends RecyclerView.ViewHolder {
        public TextView tvReceiverMsg;
        public LinearLayout llReceiverText;
        public LinearLayout llReceiverVideo;
        public FrameLayout flReceiverVideo;
        public LinearLayout llReceiverPicture;
        public TextView tvMsgTime;
        public ImageView ivReceiverPic;
        public ImageView ivReceiverVideoThumb;
        public LinearLayout llReceiverAudio;
        public SeekBar seekBar;
        public ImageView ivPlay;
        public TextView mTvDuration;
        public ProgressBar mVoiceProgressBar;
        public Chronometer chronometerTimer;

        public ReceiverViewHolder(View view) {
            super(view);
            llReceiverText = (LinearLayout) view.findViewById(R.id.llReceiverText);
            llReceiverVideo = (LinearLayout) view.findViewById(R.id.llReceiverVideo);
            flReceiverVideo = (FrameLayout) view.findViewById(R.id.flReceiverVideo);
            llReceiverPicture = (LinearLayout) view.findViewById(R.id.llReceiverPicture);
            tvReceiverMsg = (TextView) view.findViewById(R.id.tvReceiverMsg);
            ivReceiverPic =  view.findViewById(R.id.ivReceiverPic);
            ivReceiverVideoThumb =  view.findViewById(R.id.ivReceiverVideoThumb);
            tvMsgTime = (TextView) view.findViewById(R.id.tvMsgTime);
            llReceiverAudio = (LinearLayout) view.findViewById(R.id.llReceiverAudio);
            seekBar = (SeekBar) view.findViewById(R.id.seekBar);
            mVoiceProgressBar = (ProgressBar) view.findViewById(R.id.voiceProgressBar);
            ivPlay = (ImageView) view.findViewById(R.id.ivPlay);
            chronometerTimer = (Chronometer) view.findViewById(R.id.chronometerTimer);
            mTvDuration = (TextView) view.findViewById(R.id.tvDuration);
        }
    }

    public class SenderViewHolder extends RecyclerView.ViewHolder {
        public FrameLayout flSenderVideo;
        public LinearLayout llSenderText;
        public LinearLayout llSenderVideo;
        public LinearLayout llSenderPicture;
        public TextView tvMsgTime;
        public TextView tvSenderMsg;
        public ImageView ivSenderPic;
        public ImageView ivSenderVideoThumb;
        public LinearLayout llSenderAudio;
        public SeekBar seekBar;
        public ImageView ivPlay;
        public ProgressBar mVoiceProgressBar;
        public Chronometer chronometerTimer;
        public TextView mTvDuration;

        private MediaPlayer mPlayer;
        private Handler mHandler = new Handler();
        private int lastProgress = 0;
        private boolean isPlaying = false;

        public SenderViewHolder(View view) {
            super(view);
            flSenderVideo = (FrameLayout) view.findViewById(R.id.flSenderVideo);
            llSenderText = (LinearLayout) view.findViewById(R.id.llSenderText);
            llSenderVideo = (LinearLayout) view.findViewById(R.id.llSenderVideo);
            llSenderPicture = (LinearLayout) view.findViewById(R.id.llSenderPicture);

            tvSenderMsg = (TextView) view.findViewById(R.id.tvSenderMsg);
            tvMsgTime = (TextView) view.findViewById(R.id.tvMsgTime);
            ivSenderPic =  view.findViewById(R.id.ivSenderPic);
            ivSenderVideoThumb =  view.findViewById(R.id.ivSenderVideoThumb);
            llSenderAudio = (LinearLayout) view.findViewById(R.id.llSenderAudio);
            seekBar = (SeekBar) view.findViewById(R.id.seekBar);
            mVoiceProgressBar = (ProgressBar) view.findViewById(R.id.voiceProgressBar);
            ivPlay = (ImageView) view.findViewById(R.id.ivPlay);
            chronometerTimer = (Chronometer) view.findViewById(R.id.chronometerTimer);
            mTvDuration = (TextView) view.findViewById(R.id.tvDuration);
        }
    }

    private void stopPlayback() {
        mPlayingPosition = NOT_PLAYING;
        mProgressUpdater.mBarToUpdate = null;
        mPlayer.stop();
        mProgressUpdater.chronometer.stop();
        mProgressUpdater.chronometer.setBase(SystemClock.elapsedRealtime());
    }


    private class PlaybackUpdater implements Runnable {
        public SeekBar mBarToUpdate = null;
        public Chronometer chronometer = null;

        @Override
        public void run() {
            if ((mPlayingPosition != NOT_PLAYING) && (null != mBarToUpdate)) {
                if (mPlayer != null) {
                    mBarToUpdate.setProgress((100 * mPlayer.getCurrentPosition() / mPlayer.getDuration()));    //Cast
                    chronometer.setBase(SystemClock.elapsedRealtime() - mPlayer.getCurrentPosition());
                }
                mHandler.postDelayed(this, 500);
            } else {
                //not playing so stop updating
            }
        }
    }

    /*AUDIO PLAY*/
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (mPlayingPosition == holder.getAdapterPosition()) {
            pauseOrStopPlayer(holderRowAudio, false);
            holderRowAudio = null;
        }
    }

    boolean isActivityDestroy = false;
    boolean isActivityPause = false;

    public void setActivityDestroy(boolean isActivityDestroy) {
        this.isActivityDestroy = isActivityDestroy;
        if (!isActivityDestroy) {
            pauseOrStopPlayer(holderRowAudio, false);
        } else {
            releasePlayer();
            holderRowAudio = null;
        }
    }

    public void setActivityPause(boolean isActivityPause) {
        this.isActivityPause = isActivityPause;
        if (!isActivityPause) {
            pauseOrStopPlayer(holderRowAudio, false);
        } else {
            releasePlayer();
            holderRowAudio = null;
        }
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (isActivityPause) {
            pauseOrStopPlayer(holderRowAudio, false);
        }
    }

    public void pauseOrStopPlayer(RecyclerView.ViewHolder holder, boolean isPause) {
        try {
            if (holder != null) {
                if (holder instanceof SenderViewHolder) {
                    if (!isPause) {
                        ((SenderViewHolder) holder).seekBar.removeCallbacks(seekBarRunnable);
                        if (holder.getAdapterPosition() == mPlayingPosition) {
                            if (mPlayer != null && !mPlayer.isPlaying() && isPreparedMedia) {
                                mPlayer.pause(); //remove
                                ((SenderViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                            } else if (mPlayer != null && mPlayer.isPlaying()) {
                                mPlayer.pause();
                                ((SenderViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                            } else {
                                ((SenderViewHolder) holder).seekBar.setProgress(0);


                                String v = String.format("%02d", mPlaylingPositionAudioDuration / 60000);
                                int va = (int) ((mPlaylingPositionAudioDuration % 60000) / 1000);
                                va = va+1;
                                ((SenderViewHolder) holderRowAudio).mTvDuration.setText(v + ":" + String.format("%02d", va));
                            }
                        } else {
                            ((SenderViewHolder) holder).seekBar.setProgress(0);


                            String v = String.format("%02d", mPlaylingPositionAudioDuration / 60000);
                            int va = (int) ((mPlaylingPositionAudioDuration % 60000) / 1000);
                            va = va+1;
                            ((SenderViewHolder) holderRowAudio).mTvDuration.setText(v + ":" + String.format("%02d", va));
                        }
                    } else {
                        if (mPlayer != null) {
                            if (mPlayer.isPlaying()) {
                                mPlayer.pause();
                            } else {

                            }
                        }
                    }
                    ((SenderViewHolder) holder).ivPlay.setImageResource(R.drawable.ic_play);
                } else {
                    if (!isPause) {
                        ((ReceiverViewHolder) holder).seekBar.removeCallbacks(seekBarRunnable);
                        if (holder.getAdapterPosition() == mPlayingPosition) {
                            if (mPlayer != null && !mPlayer.isPlaying() && isPreparedMedia) {
                                ((ReceiverViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                            } else if (mPlayer != null && mPlayer.isPlaying()) {
                                mPlayer.pause();
                                ((SenderViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                            } else {
                                ((ReceiverViewHolder) holder).seekBar.setProgress(0);

                                String v = String.format("%02d", mPlaylingPositionAudioDuration / 60000);
                                int va = (int) ((mPlaylingPositionAudioDuration % 60000) / 1000);
                                va = va+1;
                                ((ReceiverViewHolder) holder).mTvDuration.setText(v + ":" + String.format("%02d", va));
                            }
                        } else {
                            ((ReceiverViewHolder) holder).seekBar.setProgress(0);

                            String v = String.format("%02d", mPlaylingPositionAudioDuration / 60000);
                            int va = (int) ((mPlaylingPositionAudioDuration % 60000) / 1000);
                            va = va+1;
                            ((ReceiverViewHolder) holder).mTvDuration.setText(v + ":" + String.format("%02d", va));
                        }
                    } else {
                        if (mPlayer != null) {
                            if (mPlayer.isPlaying()) {
                                mPlayer.pause();
                            } else {

                            }
                        }
                    }
                    ((ReceiverViewHolder) holder).ivPlay.setImageResource(R.drawable.ic_play);
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void pauseOrStopPlayer(RecyclerView.ViewHolder holder, boolean isPause, int position) {
        try {
            if (holder != null) {
                if (holder instanceof SenderViewHolder) {
                    if (!isPause) {
                        ((SenderViewHolder) holder).seekBar.removeCallbacks(seekBarRunnable);
                        if (holder.getAdapterPosition() == mPlayingPosition) {
                            if (mPlayer != null && !mPlayer.isPlaying() && isPreparedMedia) {
                                mPlayer.pause(); //remove
                                ((SenderViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                            } else if (mPlayer != null && mPlayer.isPlaying()) {
                                mPlayer.pause();
                                ((SenderViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                            } else {
                                ((SenderViewHolder) holder).seekBar.setProgress(0);
                                if (position != -1) {
                                    String v = String.format("%02d", arrMessages.get(position).getAudioDuration() / 60000);
                                    int va = (int) ((arrMessages.get(position).getAudioDuration() % 60000) / 1000);
                                    ((SenderViewHolder) holderRowAudio).mTvDuration.setText(v + ":" + String.format("%02d", va));
                                } else
                                    ((SenderViewHolder) holderRowAudio).mTvDuration.setText("00:00");
                            }
                        } else {
                            ((SenderViewHolder) holder).seekBar.setProgress(0);
                            if (position != -1) {
                                String v = String.format("%02d", arrMessages.get(position).getAudioDuration() / 60000);
                                int va = (int) ((arrMessages.get(position).getAudioDuration() % 60000) / 1000);
                                ((SenderViewHolder) holderRowAudio).mTvDuration.setText(v + ":" + String.format("%02d", va));
                            } else
                                ((SenderViewHolder) holderRowAudio).mTvDuration.setText("00:00");
                        }
                    } else {
                        if (mPlayer != null) {
                            if (mPlayer.isPlaying()) {
                                mPlayer.pause();
                            } else {

                            }
                        }
                    }
                    ((SenderViewHolder) holder).ivPlay.setImageResource(R.drawable.ic_play);
                } else {
                    if (!isPause) {
                        ((ReceiverViewHolder) holder).seekBar.removeCallbacks(seekBarRunnable);
                        if (holder.getAdapterPosition() == mPlayingPosition) {
                            if (mPlayer != null && !mPlayer.isPlaying() && isPreparedMedia) {
                                ((ReceiverViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                            } else if (mPlayer != null && mPlayer.isPlaying()) {
                                mPlayer.pause();
                                ((ReceiverViewHolder) holder).seekBar.setProgress(mPlayer.getCurrentPosition());
                            } else {
                                if(position != -1) {
                                    ((ReceiverViewHolder) holder).seekBar.setProgress(0);
                                    String v = String.format("%02d", arrMessages.get(position).getAudioDuration() / 60000);
                                    int va = (int) ((arrMessages.get(position).getAudioDuration() % 60000) / 1000);
                                    ((ReceiverViewHolder) holderRowAudio).mTvDuration.setText(v + ":" + String.format("%02d", va));
                                }else
                                    ((ReceiverViewHolder) holderRowAudio).mTvDuration.setText("00:00");
                            }
                        } else {
                            if(position != -1) {
                                ((ReceiverViewHolder) holder).seekBar.setProgress(0);
                                String v = String.format("%02d", arrMessages.get(position).getAudioDuration() / 60000);
                                int va = (int) ((arrMessages.get(position).getAudioDuration() % 60000) / 1000);
                                ((ReceiverViewHolder) holderRowAudio).mTvDuration.setText(v + ":" + String.format("%02d", va));
                            }else
                                ((ReceiverViewHolder) holderRowAudio).mTvDuration.setText("00:00");
                        }
                    } else {
                        if (mPlayer != null) {
                            if (mPlayer.isPlaying()) {
                                mPlayer.pause();
                            } else {

                            }
                        }
                    }
                    ((ReceiverViewHolder) holder).ivPlay.setImageResource(R.drawable.ic_play);
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    AsyncTask playAudioAysctask;

    private void startPlayer(final String path) {
        if (!Utils.isStringNull(path)) {

            if (playAudioAysctask != null) {
                playAudioAysctask.cancel(true);
            }
            if (!isPreparedMedia) {
                if (mPlayer != null) {
                    releasePlayer();
                }
            }
            playAudioAysctask = new AsyncTask<Boolean, Boolean, Boolean>() {
                protected void onPreExecute() {

                }

                protected Boolean doInBackground(Boolean... params) {
                    try {
                        isPreparedMedia = false;
                        mPlayer = new MediaPlayer();
                        mPlayer.setDataSource(mContext, Uri.parse(path));
                        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                isPreparedMedia = true;
                                if (holderRowAudio instanceof SenderViewHolder) {
                                    System.out.println("Load progress redListene");
                                    ((SenderViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                                } else {
                                    ((ReceiverViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                                }
                            }
                        });
                        mPlayer.prepare();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return true;
                }

                protected void onPostExecute(Boolean result) {
                    mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            releasePlayer();
                        }
                    });

                    mPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {

                        @Override
                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
                            switch (what) {
                                case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                                    if (holderRowAudio instanceof SenderViewHolder) {
                                        System.out.println("Load progress MEDIA_INFO_BUFFERING_START");
                                        ((SenderViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.VISIBLE);
                                    } else {
                                        ((ReceiverViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.VISIBLE);
                                    }
                                    break;
                                case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                                    if (holderRowAudio instanceof SenderViewHolder) {
                                        System.out.println("Load progress MEDIA_INFO_BUFFERING_END");
                                        ((SenderViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                                    } else {
                                        ((ReceiverViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                                    }
                                    break;
                            }
                            return false;
                        }
                    });

                    mPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                        public void onBufferingUpdate(MediaPlayer mp, int percent) {
                            double ratio = percent / 100.0;
                            int bufferingLevel = (int) (mp.getDuration() * ratio);

                            if (holderRowAudio instanceof SenderViewHolder) {
                                ((SenderViewHolder) holderRowAudio).seekBar.setSecondaryProgress(bufferingLevel);
                            } else {
                                if (holderRowAudio instanceof ReceiverViewHolder) {
                                    ((ReceiverViewHolder) holderRowAudio).seekBar.setSecondaryProgress(bufferingLevel);
                                }
                            }
                        }

                    });
                    mPlayer.start();
                    handleViewAndControll();
                }
            }.execute();

        } else {
            if (isPreparedMedia) {
                if (mPlayer != null) {
                    handleViewAndControll();
                }
            }
        }
    }

    private void handleViewAndControll() {

        if (holderRowAudio instanceof SenderViewHolder) {
            ((SenderViewHolder) holderRowAudio).seekBar.setMax(mPlayer.getDuration());
            ((SenderViewHolder) holderRowAudio).seekBar.setProgress(mPlayer.getCurrentPosition());
            ((SenderViewHolder) holderRowAudio).seekBar.setEnabled(true);
            if (mPlayer.isPlaying()) {
                ((SenderViewHolder) holderRowAudio).seekBar.postDelayed(seekBarRunnable, 100);
                ((SenderViewHolder) holderRowAudio).ivPlay.setImageResource(R.drawable.ic_pausevideo);
            } else {
                ((SenderViewHolder) holderRowAudio).seekBar.removeCallbacks(seekBarRunnable);
                ((SenderViewHolder) holderRowAudio).ivPlay.setImageResource(R.drawable.ic_playvideo);
            }
            ((SenderViewHolder) holderRowAudio).seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        mPlayer.seekTo(progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

        } else {
            long tempSec = mPlayer.getDuration();

            ((ReceiverViewHolder) holderRowAudio).seekBar.setMax(mPlayer.getDuration());
            ((ReceiverViewHolder) holderRowAudio).seekBar.setProgress(mPlayer.getCurrentPosition());
            ((ReceiverViewHolder) holderRowAudio).seekBar.setEnabled(true);
            if (mPlayer.isPlaying()) {
                ((ReceiverViewHolder) holderRowAudio).seekBar.postDelayed(seekBarRunnable, 100);
                ((ReceiverViewHolder) holderRowAudio).ivPlay.setImageResource(R.drawable.ic_pausevideo);
            } else {
                ((ReceiverViewHolder) holderRowAudio).seekBar.removeCallbacks(seekBarRunnable);
                ((ReceiverViewHolder) holderRowAudio).ivPlay.setImageResource(R.drawable.ic_playvideo);
            }

            ((ReceiverViewHolder) holderRowAudio).seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        mPlayer.seekTo(progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

        }
    }

    public void releasePlayer() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
            isPreparedMedia = false;
            mLastPlayingPosition = mPlayingPosition;
            mPlayingPosition = NOT_PLAYING;

            if (holderRowAudio != null) {
                if (holderRowAudio instanceof SenderViewHolder) {
                    ((SenderViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                } else {
                    ((ReceiverViewHolder) holderRowAudio).mVoiceProgressBar.setVisibility(View.GONE);
                }
                if (holderRowAudio instanceof SenderViewHolder) {

                    ((SenderViewHolder) holderRowAudio).mTvDuration.setText("00:00");
                } else {

                    ((ReceiverViewHolder) holderRowAudio).mTvDuration.setText("00:00");
                }
                pauseOrStopPlayer(holderRowAudio, false);

            }
        }
    }

    private class SeekBarRunnable implements Runnable {
        @Override
        public void run() {
            try {
                if (holderRowAudio != null) {
                    if (holderRowAudio instanceof SenderViewHolder) {
                        ((SenderViewHolder) holderRowAudio).seekBar.setProgress(mPlayer.getCurrentPosition());
                        ((SenderViewHolder) holderRowAudio).seekBar.postDelayed(this, 100);

                        String v = String.format("%02d", mPlayer.getCurrentPosition() / 60000);

                        double temp = mPlayer.getCurrentPosition() % 60000;
                        double tempva =  temp / 1000;
                        double tempCeil = Math.ceil(tempva);

                        ((SenderViewHolder) holderRowAudio).mTvDuration.setText(v + ":" + String.format("%02d",(int) tempCeil));
                    } else {
                        ((ReceiverViewHolder) holderRowAudio).seekBar.setProgress(mPlayer.getCurrentPosition());
                        ((ReceiverViewHolder) holderRowAudio).seekBar.postDelayed(this, 100);
                        long tempCurre = mPlayer.getCurrentPosition();
                        String v = String.format("%02d", mPlayer.getCurrentPosition() / 60000);

                        double temp = mPlayer.getCurrentPosition() % 60000;
                        double tempva =  temp / 1000;
                        double tempCeil = Math.ceil(tempva);

                        float va =  ((mPlayer.getCurrentPosition() % 60000) / 1000);
                        ((ReceiverViewHolder) holderRowAudio).mTvDuration.setText(v + ":" + String.format("%02d",(int) tempCeil));
                    }
                }
            }catch (IllegalStateException e){
                e.printStackTrace();
            }
        }
    }
}
