package com.example.talentogram.chat.helper;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;

import com.example.talentogram.R;

public class DialogAlert {

    public static void show_dialog(final Context context, final String message) {
        show_dialog_title(context, message, null);
    }

    public static void show_dialog_title(final Context context, final String message, final String title) {

        final SpannableString spButton = new SpannableString(context.getString(android.R.string.ok));
        spButton.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spButton.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        final SpannableString spMessage = new SpannableString(message);
        spMessage.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spMessage.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alerDialog = new AlertDialog.Builder(context, R.style.MyDialogTheme).setMessage(spMessage)
                        .setNegativeButton(spButton, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if (!TextUtils.isEmpty(title)) {
                    SpannableString spTitle = new SpannableString(title);
                    spTitle.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spTitle.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    alerDialog.setTitle(spTitle);
                }


                AlertDialog alert = alerDialog.create();
                alert.show();

                TextView textView = alert.findViewById(android.R.id.message);
                textView.setTextSize(16);
                Button btn1 = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                btn1.setTextSize(16);
            }
        });
    }
    public static void show_dialog_responce(final Context context, final String message, final OnItemClick onItemClick) {
        final SpannableString spMessage = new SpannableString(message);
        spMessage.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spMessage.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        final SpannableString spButton = new SpannableString(context.getString(android.R.string.ok));
        final SpannableString spButton1 = new SpannableString(context.getString(android.R.string.cancel));
        spButton1.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spButton1.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spButton.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spButton.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alerDialog = new AlertDialog.Builder(context).setMessage(spMessage)
                        .setPositiveButton(spButton, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onItemClick.OnItemClickListner(1);
                            }
                        })
                        .setNegativeButton(spButton1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = alerDialog.create();
                alert.show();

                TextView textView = alert.findViewById(android.R.id.message);
                textView.setTextSize(16);

                Button btn1 = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                btn1.setTextSize(16);

                Button btn2 = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                btn2.setTextSize(16);
            }
        });
    }
    public static void show_dialog_ok(final Context context, final String message, final OnItemClick onItemClick) {
        try {
            final SpannableString spMessage = new SpannableString(message);
            spMessage.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spMessage.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final SpannableString spButton = new SpannableString(context.getString(android.R.string.ok));
            spButton.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spButton.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder alerDialog = new AlertDialog.Builder(context, R.style.MyDialogTheme).setMessage(spMessage)
                            .setPositiveButton(spButton, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onItemClick.OnItemClickListner(1);
                                }
                            });
                    AlertDialog alert = alerDialog.create();
                    alert.show();

                    TextView textView = alert.findViewById(android.R.id.message);
                    textView.setTextSize(16);
                    Button btn1 = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    btn1.setTextSize(16);
                }
            });
        } catch (NullPointerException e) {

        }
    }

    public static void show_dialog_dismiss_ok(final Context context, final String message, final OnItemClick onItemClick) {
        final SpannableString spMessage = new SpannableString(message);
        spMessage.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spMessage.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        final SpannableString spButton = new SpannableString(context.getString(android.R.string.ok));
        spButton.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spButton.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alerDialog = new AlertDialog.Builder(context, R.style.MyDialogTheme).setMessage(spMessage)
                        .setCancelable(false)
                        .setPositiveButton(spButton, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onItemClick.OnItemClickListner(1);
                            }
                        });
                AlertDialog alert = alerDialog.create();
                alert.show();

                TextView textView = alert.findViewById(android.R.id.message);
                textView.setTextSize(16);
                Button btn1 = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                btn1.setTextSize(16);
            }
        });
    }

    public static void show_alert_dialog(final Context context, final String message) {
        show_dialog_impl(context, message, false);
    }

    private static void show_dialog_impl(final Context context, final String message, final boolean login) {
        final SpannableString spMessage = new SpannableString(message);
        spMessage.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spMessage.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        final SpannableString spButton = new SpannableString(context.getString(android.R.string.ok));
        spButton.setSpan(new TypefaceSpan(context, "montserrat_medium.ttf"), 0, spButton.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alerDialog = new AlertDialog.Builder(context, R.style.MyDialogTheme).setMessage(spMessage)

                        .setNegativeButton(spButton, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                if (login) {
                                    ((Activity) context).onBackPressed();
                                }
                            }
                        });
                AlertDialog alert = alerDialog.create();
                alert.show();

                TextView textView = alert.findViewById(android.R.id.message);
                textView.setTextSize(16);
                Button btn1 = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                btn1.setTextSize(16);
            }
        });
    }
}
