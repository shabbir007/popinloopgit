package com.example.talentogram.chat.activity;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.example.talentogram.R;
import com.example.talentogram.chat.helper.DialogAlert;
import com.example.talentogram.others.TouchImageView;
import com.example.talentogram.others.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ActivityPictureFullView extends AppCompatActivity {

    private ImageView mIvBtnClose;
    private TouchImageView mIvPicture;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_full_view);

        init();
        if (getIntent().hasExtra("picture")) {
            String picture = getIntent().getStringExtra("picture");
            if (!TextUtils.isEmpty(picture)) {
                if (getIntent().hasExtra("localImage")) {
                    Picasso.with(ActivityPictureFullView.this).load(new File(picture))
                            .resize(1600,1600).placeholder(R.drawable.noimage).into(mIvPicture);
                } else if(getIntent().hasExtra("chat")){

                    Bitmap bitmap = Utils.getBitmapFromURL(picture);

                    try {
                        Long tsLong = System.currentTimeMillis() / 1000;
                        String ts = tsLong.toString();
                        ContextWrapper cw = new ContextWrapper(ActivityPictureFullView.this);

                        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                        File f=new File(directory, picture.hashCode()+".jpg");

                        RequestOptions options = new RequestOptions()
                                .placeholder(R.drawable.noimage)
                                .error(R.drawable.noimage);

                        Glide.with(ActivityPictureFullView.this).load("file://"+f.getPath()).apply(options).into(mIvPicture);

                    }
                    catch (OutOfMemoryError e){
                        e.printStackTrace();
                        DialogAlert.show_alert_dialog(ActivityPictureFullView.this,getResources().getString(R.string.memory_msg));
                    }
                } else{
                    Picasso.with(ActivityPictureFullView.this).load(picture).placeholder(R.drawable.noimage).into(mIvPicture);

                }
            }
        }
        mIvBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void init() {
        mIvBtnClose = (ImageView) findViewById(R.id.ivBtnClose);
        mIvPicture = (TouchImageView) findViewById(R.id.ivPicture);

    }
}
