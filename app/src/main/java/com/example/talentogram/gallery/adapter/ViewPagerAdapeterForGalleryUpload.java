package com.example.talentogram.gallery.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.talentogram.gallery.fragment.ImagesFragment;
import com.example.talentogram.gallery.fragment.VideoFragment;

public class ViewPagerAdapeterForGalleryUpload extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public ViewPagerAdapeterForGalleryUpload(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                VideoFragment videoFragment = new VideoFragment();
                return videoFragment;
            case 1:
                ImagesFragment imagesFragment = new ImagesFragment();
                return imagesFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
