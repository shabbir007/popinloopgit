package com.example.talentogram.gallery.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.talentogram.gallery.fragment.AudioFragment;
import com.example.talentogram.gallery.fragment.ImagesFragment;
import com.example.talentogram.gallery.fragment.VideoFragment;
import com.example.talentogram.gallery.fragment.WritingFragment;

public class GalleryViewPagerAdapeter extends FragmentPagerAdapter {
    private Context myContext;
    int totalTabs;

    public GalleryViewPagerAdapeter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                AudioFragment audioFragment = new AudioFragment();
                return audioFragment;
            case 1:
                VideoFragment videoFragment = new VideoFragment();
                return videoFragment;
            case 2:
                ImagesFragment imagesFragment = new ImagesFragment();
                return imagesFragment;
            case 3:
                WritingFragment writingFragment = new WritingFragment();
                return writingFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
