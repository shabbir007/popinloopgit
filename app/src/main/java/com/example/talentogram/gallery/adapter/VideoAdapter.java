package com.example.talentogram.gallery.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.Video_recording.activity.VideoViewActivity;
import com.example.talentogram.model.VideoModel;

import java.util.ArrayList;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private ArrayList<VideoModel> al_video;
    private Context context;
    private Activity activity;

    public VideoAdapter(Context context, ArrayList<VideoModel> al_video, Activity activity) {
        this.al_video = al_video;
        this.context = context;
        this.activity = activity;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_image;
        public ViewHolder(View v) {
            super(v);
            iv_image = v.findViewById(R.id.iv_image);
        }
    }

    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Glide.with(context).load(al_video.get(position).getStr_thumb())
                .skipMemoryCache(false)
                .into(holder.iv_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_gallery = new Intent(context, VideoViewActivity.class);
                intent_gallery.putExtra("video", al_video.get(position).getStr_path());
                activity.startActivity(intent_gallery);
            }
        });
    }
    @Override
    public int getItemCount() {
        return al_video.size();
    }

}

