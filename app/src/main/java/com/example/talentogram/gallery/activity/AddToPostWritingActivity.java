package com.example.talentogram.gallery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.talentogram.R;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.custom_views.CustomEditText;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddToPostWritingActivity extends AppCompatActivity {
    private Dialog dialog;
    private int postTypeId = 0;

    @BindView(R.id.etPostDescription)
    CustomEditText etDescription;

    @BindView(R.id.llChoice)
    LinearLayout llChoice;

    @BindView(R.id.tvPostType)
    CustomTextView tvPostType;

    @BindView(R.id.swSwitch)
    SwitchCompat swSwitch;

    @BindView(R.id.btnPost)
    Button btnPost;

    @BindView(R.id.chAllowDownload)
    CheckBox chAllowDownload;

    Bitmap bmp,converetdImage;
    int IsChecked=1;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvWriting)
    TextView tvWriting;

    @BindView(R.id.llWritingView)
    LinearLayout llWritingView;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    ProgressDialog progressDialog;
    public String color,font,description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_post_writing);
        ButterKnife.bind(this);
        init();
        tvTitle.setText("Go to gallery");

        color = getIntent().getStringExtra("color");
        font = getIntent().getStringExtra("font");
        description = getIntent().getStringExtra("description");

        String color_ = "#" + color;
        llWritingView.setBackgroundColor(Color.parseColor(color_));
        Typeface font_t = Typeface.createFromAsset(getAssets(), "fonts/" + font);
        tvWriting.setTypeface(font_t);
        tvWriting.setText(description);

    }

    private void init() {
        llBack.setOnClickListener(view ->onBackPressed());

        chAllowDownload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsChecked = 1;
                } else {
                    IsChecked = 0;
                }
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPost();
            }
        });
        swSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Bitmap content_ = loadBitmapFromView(llWritingView);
                    SaveWritingContent(content_);
                } else {
                    // The toggle is disabled
                }
            }
        });

        llChoice.setOnClickListener(view -> {
            showMenuPopup(view);
        });
    }

    private void SaveWritingContent(Bitmap finalBitmap) {
        File myDir = new File(MyConstants.app_folder);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String fname = Utils.getRandomString() + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

            ContextThemeWrapper ctw = new ContextThemeWrapper(AddToPostWritingActivity.this, R.style.AppTheme);
            final androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ctw);
            alertDialogBuilder.setTitle("File ");
            alertDialogBuilder.setMessage("File Saved Successfully");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });
            alertDialogBuilder.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    private void addPost() {
        String Description = etDescription.getText().toString().trim();
        String PostType = tvPostType.getText().toString().trim();

        switch (PostType) {
            case "Public":
                postTypeId = 0;
                break;
            case "Private":
                postTypeId = 1;
                break;
            case "Followers":
                postTypeId = 2;
                break;
        }

        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("Title", Description);
        userdata.put("PostType", String.valueOf(3));
        userdata.put("PostHashTagIds", String.valueOf(173));
        userdata.put("NewHashtags", "#quotes");
        userdata.put("Font", font);
        userdata.put("Color", color);
        userdata.put("PostVisibleType", String.valueOf(postTypeId));
        userdata.put("Description", description);
        userdata.put("AdminSongId", String.valueOf(1));
        userdata.put("isDownload", String.valueOf(IsChecked));

        new NetworkCall(AddToPostWritingActivity.this, Services.ADDPOST,
                userdata, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            new AlertDialog.Builder(AddToPostWritingActivity.this)
                                    .setMessage(message)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            startActivity(new Intent(AddToPostWritingActivity.this, DashBoardActivity.class));
                                            finish();
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        } else {
                            Utils.showWarn(AddToPostWritingActivity.this,message);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showMenuPopup(View v) {
        int gravity = Gravity.CENTER;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.dialog_who_view, null);
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });
        popupView.findViewById(R.id.tvPrivate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPostType.setText("Public");
                popupWindow.dismiss();
            }
        });
        popupView.findViewById(R.id.tvfollower).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPostType.setText("Followers");
                popupWindow.dismiss();
            }
        });
        popupView.findViewById(R.id.tvPublic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPostType.setText("Private");
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(v, gravity, 0, 0);
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {
        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddToPostWritingActivity.this);
            progressDialog.setMessage("Downloading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (bmp != null) {
                    progressDialog.dismiss();
                    ContextThemeWrapper ctw = new ContextThemeWrapper(AddToPostWritingActivity.this, R.style.AppTheme);
                    final androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ctw);
                    alertDialogBuilder.setTitle("File ");
                    alertDialogBuilder.setMessage("File Downloaded Successfully");
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
                    alertDialogBuilder.show();
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                        }
                    }, 500);

                    Log.e(MyConstants.TAG, "Download Failed");

                }
            } catch (Exception e) {
                e.printStackTrace();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 500);
                Log.e(MyConstants.TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }
            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "Talentogram/");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }
                String time = String.valueOf(System.currentTimeMillis());
                File newFile = new File(folder.getAbsolutePath() + File.separator + time + ".mp4");
                try {
                    copyFile(new File(bmp+".png"), newFile);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(MyConstants.TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    } public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

}