package com.example.talentogram.gallery.fragment;


import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.gallery.adapter.AudioAdapter;
import com.example.talentogram.helper.CustomTouchListener;
import com.example.talentogram.helper.MediaPlayerService;
import com.example.talentogram.helper.StorageUtil;
import com.example.talentogram.helper.onItemClickListener;
import com.example.talentogram.model.AudioModel;

import java.util.ArrayList;

import static com.example.talentogram.others.MyConstants.Broadcast_PLAY_NEW_AUDIO;

/**
 * A simple {@link Fragment} subclass.
 */
public class AudioFragment extends Fragment {

    private MediaPlayerService player;
    boolean serviceBound = false;
    private ArrayList<AudioModel> audioModelList;
    private RecyclerView recyclerView;

    public AudioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_audio_fragment3, container, false);
        recyclerView =view.findViewById(R.id.recyclerview);

        loadAudioList();

        return  view;
    }

    private void loadAudioList() {
        loadAudio();
        initRecyclerView();
    }

    private void loadAudio() {
        ContentResolver contentResolver = getActivity().getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";

        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            audioModelList = new ArrayList<>();
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                // Save to audioModelList
                audioModelList.add(new AudioModel(data, title, album, artist,duration));
            }
        }
        if (cursor != null)
            cursor.close();
    }

    private void initRecyclerView() {
        if (audioModelList != null && audioModelList.size() > 0) {
            AudioAdapter audioAdapter = new AudioAdapter(audioModelList, getActivity());
            recyclerView.setAdapter(audioAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.addOnItemTouchListener(new CustomTouchListener(getActivity(), new onItemClickListener() {
                @Override
                public void onClick(View view, int index) {
                   // playAudio(index);
                }
            }));
        }
    }

    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    private void playAudio(int audioIndex) {
        //Check is service is active
        if (!serviceBound) {
            //Store Serializable audioModelList to SharedPreferences
            StorageUtil storage = new StorageUtil(getActivity());
            storage.storeAudio(audioModelList);
            storage.storeAudioIndex(audioIndex);

            getActivity().startService(new Intent(getActivity(),MediaPlayerService.class));
            getActivity().bindService(new Intent(getActivity(),MediaPlayerService.class),
                    serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            //Store the new audioIndex to SharedPreferences
            StorageUtil storage = new StorageUtil(getActivity());
            storage.storeAudioIndex(audioIndex);

            //Service is active
            //Send a broadcast to the service -> PLAY_NEW_AUDIO
            Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
            getActivity().sendBroadcast(broadcastIntent);
        }
    }

    /**
     * Load audio files using {@link ContentResolver}
     *
     * If this don't works for you, load the audio files to audioModelList Array your oun way
     */

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (serviceBound) {
            getActivity().unbindService(serviceConnection);
            player.stopSelf();
        }
    }

}
