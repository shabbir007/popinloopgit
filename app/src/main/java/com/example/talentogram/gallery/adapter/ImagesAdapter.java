package com.example.talentogram.gallery.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import androidx.annotation.RequiresApi;
import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.gallery.activity.ImageFilterActivity;

import java.util.ArrayList;


public class ImagesAdapter extends BaseAdapter {
    private Activity context;
    private ArrayList<String> imageList;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public ImagesAdapter(Activity localContext,  ArrayList<String> imageList) {
        context = localContext;
        this.imageList = imageList;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        @SuppressLint("ViewHolder")
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_images, parent, false);
        ImageView picturesView;
        picturesView = itemView.findViewById(R.id.ivImage);
        picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);

        Glide.with(context).load(imageList.get(position))
                .placeholder(R.drawable.ic_gallery_img).centerCrop()
                .into(picturesView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_gallery = new Intent(context, ImageFilterActivity.class);
                intent_gallery.putExtra("image", imageList.get(position));
                context.startActivity(intent_gallery);
            }
        });

        return itemView;
    }


}
