package com.example.talentogram.gallery.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.activity.AddAudioActivity;
import com.example.talentogram.contest.AudioDialogActivity;
import com.example.talentogram.helper.MediaPlayerService;
import com.example.talentogram.model.AudioModel;

import java.util.Collections;
import java.util.List;

import static com.example.talentogram.others.Utils.timeConversion;

public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.MyViewHolder> {
    public List<AudioModel> audioModelList = Collections.emptyList();
    public Context context;
    private MediaPlayerService player;

    public AudioAdapter(List<AudioModel> list, Context context) {
        this.audioModelList = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audio, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        AudioModel aModel = audioModelList.get(position);
        holder.tvTitle.setText(aModel.getTitle());
        holder.tvDuration.setText(timeConversion(Long.parseLong(aModel.getDuration())));

        if (aModel.isPlaying) {
            holder.ivPlay.setVisibility(View.GONE);
            holder.ivPause.setVisibility(View.VISIBLE);

        } else {
            holder.ivPlay.setVisibility(View.VISIBLE);
            holder.ivPause.setVisibility(View.GONE);
        }
        holder.btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aModel.isPlaying) {
                    player.pauseMedia();
                    Intent intent_audio = new Intent(context, AddAudioActivity.class);
                    intent_audio.putExtra("audio", aModel.getData());
                    context.startActivity(intent_audio);
                } else {
                    Intent intent_audio = new Intent(context, AddAudioActivity.class);
                    intent_audio.putExtra("audio", aModel.getData());
                    context.startActivity(intent_audio);;
                }

            }
        });

        holder.ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = aModel.getData();
                String title = aModel.getTitle();
                Intent i = new Intent(context, AudioDialogActivity.class);
                i.putExtra("AudioUrl", url);
                i.putExtra("title", title);
                context.startActivity(i);
                notifyDataSetChanged();
            }
        });
        holder.ivPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (aModel.isPlaying) {
                    holder.ivPlay.setVisibility(View.VISIBLE);
                    holder.ivPause.setVisibility(View.GONE);

                } else {
                    holder.ivPlay.setVisibility(View.GONE);
                    holder.ivPause.setVisibility(View.VISIBLE);
                }
                player.pauseMedia();

            }
        });
    }

    @Override
    public int getItemCount() {
        return audioModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvDuration;
        public ImageView ivPlay, ivMusic,ivPause;
        public Button btnPost;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.title);
            ivPlay = (ImageView) itemView.findViewById(R.id.ivPlay);
            tvDuration = (TextView) itemView.findViewById(R.id.duration);
            ivPause = (ImageView) itemView.findViewById(R.id.ivPause);
            btnPost =  itemView.findViewById(R.id.btnPost);

        }
    }
}
