package com.example.talentogram.gallery.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.example.talentogram.R;
import com.example.talentogram.contest.activity.SaveContestWritingAvtivity;
import com.example.talentogram.gallery.activity.AddToPostWritingActivity;
import com.example.talentogram.others.Utils;

import java.io.ByteArrayOutputStream;

/**
 * A simple {@link Fragment} subclass.
 */
public class WritingFragment extends Fragment {

    private int color_index = 0, font_index = 0, bg_img_index = 0;
    private String color = "F4A460";
    private String font = "font_1.ttf";
    private RelativeLayout relative_layout_upload_status;
    private ImageView ivPickColor, ivfont, ivImage;
    private Button btnNext;
    private EditText etDescription;
    boolean background = true;

    public WritingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.writingfragment, container, false);

        inIt(view);
        bindView();
        relative_layout_upload_status.setDrawingCacheEnabled(true);
        relative_layout_upload_status.setBackgroundResource(R.drawable.d_bg_writing);
        GradientDrawable drawable = (GradientDrawable) relative_layout_upload_status.getBackground();
        drawable.setColor(Color.parseColor("#" + color));
        return  view;
    }
    private void inIt(View view) {
        relative_layout_upload_status = view.findViewById(R.id.relative_layout_upload_status);
        ivPickColor = view.findViewById(R.id.ivPickColor);
        btnNext = view.findViewById(R.id.btnNext);
        etDescription = view.findViewById(R.id.etDescription);
        ivfont = view.findViewById(R.id.ivfont);
        ivImage = view.findViewById(R.id.ivImage);
    }

    private void bindView() {
        ivPickColor.setOnClickListener(v -> {
            background = true;
            nextColor();
        });
        ivfont.setOnClickListener(v -> nextFont());

        btnNext.setOnClickListener(v -> {
            String content = etDescription.getText().toString().trim();
            if (content.isEmpty()) {
                Utils.showWarn(getContext(), getString(R.string.error_writing_), 3);
            } else {
                Bitmap content_ = loadBitmapFromView(relative_layout_upload_status);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                content_.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                Intent i = new Intent(getContext(), AddToPostWritingActivity.class);
                i.putExtra("color", color);
                i.putExtra("font", font);
                i.putExtra("description", etDescription.getText().toString());
                startActivity(i);
            }
        });

    }

    public void nextFont() {
        this.font_index++;
        if (font_index == 11) {
            this.font_index = 1;
        }
        switch (font_index) {
            case 1:
                font = "font_1.ttf";
                break;
            case 2:
                font = "font_2.ttf";
                break;
            case 3:
                font = "font_3.ttf";
                break;
            case 4:
                font = "font_4.ttf";
                break;
            case 5:
                font = "font_5.ttf";
                break;
            case 6:
                font = "font_6.ttf";
                break;
            case 7:
                font = "font_7.ttf";
                break;
            case 8:
                font = "font_8.ttf";
                break;
            case 9:
                font = "font_9.ttf";
                break;
            case 10:
                font = "font_10.ttf";
                break;
        }
        etDescription.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/" + font), Typeface.BOLD);
    }

    public void nextColor() {
        this.color_index++;
        if (color_index == 21) {
            this.color_index = 1;
        }
        switch (color_index) {
            case 1:
                color = "C51062";
                break;
            case 2:
                color = "034182";
                break;
            case 3:
                color = "1a2634";
                break;
            case 4:
                color = "288fb4";
                break;
            case 5:
                color = "2f3c4e";
                break;
            case 6:
                color = "3d065a";
                break;
            case 7:
                color = "449187";
                break;
            case 8:
                color = "61519f";
                break;
            case 9:
                color = "f45e5e";
                break;
            case 10:
                color = "f8aa27";
                break;
            case 11:
                color = "FFB6C1";
                break;
            case 12:
                color = "FA8072";
                break;
            case 13:
                color = "DC143C";
                break;
            case 14:
                color = "BDB76B";
                break;
            case 15:
                color = "B0E0E6";
                break;
            case 16:
                color = "6B8E23";
                break;
            case 17:
                color = "B0C4DE";
                break;
            case 18:
                color = "8B4513";
                break;
            case 19:
                color = "778899";
                break;
            case 20:
                color = "6495ED";
                break;

        }

        relative_layout_upload_status.setBackgroundResource(R.drawable.d_bg_writing);
        GradientDrawable drawable = (GradientDrawable) relative_layout_upload_status.getBackground();
        drawable.setColor(Color.parseColor("#" + color));
    }

    private Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }
}
