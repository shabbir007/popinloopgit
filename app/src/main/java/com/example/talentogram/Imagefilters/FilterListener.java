package com.example.talentogram.Imagefilters;

import ja.burhanrashid52.photoeditor.PhotoFilter;

public interface FilterListener {
    void onFilterSelected(PhotoFilter photoFilter);
}