package com.example.talentogram.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.model.NotificationModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.skydoves.elasticviews.ElasticButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<NotificationModel> notificationModelArrayList;
    long DURATION = 500;
    private boolean on_attach = true;


    public NotificationAdapter(Context context, ArrayList<NotificationModel> notificationModelArrayList) {
        this.context = context;
        this.notificationModelArrayList = notificationModelArrayList;

    }

    @NonNull
    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list, parent, false);
        return new NotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.MyViewHolder holder, int position) {
        NotificationModel notificationModel = notificationModelArrayList.get(position);

        //---->notification Type from model  1-Like, 2-Comment , 3-follow
        if(!notificationModel.isIfFriendRequest()) {
            if (notificationModel.getNotification_type() == 1 ) {
                holder.rl_likescomment.setVisibility(View.VISIBLE);
                holder.ivlike.setVisibility(View.VISIBLE);
                holder.ivcomment.setVisibility(View.GONE);
                holder.llAcceptRequest.setVisibility(View.GONE);
                Glide.with(context)
                        .load(notificationModel.getSenderProfilePicUrl())
                        .thumbnail(Glide.with(context).load(notificationModel.getSenderProfilePicUrl()))
                        .error(R.drawable.noimage)
                        .into(holder.ivUserimg);

                Glide.with(context)
                        .load(notificationModel.getPostUrl())
                        .thumbnail(Glide.with(context).load(notificationModel.getPostUrl()))
                        .error(R.drawable.noimage)
                        .into(holder.ivpost);
                holder.tvUsername.setText(notificationModel.getUserdata().getFirstName()+" "+notificationModel.getUserdata().getLastName());
                holder.tvUserdesc.setText("Liked your Post");


            }
            else if (notificationModel.getNotification_type() == 2 ) {
                holder.rl_likescomment.setVisibility(View.VISIBLE);
                holder.ivlike.setVisibility(View.GONE);
                holder.ivcomment.setVisibility(View.GONE);
                holder.llAcceptRequest.setVisibility(View.GONE);

                Glide.with(context)
                        .load(notificationModel.getSenderProfilePicUrl())
                        .thumbnail(Glide.with(context).load(notificationModel.getSenderProfilePicUrl()))
                        .error(R.drawable.noimage)
                        .into(holder.ivUserimg);

                Glide.with(context)
                        .load(notificationModel.getPostUrl())
                        .thumbnail(Glide.with(context).load(notificationModel.getPostUrl()))
                        .error(R.drawable.noimage)
                        .into(holder.ivpost);
                holder.tvUsername.setText(notificationModel.getUserdata().getFirstName()+" "+notificationModel.getUserdata().getLastName());
                holder.tvUserdesc.setText("Commented on your Post");
            }
        }

        else if (notificationModel.isIfFriendRequest()) {
                holder.rl_likescomment.setVisibility(View.GONE);
                holder.ivlike.setVisibility(View.GONE);
                holder.ivcomment.setVisibility(View.GONE);
                holder.llAcceptRequest.setVisibility(View.VISIBLE);

            Glide.with(context)
                    .load(notificationModel.getFollow_user().getProfilePic())
                    .thumbnail(Glide.with(context).load(notificationModel.getFollow_user().getProfilePic()))
                    .error(R.drawable.noimage)
                    .into(holder.ivUserimg);

                holder.tvUsername.setText(notificationModel.getFollow_user().getFirstName() + " " + notificationModel.getFollow_user().getLastName());
                holder.tvUserdesc.setText(R.string.txt_wants_to_follow_you);
                holder.btnAcceptRequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        acceptFollowRequest(notificationModel.getId(), position);
                    }
                });
            holder.btnCancelRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelFollowRequest(notificationModel.getId(), position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return notificationModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView tvUsername, tvUserdesc;
        public ImageView ivUserimg, ivpost, ivlike, ivcomment;
        public LinearLayout rowFG, llAcceptRequest;
        public RelativeLayout rowBG, rl_likescomment;
        public ElasticButton btnAcceptRequest,btnCancelRequest;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvUsername = itemView.findViewById(R.id.tvUserName);
            tvUserdesc = itemView.findViewById(R.id.tvUserdesc);
            ivUserimg = itemView.findViewById(R.id.ivUser);
            rowFG = itemView.findViewById(R.id.rowFG);
            rowBG = itemView.findViewById(R.id.rowBG);
            ivpost = itemView.findViewById(R.id.ivPost);
            ivlike = itemView.findViewById(R.id.ivlike);
            ivcomment = itemView.findViewById(R.id.ivcomment);
            rl_likescomment = itemView.findViewById(R.id.rl_likesComment);
            btnAcceptRequest = itemView.findViewById(R.id.btnAcceptRequest);
            btnCancelRequest = itemView.findViewById(R.id.btnCancelRequest);
            llAcceptRequest = itemView.findViewById(R.id.llAcceptRequest);

        }
    }

    private void acceptFollowRequest(int notificationModelId, int position) {
        HashMap<String, String> data = new HashMap<>();
        data.put("RequestId", String.valueOf(notificationModelId));
        data.put("Status", "1");
        new NetworkCall(context, Services.ACCEPT_FOLLOW_REQUEST, data,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");
                    String message = jsonObject.getString("message");

                    if (Success) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Success");
                        alertDialog.setMessage(message);
                        alertDialog.setPositiveButton("ok", (dialog, which) -> {
                            notificationModelArrayList.remove(position);
                            notifyDataSetChanged();

                        });
                        alertDialog.show();

                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage(message);
                        notifyDataSetChanged();
                        alertDialog.show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
    }

    private void cancelFollowRequest(int notificationModelId, int position) {
        HashMap<String, String> data = new HashMap<>();
        data.put("RequestId", String.valueOf(notificationModelId));
        data.put("Status", "0");
        new NetworkCall(context, Services.ACCEPT_FOLLOW_REQUEST, data,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");
                    String message = jsonObject.getString("message");

                    if (Success) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Success");
                        alertDialog.setMessage(message);
                        alertDialog.setPositiveButton("ok", (dialog, which) -> {
                            notificationModelArrayList.remove(position);
                            notifyDataSetChanged();

                        });
                        alertDialog.show();

                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage(message);
                        notifyDataSetChanged();
                        alertDialog.show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
    }



}
