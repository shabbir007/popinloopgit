package com.example.talentogram.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.activity.AboutUsActivity;
import com.example.talentogram.activity.BankDetailActivity;
import com.example.talentogram.activity.CopyRightPolicyActivity;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.activity.HelpCenterActivity;
import com.example.talentogram.activity.LoginActivity;
import com.example.talentogram.activity.PrivacyPolicyActivity;
import com.example.talentogram.setting.activity.SettingActivity;
import com.example.talentogram.activity.TermsOfUseActivity;
import com.example.talentogram.activity.WalletActivity;
import com.example.talentogram.fragment.PostFragment;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;


import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("SetTextI18n")
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.MyViewHolder> {
    private ArrayList<String> nameList;
    private ArrayList<Integer> iconList;
    private Context context;
    private DrawerLayout drawer;
    public static Dialog dialog;


    public DrawerAdapter(Context context, ArrayList<String> nameList,
                         ArrayList<Integer> iconList, DrawerLayout drawer) {
        super();
        this.context = context;
        this.nameList = nameList;
        this.iconList = iconList;
        this.drawer = drawer;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.drawer_item, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder,
                                 @SuppressLint("RecyclerView") final int position) {

        holder.tvDrawer.setText(nameList.get(position));
        holder.ivDrawer.setImageResource(iconList.get(position));


        holder.layoutDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSideMenu(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvDrawer;
        ImageView ivDrawer;
        LinearLayout layoutDrawer;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivDrawer = itemView.findViewById(R.id.ivDrawer);
            tvDrawer = itemView.findViewById(R.id.tvDrawer);
            layoutDrawer = itemView.findViewById(R.id.layoutDrawer);
        }
    }


    private void openSideMenu(int pos) {
        if (drawer != null)
            drawer.closeDrawer(GravityCompat.START);
        switch (pos) {
            case 0: {//
                PostFragment fragment = new PostFragment();
                DashBoardActivity.changeFragment(context, fragment, false);
                ((DashBoardActivity) context).setCurrentFragment();

                break;
            }
            case 1: {//Push Notification
              //  context.startActivity(new Intent(context, UnityPlayerActivity.class));
                break;
            }
            case 2: {//Report a Problem
                break;
            }
            case 3: {//Blocked User
                context.startActivity(new Intent(context, SettingActivity.class));
                break;
            }
            case 4: {//Terms of Use
                context.startActivity(new Intent(context, TermsOfUseActivity.class));
                break;
            }
            case 5: {//About Us
                context.startActivity(new Intent(context, AboutUsActivity.class));
                break;
            }
            case 6: {//Privacy Policy
                context.startActivity(new Intent(context, PrivacyPolicyActivity.class));
                break;
            }
            case 7:{
                //Bank Account Detail
                context.startActivity(new Intent(context, BankDetailActivity.class));
                break;
            }
            case 8: {
                //Wallet
                context.startActivity(new Intent(context, WalletActivity.class));
                break;
            }
            case 9: {//CopyRight Policy
                context.startActivity(new Intent(context, CopyRightPolicyActivity.class));
                break;
            }
            case 10: {//Help Center
                context.startActivity(new Intent(context, HelpCenterActivity.class));
                break;
            }
            case 11: {//Log Out
                logout();
                break;
            }
        }

    }

    private void logout() {
        new AlertDialog.Builder(context)
                .setMessage(R.string.action_logout)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        logoutUser();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void logoutUser() {

        new NetworkCall(context, Services.LOGOUT,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            new MySharedPref(context).sharedPrefClear(context);
                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            ActivityCompat.finishAffinity((Activity) context);
                            ((Activity) context).finish();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
