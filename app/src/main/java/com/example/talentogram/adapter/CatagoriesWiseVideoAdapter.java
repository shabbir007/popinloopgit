package com.example.talentogram.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.custom_views.PicassoTrustAll;
import com.example.talentogram.model.CatagoryWiseVideoModel;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class CatagoriesWiseVideoAdapter extends RecyclerView.Adapter<CatagoriesWiseVideoAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<CatagoryWiseVideoModel> catagoryWiseVideoArrayList;

    public CatagoriesWiseVideoAdapter(Context context, ArrayList<CatagoryWiseVideoModel> catagoryWiseVideoArrayList) {
        this.context = context;
        this.catagoryWiseVideoArrayList = catagoryWiseVideoArrayList;
    }

    @NonNull
    @Override
    public CatagoriesWiseVideoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_video_catagories_wise, parent, false);
        return new CatagoriesWiseVideoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CatagoriesWiseVideoAdapter.MyViewHolder holder, int position) {
        CatagoryWiseVideoModel catagoryWiseVideoModel = catagoryWiseVideoArrayList.get(position);

        holder.tvCatagoryTitle.setText(catagoryWiseVideoModel.getCtTitle());
        holder.tvVideoCount.setText(catagoryWiseVideoModel.getVideoCount());

        try {
            PicassoTrustAll.getInstance(context)
                    .load(catagoryWiseVideoModel.getfImgUrl())
                    .into(holder.ivImageOne, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PicassoTrustAll.getInstance(context)
                    .load(catagoryWiseVideoModel.getsImgUrl())
                    .into(holder.ivImageTwo, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return catagoryWiseVideoArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvCatagoryTitle, tvVideoCount;
        ImageView ivImageOne, ivImageTwo;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCatagoryTitle = itemView.findViewById(R.id.tvCatagoryTitle);
            tvVideoCount = itemView.findViewById(R.id.tvVideoCount);
            ivImageOne = itemView.findViewById(R.id.ivImageOne);
            ivImageTwo = itemView.findViewById(R.id.ivImageTwo);

        }
    }
}
