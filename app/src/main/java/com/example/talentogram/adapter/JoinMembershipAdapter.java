package com.example.talentogram.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.talentogram.fragment.BasicFragment;
import com.example.talentogram.fragment.PremiumFragment;
import com.example.talentogram.fragment.StandardFragment;


public class JoinMembershipAdapter extends FragmentStatePagerAdapter {

    public JoinMembershipAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {

        switch (pos) {

            case 0:
                BasicFragment b1 = new BasicFragment();
                return b1;
            case 1:
                StandardFragment s1 = new StandardFragment();
                return s1;
            case 2:
                PremiumFragment p1 = new PremiumFragment();
                return p1;

            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
