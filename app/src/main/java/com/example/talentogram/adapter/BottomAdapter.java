package com.example.talentogram.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.Video_recording.activity.VideoRecorderActivity;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.contest.activity.ContestActivity;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.fragment.HomeFragment;
import com.example.talentogram.fragment.NotificationFragment;
import com.example.talentogram.profile.ProfileFragment;

import java.util.ArrayList;

@SuppressLint("SetTextI18n")
public class BottomAdapter extends RecyclerView.Adapter<BottomAdapter.MyViewHolder> {

    private ArrayList<String> nameArrayList;
    private ArrayList<Integer> iconArrayList;
    private Context context;
    private int selPos;
    private RecyclerView rvBottom;

    public BottomAdapter(Context context, int selPos, RecyclerView rvBottom) {
        super();
        this.context = context;
        this.selPos = selPos;
        this.rvBottom = rvBottom;

        nameArrayList = new ArrayList<>();
        iconArrayList = new ArrayList<>();

        switch (selPos) {
            case 0:

                break;
            case 1:

                break;
            case 2:

                break;
            case 3:
                break;
            case 4:
                break;
        }

        nameArrayList.add("Talento");
        iconArrayList.add(selPos == 0 ?
                R.drawable.talengra_mlogo
                : R.drawable.talengra_mlogo);

        nameArrayList.add("Notification");
        iconArrayList.add(selPos == 1 ?
                R.drawable.notificationorange
                : R.drawable.notification);


        nameArrayList.add("Post");
        iconArrayList.add(selPos == 2 ?
                R.drawable.ic_post
                : R.drawable.ic_post);

        nameArrayList.add("Contest");
        iconArrayList.add(selPos == 3 ?
                R.drawable.ic_contestorange
                : R.drawable.contest);

        nameArrayList.add("Profile");
        iconArrayList.add(selPos == 4 ?
                R.drawable.userorange
                : R.drawable.user1);
    }

    @NonNull
    @Override
    public BottomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.bottom_item, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull BottomAdapter.MyViewHolder holder,
                                 @SuppressLint("RecyclerView") final int position) {

        if (position == 2) {
            holder.ivCenter.setImageDrawable(context.getResources()
                    .getDrawable(R.drawable.ic_bottom_center));
            holder.tvBottom.setTextColor(Color.WHITE);
        }

        holder.ivBottom.setImageResource(iconArrayList.get(position));
        holder.tvBottom.setText(nameArrayList.get(position));


        holder.layoutBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBottomMenu(position);
            }
        });
    }


    private void openBottomMenu(int pos) {
        boolean isBack = pos != selPos;
        switch (pos) {
            case 0: {
                DashBoardActivity.changeFragment(context, new HomeFragment(), false);
                break;
            }
            case 1: {
                NotificationFragment fragment = new NotificationFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(DashBoardActivity.EXTRA_TYPE, 1);
                fragment.setArguments(bundle);
                DashBoardActivity.changeFragment(context, fragment, false);
                break;
            }

            case 2: {
                context.startActivity(new Intent(context, VideoRecorderActivity.class));
                break;
            }

            case 3: {
                context.startActivity(new Intent(context, ContestActivity.class));
                break;
            }

            case 4: {
                ProfileFragment fragment = new ProfileFragment();
                DashBoardActivity.changeFragment(context, fragment, false);
                break;

            }
        }
        rvBottom.setAdapter(new BottomAdapter(context, pos, rvBottom));
    }

    @Override
    public int getItemCount() {
        return nameArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        CustomTextView tvBottom;
        ImageView ivBottom, ivCenter;
        FrameLayout layoutBottom;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivBottom = itemView.findViewById(R.id.ivBottom);
            tvBottom = itemView.findViewById(R.id.tvBottom);
            layoutBottom = itemView.findViewById(R.id.layoutBottom);
            ivCenter = itemView.findViewById(R.id.ivCenter);

        }
    }
}

