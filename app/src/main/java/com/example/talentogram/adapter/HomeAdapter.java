package com.example.talentogram.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.activity.CommentActivity;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.model.HomeModel;
import com.example.talentogram.others.DownloadTask;
import com.example.talentogram.profile.OtherUserProfileFragment;
import com.example.talentogram.profile.ProfileFragment;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.CustomViewHolder> {

    public Context context;
    private ArrayList<HomeModel> dataList;
    private HomeAdapter.OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(int positon, HomeModel item, View view);
    }

    public HomeAdapter(Context context, ArrayList<HomeModel> dataList) {
        this.context = context;
        this.dataList = dataList;
        this.listener = listener;
    }

    @Override
    public HomeAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewtype) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_home_layout, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,
                RecyclerView.LayoutParams.MATCH_PARENT));
        HomeAdapter.CustomViewHolder viewHolder = new HomeAdapter.CustomViewHolder(view);
        return viewHolder;
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }


    @Override
    public void onBindViewHolder(final HomeAdapter.CustomViewHolder holder, final int i) {
        final HomeModel item = dataList.get(i);
        holder.setIsRecyclable(false);
        if (!TextUtils.isEmpty(item.getIsInContest())) {
            if (item.getIsInContest().equals("1")) {
                holder.ic_contest.setImageResource(R.drawable.star_on);
            }
        }
        Glide.with(context)
                .load(item.getUserdata().getProfilePic())
                .thumbnail(Glide.with(context).load(R.drawable.user))
                .error(R.drawable.noimage)
                .into(holder.ivProfilePic);

        holder.llComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CommentActivity.class);
                i.putExtra("postId", String.valueOf(item.getPost_detail().getPostId()));
                context.startActivity(i);
            }
        });
        holder.ivHeart.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                holder.ivHeart.setLiked(true);
                likePost(item.getId(), i, item.getPost_detail().getLikeCount());
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                holder.ivHeart.setLiked(false);
                disLikePost(item.getId(), i, item.getPost_detail().getLikeCount());
            }
        });
        if (item.ispostliked != 0) {
            holder.ivHeart.setLiked(true);
        } else {
            holder.ivHeart.setLiked(false);
        }
        holder.llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadTask(context, item.getPost_detail().getPostVideo().get(0).getVideoUrl());
            }
        });
        holder.shared_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "TALENTOGRAM");
                    String shareMessage = "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + Services.BASE_URL + "share/" + item.getPost_detail().getId();
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    context.startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.toString();
                }
            }
        });
        int userId = item.getUserId();
        int loggedInuserId = Integer.parseInt(MySharedPref.getString(context, MySharedPref.USER_ID, ""));
        holder.ivProfilePic.setOnClickListener(v -> {
            if (userId == loggedInuserId) {
                DashBoardActivity.changeFragment(context, new ProfileFragment(), true);
            } else {
                Bundle b = new Bundle();
                b.putInt("userId", item.getUserId());
                Fragment fragment = new OtherUserProfileFragment();
                fragment.setArguments(b);
                DashBoardActivity.changeFragment(context, fragment, true);
            }
        });
        holder.tvLikeCount.setText(String.valueOf(item.getPost_detail().getLikeCount()));
        holder.tvCommentCount.setText(String.valueOf(item.getPost_detail().getCommentCount()));
        holder.tvUsername.setText(item.getUserdata().getFirstName() + " " + item.getUserdata().getLastName());

    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivProfilePic, ic_contest;
        public LinearLayout llComment, llDownload, shared_layout;
        public LikeButton ivHeart;
        public TextView tvLikeCount, tvCommentCount, tvUsername;

        public CustomViewHolder(View view) {
            super(view);
            ivProfilePic = view.findViewById(R.id.ivProfilePic);
            llComment = view.findViewById(R.id.llComment);
            llDownload = view.findViewById(R.id.llDownload);
            ivHeart = view.findViewById(R.id.ic_heart);
            tvLikeCount = view.findViewById(R.id.tvLikeCount);
            tvCommentCount = view.findViewById(R.id.tvCommentCount);
            tvUsername = view.findViewById(R.id.tvUsername);
            shared_layout = view.findViewById(R.id.shared_layout);
            ic_contest = view.findViewById(R.id.ic_contest);
        }

        public void bind(final int postion, final HomeModel item, final HomeAdapter.OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(postion, item, v);
                }
            });

        }

    }

    private void likePost(int itemId, int i, int likeCount) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(itemId));
        userData.put("IsLike", String.valueOf(0));

        new NetworkCall(context, Services.LIKE_UNLIKE, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        JSONArray jPostData = obj.getJSONArray("data");
                        String message = obj.getString("message");
                        if (Success) {
                            dataList.get(i).getPost_detail().setLikeCount(likeCount + 1);
                            dataList.get(i).setIspostliked(1);
                            notifyDataSetChanged();
                        } else {
                            disLikePost(itemId, i, likeCount);
                        }
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void disLikePost(int itemId, int i, int likeCount) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(itemId));
        userData.put("IsLike", String.valueOf(1));

        new NetworkCall(context, Services.LIKE_UNLIKE, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        JSONArray jPostData = obj.getJSONArray("data");
                        String message = obj.getString("message");
                        if (Success) {
                            dataList.get(i).getPost_detail().setLikeCount(likeCount - 1);
                            dataList.get(i).setIspostliked(0);
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
                        }
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }
}
