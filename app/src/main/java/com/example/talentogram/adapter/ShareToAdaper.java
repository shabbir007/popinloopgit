package com.example.talentogram.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.model.ShareModel;

import java.util.List;

public class ShareToAdaper extends RecyclerView.Adapter<ShareToAdaper.MyViewHolder> {

    private Context context;
    private List<ShareModel> listData;

    public ShareToAdaper(Context context, List<ShareModel> listData) {
        this.context = context;
        this.listData = listData;
    }


    @NonNull
    @Override
    public ShareToAdaper.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_shareto, parent, false);
        return new MyViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull ShareToAdaper.MyViewHolder holder, int position) {

        holder.tvName.setText(listData.get(position).getAppName());
        holder.ivIcon.setImageDrawable(listData.get(position).getAppIcon());

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        CustomTextView tvName;
        ImageView ivIcon;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvName = view.findViewById(R.id.tvSharename);
            ivIcon = view.findViewById(R.id.ivShareicon);
        }
    }
}
