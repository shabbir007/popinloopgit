package com.example.talentogram.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.custom_views.CustomBoldTextView;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.model.FollowingModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<FollowingModel> followingList;

    public FollowingAdapter(Context context, ArrayList<FollowingModel> followingList) {
        this.context = context;
        this.followingList = followingList;
    }

    @NonNull
    @Override
    public FollowingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_following, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FollowingAdapter.MyViewHolder holder, int position) {
        FollowingModel followingModel = followingList.get(position);

        holder.tvUserName.setText(followingModel.getFollowing_user().getFirstName() + " " + followingModel.getFollowing_user().getLastName());

        Glide.with(context)
                .load(followingModel.getFollowing_user().getProfilePic())
                .thumbnail(Glide.with(context).load(R.drawable.user))
                .into(holder.ivProfile);

        holder.tvAddress.setText(followingModel.getFollowing_user().getAddress());

        holder.btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unFollowUser(followingModel.getFollowing_user().getId(), position);
            }

        });

    }

    private void unFollowUser(int unfollowID, int position) {
        HashMap<String, String> data = new HashMap<>();
        data.put("FriendsId", String.valueOf(unfollowID));

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle("Are you sure you want to unfollow?");

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall(context, Services.UNFOLLOW, data,
                    true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean Success = jsonObject.getBoolean("Success");
                        String message = jsonObject.getString("message");
                        if (Success) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            alertDialog.setTitle("Success");
                            alertDialog.setMessage(message);
                            alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                followingList.remove(position);
                                notifyDataSetChanged();
                            });
                            alertDialog.show();

                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            alertDialog.setTitle("Error");
                            alertDialog.setMessage(message);
                            notifyDataSetChanged();
                            alertDialog.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });


        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return followingList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivProfile;
        private CustomBoldTextView tvUserName;
        private CustomTextView tvAddress;
        private Button btnFollow;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            btnFollow = itemView.findViewById(R.id.btnFollow);
        }
    }
}
