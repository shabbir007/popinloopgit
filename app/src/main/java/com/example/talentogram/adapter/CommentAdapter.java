package com.example.talentogram.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.helper.OnCommentEditListener;
import com.example.talentogram.model.CommentModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<CommentModel> commentModels;
    AlertDialog.Builder alertDialog;
    private String UserId;
    private OnCommentEditListener clickListener;

    public CommentAdapter(Context context, ArrayList<CommentModel> commentModels, final OnCommentEditListener clickListener) {
        this.context = context;
        this.commentModels = commentModels;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CommentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentAdapter.MyViewHolder holder, int position) {
        CommentModel commentModel = commentModels.get(position);
        holder.tvComment.setText(commentModel.getCommentMessage());
        holder.tvUserName.setText(commentModel.getUserdata().getFirstName() + " " + commentModel.getUserdata().getLastName());
        holder.tvtime.setText(Utils.getdate(commentModel.getUserdata().getCreated_at()));

        UserId = new MySharedPref(context).getString(context, MySharedPref.USER_ID, "");
        if (!UserId.equals(String.valueOf(commentModel.getUserId()))) {
            holder.etComment.setVisibility(View.GONE);
            holder.tvEditComment.setVisibility(View.GONE);
            holder.tvPostDelete.setVisibility(View.GONE);
        }
        holder.tvEditComment.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                clickListener.onClick(commentModel.getId(), commentModel.getCommentMessage(), position);
                holder.tvEditComment.setTextColor(R.color.Red);
                holder.tvEditComment.setTypeface(null, Typeface.BOLD);

            }
        });

        holder.tvPostDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle(R.string.blant_warning_delete);
                alertDialog.setPositiveButton("Yes", (dialog, which) -> {
                    deleteComment(context, commentModel.getId(), position);
                });
                alertDialog.setNegativeButton("No", (dialog, which) -> {
                    dialog.cancel();
                });
                alertDialog.show();
            }


        });
        Glide.with(context).
                load(commentModel.getUserdata().getProfilePic())
                .error(R.drawable.noimage)
                .into(holder.ivUserProfile);
    }

    @Override
    public int getItemCount() {
        return commentModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserName, tvComment, tvEditComment, tvPostDelete, tvtime;
        public ImageView ivUserProfile;
        public EditText etComment;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvComment = itemView.findViewById(R.id.tvComment);
            ivUserProfile = itemView.findViewById(R.id.ivUserProfile);
            tvEditComment = itemView.findViewById(R.id.tvEditComment);
            etComment = itemView.findViewById(R.id.etComment);
            tvPostDelete = itemView.findViewById(R.id.tvPostDelete);
            tvtime = itemView.findViewById(R.id.tvtime);
        }
    }

    private void deleteComment(Context context, int id, int Position) {
        String auth = new MySharedPref(context).getString(context, MySharedPref.USER_AUTH, "");
        AndroidNetworking.upload(Services.DELETE_COMMENT)
                .addHeaders("X-Requested-With", "XMLHttpRequest")
                .addHeaders("Authorization", "Bearer " + auth)
                .addMultipartParameter("CommentId", String.valueOf(id))
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean Success = response.getBoolean("Success");
                            String message = response.getString("message");
                            if (Success) {
                                commentModels.remove(Position);
                                notifyDataSetChanged();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError ===>", String.valueOf(anError));
                    }
                });

    }


}
