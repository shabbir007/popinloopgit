package com.example.talentogram.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.model.WalletHistoryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.MyViewHolder> {

    Context context;
    ArrayList<WalletHistoryModel.History> walletHistoryModels;

    public WalletHistoryAdapter(Context context, ArrayList<WalletHistoryModel.History> historyArrayList) {
        this.context = context;
        this.walletHistoryModels = historyArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_wallet_history, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        WalletHistoryModel.History model = walletHistoryModels.get(position);
        holder.txtTitle.setText(model.getContestTitle());
        holder.txtAmt.setText("₹" + model.getAmount());
        holder.txtTransactionId.setText(model.getBankTxnId());
        holder.txtCalendar.setText(model.getCreated_at());
        holder.txtTransactionType.setText(model.getTransferType());
        if (model.getTransactionsType().equals("debit")) {
            holder.txtTransactionType.setTextColor(ContextCompat.getColor(context, R.color.red_btn_bg_color));
        } else {
            holder.txtTransactionType.setTextColor(ContextCompat.getColor(context, R.color.main_green_color));
        }

    }

    @Override
    public int getItemCount() {
        return walletHistoryModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_amt)
        TextView txtAmt;
        @BindView(R.id.txt_transactionId)
        TextView txtTransactionId;
        @BindView(R.id.txt_calendar)
        TextView txtCalendar;
        @BindView(R.id.txt_transaction_type)
        TextView txtTransactionType;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
