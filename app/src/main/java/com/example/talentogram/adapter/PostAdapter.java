package com.example.talentogram.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.example.talentogram.R;
import com.example.talentogram.activity.CommentActivity;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.activity.ZoomImageActivity;
import com.example.talentogram.contest.AudioDialogActivity;
import com.example.talentogram.contest.activity.ViewWritingContestDetailActivity;
import com.example.talentogram.custom_views.CustomBoldTextView;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.profile.OtherUserProfileFragment;
import com.example.talentogram.profile.ProfileFragment;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.model.PostModel;
import com.example.talentogram.others.DownloadTask;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import com.like.LikeButton;
import com.like.OnLikeListener;


import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.example.talentogram.others.Utils.getTimeDiff;

public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String[] strings = {
            "like", "love", "laugh", "wow", "sad", "angry"
    };
    public static final int VIDEOVIEW = 0;
    public static final int RESTVIEW = 1;

    Bitmap bmThumbnail;
    private Context context;
    private ArrayList<PostModel> postModelList;
    private RequestManager requestManager;

    public interface OnItemClickListener {
        void onItemClick(int positon, PostModel item, View view);
    }

    @Override
    public int getItemViewType(int position) {
        if (postModelList.get(position).getPostType() == 1) {
            return VIDEOVIEW;
        } else {
            return RESTVIEW;
        }
    }

    public PostAdapter(Context context, ArrayList<PostModel> postModelList) {
        this.context = context;
        this.postModelList = postModelList;
        this.requestManager = requestManager;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (VIDEOVIEW == viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_video_layout, parent, false);
            return new VideoViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
            return new MyViewHolder(itemView);
        }
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderMain, int position) {
        PostModel postModel = postModelList.get(position);

        if (VIDEOVIEW == holderMain.getItemViewType()) {
            VideoViewHolder holder = (VideoViewHolder) holderMain;
            holder.setIsRecyclable(false);
            if (postModel.getIsInContest().equals("1")) {
                holder.llContest.setVisibility(View.VISIBLE);
            } else {
                holder.llContest.setVisibility(View.GONE);
            }
            try {
                Glide.with(context)
                        .load(postModel.getUserdata().getProfilePics())
                        .thumbnail(Glide.with(context).load(R.drawable.noimage))
                        .into(holder.ivUserProfile);
            } catch (Exception e) {
                e.printStackTrace();

            }
            holder.tvTitle.setText(postModel.getUserdata().getFirstName() + " " + postModel.getUserdata().getLastName());
            holder.tvlike.setText("" + postModel.getPost_detail().getLikeCount());
            holder.tvcomment.setText("" + postModel.getPost_detail().getCommentCount());
            holder.tvtime.setText(Utils.getdate(postModel.getCreated_at()));
            holder.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, CommentActivity.class);
                    i.putExtra("postId", String.valueOf(postModel.getPost_detail().getPostId()));
                    context.startActivity(i);
                }
            });
            holder.ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenuPopup(v, postModel.getVideoUrl().get(0).getVideoUrl(), postModel.getId(), position, postModel.is_download, true);
                }
            });
            holder.llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "TALENTOGRAM");
                        String shareMessage = "\nLet me recommend you this application\n\n";
                        shareMessage = shareMessage + Services.BASE_URL + "share/" + postModel.getEncryptedId();
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        context.startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } catch (Exception e) {
                        e.toString();
                    }
                }
            });
            holder.ivLike.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    holder.ivLike.setLiked(true);
                    likeVideo(postModel.getId(), holder, postModel.getPost_detail().getLikeCount(), position);
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    holder.ivLike.setLiked(false);
                    disLikeVideo(postModel.getId(), holder, postModel.getPost_detail().getLikeCount(), position);
                }
            });
            if (postModel.ispostliked != 0) {
                holder.ivLike.setLiked(true);
            } else {
                holder.ivLike.setLiked(false);
            }
            int userId = postModel.getUserId();
            int loggedInuserId = Integer.parseInt(MySharedPref.getString(context, MySharedPref.USER_ID, ""));
            holder.ivUserProfile.setOnClickListener(v -> {
                if (userId == loggedInuserId) {
                    DashBoardActivity.changeFragment(context, new ProfileFragment(), true);
                } else {
                    Bundle b = new Bundle();
                    b.putInt("userId", postModel.getUserId());
                    Fragment fragment = new OtherUserProfileFragment();
                    fragment.setArguments(b);
                    DashBoardActivity.changeFragment(context, fragment, true);
                }
            });
            holder.tvTitle.setOnClickListener(v -> {
                if (userId == loggedInuserId) {
                    DashBoardActivity.changeFragment(context, new ProfileFragment(), true);
                } else {
                    Bundle b = new Bundle();
                    b.putInt("userId", postModel.getUserId());
                    Fragment fragment = new OtherUserProfileFragment();
                    fragment.setArguments(b);
                    DashBoardActivity.changeFragment(context, fragment, true);
                }
            });

        } else {
            MyViewHolder holder = (MyViewHolder) holderMain;
            if (postModel.getIsInContest().equals("1")) {
                holder.llContest.setVisibility(View.VISIBLE);
            } else {
                holder.llContest.setVisibility(View.GONE);
            }
            try {
                Glide.with(context)
                        .load(postModel.getUserdata().getProfilePics())
                        .thumbnail(Glide.with(context).load(R.drawable.noimage))
                        .into(holder.ivUserProfile);
            } catch (Exception e) {
                e.printStackTrace();

            }//audio View
            if (postModel.getPostType() == 0) {
                holder.flImage.setVisibility(View.GONE);
                holder.flAudio.setVisibility(View.VISIBLE);
                holder.flWriting.setVisibility(View.GONE);
                try {
                    Glide.with(context)
                            .load(R.drawable.audio_visualizer)
                            .thumbnail(Glide.with(context).load(R.drawable.audio_visualizer))
                            .into(holder.ivPostAudio);

                } catch (Exception e) {
                    e.printStackTrace();

                }
                holder.flAudio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = postModel.getAudioUrls().get(0).getAudioUrl();
                        String title = postModel.getPost_detail().getTitle();
                        Intent i = new Intent(context, AudioDialogActivity.class);
                        i.putExtra("AudioUrl", url);
                        i.putExtra("title", title);
                        context.startActivity(i);
                    }
                });

            }
            //imageView
            else if (postModel.getPostType() == 2 && postModel.getImageUrl().size() > 0) {
                holder.flImage.setVisibility(View.VISIBLE);
                holder.flAudio.setVisibility(View.GONE);
                holder.flWriting.setVisibility(View.GONE);
                try {
                    Glide.with(context)
                            .load(postModel.getImageUrl().get(0).getVideoUrl())
                            .thumbnail(Glide.with(context).load(R.drawable.noimage))
                            .error(R.drawable.noimage)
                            .into(holder.ivPostImage);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.ivPostImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popUpForZoomImage(postModel.getImageUrl().get(0).getVideoUrl());
                    }
                });
            }
            //Writing view
            else if (postModel.getPostType() == 3) {
                holder.flImage.setVisibility(View.GONE);
                holder.flAudio.setVisibility(View.GONE);
                holder.flWriting.setVisibility(View.VISIBLE);
                holder.tvWriting.setText(postModel.getPost_detail().getDescription());

                if (!TextUtils.isEmpty(postModel.getColor())) {
                    String color_ = "#" + postModel.getColor();
                    holder.cvWritingView.setBackgroundColor(Color.parseColor(color_));
                } else {
                    holder.cvWritingView.setBackgroundColor(R.color.DarkGray);
                }
                if (!TextUtils.isEmpty(postModel.getFont())) {
                    String fontName = postModel.getFont();
                    Typeface font = Typeface.createFromAsset(context.getAssets(),
                            "fonts/" + fontName);
                    holder.tvWriting.setTypeface(font);
                }


                holder.tvWriting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, ViewWritingContestDetailActivity.class);
                        i.putExtra("description", postModel.getPost_detail().getDescription());
                        i.putExtra("title", postModel.getPost_detail().getTitle());
                        i.putExtra("color", postModel.getColor());
                        i.putExtra("font", postModel.getFont());
                        context.startActivity(i);
                    }
                });
            }

            getTimeDiff(postModel.getCreated_at());

            holder.tvTitle.setText(postModel.getUserdata().getFirstName() + " " + postModel.getUserdata().getLastName());
            holder.tvlike.setText("" + postModel.getPost_detail().getLikeCount());
            holder.tvcomment.setText("" + postModel.getPost_detail().getCommentCount());
            holder.tvtime.setText(Utils.getdate(postModel.getCreated_at()));

            holder.llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "TALENTOGRAM");
                        String shareMessage = "\nLet me recommend you this application\n\n";
                        shareMessage = shareMessage + Services.BASE_URL + "share/" + postModel.getEncryptedId();
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        context.startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } catch (Exception e) {
                        e.toString();
                    }
                }
            });

            holder.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, CommentActivity.class);
                    i.putExtra("postId", String.valueOf(postModel.getPost_detail().getPostId()));
                    context.startActivity(i);
                }
            });

            holder.ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (postModel.getPostType() == 0) {
                        showMenuPopup(v, postModel.getAudioUrls().get(0).getAudioUrl(), postModel.getId(), position, postModel.is_download, true);
                    } else if (postModel.getPostType() == 2) {
                        showMenuPopup(v, postModel.getImageUrl().get(0).getVideoUrl(), postModel.getId(), position, postModel.is_download, true);
                    } else if (postModel.getPostType() == 3) {
                        showMenuForDownload(v, holder.cvWritingView, postModel.getId(), position, postModel.is_download, false);
                    }
                }
            });

            holder.ivlike.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    holder.ivlike.setLiked(true);
                    likePost(postModel.getId(), holder, postModel.getPost_detail().getLikeCount(), position);
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    holder.ivlike.setLiked(false);
                    disLikePost(postModel.getId(), holder, postModel.getPost_detail().getLikeCount(), position);
                }
            });

            if (postModel.ispostliked != 0) {
                holder.ivlike.setLiked(true);
            } else {
                holder.ivlike.setLiked(false);
            }
            int userId = postModel.getUserId();
            int loggedInuserId = Integer.parseInt(MySharedPref.getString(context, MySharedPref.USER_ID, ""));
            holder.ivUserProfile.setOnClickListener(v -> {
                if (userId == loggedInuserId) {
                    DashBoardActivity.changeFragment(context, new ProfileFragment(), true);
                } else {
                    Bundle b = new Bundle();
                    b.putInt("userId", postModel.getUserId());
                    Fragment fragment = new OtherUserProfileFragment();
                    fragment.setArguments(b);
                    DashBoardActivity.changeFragment(context, fragment, true);
                }
            });
            holder.tvTitle.setOnClickListener(v -> {
                if (userId == loggedInuserId) {
                    DashBoardActivity.changeFragment(context, new ProfileFragment(), true);
                } else {
                    Bundle b = new Bundle();
                    b.putInt("userId", postModel.getUserId());
                    Fragment fragment = new OtherUserProfileFragment();
                    fragment.setArguments(b);
                    DashBoardActivity.changeFragment(context, fragment, true);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return postModelList.size();
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llContest, llComment, llShare;
        private ImageView ivcomment, ivshare, ivUserProfile, ivMore;
        private TextView tvtime, tvshare, tvlike, tvcomment, tvTitle;
        private LikeButton ivLike;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);
            llContest = itemView.findViewById(R.id.llContest);
            ivcomment = itemView.findViewById(R.id.ivComment);
            ivshare = itemView.findViewById(R.id.ivShare);
            tvTitle = itemView.findViewById(R.id.tvPosttitle);
            tvtime = itemView.findViewById(R.id.tvTime);
            tvshare = itemView.findViewById(R.id.tvShare);
            tvlike = itemView.findViewById(R.id.tvLike);
            tvcomment = itemView.findViewById(R.id.tvComment);
            ivUserProfile = itemView.findViewById(R.id.ivUserProfile);
            llComment = itemView.findViewById(R.id.llComment);
            ivMore = itemView.findViewById(R.id.ivMore);
            ivLike = itemView.findViewById(R.id.ivLike);
            llShare = itemView.findViewById(R.id.llShare);

        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        //    private EmojiReactionView emojiView;
        private FrameLayout flImage, flAudio, flWriting;
        private CustomTextView tvtime, tvshare, tvlike, tvcomment;
        private CustomBoldTextView tvTitle;
        private ImageView ivcomment, ivshare, ivPostImage, ivUserProfile, ivMore;
        private ImageView ivPostAudio;
        private LinearLayout llContest, llComment, llShare;
        LikeButton ivlike;
        private LinearLayout cvWritingView;
        private TextView tvWriting;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivlike = itemView.findViewById(R.id.ivLike);
            llContest = itemView.findViewById(R.id.llContest);
            ivcomment = itemView.findViewById(R.id.ivComment);
            ivshare = itemView.findViewById(R.id.ivShare);
            ivPostImage = itemView.findViewById(R.id.ivPostImage);
            tvTitle = itemView.findViewById(R.id.tvPosttitle);
            tvtime = itemView.findViewById(R.id.tvTime);
            tvshare = itemView.findViewById(R.id.tvShare);
            tvlike = itemView.findViewById(R.id.tvLike);
            tvcomment = itemView.findViewById(R.id.tvComment);
            ivUserProfile = itemView.findViewById(R.id.ivUserProfile);
            ivMore = itemView.findViewById(R.id.ivMore);
            llComment = itemView.findViewById(R.id.llComment);
            llShare = itemView.findViewById(R.id.llShare);
            flImage = itemView.findViewById(R.id.flImage);
            flAudio = itemView.findViewById(R.id.flAudio);
            flWriting = itemView.findViewById(R.id.flWriting);
            ivPostAudio = itemView.findViewById(R.id.ivPostAudio);
            tvWriting = itemView.findViewById(R.id.tvWriting);
            cvWritingView = itemView.findViewById(R.id.llWritingView);

        }

        public void bind(final int postion, final PostModel item, final PostAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(postion, item, v);
                }
            });
        }

    }

    private void showMenuForDownload(View v, LinearLayout content, int postId, int position, int isDownload, boolean isUrl) {
        int gravity = Gravity.CENTER;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.dialog_menu, null);
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });
        popupView.findViewById(R.id.tvSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isUrl) {
                    Bitmap content_ = loadBitmapFromView(content);
                    SaveWritingContent(content_);
                    savePost(context, postId);
                }
                popupWindow.dismiss();
            }
        });

        popupView.findViewById(R.id.tvHide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidePost(context, postId, position);
                popupWindow.dismiss();
            }
        });
        popupView.findViewById(R.id.tvReport).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportPost(context, postId);
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(v, gravity, 0, 0);
    }

    private void SaveWritingContent(Bitmap finalBitmap) {
        File myDir = new File(MyConstants.app_folder);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String fname = Utils.getRandomString() + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

            ContextThemeWrapper ctw = new ContextThemeWrapper(context, R.style.AppTheme);
            final androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ctw);
            alertDialogBuilder.setTitle("File ");
            alertDialogBuilder.setMessage("File Saved Successfully");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });
            alertDialogBuilder.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    private void showMenuPopup(View v, String videoUrl, int postId, int position, int isDownload, boolean isUrl) {
        int gravity = Gravity.CENTER;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.dialog_menu, null);
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });
        popupView.findViewById(R.id.tvSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDownload == 1 && isUrl) {
                    new DownloadTask(context, videoUrl);
                    savePost(context, postId);
                } else if (isDownload == 0 && isUrl) {
                    Utils.showWarn(context, "You are not allow to download", 0);
                } else if (!isUrl) {
                    Utils.showWarn(context, "You are not allow to download", 0);
                }
                popupWindow.dismiss();
            }
        });

        popupView.findViewById(R.id.tvHide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidePost(context, postId, position);
                popupWindow.dismiss();
            }
        });
        popupView.findViewById(R.id.tvReport).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportPost(context, postId);
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(v, gravity, 0, 0);
    }

    private void savePost(Context context, int postId) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("post_id", String.valueOf(postId));
        new NetworkCall(context, Services.SAVE_POST, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            System.out.println(message + "::");
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }


    private void reportPost(Context context, int postId) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("post_id", String.valueOf(postId));

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(R.string.blank_warning_reportpost);

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall(context, Services.REPORT_POST, userData,
                    true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            String message = obj.getString("message");
                            if (Success) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle("Success");
                                alertDialog.setMessage(message);
                                alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                    //postModelList.remove(position);
                                    notifyDataSetChanged();
                                });
                                alertDialog.show();

                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }

    private void hidePost(Context context, int postId, int position) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("post_id", String.valueOf(postId));

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(R.string.blank_warning_hidepost);

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall(context, Services.HIDE_POST, userData,
                    true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            String message = obj.getString("message");
                            if (Success) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle("Success");
                                alertDialog.setMessage(message);
                                alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                    postModelList.remove(position);
                                    notifyDataSetChanged();
                                });
                                alertDialog.show();

                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }

    private void likePost(int id, MyViewHolder holder, int likeCount, int position) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(id));
        userData.put("IsLike", String.valueOf(0));

        new NetworkCall(context, Services.LIKE_UNLIKE, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            postModelList.get(position).getPost_detail().setLikeCount(likeCount + 1);
                            postModelList.get(position).setIspostliked(1);
                            notifyDataSetChanged();
                        } else {
                            disLikePost(id, holder, likeCount, position);
                        }
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void disLikePost(int id, MyViewHolder holder, int likeCount, int position) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(id));
        userData.put("IsLike", String.valueOf(1));

        new NetworkCall(context, Services.LIKE_UNLIKE, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            postModelList.get(position).getPost_detail().setLikeCount(likeCount - 1);
                            postModelList.get(position).setIspostliked(0);
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
                        }
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void likeVideo(int id, VideoViewHolder holder, int likeCount, int position) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(id));
        userData.put("IsLike", String.valueOf(0));

        new NetworkCall(context, Services.LIKE_UNLIKE, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            //holder.ivLike.setLiked(true);
                            postModelList.get(position).getPost_detail().setLikeCount(likeCount + 1);
                            postModelList.get(position).setIspostliked(1);
                            notifyDataSetChanged();
                        } else {
                            disLikeVideo(id, holder, likeCount, position);
                        }
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void disLikeVideo(int id, VideoViewHolder holder, int likeCount, int position) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(id));
        userData.put("IsLike", String.valueOf(1));

        new NetworkCall(context, Services.LIKE_UNLIKE, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            postModelList.get(position).getPost_detail().setLikeCount(likeCount - 1);
                            postModelList.get(position).setIspostliked(0);
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
                        }
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }


    private void popUpForZoomImage(String imageUrl) {
        Intent i = new Intent(context, ZoomImageActivity.class);
        i.putExtra("imageurl",imageUrl);
        context.startActivity(i);
        /*LayoutInflater li = com.google.ads.interactivemedia.v3.internal.li.from(context);
        View promptsView = li.inflate(R.layout.profile_zoom, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        final TouchImageView ivProfile = promptsView.findViewById(R.id.ivProfile);
        Glide.with(context)
                .load(imageUrl)
                .error(R.drawable.noimage)
                .placeholder(R.drawable.noimage)
                .into(ivProfile);
        alertDialog.show();*/
    }
}
