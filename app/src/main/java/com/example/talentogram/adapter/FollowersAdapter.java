package com.example.talentogram.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.custom_views.CustomBoldTextView;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.model.FollowersModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<FollowersModel> followersList;

    public FollowersAdapter(Context context, ArrayList<FollowersModel> followersList) {
        this.context = context;
        this.followersList = followersList;
    }

    @NonNull
    @Override
    public FollowersAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_followers, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FollowersAdapter.MyViewHolder holder, int position) {
        FollowersModel followersModel = followersList.get(position);

        holder.tvUserName.setText(followersModel.getFollowing_user().getFirstName()+" "+followersModel.getFollowing_user().getLastName());

        Glide.with(context)
                .load(followersModel.getFollowing_user().getProfilePic())
                .thumbnail(Glide.with(context).load(R.drawable.user))
                .into(holder.ivProfile);

        holder.tvAddress.setText(followersModel.getFollowing_user().getAddress());
        holder.btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followUser(followersModel.getFollowing_user().getId(),position);
            }

        });
    }
    private void followUser(int following_user_id,int position ) {
        HashMap<String,String>user_data = new HashMap<>();
        user_data.put("FollowerUserId",String.valueOf(following_user_id));

        new NetworkCall(context, Services.DOFOLLOW,user_data,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            alertDialog.setTitle("Success");
                            alertDialog.setMessage(message);
                            alertDialog.setPositiveButton("ok", (dialog, which) -> {
                            });
                            alertDialog.show();

                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            alertDialog.setTitle("Error");
                            alertDialog.setMessage(message);
                            notifyDataSetChanged();
                            alertDialog.show();
                        }
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
    @Override
    public int getItemCount() {
        return followersList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivProfile;
        private CustomBoldTextView tvUserName;
        private CustomTextView tvAddress;
        private Button btnFollow;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(context, itemView);

            ivProfile = itemView.findViewById(R.id.ivProfile);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            btnFollow = itemView.findViewById(R.id.btnFollow);
        }
    }
}
