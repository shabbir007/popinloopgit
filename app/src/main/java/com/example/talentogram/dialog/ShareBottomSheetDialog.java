package com.example.talentogram.dialog;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.adapter.ShareToAdaper;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.model.ShareModel;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class ShareBottomSheetDialog extends BottomSheetDialogFragment {

    private CustomTextView tvcancel;
    private  RecyclerView rv_share;
    private ImageView ivSave;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.shareto, container, false);


        loadApps();

        tvcancel = view.findViewById(R.id.tvcancel);
        rv_share = view.findViewById(R.id.rv_Share);
        ivSave = view.findViewById(R.id.ivSave);
        rv_share.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
        rv_share.setAdapter(new ShareToAdaper(getActivity(), loadApps()));


        tvcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;


    }

    private List<ShareModel> loadApps() {
        PackageManager manager;
        manager = getActivity().getPackageManager();
        List<ShareModel> res = new ArrayList<ShareModel>();

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");


        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
        for (ResolveInfo ri : availableActivities) {
            ShareModel newInfo = new ShareModel();
            newInfo.setAppName("" + ri.loadLabel(manager));
            newInfo.setPackageName(ri.activityInfo.packageName);
            newInfo.setAppIcon(ri.activityInfo.loadIcon(manager));

            System.out.println("------- " + "" + ri.loadLabel(manager));

            res.add(newInfo);
        }
        return res;
    }

}
