package com.example.talentogram.retrofit;

import android.content.Context;

import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.network.Services;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient(Context mContext) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

          httpClient.callTimeout(180, TimeUnit.SECONDS)  ;
          httpClient.readTimeout(180, TimeUnit.SECONDS)  ;


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("X-Requested-With", "XMLHttpRequest");

                String token = Prefs.getString(MySharedPref.USER_ACCESS_TOKEN, "");

                if (!token.isEmpty()) {
                    requestBuilder.addHeader("Content-Type", "application/json")
                            .addHeader("Authorization", "Bearer " + token);
                }


                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(interceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(Services.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        return retrofit;
    }


}
