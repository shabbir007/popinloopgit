package com.example.talentogram.retrofit;

import com.example.talentogram.model.LoginModel;
import com.example.talentogram.model.SuccessModel;
import com.example.talentogram.model.WalletHistoryModel;
import com.example.talentogram.network.Services;
import com.facebook.login.Login;

import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface ApiInterface {

    //CHECK_BALANCE
    @FormUrlEncoded
    @POST(Services.CHECK_BALANCE)
    Call<SuccessModel> checkBalance(@Field("amount") String amount);

    //CreditWallet
    @FormUrlEncoded
    @POST(Services.CREDIT_WALLET)
    Call<SuccessModel> creditWallet(@Field("amount") String amount,@Field("RazorPayTxnID") String RazorPayTxnID);

    //addBankAccount
    @POST(Services.ADD_BANK_ACCOUNT)
    Call<SuccessModel> addBankAccount(@Body HashMap<String, String> hashMap);

    //TRANSFER_AMT
    @FormUrlEncoded
    @POST(Services.TRANSFER_AMT)
    Call<SuccessModel> transferAmtToBank(@Field("amount") String amount);


    //TRANSFER_AMT
    @POST(Services.WALLET_HISTORY)
    Call<WalletHistoryModel> getHistory();

    //Login
    @POST(Services.LOGIN)
    Call<LoginModel> doLogin(@Body HashMap<String, Object> hashMap);

  //Add Post
  /*  @POST(Services.LOOSE_ITEMS)
    Call<LooseMaterialModel> doLooseMaterial(@Part MultipartBody.Part login_token,
                                             @Part MultipartBody.Part site_id,
                                             @Part MultipartBody.Part vendor_id,
                                             @Part MultipartBody.Part note,
                                             @Part MultipartBody.Part sub_total,
                                             @Part MultipartBody.Part total_tax,
                                             @Part MultipartBody.Part date,
                                             @Part MultipartBody.Part item_category,
                                             @Part MultipartBody.Part item,
                                             @Part MultipartBody.Part qty,
                                             @Part MultipartBody.Part unit_prices,
                                             @Part MultipartBody.Part tax);*/






}
