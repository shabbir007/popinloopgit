package com.example.talentogram.others;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.example.talentogram.contest.model.ViewContestModel;
import com.example.talentogram.model.MyPostModel;
import com.example.talentogram.model.OtherUsersProfileModel;
import com.example.talentogram.model.UserModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MyConstants {
    private static final String TODO = "";

    public static boolean sec_tab = false;
    public static boolean public_post = false;
    public static boolean followers_post = false;
    public static boolean private_post = false;
    public static boolean visible_me = true;
    public static boolean visible_friends = false;
    public static boolean visible_public = false;

    public static final String root= Environment.getExternalStorageDirectory().toString();
    public static final String app_folder = root+"/Talentogram/";
    public static String outputfile2=app_folder + "output2.mp4";
    public static String output_filter_file=app_folder + "output-filtered.mp4";
    public static final String draft_app_folder=app_folder+"Draft/";

    public static DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static final String Broadcast_PLAY_NEW_AUDIO = "com.example.talentogram";
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static final int SELECT_FILE = 2888;
    public static final int REQUEST_CAMERA = 1888;
    public static final int REQUEST_TAKE_GALLERY_VIDEO = 1;
    public static final int REQUEST_TAKE_AUDIO = 2;
    public static final int REQUEST_PICK_IMAGE = 101;
    public static final int REQUEST_TAKE_VIDEO_NEXTACTIVITY = 3;
    public static final int REQUEST_TAKE_IMAGE_NEXTACTIVITY = 4;
    public static final int REQUEST_PERMISSIONS = 100;
    public static final String TAG = "Download Task";
    public static String USER_ID = "user_id";

    public static ArrayList<ViewContestModel> viewContestModels = new ArrayList<>();
    public static ArrayList<MyPostModel.UserOwnPost> userOwnPosts = new ArrayList<>();
    public static ArrayList<MyPostModel.UserOwnLikeVideo> userOwnLikeVideos = new ArrayList<>();

    public static  ArrayList<OtherUsersProfileModel.userPostVideo> otherUserPostVideos = new ArrayList<>();
    public static  ArrayList<OtherUsersProfileModel.userLikeVideo> otherUserLikeVideos = new ArrayList<>();
    public static  String otherUserId="otherUserId";
    public static  String GROUP_ID="GROUP_ID";

    /*Firebase*/
    public static final String FIREBASE_AUTH="AAAA4a6RdLg:APA91bFTfcwFKvWTE-eXCpPY_ooI5KK5PrCPBQUe2iESCUcAsnBIk3LYfIZXyLXSJc6AgfxpnDPLNTIhyuBc9MFjnIlL1Jm2sZoi6d6qBte5L6aC_gVmxsGQ5e1QGSOcKW33a-gQDJLF";
    public static final String TBL_USER="Users";
    public static final String TBL_MESSAGES="Messages";
    public static final String TBL_RECENT="Recent";
    public static final String TBL_TYPING="Typing";
    public static final String STORAGE_IMAGE = "UserProfilePictures/";
    public static final String STORAGE_CHAT_PICTURE = "ChatPictures/";
    public static final String STORAGE_CHAT_VIDEOS = "ChatVideos/";
    public static final String STORAGE_CHAT_VIDEOS_THUMBS = "ChatVideosThumbs/";
    public static final String STORAGE_CHAT_AUDIOS = "ChatAudios/";
    public static final String STORAGE_DEVICE_TOKEN = "ChatAudios/";

    public static final String USERID="user_id";
    public static final String USER_NAME="user_name";
    public static final String USER_EMAIL="user_email";
    public static final String USER_PROFILEIMAGE="user_profileimage";
    public static int COUNT_MESSAGE = 0;


    public static UserModel loggedInUser;

    public static int getAppVersionCode(Context context) {
        int versionCode = 1;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            //Crashlytics.logException(e);
        }
        return versionCode;
    }

    public static String getDeviceId(Context context) {
        String deviceid = "";
        try {
            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return TODO;
            }
            deviceid = mTelephonyManager.getDeviceId();
            Log.d("msg", "DeviceImei " + deviceid);
        } catch (Exception e) {
            e.printStackTrace();
           // Crashlytics.logException(e);
        }
        return deviceid;
    }

    public static String getDeviceIMEI(Context context) {
        String deviceUniqueIdentifier = null;
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return TODO;
            }
            deviceUniqueIdentifier = tm.getDeviceId();
        }
        if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
            deviceUniqueIdentifier = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return deviceUniqueIdentifier;
    }
}
