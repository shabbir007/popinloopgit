package com.example.talentogram.others;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.talentogram.R;
import com.example.talentogram.chat.helper.DialogAlert;
import com.example.talentogram.chat.model.Messages;
import com.example.talentogram.chat.model.Recent;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressCustom;
import cc.cloudist.acplibrary.ACProgressFlower;
import cc.cloudist.acplibrary.ACProgressPie;
import cn.pedant.SweetAlert.SweetAlertDialog;
public class Utils {
    public static DecimalFormat towDecimalFormat = new DecimalFormat("#.00");
    public static long elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds;
    public static SweetAlertDialog sweetAlertDialog1;
    public static  String DateOnly = "";
   // public static ACProgressFlower dialog1;
   public static Dialog dialog1;
    private static final int BUFFER_SIZE = 4096;


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;

    }
    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            DialogAlert.show_dialog(context, context.getResources().getString(R.string.internet_msg));
            return false;
        } else
            return true;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
    public static String convertToTwoDecimal(String value) {
        try {
            return towDecimalFormat.format(Double.valueOf(value));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return value;
        }
    }
    public static void showWarn(Context context, String msg) {
        showWarn(context, msg, 3);
    }

    public static void showWarn(Context context, String msg, int type) {
        String title;
        int t;
        switch (type) {
            case 0:
                t = SweetAlertDialog.NORMAL_TYPE;
                title = "Information";
                break;
            case 1:
                t = SweetAlertDialog.ERROR_TYPE;
                title = "Error";
                break;
            case 2:
                t = SweetAlertDialog.SUCCESS_TYPE;
                title = "Success";
                break;
            default://3
                t = SweetAlertDialog.WARNING_TYPE;
                title = "Warning";
                break;
        }
        new SweetAlertDialog(context, t)
                .setTitleText(title)
                .setContentText(msg)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    public static void hidePopup() {

        if (sweetAlertDialog1 != null && sweetAlertDialog1.isShowing()) {
            sweetAlertDialog1.dismissWithAnimation();
        }


    }

    public static void showPopup(Context ct) {

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ct,
                SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Please Wait")
                .setContentText("Loading...");

        sweetAlertDialog.show();

    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showAlert(String msg, final Context ctx) {
        try {
            new AlertDialog.Builder(ctx)
                    .setMessage(msg)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void startCropImageActivity(Uri imageUri,Context context) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start((Activity) context);
    }
    public static boolean isValidEmail(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isInvalidEmail(CharSequence target) {
        return target == null || !Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

    }

    public static String getOSVersion() {
        return Build.VERSION.RELEASE;
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static void getTimeDiff(String scheduled_at) {
        String timme = scheduled_at;//2019-12-19 17:30:00
        Date date = null;
        try {
            date = MyConstants.sdf.parse(timme);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        Date today = new Date();
        String today_date = MyConstants.sdf.format(today);
        String scheduled_time = MyConstants.sdf.format(date);

        Date d1 = null;
        Date d2 = null;
        try {
            d1 = MyConstants.sdf.parse(today_date);
            d2 = MyConstants.sdf.parse(scheduled_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long different = d2.getTime() - d1.getTime();


        System.out.println("different : " + today_date);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        elapsedHours = different / hoursInMilli;

        different = different % hoursInMilli;

        elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        elapsedSeconds = different / secondsInMilli;

     /*   System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);*/
    }

     public static String getdate (String scheduled_at)
     {
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         Date testDate = null;
         try {
             testDate = sdf.parse(scheduled_at);
         }catch(Exception ex){
             ex.printStackTrace();
         }
         SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
         DateOnly = formatter.format(testDate);
         return DateOnly;

     }
    public static String timeConversion(long value) {
        String songTime;
        int dur = (int) value;
        int hrs = (dur / 3600000);
        int mns = (dur / 60000) % 60000;
        int scs = dur % 60000 / 1000;

        if (hrs > 0) {
            songTime = String.format("%02d:%02d:%02d", hrs, mns, scs);
        } else {
            songTime = String.format("%02d:%02d", mns, scs);
        }
        return songTime;
    }

    public static String getRandomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    androidx.appcompat.app.AlertDialog.Builder alertBuilder = new androidx.appcompat.app.AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    androidx.appcompat.app.AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public static void hideSoftKeyboard(Activity mActivity) {
        try {
            View view = mActivity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static  Bitmap getBitmapFromURL(String src) {
        try {
            System.out.printf("src=+=", src);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            System.out.printf("Bitmap", "returned");
            myBitmap = Bitmap.createScaledBitmap(myBitmap, 100, 100, false);//This is only if u want to set the image size.
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.printf("Exception", e.getMessage());
            return null;
        }
    }
   /* public static void showProgressDialog(Context context, String msg) {
        try {
            if (dialog1 == null) {
               dialog1 = new ACProgressFlower.Builder(context)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                       .themeColor(Color.WHITE)
                       .fadeColor(Color.DKGRAY).build();

                dialog1.setCancelable(true);
                dialog1.setCanceledOnTouchOutside(true);

            }
            if (!dialog1.isShowing())
                dialog1.show();

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public static void showProgressDialog(Context context, String msg)  {
        try {
            if (dialog1 != null && dialog1.isShowing()) {
                dialog1.cancel();
            }
            if (dialog1 == null) {
                dialog1 = new Dialog(context);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog1.setCancelable(false);
                dialog1.setContentView(R.layout.progressbar_loader);
            }

            if (!dialog1.isShowing())
                dialog1.show();

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void showProgressDialog(Context context) {
        showProgressDialog(context,"Please wait...");
    }
    public static void hideProgressDialog() {
        try {
            if (dialog1 != null) {
                if (dialog1.isShowing()) {
                    dialog1.dismiss();
                    dialog1 = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createGroupId(String userid, String userid1) {

        String strSort = Utils.sortString(userid + "" + userid1);
        String md5 = Utils.convertStringIntoMD5(strSort);
        return md5;
    }
    public static String sortString(String input) {
        char[] charArray = input.toCharArray();
        Arrays.sort(charArray);
        return new String(charArray);

    }
    public static final String convertStringIntoMD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
    public static void sortMessageConversation(ArrayList<Messages> arrMessage) {
        Collections.sort(arrMessage, new Comparator<Messages>() {
            @Override
            public int compare(Messages messageModel, Messages t1) {
                if (messageModel.getCreatedAt() > t1.getCreatedAt()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });

    }

    public static boolean checkValidResult(Context context, int resultCode) {
        if (resultCode == ((Activity) context).RESULT_OK) {
            return true;
        } else if (resultCode == ((Activity) context).RESULT_CANCELED) {
            return false;
        } else {
            return false;

        }
    }
    public static String randomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 20) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    public static String getMimeExtension(String url) {

        String tempUrl = url.replaceAll("[^a-zA-Z0-9\\.\\/]", ""); //ignore all special characters and blank space excepting dot and slash

        String extension = MimeTypeMap.getFileExtensionFromUrl(tempUrl);

        if (extension == null) {
            if (url.lastIndexOf(".") != -1)
                extension = url.substring(url.lastIndexOf(".") + 1);
        }

        return extension;
    }
    private void getDropboxIMGSize(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

    }
    public static File getThumbImage(Calendar cal, String picturePath) {
        String extension = Utils.getMimeExtension(picturePath);

        File originalFile = null;
        try {
            Uri file = Uri.fromFile(new File(picturePath));
            final String type = file.toString().replace(
                    file.toString().replaceFirst("[.][^.]+$", ""), "");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(new File(file.getPath()).getAbsolutePath(), options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;

            Bitmap bm = null;
            //bm = BitmapFactory.decodeFile(picturePath);
            //bm = scaleDown(bm, 800, true);
            bm = decodeFile(new File(picturePath), imageWidth, imageWidth);
            String extStorageDirectorytemp = Environment.getExternalStorageDirectory().toString() + File.separator + "Talentogram";
            File extStorageDirectoryFile = new File(extStorageDirectorytemp);

            String extStorageDirectory = Environment.getExternalStorageDirectory().toString() + File.separator +"Talentogram" + File.separator + "Images";
            File extStorageDirectoryImage = new File(extStorageDirectory);

            if (!extStorageDirectoryFile.exists()) {
                extStorageDirectoryFile.mkdir();
                if (!extStorageDirectoryImage.exists()) {
                    extStorageDirectoryImage.mkdir();
                }
            } else {
                if (!extStorageDirectoryImage.exists()) {
                    extStorageDirectoryImage.mkdir();
                }
            }

            ExifInterface ei = new ExifInterface(picturePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bm, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bm, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bm, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bm;
            }

            originalFile = new File(extStorageDirectory,
                    String.valueOf(cal.getTimeInMillis()) + "." + extension);
            FileOutputStream outStream = new FileOutputStream(originalFile);
            if (extension.equals("jpeg") || extension.equals("jpg")) {
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            } else {
                rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            }
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }

        return originalFile;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        } catch (OutOfMemoryError e) {
        }
        return null;
    }

    public static Boolean isStringNull(String nullableString) {
        Boolean result = true;
        if (nullableString != null && !nullableString.equals("null") && !nullableString.equals("N/A") && !nullableString.equals("0") && !nullableString.equals("0.0") && !nullableString.equals("") && !nullableString.equals("[ ]") && !nullableString.equals("[]") && !nullableString.equals("{}") && nullableString != null) {
            result = false;
        }
        return result;
    }
    public static File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "caches"); //NON-NLS
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            return null;
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date()); //NON-NLS
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg"); //NON-NLS NON-NLS
    }
    public static String convertTimestampToString(long time, String format) {
        String strTime = "";
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        strTime = DateFormat.format(format, cal).toString();
        return strTime;
    }
    public static String getMessageTimeInChat(long createTime) {
        String messageTime = "";
        try {
            Date startDate = new Date();
            Date CurrentDate = new Date();

            startDate.setTime(createTime);
            CurrentDate.setTime(System.currentTimeMillis());

            long timeDiff = CurrentDate.getTime() - startDate.getTime();
            if (timeDiff > 0) {

                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;
                long daysInMilli = hoursInMilli * 24;
                long monthInMilli = daysInMilli * 31;
                long yearInMilli = monthInMilli * 12;

                long year = timeDiff / yearInMilli;
                timeDiff = timeDiff % yearInMilli;

                long months = timeDiff / monthInMilli;
                timeDiff = timeDiff % monthInMilli;

                long days = timeDiff / daysInMilli;
                timeDiff = timeDiff % daysInMilli;


                int week = (int) (days / 7);

                if (year != 0) {
                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(createTime);
                    messageTime = DateFormat.format("MMM dd, h:mma", cal).toString();
                } else if (months != 0) {
                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(createTime);
                    messageTime = DateFormat.format("MMM dd, h:mma", cal).toString();
                } else if (week != 0 && week <= 4) {
                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(createTime);
                    messageTime = DateFormat.format("MMM dd, h:mma", cal).toString();
                } else if (days != 0) {
                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(createTime);
                    messageTime = DateFormat.format("EEEE", cal).toString();
                } else {
                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(createTime);
                    messageTime = DateFormat.format("h:mma", cal).toString();
                }
                messageTime.replace("AM", "am").replace("PM", "pm");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageTime;
    }
    public static String saveToInternalStorage(Context context, Bitmap bitmapImage, String url) {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        File mypath = new File(directory, url.hashCode() + ".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 80, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }
    public static File getThumb(Context mContext, String path) {
        File file = null;
        try {
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(path,
                    MediaStore.Images.Thumbnails.MINI_KIND);
            Matrix matrix = new Matrix();
            Bitmap bmThumbnail = Bitmap.createBitmap(thumb, 0, 0, thumb.getWidth(), thumb.getHeight(),
                    matrix, true);
            //create a file to write bitmap data
            file = new File(mContext.getCacheDir(), "thumb_" + path.hashCode() + ".jpg");
            file.createNewFile();

//Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bmThumbnail.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;

    }

    public static void downloadAudio(String fileURL, String saveDir)
            throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileURL.hashCode() + ".mp3";

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.close();
            inputStream.close();

            System.out.println("File downloaded");
        } else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();
    }

    public static void downloadFile(String fileURL, String saveDir)
            throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileURL.hashCode() + ".Mp4";

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();

            System.out.println("File downloaded");
        } else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {

            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                //Read byte from input stream

                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;

                //Write byte from output stream

                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static void sortRecentChatList(ArrayList<Recent> arrRecentList) {
        Collections.sort(arrRecentList, new Comparator<Recent>() {
            @Override
            public int compare(Recent messageModel, Recent t1) {
                if (messageModel.getCreatedAt() > t1.getCreatedAt()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });

    }

    public static boolean isNumeric(String strNum){
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    @SuppressLint("NewApi")
    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {


            if (isGooglePhotosUri(uri)) {
                return uri.getLastPathSegment();
            }

            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
}
