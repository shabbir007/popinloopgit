package com.example.talentogram.network;

public class Services {
    public static String DEVICE_ID = "";

    //public static String Authorization = "";
    public static String Authorization = "Basic YWRtaW46QVBJQEtSVVBBQ0FCUyEjJFdFQiQ=";
    static String X_API_KEY = "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss";//X-API-KEY

   // public static String BASE_URL = "http://beta.talentogram.in/";
    public static final String BASE_URL = "https://talentogram.co.in/";
    private static final String MAIN_URL = BASE_URL + "api/";

    public static String REGISTER = MAIN_URL + "DoRegister";
    public static final String  LOGIN = MAIN_URL + "DoLogin";
    public static String FORGET_PASS = MAIN_URL + "ForgotPassword";
    public static String LOGOUT = MAIN_URL + "logout";
    public static String SOCIAL_LOGIN = MAIN_URL + "SocialLogin";
    public static String GETUSERPROFILE = MAIN_URL + "GetUserProfile";
    public static String EDITUSERPROFILE = MAIN_URL + "EditProfile";
    public static String GET_PAID_CONTEST = MAIN_URL + "GetPaidContest";
    public static String GET_FREE_CONTEST = MAIN_URL + "GetFreeContest";
    public static String PENDING_FOLLOW_REQUEST = MAIN_URL + "PendingFollowRequest";
    public static String ACCEPT_FOLLOW_REQUEST = MAIN_URL + "AcceptFollowRequest";
    public static String ADDPOST = MAIN_URL + "AddPost";
    public static String FOLLOWER_LIST = MAIN_URL + "FollowersList";
    public static String FOLLOWING_LIST = MAIN_URL + "FollowingList";
    public static String DOFOLLOW = MAIN_URL + "Dofollow";
    public static String GETPOST = MAIN_URL + "GetPost";
    public static String UNFOLLOW = MAIN_URL + "Unfollow";
    public static String LIKE_UNLIKE = MAIN_URL + "LikeUnlikePost";
    public static String COMMENT_POST = MAIN_URL + "CommentPost";
    public static String COMMENT_LIST = MAIN_URL + "CommentList";
    public static String EDIT_COMMENT = MAIN_URL + "EditComment";
    public static String DELETE_COMMENT = MAIN_URL + "DeleteComment";
    public static String APPLY_FOR_FREE_CONTEST = MAIN_URL + "ApplyForFreeContest";
    public static String APPLY_FOR_PAID_CONTEST = MAIN_URL + "ApplyforPaidContest";
    public static String GET_USER_OWN_POST_VIDEO = MAIN_URL + "GetUserOwnPost";
    public static String CHANGE_PASSWORD = MAIN_URL + "ChangePassword";
    public static String GET_ALL_VIDEOS = MAIN_URL + "GetAllVideos";
    public static String GET_OTHER_USER_PROFILE = MAIN_URL + "ViewOtherUserProfile";
    public static String USER_OWN_LIKE_POST = MAIN_URL + "UserOwnLikePost";
    public static String BLOCK_USER = MAIN_URL + "DoBlockUser";
    public static String HIDE_POST = MAIN_URL + "DoHidePost";
    public static String UNHIDE_POST = MAIN_URL + "UnHidePost";
    public static String REPORT_POST = MAIN_URL + "ReportPost";
    public static String SAVE_POST = MAIN_URL + "DoSavePost";
    public static String DELETE_ACCOUNT = MAIN_URL + "DeleteAccount";
    public static String BLOCK_USER_LIST = MAIN_URL + "BlockUserList";
    public static String GET_NOTIFICATION = MAIN_URL + "GetAllNotification";
    public static String GET_ALL_CONTEST_DETAILS = MAIN_URL + "GetAllContestDetails";
    public static String GET_HIDDENPOST = MAIN_URL + "GetHidePost";
    public static String GET_SAVEDPOST = MAIN_URL + "GetSavePost";
    public static String UNSAVEPOST = MAIN_URL + "UnSavePost";
    public static String GET_REPORTED_POST = MAIN_URL + "GetReportPost";
    public static String DO_BACK_REPORTED_POST = MAIN_URL + "UnReportPost";
    public static String ALL_REQUEST_SENT= MAIN_URL + "ListOfAllFollowingRequest";
    public static String CANCEL_REQUEST= MAIN_URL + "CancelPendingFollowingRequest";
    public static String GET_POST_DETAIL= MAIN_URL + "GetPostDetail/";

    public static final String CHECK_BALANCE= MAIN_URL + "AvailBalance";
    public static final String CREDIT_WALLET= MAIN_URL + "CreditWallet";
    public static final String ADD_BANK_ACCOUNT= MAIN_URL + "AddBankAccount";
    public static final String TRANSFER_AMT= MAIN_URL + "TransferToBankAccount";
    public static final String WALLET_HISTORY= MAIN_URL + "getHistory";



}
