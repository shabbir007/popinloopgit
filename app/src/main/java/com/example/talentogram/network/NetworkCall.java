package com.example.talentogram.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.Utils;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressWarnings("deprecation")
public class NetworkCall {

    private Context context;
    private AppCompatActivity mActivity;
    private String url, req_type;
    private HashMap<String, File> postFiles;
    private HashMap<String, String> postData;
    private MultiPartRequest request_obj;
    private Boolean isShow, isHeaderswithAuth;
    //private SweetAlertDialog sweetAlertDialog;
    private ProgressDialog mProgressPopup;


    public static String REQ_GET = "GET";
    public static String REQ_POST = "POST";
    public static String REQ_PUT = "PUT";
    public static String REQ_DELETE = "DELETE";

    public NetworkCall(Context context, String url,
                       HashMap<String, String> postData,
                       HashMap<String, File> postFiles,
                       Boolean isShow, String req_type,
                       Boolean isHeaderswithAuth, MultiPartRequest request) {
        this.context = context;
        mActivity = (AppCompatActivity) context;
        this.url = url;
        this.postFiles = postFiles;
        this.postData = postData;
        this.request_obj = request;
        this.isShow = isShow;
        this.req_type = req_type;
        this.isHeaderswithAuth = isHeaderswithAuth;

        if (isNetworkAvailable()) {
            showPopup();
            sendData();
        } else
            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
    }

    public NetworkCall(Context context, String url,
                       HashMap<String, String> postData,
                       Boolean isShow, String req_type,
                       Boolean isHeaderswithAuth, MultiPartRequest request) {
        this.context = context;
        mActivity = (AppCompatActivity) context;
        this.url = url;
        this.postFiles = new HashMap<>();
        this.postData = postData;
        this.request_obj = request;
        this.isShow = isShow;
        this.req_type = req_type;
        this.isHeaderswithAuth = isHeaderswithAuth;

        if (isNetworkAvailable()) {
            showPopup();
            sendData();
        } else
            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
    }

    public NetworkCall(Context context, String url,
                       Boolean isShow, String req_type,
                       Boolean isHeaderswithAuth,
                       MultiPartRequest request) {
        this.context = context;
        mActivity = (AppCompatActivity) context;
        this.url = url;
        this.postFiles = new HashMap<>();
        this.postData = new HashMap<>();
        this.request_obj = request;
        this.isShow = isShow;
        this.req_type = req_type;
        this.isHeaderswithAuth = isHeaderswithAuth;

        if (isNetworkAvailable()) {
            showPopup();
            sendData();
        } else
          Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();

    }

    private boolean isNetworkAvailable() {
        if(context!= null)
        {ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null)
            return false;
        else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            for (NetworkInfo anInfo : info)
                if (anInfo.getState() == NetworkInfo.State.CONNECTED)
                    return true;
        }}
        return false;

    }

    private void sendData() {

        url = url.replaceAll(" ", "%20");

        ArrayList<String> putData = new ArrayList<>();

        Log.e("Url==>", url);
        List<Part> data = new ArrayList<>();
        for (String key : postData.keySet()) {
            String val = postData.get(key);
            data.add(new StringPart(key,
                    val == null ? "" : val));
            putData.add(key + "=" + val);

            Log.e("postData", key + "==>" + val);
        }

            List<Part> files = new ArrayList<>();
            for (String key : postFiles.keySet()) {
                File val = postFiles.get(key);
                if (val != null) {
                    files.add(new FilePart(key, val));

                    Log.e("postFiles", key + "==>" + val.getAbsolutePath());
                }
            }

        if (req_type.equals(REQ_PUT))
            callPut(TextUtils.join("&", putData));

        else if (req_type.equals(REQ_DELETE))
            callDelete(TextUtils.join("&", putData));
        else
            setData(data, files);

    }

    @SuppressWarnings("ConstantConditions")
    private void callPut(String putData) {
        putData = putData.replaceAll(" ", "%20");
        Log.e("putData===>", putData);
        String auth = new MySharedPref(context).getString(context, MySharedPref.USER_AUTH, "");
        Log.e("Authorization===>", auth);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, putData);

        Request request = new Request.Builder()
                .url(url)
                .method("PUT", body)
                //.post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Authorization", "Bearer " + auth)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                request_obj.myResponseMethod("");
                if (isShow)
                    //mProgressPopup.dismiss();
                    Utils.hideProgressDialog();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                String s = "";
                try {
                    s = response.body().string();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("response ==>", s + "_");
                request_obj.myResponseMethod(s);
                if (isShow)
                   // mProgressPopup.dismiss();
                    Utils.hideProgressDialog();
            }
        });
    }

    private void callDelete(String putData) {
        putData = putData.replaceAll(" ", "%20");

        Log.e("putData===>", putData);

        String auth = new MySharedPref(context).getString(context, MySharedPref.USER_AUTH, "");

        Log.e("Authorization===>", auth);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, putData);

        Request request = new Request.Builder()
                .url(url)
                .method("DELETE", body)
                //.post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Authorization", "Bearer " + auth)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                request_obj.myResponseMethod("");
                if (isShow)
                   // mProgressPopup.dismiss();
                    Utils.hideProgressDialog();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                String s = "";
                try {
                    s = response.body().string();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("response ==>", s + "_");
                request_obj.myResponseMethod(s);
                if (isShow)
                    //mProgressPopup.dismiss();
                    Utils.hideProgressDialog();
            }
        });
    }

    private void setData(List<Part> data, List<Part> files) {

        Log.e("data-->=", String.valueOf(data));


        Builders.Any.B ion = Ion.with(context).load(req_type, url);

        if (req_type.equals(REQ_PUT)) {
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            ion.addHeader("Content-Type", "application/x-www-form-urlencoded");

            for (String key : postData.keySet()) {
                String val = postData.get(key);
                ion.setBodyParameter(key, val == null ? "" : val);

            }
        }

        String auth = new MySharedPref(context).getString(context, MySharedPref.USER_AUTH, "");


        if (isHeaderswithAuth) {
            Log.e("Authorization===>", auth);

            ion.addHeader("X-Requested-With", "XMLHttpRequest");

            if (!auth.equals(""))
                ion.addHeader("Authorization", "Bearer " + auth);
        } else {
            ion.addHeader("X-Requested-With", "XMLHttpRequest");

        }


        if (files.size() != 0 && data.size() != 0)
            ion.addMultipartParts(files).addMultipartParts(data);
        else if (files.size() != 0)
            ion.addMultipartParts(files);
        else if (data.size() != 0)
            ion.addMultipartParts(data);

        ion.asString().setCallback((e, result) -> {
            try {
                Log.e("response ==>", result + "_");
                request_obj.myResponseMethod(result);

            } catch (Exception e1) {
                e1.printStackTrace();
                request_obj.myResponseMethod("");

            } finally {
                if (isShow)
                 //   mProgressPopup.dismiss();
                    Utils.hideProgressDialog();
            }
        });
    }

    private void showPopup() {
        mProgressPopup = new ProgressDialog(mActivity);

        if (isShow) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //mProgressPopup.show();
                    Utils.showProgressDialog(mActivity);
                }
            });
        }
    }

    public interface MultiPartRequest {
        void myResponseMethod(String response);
    }
}
