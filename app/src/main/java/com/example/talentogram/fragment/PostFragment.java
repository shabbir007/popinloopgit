package com.example.talentogram.fragment;


import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.danikula.videocache.HttpProxyCacheServer;
import com.example.talentogram.R;
import com.example.talentogram.SegmentProgress.Variables;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.gallery.GalleryActivity;
import com.example.talentogram.adapter.PostAdapter;
import com.example.talentogram.custom_views.CustomEditText;
import com.example.talentogram.helper.JsonParserUniversal;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.model.PostModel;
import com.example.talentogram.others.VerticalSpacingItemDecorator;
import com.example.talentogram.profile.ProfileFragment2;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;

import static com.example.talentogram.activity.DashBoardActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 * ++---------------------
 */
public class PostFragment extends Fragment implements Player.EventListener {
    private CircularImageView ivUserProfile;
    private CustomEditText edtusername;
    private ImageView poopinloop;
    private RecyclerView rvPost;
    private View view;
    private ImageView ivGallery;

    private PostAdapter postAdapter;
    private ArrayList<PostModel> postList;
    private ArrayList<PostModel> postVideoList;
    public JsonParserUniversal jParser;
    boolean is_user_stop_video = false, isReloadList = false;
    int currentPage = -1;
    LinearLayoutManager layoutManager;
    SwipeRefreshLayout swiperefresh;

    public PostFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_post, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(getActivity());
        jParser = new JsonParserUniversal();
        initView();
        getUserProfile();
        bindWidgetEvent();

        swiperefresh = view.findViewById(R.id.swiperefresh);
        swiperefresh.setProgressViewOffset(false, 0, 200);

        swiperefresh.setColorSchemeResources(R.color.Black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = -1;
              getPost();
            }
        });
        return view;
    }

    private void bindWidgetEvent() {
        ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GalleryActivity.class);
                Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getActivity(), R.anim.animstart, R.anim.animation2).toBundle();
                startActivity(intent, bndlanimation);
            }
        });
        ivUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.changeFragment(getContext(), new ProfileFragment2(), true);
            }
        });
        rvPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (isReloadList && newState == 0) {
                    isReloadList = false;
                    currentPage = -1;
                    setData();

                }

                System.out.println("onScrollStateChanged-----" + newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                final int height = recyclerView.getHeight();

                int page_no = layoutManager.findLastCompletelyVisibleItemPosition();

                if (page_no != currentPage && page_no > 0) {
                    currentPage = page_no;
                    releasePreviousPlayer();
                    if (postList.get(currentPage).getPostType() == 1) {
                        Set_Player(currentPage);
                    }
                }

                System.out.println("page_no-----" + page_no);
                System.out.println("currentPage-----" + currentPage);
            }
        });

    }

    private void getPost() {
        new NetworkCall(getActivity(), Services.GETPOST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        JSONArray jPostData = obj.getJSONArray("data");

                        if (Success) {
                            swiperefresh.setRefreshing(false);
                            postList = new ArrayList<>();
                            postList = (ArrayList<PostModel>) new Gson().fromJson(jPostData.toString(),
                                    new TypeToken<ArrayList<PostModel>>() {
                                    }.getType());
                            postList.size();

                            setData();

                        } else {
                            Toast.makeText(getActivity(), R.string.blank_no_data_found, Toast.LENGTH_SHORT).show();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void initView() {
        ivUserProfile = view.findViewById(R.id.ivUserProfile);
        edtusername = view.findViewById(R.id.edtusername);
        poopinloop = view.findViewById(R.id.poopinloop);
        rvPost = view.findViewById(R.id.rv_post);
        ivGallery = view.findViewById(R.id.ivGallery);

    }

    private void setData() {
        swiperefresh.setRefreshing(false);
        layoutManager = new LinearLayoutManager(getContext());
        rvPost.setLayoutManager(layoutManager);
        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(10);
        rvPost.addItemDecoration(itemDecorator);
        postAdapter = new PostAdapter(getContext(), postList);
        rvPost.setAdapter(postAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (postList.get(0).getPostType() == 1) {
                    currentPage = 0;
                    Set_Player(0);
                    swiperefresh.setRefreshing(false);
                }
            }
        }, 1000);
    }

    private void getUserProfile() {
        String userId = new MySharedPref(getActivity()).getLoggedInUserID();
        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("UserProfileId", userId);

        new NetworkCall(getContext(), Services.GETUSERPROFILE,
                userdata, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");

                        if (Success) {
                            JSONObject user_data = obj.getJSONObject("user_data");
                            String FirstName = user_data.getString("FirstName");
                            String LastName = user_data.getString("LastName");
                            String Profile = user_data.getString("ProfilePic");

                            MySharedPref.setString(getActivity(), MySharedPref.USER_FIRSTNAME, FirstName);
                            MySharedPref.setString(getActivity(), MySharedPref.USER_LASTNAME, LastName);
                            MySharedPref.setString(getActivity(), MySharedPref.USER_PROFILE_PIC, Profile);

                            Glide.with(getContext())
                                    .load(Profile)
                                    .thumbnail(Glide.with(getContext()).load(Profile))
                                    .error(R.drawable.noimage)
                                    .into(ivUserProfile);
                            getPost();

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        toolbar.setVisibility(View.VISIBLE);

    }

    public void Call_cache() {
        if ((currentPage) < postList.size()) {
            HttpProxyCacheServer proxy = getProxy(getContext());
            proxy.getProxyUrl(postList.get(currentPage).getVideoUrl().get(0).getVideoUrl());
        }
    }

    private HttpProxyCacheServer proxy;

    public HttpProxyCacheServer getProxy(Context context) {
        return proxy == null ? (proxy = newProxy()) : proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(getContext())
                .maxCacheSize(1024 * 1024 * 1024)
                .maxCacheFilesCount(20)
                .build();
    }

    public SimpleExoPlayer privious_player;

    public void releasePreviousPlayer() {
        if (privious_player != null) {
            privious_player.removeListener(this);
            privious_player.release();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (privious_player != null) {
            privious_player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (privious_player != null) {
            privious_player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (privious_player != null) {
            privious_player.release();
        }
    }

    boolean is_visible_to_user;

    @Override
    public void setMenuVisibility(boolean isVisibleToUser) {
        super.setMenuVisibility(isVisibleToUser);
        is_visible_to_user = isVisibleToUser;
        if (privious_player != null && (isVisibleToUser && !is_user_stop_video)) {
            privious_player.setPlayWhenReady(true);

        } else if (privious_player != null && !isVisibleToUser) {
            privious_player.setPlayWhenReady(false);
        }
        Log.e("Resume", "=====");
    }

    // this will call when swipe for another video and
    // this function will set the player to the current video
    public void Set_Player(final int currentPage) {

        final PostModel item = postList.get(currentPage);
        Call_cache();

        HttpProxyCacheServer proxy = getProxy(getContext());
        String proxyUrl = proxy.getProxyUrl(item.getVideoUrl().get(0).getVideoUrl());

        DefaultLoadControl loadControl = new DefaultLoadControl.Builder().
                setBufferDurationsMs(1 * 1024, 1 * 1024,
                        500, 1024).createDefaultLoadControl();

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector, loadControl);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), getContext().getResources().getString(R.string.app_name)));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(proxyUrl));

        Log.d(Variables.tag, item.getVideoUrl().get(0).getVideoUrl());
        Log.d(Variables.tag, proxyUrl);

        player.prepare(videoSource);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.addListener(this);
        final PlayerView playerView;
        if (layoutManager != null) {
            View layout = layoutManager.findViewByPosition(currentPage);
            if (layout != null) {
                playerView = layout.findViewById(R.id.playerview);
                playerView.setPlayer(player);
                player.setPlayWhenReady(is_visible_to_user);
                privious_player = player;

                final RelativeLayout mainlayout = layout.findViewById(R.id.mainlayout);

                playerView.setOnTouchListener(new View.OnTouchListener() {
                    private GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
                        @Override
                        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                            super.onFling(e1, e2, velocityX, velocityY);
                            float diffY = e2.getY() - e1.getY();
                            float diffX = e2.getX() - e1.getX();
                            if (Math.abs(diffX) > Math.abs(diffY)) {
                                if (Math.abs(diffX) > 100 && Math.abs(velocityX) > 1000) {
                                    if (diffX > 0) {
                                        if (player.getPlayWhenReady()) {
                                            is_user_stop_video = true;
                                            privious_player.setPlayWhenReady(false);
                                        }
                                    }
                                }
                            }
                            return true;
                        }

                        @Override
                        public boolean onSingleTapConfirmed(MotionEvent e) {
                            if (!player.getPlayWhenReady()) {
                                is_user_stop_video = false;
                                privious_player.setPlayWhenReady(true);

                            } else {
                                is_user_stop_video = true;
                                privious_player.setPlayWhenReady(false);
                            }
                            return super.onSingleTapConfirmed(e);
                        }

                        @Override
                        public boolean onSingleTapUp(MotionEvent e) {
                            super.onSingleTapUp(e);
                            return true;
                        }

                        @Override
                        public void onLongPress(MotionEvent e) {
                            super.onLongPress(e);

                        }

                    });

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        gestureDetector.onTouchEvent(event);
                        return true;
                    }
                });

            }
        }
    }

}
