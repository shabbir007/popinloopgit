package com.example.talentogram.fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.adapter.NotificationAdapter;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.helper.JsonParserUniversal;
import com.example.talentogram.model.NotificationModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.talentogram.activity.DashBoardActivity.drawer;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    private View view;
    private RecyclerView rv_notification;
    private ArrayList<NotificationModel> notificationArrayList = new ArrayList<>();
    private ArrayList<NotificationModel> arrModel = new ArrayList<>();

    private NotificationAdapter notificationAdapter;
    private RelativeLayout llNotification;
    private Dialog dialog;
    private ImageView ivBack;
    private JsonParserUniversal jsonParserUniversal;
    CustomTextView tvNoDataFound;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        setHasOptionsMenu(true);
        jsonParserUniversal = new JsonParserUniversal();
        bindReference();
        bindWidgetEvent();
        clickEvents();
        getAllPendingRequest();
        setData();
        return view;


    }

    private void bindWidgetEvent() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
    }

    private void clickEvents() {
        llNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(getActivity(), R.style.ChatDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_notification);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());

                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.TOP;
                window.setAttributes(lp);

                dialog.findViewById(R.id.llAll).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notificationArrayList.clear();
                        arrModel.clear();
                        getAllPendingRequest();
                        notificationAdapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.llLikes).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notificationArrayList.clear();
                        if (arrModel.size() > 0) {
                            for (NotificationModel d : arrModel) {
                                if (d.getNotification_type() == 1) {
                                    NotificationModel notificationModel = new NotificationModel();
                                    notificationModel.setNotification_type(d.getNotification_type());
                                    notificationModel.setPostUrl(d.getPostUrl());
                                    notificationModel.setSenderProfilePicUrl(d.getSenderProfilePicUrl());
                                    notificationArrayList.add(notificationModel);
                                    notificationAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.llComment).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notificationArrayList.clear();
                        if (arrModel.size() > 0) {
                            for (NotificationModel d : arrModel) {
                                if (d.getNotification_type() == 2) {
                                    NotificationModel notificationModel = new NotificationModel();
                                    notificationModel.setNotification_type(d.getNotification_type());
                                    notificationModel.setPostUrl(d.getPostUrl());
                                    notificationModel.setSenderProfilePicUrl(d.getSenderProfilePicUrl());
                                    notificationArrayList.add(notificationModel);
                                    notificationAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.llFollows).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notificationArrayList.clear();
                        if (arrModel.size() > 0) {
                            for (NotificationModel d : arrModel) {
                                if (d.isIfFriendRequest()) {
                                    if (!notificationArrayList.contains(d)) {
                                        notificationArrayList.add(d);
                                    }
                                    notificationAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    private void bindReference() {
        rv_notification = view.findViewById(R.id.rv_notification);
        llNotification = view.findViewById(R.id.llNotification);
        ivBack = view.findViewById(R.id.ivBack);
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);
    }

    private void getAllPendingRequest() {
        new NetworkCall(getContext(), Services.PENDING_FOLLOW_REQUEST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");

                    if (Success) {
                        JSONArray jNotificationList = jsonObject.getJSONArray("data");

                        List<NotificationModel> list = (ArrayList<NotificationModel>)
                                new Gson().fromJson(jNotificationList.toString(),
                                        new TypeToken<ArrayList<NotificationModel>>() {
                                        }.getType());
                        arrModel.clear();
                        notificationArrayList.clear();
                        for (NotificationModel d : list) {
                            NotificationModel tempModel = new NotificationModel(d.getId(), d.getUserId(), d.getFriendsId(), d.getIsFollowing(), d.getIsActive(),
                                    d.getCreated_at(), d.getUpdated_at(), d.getFollow_user(), true, d.getType(), d.getNotification_type(),
                                    d.getNotifiable_type(), d.getNotifiable_id(), d.getData(), d.getSender_id(), d.getReceiver_id(), d.getSenderProfilePicUrl(), d.getPostUrl(),d.getUserdata());
                            if (!arrModel.contains(tempModel)) {
                                arrModel.add(tempModel);
                                notificationArrayList.add(tempModel);
                            }
                        }
                        getAllNotification();
                        notificationAdapter.notifyDataSetChanged();
                        rv_notification.setVisibility(View.VISIBLE);
                        tvNoDataFound.setVisibility(View.GONE);

                    } else {
                        rv_notification.setVisibility(View.GONE);
                        tvNoDataFound.setVisibility(View.VISIBLE);
                        getAllNotification();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getAllNotification();
                }
            }
        });
    }

    private void getAllNotification() {
        new NetworkCall(getContext(), Services.GET_NOTIFICATION,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");

                    if (Success) {
                        JSONArray jNotificationList = jsonObject.getJSONArray("user_data");
                        List<NotificationModel> list = (ArrayList<NotificationModel>) new Gson().fromJson(jNotificationList.toString(),
                                new TypeToken<ArrayList<NotificationModel>>() {
                                }.getType());
                        for (NotificationModel d : list) {
                            if (d.getNotification_type() != 3) {
                                NotificationModel temp = new NotificationModel(d.getId(), d.getUserId(), d.getFriendsId(), d.getIsFollowing(), d.getIsActive(),
                                        d.getCreated_at(), d.getUpdated_at(), d.getFollow_user(), false, d.getType(), d.getNotification_type(),
                                        d.getNotifiable_type(), d.getNotifiable_id(), d.getData(), d.getSender_id(), d.getReceiver_id(), d.getSenderProfilePicUrl(),
                                        d.getPostUrl(),d.getUserdata());
                                arrModel.add(temp);
                                notificationArrayList.add(temp);
                            }
                        }
                        notificationAdapter.notifyDataSetChanged();
                        rv_notification.setVisibility(View.VISIBLE);
                        tvNoDataFound.setVisibility(View.GONE);

                    } else {
                        rv_notification.setVisibility(View.GONE);
                        tvNoDataFound.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }


            }
        });
    }

    private void setData() {
        notificationAdapter = new NotificationAdapter(getActivity(), notificationArrayList);
        rv_notification.setAdapter(notificationAdapter);
        rv_notification.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        DashBoardActivity.toolbar.setVisibility(View.GONE);
    }
}
