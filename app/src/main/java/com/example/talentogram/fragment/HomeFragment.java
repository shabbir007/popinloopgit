package com.example.talentogram.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.danikula.videocache.HttpProxyCacheServer;
import com.example.talentogram.R;
import com.example.talentogram.SegmentProgress.Variables;

import com.example.talentogram.adapter.HomeAdapter;
import com.example.talentogram.model.HomeModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 */

// this is the main view which is show all  the video in list
public class HomeFragment extends Fragment implements Player.EventListener {
    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private ProgressBar p_bar, mDurationProgress;
    private ArrayList<HomeModel> data_list;
    int currentPage = -1;
    LinearLayoutManager layoutManager;
    SwipeRefreshLayout swiperefresh;
    boolean is_user_stop_video = false, isReloadList = false;

    public HomeFragment() {
    }

    int swipe_count = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getContext();

        p_bar = view.findViewById(R.id.p_bar);
        mDurationProgress = view.findViewById(R.id.duration_progress);

        recyclerView = view.findViewById(R.id.recylerview);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);

        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);

        // this is the scroll listener of recycler view which will tell the current item number
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (isReloadList && newState == 0) {
                    isReloadList = false;
                    currentPage = -1;
                    Set_Adapter();
                }

                System.out.println("onScrollStateChanged-----" + newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                final int height = recyclerView.getHeight();
                int page_no = scrollOffset / height;

                if (page_no != currentPage) {
                    currentPage = page_no;
                    releasePreviousPlayer();
                    Set_Player(currentPage);
                }
                System.out.println("page_no-----" + page_no);
                System.out.println("currentPage-----" + currentPage);
            }
        });

        swiperefresh = view.findViewById(R.id.swiperefresh);
        swiperefresh.setProgressViewOffset(false, 0, 200);

        swiperefresh.setColorSchemeResources(R.color.Black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = -1;
                callApiForGetAllvideos();
            }
        });

        callApiForGetAllvideos();

        return view;
    }

    HomeAdapter adapter;

    public void Set_Adapter() {
        adapter = new HomeAdapter(context, data_list);
        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);
    }

    // Bottom two function will call the api and get all the videos form api and parse the json data
    private void callApiForGetAllvideos() {
        swiperefresh.setRefreshing(false);
        parseData();
    }

    private void parseData() {
        data_list = new ArrayList<>();
        new NetworkCall(getActivity(), Services.GET_ALL_VIDEOS,
                true, NetworkCall.REQ_GET,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        if (Success) {
                            JSONArray jPostData = obj.getJSONArray("data");

                            data_list = (ArrayList<HomeModel>) new Gson().fromJson(jPostData.toString(),
                                    new TypeToken<ArrayList<HomeModel>>() {
                                    }.getType());
                            Collections.reverse(data_list);
                            Set_Adapter();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    // this will call when swipe for another video and
    // this function will set the player to the current video
    public void Set_Player(final int currentPage) {

        final HomeModel item = data_list.get(currentPage);

        Call_cache();

        HttpProxyCacheServer proxy = getProxy(context);
        String proxyUrl = proxy.getProxyUrl(item.post_detail.getPostVideo().get(0).getVideoUrl());

        DefaultLoadControl loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(1 * 1024, 1 * 1024, 500, 1024).createDefaultLoadControl();

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, context.getResources().getString(R.string.app_name)));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(item.post_detail.getPostVideo().get(0).getVideoUrl()));

        Log.d(Variables.tag, item.post_detail.getPostVideo().get(0).getVideoUrl());
        Log.d(Variables.tag, proxyUrl);

        player.prepare(videoSource);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.addListener(this);

        View layout = layoutManager.findViewByPosition(currentPage);
        final PlayerView playerView = layout.findViewById(R.id.playerview);
        playerView.setPlayer(player);

        player.setPlayWhenReady(is_visible_to_user);
        privious_player = player;


        playerView.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    if (!player.getPlayWhenReady()) {
                        is_user_stop_video = false;
                        privious_player.setPlayWhenReady(true);

                    } else {
                        is_user_stop_video = true;
                        privious_player.setPlayWhenReady(false);
                    }
                    return super.onSingleTapConfirmed(e);
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                }

            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });
        swipe_count++;
        if (swipe_count > 6) {
            swipe_count = 0;
        }

        if (currentPage == (data_list.size() - 1))
            isReloadList = true;
    }


    public void Call_cache() {
        if ((currentPage + 1) < data_list.size()) {
            HttpProxyCacheServer proxy = getProxy(context);
            proxy.getProxyUrl(data_list.get(currentPage + 1).getPost_detail().getPostVideo().get(0).getVideoUrl());
        }
    }

    private HttpProxyCacheServer proxy;

    public HttpProxyCacheServer getProxy(Context context) {
        //PlayersBeat app = (PlayersBeat) context.getApplicationContext();
        return proxy == null ? (proxy = newProxy()) : proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(context)
                .maxCacheSize(1024 * 1024 * 1024)
                .maxCacheFilesCount(20)
                .build();
    }

    boolean is_visible_to_user;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        is_visible_to_user = isVisibleToUser;

        if (privious_player != null && (isVisibleToUser && !is_user_stop_video)) {
            privious_player.setPlayWhenReady(true);

        } else if (privious_player != null && !isVisibleToUser) {
            privious_player.setPlayWhenReady(false);

        }
    }

    // when we swipe for another video this will relaese the privious player
    public SimpleExoPlayer privious_player;
    public void releasePreviousPlayer() {
        if (privious_player != null) {
            privious_player.removeListener(this);
            privious_player.release();
        }
    }


    public boolean is_fragment_exits() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (fm.getBackStackEntryCount() == 0) {
            return false;
        } else {
            return true;
        }
    }

    // this is lifecyle of the Activity which is importent for play,pause video or relaese the player
    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        if (privious_player != null && (is_visible_to_user && !is_user_stop_video)
                && !is_fragment_exits()) {
            privious_player.setPlayWhenReady(true);
            Log.e("Resume", "=====");
        } else {
            Log.e("Resume", "=====");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (privious_player != null) {
            privious_player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (privious_player != null) {
            privious_player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (privious_player != null) {
            privious_player.release();
        }
    }

    // Bottom all the function and the Call back listener of the Expo player
    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }


    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }


    @Override
    public void onLoadingChanged(boolean isLoading) {

    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if (playbackState == Player.STATE_BUFFERING) {
            p_bar.setVisibility(View.VISIBLE);
        } else if (playbackState == Player.STATE_READY) {
            p_bar.setVisibility(View.GONE);
        }


    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }


    @Override
    public void onSeekProcessed() {

    }


}
