package com.example.talentogram.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.talentogram.R;
import com.example.talentogram.activity.JoinMembershipActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class StandardFragment extends Fragment {

    private ImageView ivPrevious, ivNext;


    public StandardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_standard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivNext = view.findViewById(R.id.ivNext);
        ivPrevious = view.findViewById(R.id.ivPrevious);

        ivPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((JoinMembershipActivity) getActivity()).moveToPrevious();
            }
        });
        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((JoinMembershipActivity) getActivity()).moveToNext();
            }
        });
    }
}
