package com.example.talentogram.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.talentogram.R;
import com.example.talentogram.activity.JoinMembershipActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PremiumFragment extends Fragment {

    private ImageView ivPremium;


    public PremiumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_premium, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivPremium = view.findViewById(R.id.ivPrevious);

        ivPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((JoinMembershipActivity) getActivity()).moveToPrevious();
            }
        });
    }
}
