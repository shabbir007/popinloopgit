package com.example.talentogram.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.example.talentogram.BuildConfig;
import com.example.talentogram.R;

public class GradientText extends AppCompatTextView {
    private final static String TAG = GradientText.class.getSimpleName();

    private int[] mColors;

    private int mAngle = 0;

    private GradientText.DIRECTION mDIRECTION;

    public enum DIRECTION {
        LEFT(0),
        TOP(90),
        RIGHT(180),
        BOTTOM(270);

        int angle;

        DIRECTION(int angle) {
            this.angle = angle;
        }
    }
    public GradientText(Context context) {
        super(context);
        init(context, null);
        init();
    }

    public GradientText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        init();
    }

    public GradientText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top,
                            int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (changed) {
            getPaint().setShader(new LinearGradient(0, 0,
                    getWidth(),
                    getHeight(),
                    ContextCompat.getColor(getContext(),
                            R.color.colorstart),
                    ContextCompat.getColor(getContext(),
                            R.color.colorend),
                    Shader.TileMode.CLAMP));
        }
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Montserrat-Regular.ttf");
        setTypeface(tf);
    }
    private void init(Context context, AttributeSet attributeSet) {

        final TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.GradientTextView);

        try {
            int colorArrayResourceId = typedArray.getResourceId(R.styleable.GradientTextView_gt_color_list, 0);
            if (colorArrayResourceId != 0) {
                mColors = getResources().getIntArray(colorArrayResourceId);
            }
            if (typedArray.hasValue(R.styleable.GradientTextView_gt_gradient_direction)) {
                int value = typedArray.getInt(R.styleable.GradientTextView_gt_gradient_direction, 0);
                mDIRECTION = DIRECTION.values()[value];
            }

            if (typedArray.hasValue(R.styleable.GradientTextView_gt_gradient_angle)) {
                mAngle = typedArray.getInt(R.styleable.GradientTextView_gt_gradient_angle, 0);
            }

        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "Exception", e);
            }
        } finally {
            typedArray.recycle();
        }
    }

}
