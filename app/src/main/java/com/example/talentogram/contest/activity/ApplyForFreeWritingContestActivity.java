package com.example.talentogram.contest.activity;

import android.content.Intent;
import android.graphics.Color;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.talentogram.R;
import com.example.talentogram.others.Utils;


public class ApplyForFreeWritingContestActivity extends AppCompatActivity {
    private int color_index = 0, font_index = 0, bg_img_index = 0;
    private String color = "F4A460";
    private String font = "font_1.ttf";
    private String bg_Image = "ContextCompat.getDrawable(this, R.drawable.a1)";
    private RelativeLayout relative_layout_upload_status;
    private ImageView ivPickColor, ivfont, ivImage;
    private Button btnNext;
    private EditText etDescription;
    private int ContestId, TimeSlotId, ContestType;
    private LinearLayout llBack;
    boolean background = true;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_free_writing_contest);

        ContestId = getIntent().getExtras().getInt("ContestId");
        ContestType = getIntent().getExtras().getInt("ContestType");
        TimeSlotId = getIntent().getExtras().getInt("TimeSlotId");

        inIt();
        bindView();
        if (background) {
            relative_layout_upload_status.setDrawingCacheEnabled(true);
            relative_layout_upload_status.setBackgroundResource(R.drawable.d_bg_writing);
            GradientDrawable drawable = (GradientDrawable) relative_layout_upload_status.getBackground();
            drawable.setColor(Color.parseColor("#" + color));
        } else {

        }
    }


    private void inIt() {
        relative_layout_upload_status = findViewById(R.id.relative_layout_upload_status);
        ivPickColor = findViewById(R.id.ivPickColor);
        btnNext = findViewById(R.id.btnNext);
        etDescription = findViewById(R.id.etDescription);
        llBack = findViewById(R.id.llBack);
        ivfont = findViewById(R.id.ivfont);
        ivImage = findViewById(R.id.ivImage);
        tvTitle = findViewById(R.id.tvTitle);

        tvTitle.setText(getResources().getString(R.string.add_free_contest));
    }

    private void bindView() {
        llBack.setOnClickListener(v -> onBackPressed());
        ivPickColor.setOnClickListener(v -> {
            background = true;
            nextColor();
        });
        ivfont.setOnClickListener(v -> nextFont());
        ivImage.setOnClickListener(v -> {
            background = false;

        });


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = etDescription.getText().toString().trim();
                if (content.isEmpty()) {
                    Utils.showWarn(ApplyForFreeWritingContestActivity.this, getString(R.string.error_writing_details), 3);
                } else {
                    Intent i = new Intent(ApplyForFreeWritingContestActivity.this, SaveContestWritingAvtivity.class);
                    i.putExtra("ContestId", String.valueOf(ContestId));
                    i.putExtra("ContestType", String.valueOf(ContestType));
                    i.putExtra("TimeSlotId", String.valueOf(TimeSlotId));
                    i.putExtra("color", color);
                    i.putExtra("font", font);
                    i.putExtra("content", etDescription.getText().toString().trim());
                    startActivity(i);
                }
            }
        });

    }

    public void nextFont() {
        this.font_index++;
        if (font_index == 11) {
            this.font_index = 1;
        }
        switch (font_index) {
            case 1:
                font = "font_1.ttf";
                break;
            case 2:
                font = "font_2.ttf";
                break;
            case 3:
                font = "font_3.ttf";
                break;
            case 4:
                font = "font_4.ttf";
                break;
            case 5:
                font = "font_5.ttf";
                break;
            case 6:
                font = "font_6.ttf";
                break;
            case 7:
                font = "font_7.ttf";
                break;
            case 8:
                font = "font_8.ttf";
                break;
            case 9:
                font = "font_9.ttf";
                break;
            case 10:
                font = "font_10.ttf";
                break;
        }
        etDescription.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/" + font), Typeface.BOLD);
    }

    public void nextColor() {
        this.color_index++;
        if (color_index == 21) {
            this.color_index = 1;
        }
        switch (color_index) {
            case 1:
                color = "C51062";
                break;
            case 2:
                color = "034182";
                break;
            case 3:
                color = "1a2634";
                break;
            case 4:
                color = "288fb4";
                break;
            case 5:
                color = "2f3c4e";
                break;
            case 6:
                color = "3d065a";
                break;
            case 7:
                color = "449187";
                break;
            case 8:
                color = "61519f";
                break;
            case 9:
                color = "f45e5e";
                break;
            case 10:
                color = "f8aa27";
                break;
            case 11:
                color = "FFB6C1";
                break;
            case 12:
                color = "FA8072";
                break;
            case 13:
                color = "DC143C";
                break;
            case 14:
                color = "BDB76B";
                break;
            case 15:
                color = "B0E0E6";
                break;
            case 16:
                color = "6B8E23";
                break;
            case 17:
                color = "B0C4DE";
                break;
            case 18:
                color = "8B4513";
                break;
            case 19:
                color = "778899";
                break;
            case 20:
                color = "6495ED";
                break;

        }

        relative_layout_upload_status.setBackgroundResource(R.drawable.d_bg_writing);
        GradientDrawable drawable = (GradientDrawable) relative_layout_upload_status.getBackground();
        drawable.setColor(Color.parseColor("#" + color));
    }

}