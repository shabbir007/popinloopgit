package com.example.talentogram.contest.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.contest.adapter.ContestDetailAdapter;
import com.example.talentogram.contest.model.ContestDetailModel;

import java.util.ArrayList;

import static com.example.talentogram.activity.DashBoardActivity.toolbar;

public class ContestDetailActivity extends AppCompatActivity {
    private ArrayList<ContestDetailModel> contestDetailModels;
    private RecyclerView rv_Contestdetail;
    private  ImageView ivBack;
    private LinearLayout llback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contest_detail);

        toolbar.setVisibility(View.VISIBLE);
        initView();

        ivBack = findViewById(R.id.ivBack);
        llback = findViewById(R.id.llBack);
        llback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initView() {
        rv_Contestdetail = findViewById(R.id.rv_Contestdetail);
        contestDetailModels = new ArrayList<>();
        rv_Contestdetail.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        ContestDetailAdapter contestDetailAdapter = new ContestDetailAdapter(this, contestDetailModels);
        rv_Contestdetail.setHasFixedSize(true);
        rv_Contestdetail.setItemViewCacheSize(20);
        rv_Contestdetail.setAdapter(contestDetailAdapter);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        toolbar.setVisibility(View.VISIBLE);
        return super.onPrepareOptionsMenu(menu);
    }
}

