package com.example.talentogram.contest.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.activity.RazorPayActivity;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ApplyFreeContestActivity extends AppCompatActivity {
    int IsCheckedForUpload = 1, IsCheckedForDownload = 1;
    private TextView tvAudio, tvSelectAudio;
    private ImageView ivPhoto, ivVideo, ivAudio, ivCameraIcon, ivVideoIcon, ivBack;
    private LinearLayout llProfilePic, llProfileVideo, llPhotoContest, llVideoContest, llAudioContest;
    private Button btnApply;
    private EditText edtContestTitle, edtContestdesc;
    private String userChoosenTask = "";
    private String pathFinal = "";
    private File destination;
    private int ContestId, TimeSlotId, ContestType, isFree;
    private CheckBox chUploadToPost, chAllowDownload;
    SharedPreferences prefs;
    String prefName = "MyPref";
    String audioID, artist_name, artist_band, razorpayPaymentId, amt;
    Uri MyUri;
    private LinearLayout llBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_free_contest);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        isFree = getIntent().getExtras().getInt("isFree");
        ContestId = getIntent().getExtras().getInt("ContestId");
        ContestType = getIntent().getExtras().getInt("ContestType");
        TimeSlotId = getIntent().getExtras().getInt("TimeSlotId");
        amt = getIntent().getExtras().getString("amt");
        razorpayPaymentId = getIntent().getExtras().getString("razorpayPaymentId");
        Init();
    }


    private void Init() {
        ivPhoto = findViewById(R.id.ivPhoto);
        tvAudio = findViewById(R.id.tvAudio);
        tvSelectAudio = findViewById(R.id.tvSelectAudio);
        llProfilePic = findViewById(R.id.llProfilePic);
        llProfileVideo = findViewById(R.id.llProfileVideo);
        ivVideo = findViewById(R.id.ivVideo);
        ivAudio = findViewById(R.id.ivAudio);
        edtContestTitle = findViewById(R.id.edtContestTitle);
        edtContestdesc = findViewById(R.id.edtContestdesc);
        llVideoContest = findViewById(R.id.llVideoContest);
        llPhotoContest = findViewById(R.id.llPhotoContest);
        llAudioContest = findViewById(R.id.llAudioContest);
        btnApply = findViewById(R.id.btnApply);
        chUploadToPost = findViewById(R.id.chUploadToPost);
        chAllowDownload = findViewById(R.id.chAllowDownload);
        ivCameraIcon = findViewById(R.id.ivCameraIcon);
        ivVideoIcon = findViewById(R.id.ivVideoIcon);
        ivBack = findViewById(R.id.ivBack);
        llBack = findViewById(R.id.llBack);
        //0-audio,1-video,2-image,3-writing
        if (ContestType == 0) {
            llPhotoContest.setVisibility(View.GONE);
            llVideoContest.setVisibility(View.GONE);
            llAudioContest.setVisibility(View.VISIBLE);
        } else if (ContestType == 1) {
            llPhotoContest.setVisibility(View.GONE);
            llVideoContest.setVisibility(View.VISIBLE);
            llAudioContest.setVisibility(View.GONE);
        } else if (ContestType == 2) {
            llPhotoContest.setVisibility(View.VISIBLE);
            llVideoContest.setVisibility(View.GONE);
            llAudioContest.setVisibility(View.GONE);
        }
        chUploadToPost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsCheckedForUpload = 1;
                } else {
                    IsCheckedForUpload = 0;
                }
            }
        });
        chAllowDownload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsCheckedForDownload = 1;
                } else {
                    IsCheckedForDownload = 0;
                }
            }
        });
        bindWidget();
    }

    private void bindWidget() {
        llProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        llProfileVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Video"),
                        MyConstants.REQUEST_TAKE_GALLERY_VIDEO);

            }
        });


        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //for Free contest isFree = 0 AND For Paid IsFree = 1
                if (isFree == 0) {
                    applyForFreeContest();
                } else {
                    applyForPaidContest();
                }
            }
        });
        tvSelectAudio.setOnClickListener(v -> AudioIntent());
        llAudioContest.setOnClickListener(v -> AudioIntent());

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void applyForPaidContest() {
        String contest_title = edtContestTitle.getText().toString().trim();
        String contest_description = edtContestdesc.getText().toString().trim();


        Date currentTime = Calendar.getInstance().getTime();

        if (TextUtils.isEmpty(pathFinal)) {
            if (ContestType == 0) {
                Utils.showWarn(ApplyFreeContestActivity.this, "Please Select Audio", 3);
            } else if (ContestType == 1) {
                Utils.showWarn(ApplyFreeContestActivity.this, "Please Select Video", 3);
            } else if (ContestType == 2) {
                Utils.showWarn(ApplyFreeContestActivity.this, "Please Select Picture", 3);
            }
        } else if (TextUtils.isEmpty(contest_title)) {
            edtContestTitle.requestFocus();
            edtContestTitle.setError(getString(R.string.blank_validation_title));
        } else if (TextUtils.isEmpty(contest_description)) {
            edtContestdesc.requestFocus();
            edtContestdesc.setError(getString(R.string.blank_validation_desc));
        } else {

            HashMap<String, String> userData = new HashMap<>();
            userData.put("PostType", String.valueOf(ContestType));
            userData.put("Title", contest_title);
            userData.put("NewHashtags", "Contest_upload");
            userData.put("Description", contest_description);
            userData.put("AdminSongId", "");
            userData.put("PostHashTagIds", "");
            userData.put("UploadToPost", String.valueOf(IsCheckedForUpload));
            userData.put("TxnAmount", amt);
            userData.put("TxtID", razorpayPaymentId);
            userData.put("TxtTime", String.valueOf(currentTime));
            userData.put("TxtPlatfrom", "RazorPay");
            userData.put("TimeSlotId", String.valueOf(TimeSlotId));
            userData.put("PaymentMode", "6");
            userData.put("ContestId", String.valueOf(ContestId));

            HashMap<String, File> userFile = new HashMap<>();
            userFile.put("PostUrl[]", destination);

            new NetworkCall(ApplyFreeContestActivity.this, Services.APPLY_FOR_PAID_CONTEST,
                    userData, userFile, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            String message = obj.getString("message");
                            if (Success) {
                                new SweetAlertDialog(ApplyFreeContestActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setContentText(message)
                                        .setConfirmText("Ok")
                                        .showCancelButton(true)
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                Intent i = new Intent(ApplyFreeContestActivity.this, RazorPayActivity.class);
                                                startActivity(i);
                                                finish();
                                            }
                                        }).show();

                                Utils.showWarn(ApplyFreeContestActivity.this, message, 2);
                            } else {
                                Utils.showWarn(ApplyFreeContestActivity.this, message, 1);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void applyForFreeContest() {
        String contest_title = edtContestTitle.getText().toString().trim();
        String contest_description = edtContestdesc.getText().toString().trim();

        if (TextUtils.isEmpty(pathFinal)) {
            if (ContestType == 0) {
                Utils.showWarn(ApplyFreeContestActivity.this, "Please Select Audio", 3);
            } else if (ContestType == 1) {
                Utils.showWarn(ApplyFreeContestActivity.this, "Please Select Video", 3);
            } else if (ContestType == 2) {
                Utils.showWarn(ApplyFreeContestActivity.this, "Please Select Picture", 3);
            }
        } else if (TextUtils.isEmpty(contest_title)) {
            edtContestTitle.requestFocus();
            edtContestTitle.setError(getString(R.string.blank_validation_title));
        } else if (TextUtils.isEmpty(contest_description)) {
            edtContestdesc.requestFocus();
            edtContestdesc.setError(getString(R.string.blank_validation_desc));
        } else {

            HashMap<String, String> userData = new HashMap<>();
            userData.put("PostType", String.valueOf(ContestType));
            userData.put("Title", contest_title);
            userData.put("NewHashtags", "Contest_upload");
            userData.put("Description", contest_description);
            userData.put("AdminSongId", String.valueOf(1));
            userData.put("PostHashTagIds", String.valueOf(173));
            userData.put("UploadToPost", String.valueOf(IsCheckedForUpload));
            userData.put("isDownload", String.valueOf(IsCheckedForDownload));
            userData.put("ContestId", String.valueOf(ContestId));
            userData.put("TimeSlotId", String.valueOf(TimeSlotId));

            HashMap<String, File> userFile = new HashMap<>();
            userFile.put("PostUrl[]", destination);

            new NetworkCall(ApplyFreeContestActivity.this, Services.APPLY_FOR_FREE_CONTEST,
                    userData, userFile, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            String message = obj.getString("message");
                            if (Success) {
                                new SweetAlertDialog(ApplyFreeContestActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setContentText(message)
                                        .setConfirmText("Ok")
                                        .showCancelButton(true)
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                Intent i = new Intent(ApplyFreeContestActivity.this, RazorPayActivity.class);
                                                startActivity(i);
                                                finish();
                                            }
                                        }).show();
                            } else {
                                Utils.showWarn(ApplyFreeContestActivity.this, message, 1);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(ApplyFreeContestActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utils.checkPermission(ApplyFreeContestActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, MyConstants.SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, MyConstants.REQUEST_CAMERA);
    }

    private void AudioIntent() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, MyConstants.REQUEST_TAKE_AUDIO);
        ivAudio.setVisibility(View.GONE);
        tvAudio.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == MyConstants.SELECT_FILE) {
                Uri selectedImage = data.getData();
                File attachmentImgFile = new File(getRealPathFromURI(ApplyFreeContestActivity.this, selectedImage));
                pathFinal = attachmentImgFile.toString();

                Intent i = new Intent(ApplyFreeContestActivity.this, ContestImageFilterActivity.class);
                i.putExtra("image", attachmentImgFile.toString());
                startActivityForResult(i, MyConstants.REQUEST_TAKE_IMAGE_NEXTACTIVITY);
            }
            if (requestCode == MyConstants.REQUEST_TAKE_IMAGE_NEXTACTIVITY) {
                String image = data.getStringExtra("image");
                destination = new File(image);
                pathFinal = destination.toString();
                ivPhoto.setVisibility(View.VISIBLE);
                ivCameraIcon.setVisibility(View.GONE);

                Glide.with(ApplyFreeContestActivity.this)
                        .load(destination)
                        .thumbnail(Glide.with(ApplyFreeContestActivity.this).load(destination))
                        .error(R.drawable.noimage)
                        .into(ivPhoto);

            }

            if (requestCode == MyConstants.REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedVideo = data.getData();
                try {
                    pathFinal = Utils.getFilePath(ApplyFreeContestActivity.this, selectedVideo);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                File file = new File(pathFinal);
                long fileSizeInBytes = file.length();
                long fileSizeInMB = fileSizeInBytes / 1048576;
                if (fileSizeInMB > 50) {
                    Utils.showWarn(ApplyFreeContestActivity.this, getString(R.string.video_size));
                } else {
                    Intent i = new Intent(ApplyFreeContestActivity.this, ContestVideoFilterActivity.class);
                    i.putExtra("VideoUrl", pathFinal);
                    startActivityForResult(i, MyConstants.REQUEST_TAKE_VIDEO_NEXTACTIVITY);

                }

            }

            if (requestCode == MyConstants.REQUEST_TAKE_VIDEO_NEXTACTIVITY) {
                String VideoUrl = data.getStringExtra("VideoUrl");
                pathFinal = VideoUrl;
                Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(VideoUrl, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                ivVideo.setImageBitmap(bmThumbnail);
                ivVideo.setVisibility(View.VISIBLE);
                ivVideoIcon.setVisibility(View.GONE);
                System.out.println(data + "VideoUrl===");
            }

            if (requestCode == MyConstants.REQUEST_TAKE_AUDIO) {
                audioID = data.getDataString();
                MyUri = Uri.parse(audioID);
                String[] proj = {MediaStore.Audio.Media._ID,
                        MediaStore.Audio.Media.DATA,
                        MediaStore.Audio.Media.TITLE,
                        MediaStore.Audio.Artists.ARTIST};

                Cursor tempCursor = managedQuery(MyUri,
                        proj, null, null, null);

                tempCursor.moveToFirst(); //reset the cursor
                int col_index = -1;
                int numSongs = tempCursor.getCount();
                int currentNum = 0;
                do {
                    col_index = tempCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
                    artist_name = tempCursor.getString(col_index);
                    col_index = tempCursor.getColumnIndexOrThrow(MediaStore.Audio.Artists.ARTIST);
                    artist_band = tempCursor.getString(col_index);

                    currentNum++;
                } while (tempCursor.moveToNext());

                prefs = getSharedPreferences(prefName, MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("audio_selected", audioID);
                editor.putString("audio_name", artist_name);
                editor.commit();
                //the selected audio.
                Uri uri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(uri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);// c.getColumnIndex(filePath[0]);
                pathFinal = c.getString(columnIndex);
                destination = new File(pathFinal);
                tvAudio.setText(prefs.getString("audio_name", ""));

            }

            if (requestCode == MyConstants.REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                String pathFinal1 = saveToInternalStorage(thumbnail);
                Intent i = new Intent(ApplyFreeContestActivity.this, ContestImageFilterActivity.class);
                i.putExtra("image", pathFinal1);
                startActivityForResult(i, MyConstants.REQUEST_TAKE_IMAGE_NEXTACTIVITY);

            }
        }

    }

    String getRealPathFromURI(Context context, Uri contentUri) {
        String result = null;
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            result = contentUri.getPath();
        } else {
            if (cursor.moveToFirst()) {
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
            }
            cursor.close();
        }
        return result;
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(ApplyFreeContestActivity.this);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File mypath = new File(directory, "profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath.getAbsolutePath();
    }


}
