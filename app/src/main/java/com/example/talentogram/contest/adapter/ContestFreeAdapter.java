package com.example.talentogram.contest.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.talentogram.R;
import com.example.talentogram.contest.activity.ApplyForFreeWritingContestActivity;
import com.example.talentogram.contest.activity.ApplyFreeContestActivity;
import com.example.talentogram.contest.model.ContestFreeModel;
import java.util.ArrayList;

public class ContestFreeAdapter extends RecyclerView.Adapter<ContestFreeAdapter.MyViewHolder> {
    long DURATION = 500;
    private Context context;
    private ArrayList<ContestFreeModel> contestFreeModelArrayList;
    private boolean on_attach = true;

    public ContestFreeAdapter(Context context, ArrayList<ContestFreeModel> contestFreeModelArrayList) {
        this.context = context;
        this.contestFreeModelArrayList = contestFreeModelArrayList;

    }

    @NonNull
    @Override
    public ContestFreeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_contest_free, parent, false);
        return new ContestFreeAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ContestFreeAdapter.MyViewHolder holder, int position) {
        ContestFreeModel contestFreeModel = contestFreeModelArrayList.get(position);

        holder.tvTitle.setText(contestFreeModel.getContestTitle());
        holder.txtTimer.setText("Ends in " + contestFreeModel.getTimeSlotText());
        holder.tvPriceAmount.setText(" ₹ " + String.valueOf(contestFreeModel.getPrice()));

        // 0-audio,1-video,2-image,3-writing
        //for Free contest isFree = 0 AND For Paid IsFree = 1
        holder.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contestFreeModel.getContestType() == 0 || contestFreeModel.getContestType() == 1 || contestFreeModel.getContestType() == 2) {
                    Intent i = new Intent(context, ApplyFreeContestActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("ContestId", contestFreeModel.getId());
                    bundle.putInt("isFree", 0);
                    bundle.putInt("ContestType", contestFreeModel.getContestType());
                    bundle.putInt("TimeSlotId", contestFreeModel.getTimeSlotId());
                    i.putExtras(bundle);
                    context.startActivity(i);

                } else if (contestFreeModel.getContestType() == 3) {
                    Intent i = new Intent(context, ApplyForFreeWritingContestActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("isFree", 0);
                    bundle.putInt("ContestId", contestFreeModel.getId());
                    bundle.putInt("ContestType", contestFreeModel.getContestType());
                    bundle.putInt("TimeSlotId", contestFreeModel.getTimeSlotId());
                    i.putExtras(bundle);
                    context.startActivity(i);

                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return contestFreeModelArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTime, tvPriceAmount;
        private Button btnApply;
        private ImageView ivPaidContestImage;
        private TextView tvTitle;
        TextView txtTimer;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tvTime);
            btnApply = itemView.findViewById(R.id.btnApply);
            ivPaidContestImage = itemView.findViewById(R.id.ivPaidContestImage);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvPriceAmount = itemView.findViewById(R.id.tvPriceAmount);
            txtTimer = itemView.findViewById(R.id.txtTimer);

        }
    }
}
