package com.example.talentogram.contest.adapter;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.talentogram.contest.View_Contest_Video_Fragment;

public class ViewsTabAdapter extends FragmentPagerAdapter {
    private Context context;
    int totalTabs;

    public ViewsTabAdapter(FragmentManager fm, Context context, int totalTabs) {
        super(fm);
        this.context = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Fragment fragment_Video = new View_Contest_Video_Fragment();
                Bundle bundle = new Bundle();
                bundle.putInt("PostType", 1);
                fragment_Video.setArguments(bundle);
                return fragment_Video;
            case 1:
                Fragment fragment_Image = new View_Contest_Video_Fragment();
                Bundle b_Image = new Bundle();
                b_Image.putInt("PostType", 2);
                fragment_Image.setArguments(b_Image);
                return fragment_Image;
            case 2:
                Fragment fragment_Audio = new View_Contest_Video_Fragment();
                Bundle b_Audio = new Bundle();
                b_Audio.putInt("PostType", 0);
                fragment_Audio.setArguments(b_Audio);
                return fragment_Audio;
            case 3:
                Fragment fragment_writing = new View_Contest_Video_Fragment();
                Bundle b_Writing = new Bundle();
                b_Writing.putInt("PostType", 3);
                fragment_writing.setArguments(b_Writing);
                return fragment_writing;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Video";
            case 1:
                return "Image";
            case 2:
                return "Audio";
            case 3:
                return "Write";
            default:
                return null;
        }
    }
}
