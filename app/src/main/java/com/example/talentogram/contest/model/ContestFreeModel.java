package com.example.talentogram.contest.model;

public class ContestFreeModel {

    public int Id,UserId,ContestType,IsActive,IsPaid,no_of_contest,TimeSlotId;

    public String ContestTitle,StartTime,EndTime,RewardType,price,ContestReffrenceImageUrl,start_time,end_time,subscribe_amount,TimeSlotText;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getContestType() {
        return ContestType;
    }

    public void setContestType(int contestType) {
        ContestType = contestType;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public int getIsPaid() {
        return IsPaid;
    }

    public void setIsPaid(int isPaid) {
        IsPaid = isPaid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getNo_of_contest() {
        return no_of_contest;
    }

    public void setNo_of_contest(int no_of_contest) {
        this.no_of_contest = no_of_contest;
    }

    public int getTimeSlotId() {
        return TimeSlotId;
    }

    public void setTimeSlotId(int timeSlotId) {
        TimeSlotId = timeSlotId;
    }

    public String getContestTitle() {
        return ContestTitle;
    }

    public void setContestTitle(String contestTitle) {
        ContestTitle = contestTitle;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getRewardType() {
        return RewardType;
    }

    public void setRewardType(String rewardType) {
        RewardType = rewardType;
    }

    public String getContestReffrenceImageUrl() {
        return ContestReffrenceImageUrl;
    }

    public void setContestReffrenceImageUrl(String contestReffrenceImageUrl) {
        ContestReffrenceImageUrl = contestReffrenceImageUrl;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getSubscribe_amount() {
        return subscribe_amount;
    }

    public void setSubscribe_amount(String subscribe_amount) {
        this.subscribe_amount = subscribe_amount;
    }

    public String getTimeSlotText() {
        return TimeSlotText;
    }

    public void setTimeSlotText(String timeSlotText) {
        TimeSlotText = timeSlotText;
    }
}


/*

 {

            "id": 13,
            "ContestType": 0,
            "ContestTitle": "",
            "StartTime": "",
            "EndTime": "",
            "RewardType": "",
            "GuestRewardPoints": "",
            "PremiumRewardPoints": "",
            "ContestReffrenceImageUrl": "",
            "IsActive": 1,
            "IsPaid": 0,
            "created_at": "2020-08-27 15:12:59",
            "updated_at": "2020-08-27 15:12:59",
            "start_time": "09:00:00",
            "end_time": "12:00:00",
            "price": "5.000",
            "no_of_contest": 5,
            "subscribe_amount": "",
            "TimeSlotId": 2,
            "TimeSlotText": "3 HOUR"
            */
