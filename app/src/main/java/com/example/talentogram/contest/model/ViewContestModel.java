package com.example.talentogram.contest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ViewContestModel {

    int Id,ContestId,UserId,PostType,PostVisibleType,IsInContest,is_download;
    ArrayList<AudioUrl>AudioUrl;
    ArrayList<VideoUrl>VideoUrl;
    ArrayList<ImageUrl>ImageUrl;
    public Userdata userdata;
    public PostData post_detail;

    @SerializedName("Font")
    public String Font;

    @SerializedName("Color")
    public String Color;

    public String getFont() {
        return Font;
    }

    public void setFont(String font) {
        Font = font;
    }
    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getContestId() {
        return ContestId;
    }

    public void setContestId(int contestId) {
        ContestId = contestId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getPostType() {
        return PostType;
    }

    public void setPostType(int postType) {
        PostType = postType;
    }

    public int getPostVisibleType() {
        return PostVisibleType;
    }

    public void setPostVisibleType(int postVisibleType) {
        PostVisibleType = postVisibleType;
    }

    public int getIsInContest() {
        return IsInContest;
    }

    public void setIsInContest(int isInContest) {
        IsInContest = isInContest;
    }

    public int getIs_download() {
        return is_download;
    }

    public void setIs_download(int is_download) {
        this.is_download = is_download;
    }

    public ArrayList<ViewContestModel.AudioUrl> getAudioUrl() {
        return AudioUrl;
    }

    public void setAudioUrl(ArrayList<ViewContestModel.AudioUrl> audioUrl) {
        AudioUrl = audioUrl;
    }

    public ArrayList<ViewContestModel.VideoUrl> getVideoUrl() {
        return VideoUrl;
    }

    public void setVideoUrl(ArrayList<ViewContestModel.VideoUrl> videoUrl) {
        VideoUrl = videoUrl;
    }

    public ArrayList<ViewContestModel.ImageUrl> getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(ArrayList<ViewContestModel.ImageUrl> imageUrl) {
        ImageUrl = imageUrl;
    }

    public Userdata getUserdata() {
        return userdata;
    }

    public void setUserdata(Userdata userdata) {
        this.userdata = userdata;
    }

    public PostData getPostdata() {
        return post_detail;
    }

    public void setPostdata(PostData postdata) {
        this.post_detail = postdata;
    }

    public static class AudioUrl{
        int Id,UserId;
        String AudioUrl ;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getAudioUrl() {
            return AudioUrl;
        }

        public void setAudioUrl(String audioUrl) {
            AudioUrl = audioUrl;
        }
    }
    public static class VideoUrl{
        int Id,UserId;
        String VideoUrl ;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getVideoUrl() {
            return VideoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            VideoUrl = videoUrl;
        }
    }
    public static class ImageUrl{
        int Id,UserId;
        String VideoUrl ;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getVideoUrl() {
            return VideoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            VideoUrl = videoUrl;
        }
    }
    public static class Userdata
     {
        int id;
        String FirstName,LastName,ProfilePics;

         public int getId() {
             return id;
         }

         public void setId(int id) {
             this.id = id;
         }

         public String getFirstName() {
             return FirstName;
         }

         public void setFirstName(String firstName) {
             FirstName = firstName;
         }

         public String getLastName() {
             return LastName;
         }

         public void setLastName(String lastName) {
             LastName = lastName;
         }

         public String getProfilePics() {
             return ProfilePics;
         }

         public void setProfilePics(String profilePics) {
             ProfilePics = profilePics;
         }
     }
     public static class PostData
     {
         int Id,PostId;
         String Title,Description;

         public int getId() {
             return Id;
         }

         public void setId(int id) {
             Id = id;
         }

         public int getPostId() {
             return PostId;
         }

         public void setPostId(int postId) {
             PostId = postId;
         }

         public String getTitle() {
             return Title;
         }

         public void setTitle(String title) {
             Title = title;
         }

         public String getDescription() {
             return Description;
         }

         public void setDescription(String description) {
             Description = description;
         }
     }

}

       /* {
                "Id": 125,
                "ContestId": 21,
                "UserId": 72,
                "PostType": 0,
                "PostVisibleType": 1,
                "IsInContest": 1,
                "IsActive": 1,
                "created_at": "2020-09-07 17:10:21",
                "updated_at": "2020-09-07 17:10:21",
                "is_download": 0,
                "time_slot_id": 2,
                "is_contest_view": 0,
                "ImageUrl": [],
                "AudioUrl": [
                {
                "Id": 3,
                "UserId": 72,
                "AdminSongId": 1,
                "SongUrl": "1599478821AUD-20170210-WA0004.mp3",
                "IsActive": 1,
                "created_at": "2020-09-07 17:10:21",
                "updated_at": "2020-09-07 17:10:21",
                "AudioUrl": "http://popinloop.clientworkstore.us/uploads/Post/1599478821AUD-20170210-WA0004.mp3"
                }
                ],
                "VideoUrl": [],
                "userdata": {
                "id": 72,
                "FirstName": "Yogita",
                "LastName": "patel",
                "email": "yogi456@mailinator.com",
                "email_verified_at": "2020-07-25 05:27:37",
                "Age": "",
                "Gender": "Female",
                "Upi": "9807654089",
                "ProfilePic": "1595654837cropped1025334412.jpg",
                "created_at": "2020-07-25 05:27:17",
                "updated_at": "2020-07-28 04:44:33",
                "ProfileVid": "",
                "MobileNumber": "9807650890",
                "BirthDate": "1995-07-25",
                "WorkAt": "Rajkot",
                "Relationship": "Single",
                "Hobbies": "Dance",
                "Address": "Rajkot",
                "Status": "Good",
                "Biodata": "Good",
                "EmailVerificationToken": "",
                "IsActive": 1,
                "Roles": 2,
                "SocialId": "",
                "SocialFlag": 0,
                "is_delete": 0,
                "ProfilePics": "http://popinloop.clientworkstore.us/uploads/ProfilePic/1595654837cropped1025334412.jpg"
                },
                "post_detail": {
                "Id": 112,
                "PostId": 125,
                "Title": "test",
                "Description": "test",
                "PostPath": "",
                "LikeCount": 0,
                "CommentCount": 0,
                "PostSongsId": "3",
                "PostiVideoId": "",
                "PostImageId": "",
                "created_at": "2020-09-07 17:10:21",
                "updated_at": "2020-09-07 17:10:21"
                }
                },*/