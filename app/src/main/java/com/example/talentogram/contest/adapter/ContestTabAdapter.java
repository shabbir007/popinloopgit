package com.example.talentogram.contest.adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.talentogram.contest.UploadsContestFreeFragment;
import com.example.talentogram.contest.UploadsContestPaidFragment;

public class ContestTabAdapter extends FragmentPagerAdapter {
    private Context context;
    int totalTabs;

    public ContestTabAdapter(FragmentManager fm, Context context, int totalTabs) {
        super(fm);
        this.context = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                UploadsContestFreeFragment CFF = new UploadsContestFreeFragment();
                return CFF;
            case 1:
                UploadsContestPaidFragment CPF = new UploadsContestPaidFragment();
                return CPF;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Free";
            case 1:
                return  "Paid";
            default:
                return null;
        }
    }
}
