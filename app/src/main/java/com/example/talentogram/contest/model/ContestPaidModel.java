package com.example.talentogram.contest.model;

import java.io.Serializable;

public class ContestPaidModel implements Serializable {

   public int id,ContestType,IsActive,IsPaid,no_of_contest,TimeSlotId;
   public String ContestTitle,StartTime, EndTime,RewardType,GuestRewardPoints,PremiumRewardPoints,ContestReffrenceImageUrl,start_time,
           end_time,price,subscribe_amount,TimeSlotText;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContestType() {
        return ContestType;
    }

    public void setContestType(int contestType) {
        ContestType = contestType;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public int getIsPaid() {
        return IsPaid;
    }

    public void setIsPaid(int isPaid) {
        IsPaid = isPaid;
    }

    public int getNo_of_contest() {
        return no_of_contest;
    }

    public void setNo_of_contest(int no_of_contest) {
        this.no_of_contest = no_of_contest;
    }

    public int getTimeSlotId() {
        return TimeSlotId;
    }

    public void setTimeSlotId(int timeSlotId) {
        TimeSlotId = timeSlotId;
    }

    public String getContestTitle() {
        return ContestTitle;
    }

    public void setContestTitle(String contestTitle) {
        ContestTitle = contestTitle;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getRewardType() {
        return RewardType;
    }

    public void setRewardType(String rewardType) {
        RewardType = rewardType;
    }

    public String getGuestRewardPoints() {
        return GuestRewardPoints;
    }

    public void setGuestRewardPoints(String guestRewardPoints) {
        GuestRewardPoints = guestRewardPoints;
    }

    public String getPremiumRewardPoints() {
        return PremiumRewardPoints;
    }

    public void setPremiumRewardPoints(String premiumRewardPoints) {
        PremiumRewardPoints = premiumRewardPoints;
    }

    public String getContestReffrenceImageUrl() {
        return ContestReffrenceImageUrl;
    }

    public void setContestReffrenceImageUrl(String contestReffrenceImageUrl) {
        ContestReffrenceImageUrl = contestReffrenceImageUrl;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubscribe_amount() {
        return subscribe_amount;
    }

    public void setSubscribe_amount(String subscribe_amount) {
        this.subscribe_amount = subscribe_amount;
    }

    public String getTimeSlotText() {
        return TimeSlotText;
    }

    public void setTimeSlotText(String timeSlotText) {
        TimeSlotText = timeSlotText;
    }
}


/*
 {
         "id": 109,
         "ContestType": 0,
         "ContestTitle": "",
         "StartTime": "",
         "EndTime": "",
         "RewardType": "",
         "GuestRewardPoints": "",
         "PremiumRewardPoints": "",
         "ContestReffrenceImageUrl": "",
         "IsActive": 1,
         "IsPaid": 1,
         "created_at": "2020-08-27 16:30:39",
         "updated_at": "2020-08-27 16:30:39",
         "start_time": "10:00:00",
         "end_time": "11:00:00",
         "price": "20.000",
         "no_of_contest": 5,
         "subscribe_amount": "5.000",
         "TimeSlotId": 1,
         "TimeSlotText": "1 HOUR"
         },*/
