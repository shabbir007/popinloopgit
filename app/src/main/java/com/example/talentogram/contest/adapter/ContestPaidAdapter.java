package com.example.talentogram.contest.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.talentogram.R;
import com.example.talentogram.contest.activity.ApplyForFreeWritingContestActivity;
import com.example.talentogram.contest.activity.ApplyFreeContestActivity;
import com.example.talentogram.contest.model.ContestPaidModel;
import com.razorpay.Checkout;

import org.json.JSONObject;

import java.util.ArrayList;

public class ContestPaidAdapter extends RecyclerView.Adapter<ContestPaidAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ContestPaidModel> dataList;

    public interface OnItemClickListener {
        void onItemClick(View view, int postion, ContestPaidModel item);
    }

    public ContestPaidAdapter.OnItemClickListener listener;


    public ContestPaidAdapter(Context context, ArrayList<ContestPaidModel> contestPaidModelArrayList,ContestPaidAdapter.OnItemClickListener listener) {
        this.context = context;
        this.dataList = contestPaidModelArrayList;
        this.listener = listener;

    }

    @NonNull
    @Override
    public ContestPaidAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_contest_paid, parent, false);

        return new ContestPaidAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ContestPaidAdapter.MyViewHolder holder, int position) {

        ContestPaidModel contestPaidModel = dataList.get(position);
        holder.tvTitle.setText(contestPaidModel.getContestTitle());
        holder.txtTimer.setText("Ends in " +contestPaidModel.getTimeSlotText());
        holder.tvPriceAmount.setText(" ₹ "+String.valueOf(contestPaidModel.getPrice()));
        holder.tvSubscribesAmount.setText("₹ " +contestPaidModel.getSubscribe_amount());


        // 0-audio,1-video,2-image,3-writing
       /* holder.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contestPaidModel.getContestType() == 0 || contestPaidModel.getContestType() == 1 || contestPaidModel.getContestType() == 2) {
                    Intent i = new Intent(context, ApplyFreeContestActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("ContestId", contestPaidModel.getId());
                    bundle.putInt("ContestType", contestPaidModel.getContestType());
                    bundle.putInt("TimeSlotId", contestPaidModel.getTimeSlotId());
                    i.putExtras(bundle);
                    context.startActivity(i);
                } else if (contestPaidModel.getContestType() == 3) {
                    Intent i = new Intent(context, ApplyForFreeWritingContestActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("ContestId", contestPaidModel.getId());
                    bundle.putInt("ContestType", contestPaidModel.getContestType());
                    bundle.putInt("TimeSlotId", contestPaidModel.getTimeSlotId());
                    i.putExtras(bundle);
                    context.startActivity(i);
                }
            }
        });*/
        holder.bind(position,dataList.get(position),listener);
    }

    @Override
    public int getItemCount() {

        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTime;
        private Button btnApply;
        private ImageView ivPaidContestImage;
        private TextView tvTitle;
        private TextView txtTimer,tvPriceAmount,tvSubscribesAmount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tvTime);
            btnApply = itemView.findViewById(R.id.btnApply);
            ivPaidContestImage = itemView.findViewById(R.id.ivPaidContestImage);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            txtTimer = itemView.findViewById(R.id.txtTimer);
            tvPriceAmount = itemView.findViewById(R.id.tvPriceAmount);
            tvSubscribesAmount = itemView.findViewById(R.id.tvSubscribesAmount);

        }
        public void bind(final int pos , final ContestPaidModel item, final ContestPaidAdapter.OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(v,pos,item);
                }
            });

            btnApply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(v,pos,item);
                }
            });


        }
    }

}
