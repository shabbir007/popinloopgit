package com.example.talentogram.contest;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.activity.RazorPayActivity;
import com.example.talentogram.contest.activity.ApplyForFreeWritingContestActivity;
import com.example.talentogram.contest.activity.ApplyFreeContestActivity;
import com.example.talentogram.contest.adapter.ContestPaidAdapter;
import com.example.talentogram.helper.JsonParserUniversal;
import com.example.talentogram.contest.model.ContestPaidModel;
import com.example.talentogram.model.SuccessModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.retrofit.ApiClient;
import com.example.talentogram.retrofit.ApiInterface;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadsContestPaidFragment extends Fragment  {

    private RecyclerView rv_Contestpaid;
    private JsonParserUniversal jsonParserUniversal;
    private ArrayList<ContestPaidModel> contestPaidModels = new ArrayList<>();
    private LinearLayout llNoContest;
    private ContestPaidAdapter contestPaidAdapter;

    public UploadsContestPaidFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contest_paid, container, false);
        jsonParserUniversal = new JsonParserUniversal();
        init(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getContest();

    }

    private void init(View view) {
        rv_Contestpaid = view.findViewById(R.id.rv_paid);
        llNoContest = view.findViewById(R.id.llNoContest);
    }

    private void getContest() {
        new NetworkCall(getContext(), Services.GET_PAID_CONTEST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jContestList = jsonObject.getJSONArray("data");

                    contestPaidModels.clear();

                    for (int i = 0; i < jContestList.length(); i++) {
                        JSONObject object = jContestList.getJSONObject(i);
                        ContestPaidModel contestPaidModel = (ContestPaidModel) jsonParserUniversal.parseJson(object, new ContestPaidModel());
                        contestPaidModels.add(contestPaidModel);

                    }
                    setData();
                    if (contestPaidModels.size() == 0) {
                        llNoContest.setVisibility(View.VISIBLE);
                        rv_Contestpaid.setVisibility(View.GONE);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
    }

    private void setData() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv_Contestpaid.setLayoutManager(mLayoutManager);
        rv_Contestpaid.setItemAnimator(new DefaultItemAnimator());
        contestPaidAdapter = new ContestPaidAdapter(getActivity(), contestPaidModels, new ContestPaidAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion, ContestPaidModel item) {
                switch (view.getId()) {
                    case R.id.btnApply:
                        checkBalance(postion, item);
                        break;
                    default:
                        break;
                }
            }
        });
        rv_Contestpaid.setAdapter(contestPaidAdapter);
    }

    private void checkBalance(int postion, ContestPaidModel item) {
        
        Utils.showProgressDialog(getContext());
        ApiInterface apiInterface = ApiClient.getClient(getContext()).create(ApiInterface.class);

        Call<SuccessModel> call = apiInterface.checkBalance(item.getPrice());
        call.enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                Utils.hideProgressDialog();
                if (response.body().isSuccess()) {
                    // 0-audio,1-video,2-image,3-writing
                    //for Free contest isFree = 0 AND For Paid IsFree = 1
                    if (item.getContestType() == 0 || item.getContestType() == 1 || item.getContestType() == 2) {
                        Intent i = new Intent(getContext(), ApplyFreeContestActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("isFree", 1);
                        bundle.putInt("ContestId", item.getId());
                        bundle.putInt("ContestType", item.getContestType());
                        bundle.putInt("TimeSlotId", item.getTimeSlotId());
                        i.putExtras(bundle);
                        startActivity(i);
                    } else if (item.getContestType() == 3) {
                        Intent i = new Intent(getContext(), ApplyForFreeWritingContestActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("isFree", 1);
                        bundle.putInt("ContestId", item.getId());
                        bundle.putInt("ContestType", item.getContestType());
                        bundle.putInt("TimeSlotId", item.getTimeSlotId());
                        i.putExtras(bundle);
                        startActivity(i);
                    }
                } else {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setContentText(response.body().getMessage())
                            .setConfirmText("Ok")
                            .showCancelButton(true)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    //startPayment();
                                    Intent i = new Intent(getContext() , RazorPayActivity.class);
                                    i.putExtra("amt",item.getPrice());
                                    i.putExtra("item",item);
                                    startActivity(i);
                                }
                            }).show();
                }
            }

            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {
                Utils.hideProgressDialog();
                Utils.showWarn(getContext(), "No internet Conection");
            }
        });
    }

}
