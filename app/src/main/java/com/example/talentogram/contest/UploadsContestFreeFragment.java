package com.example.talentogram.contest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.talentogram.R;
import com.example.talentogram.contest.adapter.ContestFreeAdapter;
import com.example.talentogram.helper.JsonParserUniversal;
import com.example.talentogram.contest.model.ContestFreeModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadsContestFreeFragment extends Fragment {

    private ArrayList<ContestFreeModel> contestFreeDataList = new ArrayList<>();
    private RecyclerView rv_Contestfree;
    private JsonParserUniversal jsonParserUniversal;
    private LinearLayout llNoContest;

    public UploadsContestFreeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contest_free, container, false);
        jsonParserUniversal = new JsonParserUniversal();
        getContest();
        init(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }

    private void init(View view) {
        rv_Contestfree = view.findViewById(R.id.rv_free);
        llNoContest = view.findViewById(R.id.llNoContest);
    }

    private void getContest() {
        new NetworkCall(getContext(), Services.GET_FREE_CONTEST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                System.out.println(response+"Contest_Free_data");
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jContestList = jsonObject.getJSONArray("data");

                    contestFreeDataList.clear();

                    for (int i = 0; i < jContestList.length(); i++) {
                        JSONObject object = jContestList.getJSONObject(i);
                        ContestFreeModel contestFreeModel = (ContestFreeModel)
                                jsonParserUniversal.parseJson(object, new ContestFreeModel());
                        contestFreeDataList.add(contestFreeModel);
                    }
                    setData();
                    if (contestFreeDataList.size()==0) {
                        llNoContest.setVisibility(View.VISIBLE);
                        rv_Contestfree.setVisibility(View.GONE);

                    }

                }catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void setData() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv_Contestfree.setLayoutManager(mLayoutManager);
        rv_Contestfree.setItemAnimator(new DefaultItemAnimator());
        rv_Contestfree.setAdapter(new ContestFreeAdapter(getActivity(), contestFreeDataList));
    }

}
