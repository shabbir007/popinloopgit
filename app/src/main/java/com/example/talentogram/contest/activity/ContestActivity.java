package com.example.talentogram.contest.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.talentogram.R;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.contest.adapter.ContestViewPagerAdapter;
import com.example.talentogram.contest.Contest_Uploads_Fragment;
import com.example.talentogram.contest.Contest_View_Fragment;
import com.example.talentogram.fragment.PostFragment;
import com.example.talentogram.others.LockableViewPager;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.ButterKnife;

public class ContestActivity extends AppCompatActivity {

    public BottomNavigationView bottomNavigationView;
    public LinearLayout topbar;
    private ImageView ivBack;
    private LinearLayout llBack;
    public LockableViewPager viewPager;
    MenuItem prevMenuItem;
    Contest_View_Fragment uploads_contest_fragment;
    Contest_Uploads_Fragment participate_contest_fragment;
    public boolean isVideoOpen = false;

    public static ContestActivity instance = null;

    public ContestActivity() {
        instance = this;
    }

    public static synchronized ContestActivity getInstance() {
        if (instance == null) {
            instance = new ContestActivity();
        }
        return ContestActivity.instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contest);
        ButterKnife.bind(this);
        viewPager = findViewById(R.id.viewpager);
        topbar = findViewById(R.id.ll1);
        ivBack = findViewById(R.id.ivBack);
        llBack = findViewById(R.id.llBack);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onBackPressed();
            }
        });

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_upload:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.action_participate:
                                viewPager.setCurrentItem(1);
                                break;

                        }
                        return false;
                    }
                });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: " + position);
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ContestViewPagerAdapter adapter = new ContestViewPagerAdapter(getSupportFragmentManager());
        uploads_contest_fragment = new Contest_View_Fragment();
        participate_contest_fragment = new Contest_Uploads_Fragment();

        adapter.addFragment(uploads_contest_fragment);
        adapter.addFragment(participate_contest_fragment);

        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (isVideoOpen) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ContestActivity.this,DashBoardActivity.class);
        startActivity(i);
    }
}
