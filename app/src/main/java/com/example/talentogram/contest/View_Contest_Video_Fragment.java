package com.example.talentogram.contest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.example.talentogram.R;
import com.example.talentogram.contest.adapter.Contest_View_Adapter;
import com.example.talentogram.contest.adapter.ViewVideoPlayerRecyclerView;
import com.example.talentogram.contest.model.ViewContestModel;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.VerticalSpacingItemDecorator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class View_Contest_Video_Fragment extends Fragment {
    private ArrayList<ViewContestModel> dataList = new ArrayList<>();

    @BindView(R.id.rv_Contest_video)
    ViewVideoPlayerRecyclerView mRecyclerView;

    @BindView(R.id.rv_Contest)
    RecyclerView rv_Contest;

    @BindView(R.id.llNoContest)
    LinearLayout llNoContest;
    private int postType;
    Contest_View_Adapter viewContestVideoAdapter;
    private boolean firstTime = true;

    public View_Contest_Video_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.upload_video_contest_fragment, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        postType = bundle.getInt("PostType");
        if (postType == 1 && MyConstants.viewContestModels.size() > 0) {
            setVideoData();
        }  else  {
            setData();
        }
        return view;
    }

    private void setVideoData() {
        //set data object
        mRecyclerView.setVisibility(View.VISIBLE);
        rv_Contest.setVisibility(View.GONE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(10);
        mRecyclerView.addItemDecoration(itemDecorator);
        mRecyclerView.setMediaObjects(dataList);
        viewContestVideoAdapter = new Contest_View_Adapter(getContext(), dataList, initGlide());
        mRecyclerView.setAdapter(viewContestVideoAdapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (viewContestVideoAdapter != null) {
            dataList.clear();
            for (int i = 0; i < MyConstants.viewContestModels.size(); i++) {
                if (postType == 0 && MyConstants.viewContestModels.get(i).getPostType() == 0) {
                    dataList.add(MyConstants.viewContestModels.get(i));
                } else if (postType == 1 && MyConstants.viewContestModels.get(i).getPostType() == 1) {
                    dataList.add(MyConstants.viewContestModels.get(i));
                } else if (postType == 2 && MyConstants.viewContestModels.get(i).getPostType() == 2) {
                    dataList.add(MyConstants.viewContestModels.get(i));
                } else if (postType == 3 && MyConstants.viewContestModels.get(i).getPostType() == 3) {
                    dataList.add(MyConstants.viewContestModels.get(i));
                }
                viewContestVideoAdapter.notifyDataSetChanged();
            }

        }
        super.setUserVisibleHint(isVisibleToUser);

    }

    private void setData() {
        mRecyclerView.setVisibility(View.GONE);
        rv_Contest.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_Contest.setLayoutManager(mLayoutManager);
        rv_Contest.setItemAnimator(new DefaultItemAnimator());
        viewContestVideoAdapter = new Contest_View_Adapter(getContext(), dataList, initGlide());
        rv_Contest.setAdapter(viewContestVideoAdapter);

    }

    private RequestManager initGlide() {
        RequestOptions options = new RequestOptions();

        return Glide.with(this)
                .setDefaultRequestOptions(options);
    }

    @Override
    public void onDestroy() {
        if (mRecyclerView != null) {
            mRecyclerView.releasePlayer();
        }
        super.onDestroy();
    }

}