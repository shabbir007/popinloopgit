package com.example.talentogram.contest;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.talentogram.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.ButterKnife;
import nl.changer.audiowife.AudioWife;


public class AudioDialogActivity extends Activity {
    private View mPlayMedia;
    private View mPauseMedia;
    private SeekBar mMediaSeekBar;
    private TextView mRunTime;
    private TextView mTotalTime,tvTitle;
    private String title,audioUrl;
    protected Uri mUri;
    protected ImageView imgClose;

    public AudioDialogActivity() {
        // Required empty public constructor
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_audiodialog);

        audioUrl = getIntent().getStringExtra("AudioUrl");
        title = getIntent().getStringExtra("title");

        mPlayMedia = findViewById(R.id.play);
        mPauseMedia = findViewById(R.id.pause);
        imgClose = findViewById(R.id.imgClose);
        mMediaSeekBar = (SeekBar) findViewById(R.id.media_seekbar);
        mRunTime = (TextView) findViewById(R.id.run_time);
        mTotalTime = (TextView) findViewById(R.id.total_time);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        tvTitle.setText(title);
        mUri = Uri.parse(audioUrl);
        AudioWife.getInstance()
                .init(AudioDialogActivity.this,mUri )
                .setPlayView(mPlayMedia)
                .setPauseView(mPauseMedia)
                .setSeekBar(mMediaSeekBar)
                .setRuntimeView(mRunTime)
                .setTotalTimeView(mTotalTime);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onPause() {
        AudioWife.getInstance().release();
        mUri = null;
        super.onPause();

    }

    @Override
    public void onDestroy() {
        AudioWife.getInstance().release();
        mUri = null;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        AudioWife.getInstance().release();
        mUri = null;
        super.onBackPressed();
    }
}