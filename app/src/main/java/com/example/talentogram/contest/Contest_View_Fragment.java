package com.example.talentogram.contest;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.example.talentogram.R;
import com.example.talentogram.contest.activity.ContestActivity;
import com.example.talentogram.contest.adapter.ViewsTabAdapter;
import com.example.talentogram.contest.model.ViewContestModel;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import nl.changer.audiowife.AudioWife;

import static com.example.talentogram.activity.DashBoardActivity.toolbar;

public class Contest_View_Fragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    public FrameLayout frame;
    public static Contest_View_Fragment instance = null;

    public Contest_View_Fragment() {
        instance = this;
    }

    public static synchronized Contest_View_Fragment getInstance() {
        if (instance == null) {
            instance = new Contest_View_Fragment();
        }
        return Contest_View_Fragment.instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contest, container, false);
        setHasOptionsMenu(true);
        getContest();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout = view.findViewById(R.id.Tab_Contest);
        viewPager = view.findViewById(R.id.VP_Contest);
        frame = view.findViewById(R.id.frame);

        tabLayout.addTab(tabLayout.newTab().setText("Video"));
        tabLayout.addTab(tabLayout.newTab().setText("Image"));
        tabLayout.addTab(tabLayout.newTab().setText("Audio"));
        tabLayout.addTab(tabLayout.newTab().setText("Write"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        LinearLayout linearLayout = (LinearLayout) tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.LTGRAY);
        drawable.setSize(2, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);
        tabLayout.setupWithViewPager(viewPager);

        final ViewsTabAdapter adapter = new ViewsTabAdapter(getChildFragmentManager(), getActivity(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void getContest() {
        new NetworkCall(getActivity(), Services.GET_ALL_CONTEST_DETAILS,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");

                        if (Success) {
                            JSONArray jPostData = obj.getJSONArray("data");
                            MyConstants.viewContestModels.clear();
                            MyConstants.viewContestModels = (ArrayList<ViewContestModel>) new Gson().fromJson(jPostData.toString(),
                                    new TypeToken<ArrayList<ViewContestModel>>() {
                                    }.getType());


                        } else {

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    public void changeFragmentWithBundle(Context context, Fragment fragment, boolean doAdd, Bundle bundle) {
        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        fragment.setArguments(bundle);
        if (doAdd)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
        frame.setVisibility(View.VISIBLE);
        ContestActivity.getInstance().bottomNavigationView.setVisibility(View.GONE);
        ContestActivity.getInstance().topbar.setVisibility(View.GONE);
        ContestActivity.getInstance().isVideoOpen = true;
        ContestActivity.getInstance().viewPager.disableScroll(true);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        toolbar.setVisibility(View.VISIBLE);
    }
    @Override
    public void onPause() {
        AudioWife.getInstance().release();
        super.onPause();

    }

    @Override
    public void onDestroy() {
        AudioWife.getInstance().release();
        super.onDestroy();
    }
}
