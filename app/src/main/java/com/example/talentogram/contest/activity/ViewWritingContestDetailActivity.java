package com.example.talentogram.contest.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.talentogram.R;

public class ViewWritingContestDetailActivity extends AppCompatActivity {
private TextView tvWritingDesc,tvWritingTitle;
private ImageView ivBack;
private LinearLayout llBack,llContent;
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_writing_contest_detail);

        Intent intent = getIntent();
        String description =intent.getStringExtra("description");
        String title =intent.getStringExtra("title");
        String color =intent.getStringExtra("color");
        String font_type =intent.getStringExtra("font");

        tvWritingDesc = findViewById(R.id.tvWritingDesc);
        tvWritingTitle = findViewById(R.id.tvWritingTitle);
        ivBack = findViewById(R.id.ivBack);
        llBack = findViewById(R.id.llBack);
        llContent = findViewById(R.id.llContent);

        tvWritingDesc.setText(description);
        tvWritingTitle.setText(title);

        if (!TextUtils.isEmpty(font_type)) {
            Typeface font = Typeface.createFromAsset(getAssets(),
                    "fonts/" + font_type);
            tvWritingDesc.setTypeface(font);
        }
        if (!TextUtils.isEmpty(color)) {
            String color_ ="#"+color;
            llContent.setBackgroundColor(Color.parseColor(color_));
        } else {
            llContent.setBackgroundColor(R.color.DarkGray);
        }
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}