package com.example.talentogram.contest.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.example.talentogram.R;
import com.example.talentogram.activity.CommentActivity;
import com.example.talentogram.activity.ZoomImageActivity;
import com.example.talentogram.contest.AudioDialogActivity;
import com.example.talentogram.contest.Contest_View_Fragment;
import com.example.talentogram.contest.activity.ViewWritingContestDetailActivity;
import com.example.talentogram.contest.model.ViewContestModel;
import com.example.talentogram.others.DownloadTask;
import com.example.talentogram.others.Utils;
import com.example.talentogram.profile.ProfilePostVideoPreviewFragment;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Contest_View_Adapter extends RecyclerView.Adapter<Contest_View_Adapter.MyViewHolder> {
    private Context context;
    private ArrayList<ViewContestModel> dataList;
    private RequestManager requestManager;


    public Contest_View_Adapter(Context context, ArrayList<ViewContestModel> contestVideoModels, RequestManager requestManager) {
        this.context = context;
        this.dataList = contestVideoModels;
        this.requestManager = requestManager;

    }

    @NonNull
    @Override
    public Contest_View_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_contest_upload_video, parent, false);
        return new Contest_View_Adapter.MyViewHolder(itemView);
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull Contest_View_Adapter.MyViewHolder holder, int position) {
        ViewContestModel model = dataList.get(position);

        //0-audio
        if (model.getPostType() == 0) {
            holder.mediaContainer.setVisibility(View.GONE);
            holder.ivImage.setVisibility(View.VISIBLE);
            holder.llWritingView.setVisibility(View.GONE);
            holder.tvTitle.setText(model.userdata.getFirstName() + " " + model.userdata.getLastName());

            Glide.with(context)
                    .load(model.getUserdata().getProfilePics())
                    .error(R.drawable.noimage)
                    .into(holder.ivUserProfile);

            holder.ivMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu(v, model.getAudioUrl().get(0).getAudioUrl(), model.getIs_download());
                }
            });

            Glide.with(context)
                    .load(R.drawable.audio_visualizer)
                    .error(R.drawable.noimage)
                    .into(holder.ivImage);

            holder.ivLike.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    holder.ivLike.setLiked(true);
                    likePost(model.getId(), holder);
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    holder.ivLike.setLiked(false);
                    disLikePost(model.getId(), holder);
                }
            });
            holder.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, CommentActivity.class);
                    i.putExtra("postId", String.valueOf(model.getId()));
                    context.startActivity(i);
                }
            });

            holder.llShare.setOnClickListener(v -> {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "TALENTOGRAM");
                    String shareMessage = "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + Services.BASE_URL + "share/" + model.getId();
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    context.startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.toString();
                }

            });
            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = model.getAudioUrl().get(0).getAudioUrl();
                    String title = model.getPostdata().getTitle();
                    Intent i = new Intent(context, AudioDialogActivity.class);
                    i.putExtra("AudioUrl", url);
                    i.putExtra("title", title);
                    context.startActivity(i);
                }
            });


        }
        //1-video
        else if (model.getPostType() == 1) {
            holder.mediaContainer.setVisibility(View.VISIBLE);
            holder.ivImage.setVisibility(View.GONE);
            holder.llWritingView.setVisibility(View.GONE);
            holder.tvTitle.setText(model.userdata.getFirstName() + " " + model.userdata.getLastName());

            Glide.with(context)
                    .load(model.getUserdata().getProfilePics())
                    .error(R.drawable.noimage)
                    .into(holder.ivUserProfile);

            ((Contest_View_Adapter.MyViewHolder) holder).onBind(dataList.get(position), requestManager);

            holder.ivMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu(v, model.getVideoUrl().get(0).getVideoUrl(), model.getIs_download());
                }
            });

            holder.mediaContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = model.getVideoUrl().get(0).getVideoUrl().replaceAll(" ", "%20");
                    Bundle bundle = new Bundle();
                    bundle.putString("video", url);
                    Contest_View_Fragment.getInstance().changeFragmentWithBundle(context, new ProfilePostVideoPreviewFragment(), true, bundle);
                }
            });
            holder.ivLike.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    holder.ivLike.setLiked(true);
                    likePost(model.getId(), holder);
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    holder.ivLike.setLiked(false);
                    disLikePost(model.getId(), holder);
                }
            });

            holder.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, CommentActivity.class);
                    i.putExtra("postId", String.valueOf(model.getId()));
                    context.startActivity(i);
                }
            });
            holder.llShare.setOnClickListener(v -> {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "TALENTOGRAM");
                    String shareMessage = "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + Services.BASE_URL + "share/" + model.getId();
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    context.startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.toString();
                }

            });

        }
        // 2-image
        else if (model.getPostType() == 2) {
            holder.mediaContainer.setVisibility(View.GONE);
            holder.ivImage.setVisibility(View.VISIBLE);
            holder.llWritingView.setVisibility(View.GONE);

            Glide.with(context)
                    .load(model.getUserdata().getProfilePics())
                    .error(R.drawable.noimage)
                    .into(holder.ivUserProfile);

            holder.tvTitle.setText(model.userdata.getFirstName() + " " + model.userdata.getLastName());

            Glide.with(context)
                    .load(model.getImageUrl().get(0).getVideoUrl())
                    .error(R.drawable.noimage)
                    .into(holder.ivImage);

            holder.ivMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu(v, model.getImageUrl().get(0).getVideoUrl(), model.getIs_download());
                }
            });
            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popUp(model.getImageUrl().get(0).getVideoUrl());
                }
            });

            holder.ivLike.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    holder.ivLike.setLiked(true);
                    likePost(model.getId(), holder);
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    holder.ivLike.setLiked(false);
                    disLikePost(model.getId(), holder);
                }
            });
            holder.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, CommentActivity.class);
                    i.putExtra("postId", String.valueOf(model.getId()));
                    context.startActivity(i);
                }
            });

            holder.llShare.setOnClickListener(v -> {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "TALENTOGRAM");
                    String shareMessage = "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + Services.BASE_URL + "share/" + model.getId();
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    context.startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.toString();
                }


            });

        }
        // 3-writing
        else if (model.getPostType() == 3) {
            holder.tvTitle.setText(model.userdata.getFirstName() + " " + model.userdata.getLastName());
            holder.mediaContainer.setVisibility(View.GONE);
            holder.ivImage.setVisibility(View.GONE);
            holder.llWritingView.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(model.getUserdata().getProfilePics())
                    .error(R.drawable.noimage)
                    .into(holder.ivUserProfile);
            holder.tvWritingDesc.setText(model.getPostdata().getDescription());


            if (!TextUtils.isEmpty(model.getColor())) {
                String color_ = "#" + model.getColor();
                holder.llWritingView.setBackgroundColor(Color.parseColor(color_));
            } else {
                holder.llWritingView.setBackgroundColor(R.color.DarkGray);
            }
            if (!TextUtils.isEmpty(model.getFont())) {
                String fontName = model.getFont();
                Typeface font = Typeface.createFromAsset(context.getAssets(),
                        "fonts/" + fontName);
                holder.tvWritingDesc.setTypeface(font);
            }

            holder.ivLike.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    holder.ivLike.setLiked(true);
                    likePost(model.getId(), holder);
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    holder.ivLike.setLiked(false);
                    disLikePost(model.getId(), holder);
                }
            });

            holder.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, CommentActivity.class);
                    i.putExtra("postId", String.valueOf(model.getId()));
                    context.startActivity(i);
                }
            });
            holder.llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "TALENTOGRAM");
                        String shareMessage = "\nLet me recommend you this application\n\n";
                        shareMessage = shareMessage + Services.BASE_URL + "share/" + model.getId();
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        context.startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } catch (Exception e) {
                        e.toString();
                    }

                }
            });

            holder.tvWritingDesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ViewWritingContestDetailActivity.class);
                    i.putExtra("description", model.getPostdata().getDescription());
                    i.putExtra("color", model.getColor());
                    i.putExtra("font", model.getFont());
                    i.putExtra("title", model.getPostdata().getTitle());
                    context.startActivity(i);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivImage, ivUserProfile, ivMenu;
        private TextView tvTitle, tvWritingDesc;
        FrameLayout mediaContainer;
        ProgressBar progressBar;
        RequestManager requestManager;
        View parent;
        ImageView mediaCoverImage, volumeControl;
        private LikeButton ivLike;
        private LinearLayout llComment, llShare, llWritingView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.ivImage);
            ivUserProfile = itemView.findViewById(R.id.ivUserProfile);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            mediaCoverImage = itemView.findViewById(R.id.ivMediaCoverImage);
            mediaContainer = itemView.findViewById(R.id.mediaContainer);
            progressBar = itemView.findViewById(R.id.progressBar);
            volumeControl = itemView.findViewById(R.id.ivVolumeControl);
            tvWritingDesc = itemView.findViewById(R.id.tvWritingDesc);
            ivMenu = itemView.findViewById(R.id.ivMenu);
            ivLike = itemView.findViewById(R.id.ivLike);
            llComment = itemView.findViewById(R.id.llComment);
            llShare = itemView.findViewById(R.id.llShare);
            llWritingView = itemView.findViewById(R.id.llWritingView);
            parent = itemView;
        }

        void onBind(ViewContestModel mediaObject, RequestManager requestManager) {
            this.requestManager = requestManager;
            parent.setTag(this);
            this.requestManager
                    .load(mediaObject.getVideoUrl().get(0).getVideoUrl())
                    .into(mediaCoverImage);
        }
    }

    public void showMenu(View v, String videoUrl, int isDownload) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_delete:
                        if (isDownload == 1) {
                            new DownloadTask(context, videoUrl);
                        } else if (isDownload == 0) {
                            Utils.showWarn(context, "You are not allow to download", 0);
                        }
                        break;

                }
                return false;
            }

        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.download_menu, popup.getMenu());
        popup.show();
    }

    private void likePost(int id, Contest_View_Adapter.MyViewHolder holder) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(id));
        userData.put("IsLike", String.valueOf(0));

        new NetworkCall(context, Services.LIKE_UNLIKE, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            holder.ivLike.setLiked(true);
                            notifyDataSetChanged();
                        } else {
                            disLikePost(id, holder);
                        }
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void disLikePost(int id, Contest_View_Adapter.MyViewHolder holder) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(id));
        userData.put("IsLike", String.valueOf(1));

        new NetworkCall(context, Services.LIKE_UNLIKE, userData,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            holder.ivLike.setLiked(false);
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
                        }
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void popUp(String imageUrl) {
        Intent i = new Intent(context, ZoomImageActivity.class);
        i.putExtra("imageurl",imageUrl);
        context.startActivity(i);
       /* LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.profile_zoom, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        final TouchImageView ivProfile = promptsView.findViewById(R.id.ivProfile);
        Glide.with(context)
                .load(imageUrl)
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .into(ivProfile);
        alertDialog.show();*/
    }

}
