package com.example.talentogram.contest.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.talentogram.R;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SaveContestWritingAvtivity extends AppCompatActivity {
    int IsCheckedForUpload = 1, IsCheckedForDownload = 1;
    private TextView tvContestdetails;
    private Spinner sp_TextType;
    private ArrayAdapter<String> textTypeAdapter;
    private List<String> sp_textTypeList = new ArrayList<String>();
    private EditText edtContestTitle;
    private CheckBox chUploadToPost, chAllowDownload;
    private LinearLayout llback,llContent;
    private Button btnSave;
    private String content,bg_color,Font;
    private String ContestId, TimeSlotId, ContestType;
    private String editor_type;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_contest_writing);
        Intent i = getIntent();
        bg_color = i.getStringExtra("color");
        Font = i.getStringExtra("font");
        content = i.getStringExtra("content");
        ContestId = i.getStringExtra("ContestId");
        ContestType = i.getStringExtra("ContestType");
        TimeSlotId = i.getStringExtra("TimeSlotId");

        init();
        bindView();
        setContestTypeSpinner();
    }

    private void setContestTypeSpinner() {
        sp_textTypeList.add("Select Type");
        sp_textTypeList.add("QUOTES");
        sp_textTypeList.add("POETRY");
        sp_textTypeList.add("STORY");
        textTypeAdapter = new ArrayAdapter<String>(SaveContestWritingAvtivity.this, android.R.layout.simple_list_item_1, sp_textTypeList);
        sp_TextType.setAdapter(textTypeAdapter);
    }
    private void init() {
        sp_TextType = (Spinner) findViewById(R.id.sp_TextType);
        btnSave = (Button) findViewById(R.id.btnSave);
        edtContestTitle = findViewById(R.id.edtContestTitle);
        chAllowDownload = findViewById(R.id.chAllowDownload);
        chUploadToPost = findViewById(R.id.chUploadToPost);
        llback = findViewById(R.id.llBack);

    }
    private void bindView() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveEditor();
            }
        });
        chUploadToPost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsCheckedForUpload = 1;
                } else {
                    IsCheckedForUpload = 0;
                }
            }
        });
        chAllowDownload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsCheckedForDownload = 1;
                } else {
                    IsCheckedForDownload = 0;
                }
            }
        });
        sp_TextType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                editor_type = adapter.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
        llback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void saveEditor() {

        String contest_title = edtContestTitle.getText().toString().trim();
        if (editor_type.equals("Select Type")) {
            Utils.showWarn(SaveContestWritingAvtivity.this, "Please Select Type");
        } else if (TextUtils.isEmpty(contest_title)) {
            edtContestTitle.requestFocus();
            edtContestTitle.setError(getString(R.string.blank_validation_title));
        } else {
            HashMap<String, String> userData = new HashMap<>();
            userData.put("PostType", ContestType);
            userData.put("Title", contest_title);
            userData.put("NewHashtags", editor_type);
            userData.put("Description",content);
            userData.put("AdminSongId", String.valueOf(1));
            userData.put("PostHashTagIds", String.valueOf(173));
            userData.put("UploadToPost", String.valueOf(IsCheckedForUpload));
            userData.put("isDownload", String.valueOf(IsCheckedForDownload));
            userData.put("ContestId", ContestId);
            userData.put("TimeSlotId", TimeSlotId);
            userData.put("Color", bg_color);
            userData.put("Font", Font);

            HashMap<String, File> userFile = new HashMap<>();

            new NetworkCall(SaveContestWritingAvtivity.this, Services.APPLY_FOR_FREE_CONTEST,
                    userData, userFile, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            String message = obj.getString("message");
                            if (Success) {
                                Utils.showWarn(SaveContestWritingAvtivity.this, message, 2);
                            } else {
                                Utils.showWarn(SaveContestWritingAvtivity.this, message, 1);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }
}