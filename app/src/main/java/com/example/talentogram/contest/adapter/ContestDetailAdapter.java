package com.example.talentogram.contest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.contest.model.ContestDetailModel;

import java.util.ArrayList;

public class ContestDetailAdapter extends RecyclerView.Adapter<ContestDetailAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ContestDetailModel> contestDetaildata;

    public ContestDetailAdapter(Context context, ArrayList<ContestDetailModel> contestDetaildata) {

        this.context = context;
        this.contestDetaildata = contestDetaildata;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_contest_detail, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
