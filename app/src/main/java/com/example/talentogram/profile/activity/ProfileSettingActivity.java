package com.example.talentogram.profile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.talentogram.R;
import com.example.talentogram.activity.PrivacyActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileSettingActivity extends AppCompatActivity {

    @BindView(R.id.llPrivacy)
    LinearLayout llPrivacy;

    @BindView(R.id.llAds)
    LinearLayout llAds;

    @BindView(R.id.llPayment)
    LinearLayout llPayment;

    @BindView(R.id.llAccount)
    LinearLayout llAccount;

    @BindView(R.id.llHelp)
    LinearLayout llHelp;

    @BindView(R.id.llabout)
    LinearLayout llabout;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);
        ButterKnife.bind(this);
        tvTitle.setText(getResources().getString(R.string.settings));
        bindWidget();
    }

    private void bindWidget() {
        llAccount.setOnClickListener(v -> {
            Intent i = new Intent(ProfileSettingActivity.this, AccountsSettingActivity.class);
            startActivity(i);
        });
        llBack.setOnClickListener(v -> onBackPressed());

        llPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfileSettingActivity.this, PrivacyActivity.class);
                startActivity(i);
            }
        });
    }
}
