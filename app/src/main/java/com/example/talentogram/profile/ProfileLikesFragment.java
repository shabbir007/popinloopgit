package com.example.talentogram.profile;


import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.profile.adapter.OtherUserProfileLikedVideoAdapter;
import com.example.talentogram.profile.adapter.ProfileUserLikedPostAdapter;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.model.MyPostModel;
import com.example.talentogram.model.OtherUsersProfileModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileLikesFragment extends Fragment {

    private RecyclerView rv_likes;
    private ArrayList<MyPostModel.UserOwnLikeVideo> postlikesModelArrayList;
    int user_id;
    private TextView ivNoDataFound;
    private ArrayList<OtherUsersProfileModel.userLikeVideo> otherUsersProfileModels;

    public ProfileLikesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_likes__profile, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            user_id = bundle.getInt("userId");
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv_likes = view.findViewById(R.id.rv_likes);
        ivNoDataFound = view.findViewById(R.id.ivNoDataFound);
        if (user_id == Integer.parseInt(MySharedPref.getString(getContext(), MySharedPref.USER_ID, ""))) {

            setData();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 100);
        } else {
            setDataForOtherUser();

        }
    }

    private void getOtherUserVideo() {
        {
            HashMap<String, String> userData = new HashMap<>();
            userData.put("other_user_id", String.valueOf(user_id));

            new NetworkCall(getContext(), Services.GET_OTHER_USER_PROFILE,
                    userData, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");

                            if (Success) {
                                JSONObject jPostData = obj.getJSONObject("user_data");
                                JSONArray jPostedVideo = jPostData.getJSONArray("user_like_video");

                                otherUsersProfileModels = new ArrayList<>();

                                otherUsersProfileModels = (ArrayList<OtherUsersProfileModel.userLikeVideo>) new Gson().fromJson(jPostedVideo.toString(),
                                        new TypeToken<ArrayList<OtherUsersProfileModel.userLikeVideo>>() {
                                        }.getType());

                                rv_likes.setVisibility(View.VISIBLE);
                                ivNoDataFound.setVisibility(View.GONE);
                                setDataForOtherUser();
                            } else {
                                rv_likes.setVisibility(View.GONE);
                                ivNoDataFound.setVisibility(View.VISIBLE);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });

        }
    }

    private void getPost() {
        new NetworkCall(getActivity(), Services.USER_OWN_LIKE_POST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");

                        if (Success) {
                            JSONArray jPostData = obj.getJSONArray("data");
                            postlikesModelArrayList = new ArrayList<>();
                            postlikesModelArrayList = (ArrayList<MyPostModel.UserOwnLikeVideo>) new Gson().fromJson(jPostData.toString(),
                                    new TypeToken<ArrayList<MyPostModel.UserOwnLikeVideo>>() {
                                    }.getType());

                            rv_likes.setVisibility(View.VISIBLE);
                            ivNoDataFound.setVisibility(View.GONE);
                            setData();
                        } else {
                            rv_likes.setVisibility(View.GONE);
                            ivNoDataFound.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    private void setData() {
        rv_likes.setNestedScrollingEnabled(false);
        rv_likes.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rv_likes.setAdapter(new ProfileUserLikedPostAdapter(getActivity(), MyConstants.userOwnLikeVideos));
    }
    private void setDataForOtherUser() {
        rv_likes.setNestedScrollingEnabled(false);
        rv_likes.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rv_likes.setAdapter(new OtherUserProfileLikedVideoAdapter(getActivity(), MyConstants.otherUserLikeVideos));

    }
}
