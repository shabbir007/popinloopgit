package com.example.talentogram.profile.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.example.talentogram.R;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.activity.ZoomImageActivity;
import com.example.talentogram.contest.activity.ViewWritingContestDetailActivity;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.TouchImageView;
import com.example.talentogram.profile.ProfilePostVideoPreviewFragment;
import com.example.talentogram.model.MyPostModel;
import com.example.talentogram.others.Utils;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class ProfileUserPostedVideoAdapter extends RecyclerView.Adapter<ProfileUserPostedVideoAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<MyPostModel.UserOwnPost> myPostArray;

    public ProfileUserPostedVideoAdapter(Context context, ArrayList<MyPostModel.UserOwnPost> myPostArray) {
        this.context = context;
        this.myPostArray = myPostArray;

    }

    @NonNull
    @Override
    public ProfileUserPostedVideoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.post_profile, parent, false);

        return new ProfileUserPostedVideoAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ProfileUserPostedVideoAdapter.MyViewHolder holder, int position) {
        MyPostModel.UserOwnPost model = myPostArray.get(position);
        if (model.getIsInContest() == 1) {
            holder.ivContest.setVisibility(View.VISIBLE);
        } else {
            holder.ivContest.setVisibility(View.GONE);
        }
        //audio
        if (model.getPostType() == 0) {
            holder.rlvideoView.setVisibility(View.VISIBLE);
            holder.flWriting.setVisibility(View.GONE);

        }
        //videoView
        else if (model.getPostType() == 1) {
            holder.rlvideoView.setVisibility(View.VISIBLE);
            holder.flWriting.setVisibility(View.GONE);
            try {

            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("video", model.getVideoUrl().get(0).getVideoUrl());
                    bundle.putBoolean("isDashboard", true);
                    DashBoardActivity.changeFragmentWithBundle(context, new ProfilePostVideoPreviewFragment(), true, bundle);
                }
            });

        }
        //imageView
        else if (model.getPostType() == 2) {
            holder.rlvideoView.setVisibility(View.VISIBLE);
            holder.flWriting.setVisibility(View.GONE);

            holder.videoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popUpForImage(model.getImageUrl().get(0).getImageUrl());
                }
            });

        }
        //Writing
        else if (model.getPostType() == 3) {
            holder.rlvideoView.setVisibility(View.GONE);
            holder.flWriting.setVisibility(View.VISIBLE);

            holder.llWritingView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ViewWritingContestDetailActivity.class);
                    i.putExtra("description", model.getPost_detail().getDescription());
                    i.putExtra("title", model.getPost_detail().getTitle());
                    i.putExtra("color", model.getColor());
                    i.putExtra("font", model.getFont());
                    context.startActivity(i);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return myPostArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView videoView, ivContest;
        public TextView tvTextView;
        private RelativeLayout rlvideoView;
        private FrameLayout flWriting;
        private LinearLayout llWritingView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            videoView = itemView.findViewById(R.id.videoView);
            tvTextView = itemView.findViewById(R.id.tvTextView);
            rlvideoView = itemView.findViewById(R.id.rlvideoView);
            ivContest = itemView.findViewById(R.id.ivContest);
            flWriting = itemView.findViewById(R.id.flWriting);
            llWritingView = itemView.findViewById(R.id.llWritingView);

        }

    }

    private void popUpForImage(String imageUrl) {
        Intent i = new Intent(context, ZoomImageActivity.class);
        i.putExtra("imageurl",imageUrl);
        context.startActivity(i);
       /* LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.profile_zoom, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        final TouchImageView ivProfile = promptsView.findViewById(R.id.ivProfile);
        Glide.with(context)
                .load(imageUrl)
                .error(R.drawable.noimage)
                .placeholder(R.drawable.noimage)
                .into(ivProfile);
        alertDialog.show();*/
    }

}
