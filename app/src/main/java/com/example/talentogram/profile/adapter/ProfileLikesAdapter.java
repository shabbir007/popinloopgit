package com.example.talentogram.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.custom_views.PicassoTrustAll;
import com.example.talentogram.model.MyPostModel;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class ProfileLikesAdapter extends RecyclerView.Adapter<ProfileLikesAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<MyPostModel> MyLikeArray;

    public ProfileLikesAdapter(Context context, ArrayList<MyPostModel> MyLikeArray) {
        this.context = context;
        this.MyLikeArray = MyLikeArray;

    }

    @NonNull
    @Override
    public ProfileLikesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.likes_profile, parent, false);

        return new ProfileLikesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileLikesAdapter.MyViewHolder holder, int position) {

        MyPostModel myPostModel = MyLikeArray.get(position);
        try {
            PicassoTrustAll.getInstance(context)
                    .load(myPostModel.getPostImageUrl())
                    .into(holder.ivLikes, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return MyLikeArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivLikes;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivLikes = itemView.findViewById(R.id.ivLikes);
        }
    }
}
