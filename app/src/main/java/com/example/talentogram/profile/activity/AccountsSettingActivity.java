package com.example.talentogram.profile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.talentogram.R;
import com.example.talentogram.activity.PasswordChangeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountsSettingActivity extends AppCompatActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.llPassChange)
    LinearLayout llPassChange;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.tvTitle)
    TextView tvTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts_setting);
        ButterKnife.bind(this);
        tvTitle.setText(getResources().getString(R.string.account_settings));
        bindWidget();
    }

    private void bindWidget() {
        llBack.setOnClickListener(v -> onBackPressed());
        llPassChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AccountsSettingActivity.this, PasswordChangeActivity.class);
                startActivity(i);
            }
        });

    }
}