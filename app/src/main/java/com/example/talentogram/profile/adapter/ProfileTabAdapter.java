package com.example.talentogram.profile.adapter;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.talentogram.profile.ProfileLikesFragment;
import com.example.talentogram.profile.ProfilePostFragment;

public class ProfileTabAdapter extends FragmentPagerAdapter {

    private Context context;
    int totalTabs;
    int userId;

    public ProfileTabAdapter(Context context, FragmentManager fm, int totalTabs,int userId) {
        super(fm);
        this.context = context;
        this.totalTabs = totalTabs;
        this.userId = userId;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                ProfilePostFragment PPF= new ProfilePostFragment();;
                Bundle args = new Bundle();
                args.putInt("userId",userId);
                PPF.setArguments(args);
                return PPF;
            case 1:
                ProfileLikesFragment LPF = new ProfileLikesFragment();
                Bundle arg = new Bundle();
                arg.putInt("userId",userId);
                LPF.setArguments(arg);
                return LPF;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Post";
            case 1:
                return "Likes";
            default:
                return null;
        }
    }
}
