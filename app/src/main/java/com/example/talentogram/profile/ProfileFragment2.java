package com.example.talentogram.profile;


import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.model.MyPostModel;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.profile.activity.EditProfileActivity;
import com.example.talentogram.activity.FollowersActivity;
import com.example.talentogram.activity.FollowingActivity;
import com.example.talentogram.gallery.GalleryActivity;
import com.example.talentogram.profile.adapter.ProfileTabAdapter;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.custom_views.PicassoTrustAll;
import com.example.talentogram.custom_views.WrapContentHeightViewPager;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.skydoves.elasticviews.ElasticButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;

import static com.example.talentogram.activity.DashBoardActivity.toolbar;

public class ProfileFragment2 extends Fragment {
    private View view;
    private TabLayout tabLayout;
    private WrapContentHeightViewPager viewPager;
    private ImageView ivUserProfile,ivGallery;
    private CircularImageView ivRound;
    private PopupWindow changeStatusPopUp;
    private ElasticButton btnEditprofile;
    private LinearLayout llFollowing, llFollowers;
    private CustomTextView tvUserName, tvUserplusname, tvNumFollowing, tvNumFollowers,tvVideos;
    int followingList, followerList;
    private CustomTextView tvUsername;
    private String tvUserProfile;


    public ProfileFragment2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile2, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(getActivity());

        setHasOptionsMenu(true);
        init();
        bindView();

        getUserProfile();
        tabLayoutView();
        return view;
    }

    private void init() {
        tabLayout = view.findViewById(R.id.Tab_Profile);
        viewPager = view.findViewById(R.id.VP_Profile);
        btnEditprofile = view.findViewById(R.id.btnEditprofile);
        llFollowing = view.findViewById(R.id.llFollowing);
        llFollowers = view.findViewById(R.id.llFollowers);
        tvUsername = view.findViewById(R.id.tvUsername);
        ivRound = view.findViewById(R.id.ivround);
        tvNumFollowing = view.findViewById(R.id.tvNumFollowing);
        tvNumFollowers = view.findViewById(R.id.tvNumFollowers);
        ivUserProfile = view.findViewById(R.id.ivUserProfile);
        ivGallery = view.findViewById(R.id.ivGallery);
        tvVideos = view.findViewById(R.id.tvVideos);
    }

    private void popUpForProfile() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.profile_zoom, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        final ImageView ivProfile =  promptsView.findViewById(R.id.ivProfile);
        Glide.with(getContext())
                .load(tvUserProfile)
                .thumbnail(Glide.with(getContext()).load(R.drawable.noimage))
                .error(R.drawable.noimage)
                .into(ivProfile);
        alertDialog.show();
    }
    private void getUserProfile() {
        String userId = new MySharedPref(getContext()).getLoggedInUserID();
        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("UserProfileId", userId);

        new NetworkCall(getContext(), Services.GETUSERPROFILE,
                userdata, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");

                        if (Success) {
                            JSONObject user_data = obj.getJSONObject("user_data");
                            String FollowingCount = user_data.getString("FollowingCount");
                            String FollowerCount = user_data.getString("FollowerCount");
                            String FirstName = user_data.getString("FirstName");
                            String LastName = user_data.getString("LastName");
                            String ProfilePic = user_data.getString("ProfilePic");
                            JSONArray jUserOwnData = obj.getJSONArray("user_own_post");
                            JSONArray jUserLikedData = obj.getJSONArray("user_own_like_post");
                            tvNumFollowing.setText(FollowingCount);
                            tvNumFollowers.setText(FollowerCount);
                            tvUserName.setText(FirstName+" "+LastName);
                            tvUserplusname.setText(FirstName + " " + LastName);
                            tvVideos.setText(jUserOwnData.length()+" "+"Posts");

                            Glide.with(getContext())
                                    .load(ProfilePic)
                                    .thumbnail(Glide.with(getContext()).load(R.drawable.noimage))
                                    .error(R.drawable.noimage)
                                    .into(ivRound);
                            tvUserProfile= ProfilePic;
                            MyConstants.userOwnPosts.clear();
                            MyConstants.userOwnPosts = (ArrayList<MyPostModel.UserOwnPost>) new Gson().fromJson(jUserOwnData.toString(),
                                    new TypeToken<ArrayList<MyPostModel.UserOwnPost>>() {
                                    }.getType());
                            MyConstants.userOwnLikeVideos.clear();
                            MyConstants.userOwnLikeVideos = (ArrayList<MyPostModel.UserOwnLikeVideo>) new Gson().fromJson(jUserLikedData.toString(),
                                    new TypeToken<ArrayList<MyPostModel.UserOwnLikeVideo>>() {
                                    }.getType());

                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void bindView() {
        btnEditprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), EditProfileActivity.class));
            }
        });

        llFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FollowersActivity.class);
                startActivity(intent);
            }
        });
        llFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FollowingActivity.class);
                startActivity(intent);
            }
        });
        ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GalleryActivity.class);
                Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getActivity(), R.anim.animstart, R.anim.animation2).toBundle();
                startActivity(intent, bndlanimation);
            }
        });
        ivRound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpForProfile();
            }
        });

    }

    private void popUp() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.heart, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();
        final Button btnGot = (Button) alertDialog.findViewById(R.id.btnGot);
        btnGot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.hide();
            }
        });
    }

    private void tabLayoutView() {
        tabLayout.addTab(tabLayout.newTab().setText("Post"));
        tabLayout.addTab(tabLayout.newTab().setText("Likes"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ProfileTabAdapter adapter = new ProfileTabAdapter(getActivity(), getChildFragmentManager(), tabLayout.getTabCount(),
                Integer.parseInt(MySharedPref.getString(getContext(),MySharedPref.USER_ID,"")));
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        toolbar.setVisibility(View.VISIBLE);

    }
    public void onResume() {
        super.onResume();
        String tvUserFirstName = MySharedPref.getString(getContext(), MySharedPref.USER_FIRSTNAME, "");
        String tvUserLastName = MySharedPref.getString(getContext(), MySharedPref.USER_LASTNAME, "");
        tvUsername.setText(tvUserFirstName+" " + tvUserLastName);

        PicassoTrustAll.getInstance(getContext())
                .load(MySharedPref.getString(getContext(), MySharedPref.USER_PROFILE_PIC, ""))
                .into(ivRound);

        PicassoTrustAll.getInstance(getContext())
                .load(MySharedPref.getString(getContext(), MySharedPref.USER_PROFILE_PIC, ""))
                .into(ivUserProfile);
    }
}
