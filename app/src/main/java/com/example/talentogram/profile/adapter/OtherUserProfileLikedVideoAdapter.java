package com.example.talentogram.profile.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.profile.ProfilePostVideoPreviewFragment;
import com.example.talentogram.model.OtherUsersProfileModel;
import com.example.talentogram.others.Utils;

import java.util.ArrayList;

public class OtherUserProfileLikedVideoAdapter extends RecyclerView.Adapter<OtherUserProfileLikedVideoAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<OtherUsersProfileModel.userLikeVideo> myPostArray;


    public OtherUserProfileLikedVideoAdapter(Context context, ArrayList<OtherUsersProfileModel.userLikeVideo> myPostArray) {
        this.context = context;
        this.myPostArray = myPostArray;
    }

    @NonNull
    @Override
    public OtherUserProfileLikedVideoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.post_profile, parent, false);

        return new OtherUserProfileLikedVideoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OtherUserProfileLikedVideoAdapter.MyViewHolder holder, int position) {

        OtherUsersProfileModel.userLikeVideo otherUsersProfileModel = myPostArray.get(position);
        try {
            Bitmap bitmap = Utils.retriveVideoFrameFromVideo(otherUsersProfileModel.getVideoUrl());
            if (bitmap != null) {
                holder.videoView.setImageBitmap(bitmap);
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("video", otherUsersProfileModel.getVideoUrl());
                DashBoardActivity.changeFragmentWithBundle(context, new ProfilePostVideoPreviewFragment(), true, bundle);
            }
        });
    }


    @Override
    public int getItemCount() {
        return myPostArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView videoView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            videoView = itemView.findViewById(R.id.videoView);

        }

    }

}
