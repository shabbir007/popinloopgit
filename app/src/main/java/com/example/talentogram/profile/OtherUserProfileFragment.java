package com.example.talentogram.profile;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.chat.activity.ChatActivity;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.profile.adapter.ProfileTabAdapter;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.custom_views.PicassoTrustAll;
import com.example.talentogram.custom_views.WrapContentHeightViewPager;
import com.example.talentogram.dialog.ShareBottomSheetDialog;
import com.example.talentogram.model.OtherUsersProfileModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.talentogram.activity.DashBoardActivity.toolbar;

public class OtherUserProfileFragment extends Fragment {
    private View view;
    private TabLayout tabLayout;
    private WrapContentHeightViewPager viewPager;
    private ImageView ivdot;
    private CircularImageView ivRound;
    private CustomTextView tvUserName, tvUserplusname, tvNumFollowing, tvNumFollowers,tvVideos;
    int userId;
    private ArrayList<OtherUsersProfileModel> otherUsersProfileModels;
    private Button btnFollow, btnMessage;
    private String Profilepic;
    boolean isFriend = false;

    public OtherUserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_other_user_profile, container, false);
        setHasOptionsMenu(true);
        assert getArguments() != null;
        userId = getArguments().getInt("userId");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getUserData();
            }
        },1000);
        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        tabLayoutView();
        bindView();
    }

    private void init() {
        tabLayout = view.findViewById(R.id.Tab_Profile);
        viewPager = view.findViewById(R.id.VP_Profile);
        ivdot = view.findViewById(R.id.ivDot);
        tvUserName = view.findViewById(R.id.tvUsernamee);
        tvUserplusname = view.findViewById(R.id.tvUserplusname);
        ivRound = view.findViewById(R.id.ivround);
        btnFollow = view.findViewById(R.id.btnFollow);
        btnMessage = view.findViewById(R.id.btnMessage);
        tvNumFollowing = view.findViewById(R.id.tvNumFollowing);
        tvNumFollowers = view.findViewById(R.id.tvNumFollowers);
        tvVideos = view.findViewById(R.id.tvVideos);
    }

    private void bindView() {
        ivdot.setOnClickListener(view -> showMenu(ivdot));
        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isFriend)
                {
                    followUser();
                }
            }
        });
        ivRound.setOnClickListener(v -> popUpForProfile());
        btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getContext(), ChatActivity.class);
                intent1.putExtra("userId", String.valueOf(userId));
                intent1.putExtra("pos", "");
                intent1.putExtra("isRecentChat", "no");
                startActivity(intent1);
            }
        });
    }

    private void popUpForProfile() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.profile_zoom, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        final ImageView ivProfile = promptsView.findViewById(R.id.ivProfile);
        Glide.with(getContext())
                .load(Profilepic)
                .thumbnail(Glide.with(getContext()).load(R.drawable.noimage))
                .error(R.drawable.noimage)
                .into(ivProfile);
        alertDialog.show();
    }

    private void followUser() {
        HashMap<String, String> user_data = new HashMap<>();
        user_data.put("FollowerUserId", String.valueOf(userId));

        new NetworkCall(getContext(), Services.DOFOLLOW, user_data,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                            alertDialog.setTitle("Success");
                            alertDialog.setMessage(message);
                            alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                btnFollow.setText("Following");
                            });
                            alertDialog.show();

                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                            alertDialog.setTitle("Error");
                            alertDialog.setMessage(message);
                            alertDialog.show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(getContext(), v);

        try {
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popup);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_share:
                        ShareBottomSheetDialog share = new ShareBottomSheetDialog();
                        share.show(getActivity().getSupportFragmentManager(), "ShareBottomSheetDialog");
                        break;
                    case R.id.item_block:
                        blockUser();
                        break;

                }
                return false;
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.other_user_profilepopup, popup.getMenu());
        popup.show();

    }

    private void blockUser() {
        {
            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
            alertDialog.setTitle(R.string.blank_warning_block);

            alertDialog.setPositiveButton("Yes", (dialog, which) -> {
                HashMap<String, String> user_data = new HashMap<>();

                user_data.put("block_user_id", String.valueOf(userId));

                new NetworkCall(getContext(), Services.BLOCK_USER, user_data,
                        true, NetworkCall.REQ_POST,
                        true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response) {

                        try {
                            if (!response.isEmpty()) {
                                JSONObject obj = new JSONObject(response);
                                boolean Success = obj.getBoolean("Success");
                                String message = obj.getString("message");
                                if (Success) {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {

                                    });
                                    alertDialog.show();

                                } else {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                                    alertDialog.setTitle("Error");
                                    alertDialog.setMessage(message);
                                    alertDialog.show();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            });
            alertDialog.setNegativeButton("No", (dialog, which) -> {
                dialog.cancel();
            });
            alertDialog.show();

        }
    }

    private void tabLayoutView() {
        tabLayout.addTab(tabLayout.newTab().setText("Post"));
        tabLayout.addTab(tabLayout.newTab().setText("Likes"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ProfileTabAdapter adapter = new ProfileTabAdapter(getActivity(), getChildFragmentManager(),
                tabLayout.getTabCount(), userId);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getUserData() {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("other_user_id", String.valueOf(userId));

        new NetworkCall(getContext(), Services.GET_OTHER_USER_PROFILE,
                userData, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");

                        if (Success) {
                            JSONObject jPostData = obj.getJSONObject("user_data");
                            JSONArray jPostedVideo = jPostData.getJSONArray("user_post_video");
                            JSONArray jPostLikedVideo = jPostData.getJSONArray("user_like_video");



                            MyConstants.otherUserPostVideos = (ArrayList<OtherUsersProfileModel.userPostVideo>) new Gson().fromJson(jPostedVideo.toString(),
                                    new TypeToken<ArrayList<OtherUsersProfileModel.userPostVideo>>() {
                                    }.getType());

                            MyConstants.otherUserLikeVideos = (ArrayList<OtherUsersProfileModel.userLikeVideo>) new Gson().fromJson(jPostLikedVideo.toString(),
                                    new TypeToken<ArrayList<OtherUsersProfileModel.userLikeVideo>>() {
                                    }.getType());

                            String tvUserFirstName = jPostData.getString("FirstName");
                            String tvUserLastName = jPostData.getString("LastName");

                            tvUserName.setText(tvUserFirstName + " " + tvUserLastName);
                            tvUserplusname.setText(tvUserFirstName + " " + tvUserLastName);
                            Profilepic = jPostData.getString("ProfilePic");
                            if (!TextUtils.isEmpty(jPostData.getString("ProfilePic"))) {
                                PicassoTrustAll.getInstance(getContext())
                                        .load(jPostData.getString("ProfilePic"))
                                        .error(R.drawable.ic_add_image)
                                        .into(ivRound);
                            }
                            if (!obj.getString("IsFriend").equals("0")) {
                                btnFollow.setText("Friends");
                                isFriend = true;
                            }
                            tvNumFollowers.setText(String.valueOf(obj.getString("FollowingCount")));
                            tvNumFollowing.setText(String.valueOf(obj.getString("FollowerCount")));

                            tvVideos.setText(jPostedVideo.length()+" "+ "Posts");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        toolbar.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
