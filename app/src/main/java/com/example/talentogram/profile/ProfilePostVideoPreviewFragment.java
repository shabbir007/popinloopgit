package com.example.talentogram.profile;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.talentogram.R;
import com.example.talentogram.contest.Contest_View_Fragment;
import com.example.talentogram.contest.activity.ContestActivity;


import static com.example.talentogram.others.Utils.timeConversion;

public class ProfilePostVideoPreviewFragment extends Fragment {
    private String str_video;
    private VideoView vv_video;
    private ImageView ivBack, pause;
    private Button btnNext;

    private SeekBar seekBar;
    double current_pos, total_duration;
    private TextView current, total;
    private LinearLayout showProgress;
    private Handler mHandler, handler;

    public ProfilePostVideoPreviewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_videoview, container, false);
        str_video = this.getArguments().getString("video");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        bindWidget();
    }

    private void init(View view) {
        vv_video = (VideoView) view.findViewById(R.id.vv_video);
        ivBack = (ImageView) view.findViewById(R.id.ivBack);
        btnNext =  view.findViewById(R.id.btnNext);
        pause = (ImageView) view.findViewById(R.id.pause);
        seekBar = (SeekBar) view.findViewById(R.id.seekbar);
        current = (TextView) view.findViewById(R.id.current);
        total = (TextView) view.findViewById(R.id.total);
        showProgress = (LinearLayout) view.findViewById(R.id.showProgress);
        mHandler = new Handler();
        handler = new Handler();
        vv_video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                setVideoProgress();
            }
        });
        playVideo(str_video);
        setPause();
        btnNext.setVisibility(View.GONE);
    }

    private void bindWidget() {
        ivBack.setOnClickListener(view ->onBackPressed());
    }

    public void onBackPressed() {
       if(!ContestActivity.getInstance().isVideoOpen)
       {
          getActivity().onBackPressed();
       }
       else {
           getFragmentManager().popBackStack();
       }
   }

    public void setVideoProgress() {
        //get the video duration
        current_pos = vv_video.getCurrentPosition();
        total_duration = vv_video.getDuration();

        //display video duration
        total.setText(timeConversion((long) total_duration));
        current.setText(timeConversion((long) current_pos));
        seekBar.setMax((int) total_duration);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    current_pos = vv_video.getCurrentPosition();
                    current.setText(timeConversion((long) current_pos));
                    seekBar.setProgress((int) current_pos);
                    handler.postDelayed(this, 1000);
                } catch (IllegalStateException ed) {
                    ed.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 1000);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                current_pos = seekBar.getProgress();
                vv_video.seekTo((int) current_pos);
            }
        });
    }

    public void playVideo(String str_video) {
        try {
            vv_video.setVideoURI(Uri.parse(str_video));
            vv_video.start();
            pause.setImageResource(R.drawable.d_ic_pause_circle_filled_black_24dp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //pause video
    public void setPause() {
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vv_video.isPlaying()) {
                    vv_video.pause();
                    pause.setImageResource(R.drawable.d_ic_play_circle_filled_black_24dp);
                } else {
                    vv_video.start();
                    pause.setImageResource(R.drawable.d_ic_pause_circle_filled_black_24dp);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        assert getArguments() != null;
        if(!getArguments().containsKey("isDashboard")) {
            if (Contest_View_Fragment.getInstance() != null) {
                Contest_View_Fragment.getInstance().frame.setVisibility(View.GONE);
            }
            if (ContestActivity.getInstance() != null) {
                ContestActivity.getInstance().bottomNavigationView.setVisibility(View.VISIBLE);
                ContestActivity.getInstance().viewPager.disableScroll(false);
                ContestActivity.getInstance().isVideoOpen = false;
                ContestActivity.getInstance().topbar.setVisibility(View.VISIBLE);
            }
        }
        super.onDestroy();
    }
}
