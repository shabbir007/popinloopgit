package com.example.talentogram.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.model.MyPostModel;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.profile.activity.EditProfileActivity;
import com.example.talentogram.activity.FollowersActivity;
import com.example.talentogram.activity.FollowingActivity;
import com.example.talentogram.activity.JoinMembershipActivity;
import com.example.talentogram.activity.LoginActivity;
import com.example.talentogram.profile.activity.ProfileSettingActivity;
import com.example.talentogram.profile.adapter.ProfileTabAdapter;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.custom_views.WrapContentHeightViewPager;
import com.example.talentogram.dialog.ShareBottomSheetDialog;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.talentogram.activity.DashBoardActivity.toolbar;

public class ProfileFragment extends Fragment {

    private View view;
    private TabLayout tabLayout;
    private WrapContentHeightViewPager viewPager;
    private ImageView ivdot;
    private CircularImageView ivRound;
    private Button btnEditprofile;
    private Button btnJoinmembership;
    private LinearLayout  llFollowing, llFollowers;
    private CustomTextView tvUserName, tvUserplusname, tvNumFollowing, tvNumFollowers,tvVideos;
    PopupMenu popup;


    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        init();
        bindView();

        setHasOptionsMenu(true);
        return view;
    }


    private void bindView() {
        btnEditprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), EditProfileActivity.class));
            }
        });
        btnJoinmembership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), JoinMembershipActivity.class));
            }
        });

        llFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FollowersActivity.class);
                startActivity(intent);
            }
        });
        llFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FollowingActivity.class);
                startActivity(intent);
            }
        });
        ivdot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMenu(ivdot);

            }
        });
        ivRound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpForProfile();
            }
        });
    }

    private void init() {
        tabLayout = view.findViewById(R.id.Tab_Profile);
        viewPager = view.findViewById(R.id.VP_Profile);
        ivdot = view.findViewById(R.id.ivDot);
        btnEditprofile = view.findViewById(R.id.btnEditprofile);
        btnJoinmembership = view.findViewById(R.id.btnJoinmembership);
        llFollowing = view.findViewById(R.id.llFollowing);
        llFollowers = view.findViewById(R.id.llFollowers);
        tvUserName = view.findViewById(R.id.tvUsernamee);
        tvUserplusname = view.findViewById(R.id.tvUserplusname);
        ivRound = view.findViewById(R.id.ivround);
        tvNumFollowing = view.findViewById(R.id.tvNumFollowing);
        tvNumFollowers = view.findViewById(R.id.tvNumFollowers);
        tvVideos = view.findViewById(R.id.tvVideos);
    }

    private void tabLayoutView() {
        tabLayout.addTab(tabLayout.newTab().setText("Post"));
        tabLayout.addTab(tabLayout.newTab().setText("Likes"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);

        final ProfileTabAdapter adapter = new ProfileTabAdapter(getActivity(), getChildFragmentManager(),
                tabLayout.getTabCount(), Integer.parseInt(MySharedPref.getString(getContext(),MySharedPref.USER_ID,"")));
        viewPager.setAdapter(adapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
       }


    private void popUp() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.heart, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();
        final Button btnGot = (Button) alertDialog.findViewById(R.id.btnGot);
        btnGot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.hide();
            }
        });
    }
    private void popUpForProfile() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.profile_zoom, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        final ImageView ivProfile =  promptsView.findViewById(R.id.ivProfile);
        Glide.with(getContext())
                .load(MySharedPref.getString(getContext(), MySharedPref.USER_PROFILE_PIC, ""))
                .thumbnail(Glide.with(getContext()).load(R.drawable.noimage))
                .error(R.drawable.noimage)
                .into(ivProfile);
        alertDialog.show();
    }

    public void showMenu(View v) {
        popup = new PopupMenu(getActivity(), v);
        try {
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popup);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_share:
                        ShareBottomSheetDialog share = new ShareBottomSheetDialog();
                        share.show(getActivity().getSupportFragmentManager(), "ShareBottomSheetDialog");
                        popup.dismiss();
                        break;
                    case R.id.item_delete_Profile:
                     deleteProfile();
                        break;

                    case R.id.item_setting:
                        Intent i = new Intent(getActivity(), ProfileSettingActivity.class);
                        startActivity(i);
                        popup.dismiss();
                        break;
                }
                return false;
            }
        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.profilepopup, popup.getMenu());
        popup.show();

    }

    private void deleteProfile() {

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getContext());
            alertDialog.setTitle(R.string.blank_warning_delete_acoount);

            alertDialog.setPositiveButton("Yes", (dialog, which) -> {
                new NetworkCall(getContext(), Services.DELETE_ACCOUNT,
                        true, NetworkCall.REQ_POST,
                        true, new NetworkCall.MultiPartRequest() {
                    @Override
                    public void myResponseMethod(String response) {
                        try {
                            if (!response.isEmpty()) {
                                JSONObject obj = new JSONObject(response);
                                boolean Success = obj.getBoolean("Success");
                                String message = obj.getString("message");
                                if (Success) {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                                    alertDialog.setTitle("Success");
                                    alertDialog.setMessage(message);
                                    alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                        new MySharedPref(getContext()).sharedPrefClear(getContext());
                                        Intent intent = new Intent(getContext(), LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        getContext().startActivity(intent);
                                        ActivityCompat.finishAffinity((Activity) getActivity());
                                        ((Activity) getContext()).finish();
                                    });
                                    alertDialog.show();

                                } else {
                                    Utils.showWarn(getContext(),message);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                });
            });
            alertDialog.setNegativeButton("No", (dialog, which) -> {
                dialog.cancel();
            });
            alertDialog.show();  }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        toolbar.setVisibility(View.GONE);

    }
    private void getUserProfile() {
        String userId = new MySharedPref(getContext()).getLoggedInUserID();
        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("UserProfileId", userId);

        new NetworkCall(getContext(), Services.GETUSERPROFILE,
                userdata, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");

                        if (Success) {
                            JSONObject user_data = obj.getJSONObject("user_data");
                            String FollowingCount = user_data.getString("FollowingCount");
                            String FollowerCount = user_data.getString("FollowerCount");
                            String FirstName = user_data.getString("FirstName");
                            String LastName = user_data.getString("LastName");
                            String ProfilePic = user_data.getString("ProfilePic");
                            JSONArray jUserOwnData = obj.getJSONArray("user_own_post");
                            JSONArray jUserLikedData = obj.getJSONArray("user_own_like_post");

                            tvNumFollowing.setText(FollowingCount);
                            tvNumFollowers.setText(FollowerCount);
                            tvUserName.setText(FirstName+" "+LastName);
                            tvUserplusname.setText(FirstName + " " + LastName);

                            tvVideos.setText(jUserOwnData.length()+" "+"Posts");

                            Glide.with(getContext())
                                    .load(ProfilePic)
                                    .thumbnail(Glide.with(getContext()).load(R.drawable.noimage))
                                    .error(R.drawable.noimage)
                                    .into(ivRound);

                            MyConstants.userOwnPosts.clear();
                            MyConstants.userOwnPosts = (ArrayList<MyPostModel.UserOwnPost>) new Gson().fromJson(jUserOwnData.toString(),
                                    new TypeToken<ArrayList<MyPostModel.UserOwnPost>>() {
                                    }.getType());
                            MyConstants.userOwnLikeVideos.clear();
                            MyConstants.userOwnLikeVideos = (ArrayList<MyPostModel.UserOwnLikeVideo>) new Gson().fromJson(jUserLikedData.toString(),
                                    new TypeToken<ArrayList<MyPostModel.UserOwnLikeVideo>>() {
                                    }.getType());

                            tabLayoutView();
                            System.out.println("=====  " + MyConstants.userOwnPosts.size());
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onResume() {
       getUserProfile();
       super.onResume();

    }
    @Override
    public void onStop() {
        if(popup!= null) {
            popup.dismiss();
        }
        super.onStop();
    }

}
