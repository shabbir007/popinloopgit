package com.example.talentogram.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talentogram.R;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.profile.adapter.OtherUserProfilePostedVideoAdapter;
import com.example.talentogram.profile.adapter.ProfileUserPostedVideoAdapter;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.model.MyPostModel;
import com.example.talentogram.model.OtherUsersProfileModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilePostFragment extends Fragment {

    private RecyclerView rv_post;
    private ArrayList<MyPostModel.UserOwnPost> userOwnPostedVideo;
    private ArrayList<MyPostModel> userOwnVideo;
    private CustomTextView ivNoDataFound;
    int user_id;

    private ProfileUserPostedVideoAdapter profileUserPostedVideoAdapter;

    private ArrayList<OtherUsersProfileModel.userPostVideo> otherUsersProfileModels;

    public ProfilePostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post__profile, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            user_id = bundle.getInt("userId");
        }
        rv_post = view.findViewById(R.id.rv_post);
        ivNoDataFound = view.findViewById(R.id.ivNoDataFound);

        return view;
    }

    @Override
    public void onResume() {
        if (user_id == Integer.parseInt(MySharedPref.getString(getContext(), MySharedPref.USER_ID, ""))) {
            setData();
        } else {
            getOtherUserVideo();
        }
        super.onResume();
    }

    @Override
    public void onStart() {

        super.onStart();
    }

    private void getOtherUserVideo() {
        {
            HashMap<String, String> userData = new HashMap<>();
            userData.put("other_user_id", String.valueOf(user_id));
            new NetworkCall(getContext(), Services.GET_OTHER_USER_PROFILE, userData, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");

                            if (Success) {
                                JSONObject jPostData = obj.getJSONObject("user_data");
                                JSONArray jPostedVideo = jPostData.getJSONArray("user_post_video");

                                otherUsersProfileModels = new ArrayList<>();
                                otherUsersProfileModels = (ArrayList<OtherUsersProfileModel.userPostVideo>) new Gson().fromJson(jPostedVideo.toString(),
                                        new TypeToken<ArrayList<OtherUsersProfileModel.userPostVideo>>() {
                                        }.getType());

                                rv_post.setVisibility(View.VISIBLE);
                                ivNoDataFound.setVisibility(View.GONE);
                                setDataForOtherUser();
                            } else {
                                rv_post.setVisibility(View.GONE);
                                ivNoDataFound.setVisibility(View.VISIBLE);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });

        }
    }


    private void setData() {
        rv_post.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        profileUserPostedVideoAdapter = new ProfileUserPostedVideoAdapter(getContext(), MyConstants.userOwnPosts);
        rv_post.setAdapter(profileUserPostedVideoAdapter);
        rv_post.setNestedScrollingEnabled(false);
    }

    private void setDataForOtherUser() {
        rv_post.setNestedScrollingEnabled(false);
        rv_post.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rv_post.setAdapter(new OtherUserProfilePostedVideoAdapter(getActivity(), MyConstants.otherUserPostVideos));

    }

}
