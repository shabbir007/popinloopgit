package com.example.talentogram.profile.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.custom_views.CustomEditText;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.helper.MySharedPref;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivPhoto)
    ImageView ivPhoto;
    @BindView(R.id.edtFirstname)
    CustomEditText edtFirstname;
    @BindView(R.id.edtLastname)
    CustomEditText edtLastname;
    @BindView(R.id.tvEmailid)
    CustomTextView tvEmailid;
    @BindView(R.id.tvDob)
    CustomTextView tvDob;
    @BindView(R.id.edtworkat)
    CustomEditText edtworkat;
    @BindView(R.id.edtRelationship)
    CustomEditText edtRelationship;
    @BindView(R.id.edtHobbies)
    CustomEditText edtHobbies;
    @BindView(R.id.edtAddress)
    CustomEditText edtAddress;
    @BindView(R.id.edtMobileno)
    CustomEditText edtMobileno;
    @BindView(R.id.edtStatus)
    CustomEditText edtStatus;
    @BindView(R.id.edtBio)
    CustomEditText edtBio;
    @BindView(R.id.llDone)
    LinearLayout llDone;

    @BindView(R.id.llProfilePic)
    LinearLayout llProfilePic;
    @BindView(R.id.llBack)
    LinearLayout llBack;
    @BindView(R.id.llProfileVideo)
    LinearLayout llProfileVideo;
    @BindView(R.id.ivVideo)
    ImageView ivVideo;
    @BindView(R.id.ivPhotoUpload)
    ImageView ivPhotoUpload;
    @BindView(R.id.ivVideoUpload)
    ImageView ivVideoUpload;
    public Uri selectedUri;
    private String filePathFirst = "", ProfilePic, ProfileVid;
    private File file, profileFile;
    private String dateStr = "", mediapath = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        bindWidget();
        getUserProfile();
    }


    private void bindWidget() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        llDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });
        llProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(EditProfileActivity.this);
            }

        });
        tvDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickDate(tvDob, -1L, -1L);
            }
        });
        llProfileVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Video"), MyConstants.REQUEST_TAKE_GALLERY_VIDEO);
            }
        });
    }

    private void getUserProfile() {
        String userId = new MySharedPref(getApplicationContext()).getLoggedInUserID();

        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("UserProfileId", userId);

        new NetworkCall(EditProfileActivity.this, Services.GETUSERPROFILE,
                userdata, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");

                        if (Success) {
                            JSONObject user_data = obj.getJSONObject("user_data");
                            String FirstName = user_data.getString("FirstName");
                            String LastName = user_data.getString("LastName");
                            String email = user_data.getString("email");
                            String Age = user_data.getString("Age");
                            String DOB = user_data.getString("BirthDate");
                            String WorkAt = user_data.getString("WorkAt");
                            String Relationship = user_data.getString("Relationship");
                            String Hobbies = user_data.getString("Hobbies");
                            String Address = user_data.getString("Address");
                            String MobileNumber = user_data.getString("MobileNumber");
                            String Status = user_data.getString("Status");
                            String Biodata = user_data.getString("Biodata");
                            ProfilePic = user_data.getString("ProfilePic");
                            ProfileVid = user_data.getString("ProfileVid");

                            MySharedPref.setString(EditProfileActivity.this, "Update", "Yes");

                            MySharedPref.setString(EditProfileActivity.this, MySharedPref.USER_FIRSTNAME, FirstName);
                            MySharedPref.setString(EditProfileActivity.this, MySharedPref.USER_LASTNAME, LastName);
                            MySharedPref.setString(EditProfileActivity.this, MySharedPref.USER_PROFILE_PIC, ProfilePic);
                            MySharedPref.setString(EditProfileActivity.this, MySharedPref.USER_PROFILE_VID, ProfileVid);

                            edtFirstname.setText(FirstName);
                            edtLastname.setText(LastName);
                            tvEmailid.setText(email);
                            tvDob.setText(DOB);
                            edtworkat.setText(WorkAt);
                            edtRelationship.setText(Relationship);
                            edtHobbies.setText(Hobbies);
                            edtAddress.setText(Address);
                            edtMobileno.setText(MobileNumber);
                            edtStatus.setText(Status);
                            edtBio.setText(Biodata);

                            if (!TextUtils.isEmpty(ProfilePic)) {
                                ivPhotoUpload.setVisibility(View.VISIBLE);

                                Glide.with(EditProfileActivity.this)
                                        .load(ProfilePic)
                                        .error(R.drawable.noimage)
                                        .into(ivPhotoUpload);
                                ivPhoto.setVisibility(View.GONE);
                            }

                            if (!TextUtils.isEmpty(ProfileVid) || !ProfileVid.isEmpty()) {
                                ivVideoUpload.setVisibility(View.VISIBLE);
                                ivVideo.setVisibility(View.GONE);
                                try {
                                    Bitmap bitmap = Utils.retriveVideoFrameFromVideo(ProfileVid);
                                    if (bitmap != null) {
                                        ivVideoUpload.setImageBitmap(bitmap);
                                    }
                                } catch (Throwable throwable) {
                                    throwable.printStackTrace();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateProfile() {

        new MySharedPref(EditProfileActivity.this).setString(EditProfileActivity.this, "Update", "Yes");
        String BirthDate = tvDob.getText().toString().trim();

        if (BirthDate.equals(""))
            Utils.showWarn(EditProfileActivity.this, getString(R.string.blank_please_provide_birthdate));
        else {
            HashMap<String, String> userdata = new HashMap<>();
            userdata.put("FirstName", edtFirstname.getText().toString().trim());
            userdata.put("LastName", edtLastname.getText().toString().trim());
            userdata.put("Email", tvEmailid.getText().toString().trim());
            userdata.put("DOB", BirthDate);
            userdata.put("WorkAt", edtworkat.getText().toString().trim());
            userdata.put("Relationship", edtRelationship.getText().toString().trim());
            userdata.put("Hobbies", edtHobbies.getText().toString().trim());
            userdata.put("Address", edtAddress.getText().toString().trim());
            userdata.put("MobileNo", edtMobileno.getText().toString().trim());
            userdata.put("Status", edtStatus.getText().toString().trim());
            userdata.put("Bio", edtBio.getText().toString().trim());

            HashMap<String, File> postFile = new HashMap<>();

            if (file != null) {
                postFile.put("ProfileImage", file);
            }

            new NetworkCall(EditProfileActivity.this, Services.EDITUSERPROFILE,
                    userdata, postFile, true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            String message = obj.getString("message");
                            getUserProfile();
                            if (Success) {
                                new AlertDialog.Builder(EditProfileActivity.this)
                                        .setMessage(message)
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                onBackPressed();
                                            }
                                        })
                                        .show();

                            } else {
                                Utils.showWarn(EditProfileActivity.this, message);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void pickDate(final TextView tv_date, Long minDate, Long maxDate) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(EditProfileActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        dateStr = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth + " ";
                        tv_date.setText(dateStr);
                    }
                }, mYear, mMonth, mDay);


        c.add(Calendar.DATE, 1);
        if (minDate != 1980) {
            datePickerDialog.getDatePicker().setMinDate(minDate);
        }
        c.add(Calendar.DATE, 90);
        if (maxDate != -1L)
            datePickerDialog.getDatePicker().setMaxDate(maxDate);
        datePickerDialog.show();

    }


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(EditProfileActivity.this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(EditProfileActivity.this, imageUri)) {
                selectedUri = imageUri;
                filePathFirst = selectedUri.getPath().toString();
                file = new File(filePathFirst);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                Utils.startCropImageActivity(imageUri, EditProfileActivity.this);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivPhotoUpload.setImageURI(result.getUri());
                ivPhotoUpload.setVisibility(View.VISIBLE);
                ivPhoto.setVisibility(View.GONE);
                selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath().toString();
                file = new File(filePathFirst);

            }
        }
        if (requestCode == MyConstants.REQUEST_TAKE_GALLERY_VIDEO) {
            Uri selectedVideo = data.getData();
            String[] filePathColumn = {MediaStore.Video.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedVideo, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                mediapath = cursor.getString(columnIndex);
                Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(mediapath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                ivVideoUpload.setVisibility(View.VISIBLE);
                ivVideo.setVisibility(View.GONE);
                ivVideoUpload.setImageBitmap(bmThumbnail);
                cursor.close();

            }

        }
    }
}
