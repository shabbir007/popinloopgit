package com.example.talentogram.model;

import java.io.Serializable;

public class TrendingModel implements Serializable {


    private String TrendingId, TrendingName, TrendingImage;
    public int TrendingSelect;

    public TrendingModel(String TrendingId, String TrendingName, String TrendingImage) {
        this.TrendingId = TrendingId;
        this.TrendingName = TrendingName;
        this.TrendingImage = TrendingImage;
    }

    public String getTrendingId() {
        return TrendingId;
    }

    public void setTrendingId(String trendingId) {
        TrendingId = trendingId;
    }

    public String getTrendingName() {
        return TrendingName;
    }

    public void setTrendingName(String trendingName) {
        TrendingName = trendingName;
    }

    public String getTrendingImage() {
        return TrendingImage;
    }

    public void setTrendingImage(String trendingImage) {
        TrendingImage = trendingImage;
    }

    public int getTrendingSelect() {
        return TrendingSelect;
    }

    public void setTrendingSelect(int trendingSelect) {
        TrendingSelect = trendingSelect;
    }
}
