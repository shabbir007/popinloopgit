package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommentModel implements Serializable {

    @SerializedName("Id")
    public int Id;

    @SerializedName("UserId")
    public int UserId;

    @SerializedName("PostId")
    public int PostId;

    @SerializedName("CommentMessage")
    public String CommentMessage;

    @SerializedName("CommentLikesCount")
    public String CommentLikesCount;

    @SerializedName("IsActive")
    public int IsActive;

    @SerializedName("IsDeleted")
    public int IsDeleted;


    @SerializedName("userinfo")
    public UserModel userdata;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getPostId() {
        return PostId;
    }

    public void setPostId(int postId) {
        PostId = postId;
    }

    public String getCommentMessage() {
        return CommentMessage;
    }

    public void setCommentMessage(String commentMessage) {
        CommentMessage = commentMessage;
    }

    public String getCommentLikesCount() {
        return CommentLikesCount;
    }

    public void setCommentLikesCount(String commentLikesCount) {
        CommentLikesCount = commentLikesCount;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public int getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        IsDeleted = isDeleted;
    }

    public UserModel getUserdata() {
        return userdata;
    }

    public void setUserdata(UserModel userdata) {
        this.userdata = userdata;
    }
}
   /* "Id": 3,
            "UserId": 5,
            "PostId": 2,
            "CommentMessage": "2",
            "CommentLikesCount": "",
            "IsActive": 1,
            "IsDeleted": 0,
            "created_at": "2020-02-18 05:20:25",
            "updated_at": "2020-02-18 05:20:25",
            "userinfo": {
            "id": 5,
            "FirstName": "Nitesh",
            "LastName": "Parmar",
            "email": "parmarnitesh0123@gmail.com",
            "email_verified_at": "2019-11-25 05:25:53",
            "Age": 32,
            "Gender": "Male",
            "Upi": "446464545",
            "ProfilePic": "",
            "created_at": "2019-11-25 05:25:45",
            "updated_at": "2020-02-07 12:39:22",
            "ProfileVid": "",
            "MobileNumber": "7984731639",
            "BirthDate": "1890-08-21",
            "WorkAt": "",
            "Relationship": "",
            "Hobbies": "",
            "Address": "",
            "Status": "",
            "Biodata": "",
            "EmailVerificationToken": "",
            "IsActive": 0,
            "Roles": 2,
            "SocialId": "",
            "SocialFlag": 0
            }*/
