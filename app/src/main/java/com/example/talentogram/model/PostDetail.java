package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostDetail implements Serializable {

    int Id,PostId,LikeCount,CommentCount;
    String Title,Description,PostImageId;

    public int getId() {
        return Id;
    }
    public void setId(int id) {
        Id = id;
    }
    public int getPostId() {
        return PostId;
    }

    public void setPostId(int postId) {
        PostId = postId;
    }

    public int getLikeCount() {
        return LikeCount;
    }

    public void setLikeCount(int likeCount) {
        LikeCount = likeCount;
    }

    public int getCommentCount() {
        return CommentCount;
    }

    public void setCommentCount(int commentCount) {
        CommentCount = commentCount;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPostImageId() {
        return PostImageId;
    }

    public void setPostImageId(String postImageId) {
        PostImageId = postImageId;
    }
}
/*
  "post_detail": {
          "Id": 78,
          "PostId": 90,
          "Title": "Galllery",
          "Description": "test",
          "PostPath": "",
          "LikeCount": 0,
          "CommentCount": 2,
          "PostSongsId": "",
          "PostiVideoId": "",
          "PostImageId": "19",
          "created_at": "2020-08-17 12:57:39",
          "updated_at": "2020-08-25 16:52:27"
          }*/
