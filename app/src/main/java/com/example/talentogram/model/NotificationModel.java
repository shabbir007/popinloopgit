package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
public class NotificationModel implements Serializable {
    public NotificationModel(int id, int userId, int friendsId, int isFollowing, int isActive, String created_at,
                             String updated_at, FollowUser follow_user,
                             boolean ifFriendRequest, String type, int notification_type,
                             String notifiable_type, int notifiable_id, String data, int sender_id, int receiver_id, String senderProfilePicUrl, String postUrl,UserData userData) {
        Id = id;
        UserId = userId;
        FriendsId = friendsId;
        IsFollowing = isFollowing;
        IsActive = isActive;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.follow_user = follow_user;
        this.ifFriendRequest = ifFriendRequest;
        this.type = type;
        this.notification_type = notification_type;
        this.notifiable_type = notifiable_type;
        this.notifiable_id = notifiable_id;
        this.data = data;
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
        SenderProfilePicUrl = senderProfilePicUrl;
        this.postUrl = postUrl;
        this.userdata = userData;
    }
public NotificationModel()
{

}
    @SerializedName("Id")
    public int Id;

    @SerializedName("UserId")
    public int UserId;

    @SerializedName("FriendsId")
    public int FriendsId;

    @SerializedName("IsFollowing")
    public int IsFollowing;

    @SerializedName("IsActive")
    public int IsActive;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("follow_user")
    private FollowUser follow_user;


    boolean ifFriendRequest;
    public boolean isIfFriendRequest() {
        return ifFriendRequest;
    }

    public void setIfFriendRequest(boolean ifFriendRequest) {
        this.ifFriendRequest = ifFriendRequest;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getFriendsId() {
        return FriendsId;
    }

    public void setFriendsId(int friendsId) {
        FriendsId = friendsId;
    }

    public int getIsFollowing() {
        return IsFollowing;
    }

    public void setIsFollowing(int isFollowing) {
        IsFollowing = isFollowing;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public FollowUser getFollow_user() {
        return follow_user;
    }

    public void setFollow_user(FollowUser follow_user) {
        this.follow_user = follow_user;
    }



    public static class FollowUser {

        @SerializedName("id")
        public int Id;

        @SerializedName("FirstName")
        public String FirstName;

        @SerializedName("LastName")
        public String LastName;

        @SerializedName("email")
        public String email;

        @SerializedName("ProfilePic")
        public String ProfilePic;

        @SerializedName("MobileNumber")
        public String MobileNumber;

        @SerializedName("BirthDate")
        public String BirthDate;

        @SerializedName("Hobbies")
        public String Hobbies;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfilePic() {
            return ProfilePic;
        }

        public void setProfilePic(String profilePic) {
            ProfilePic = profilePic;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            MobileNumber = mobileNumber;
        }

        public String getBirthDate() {
            return BirthDate;
        }

        public void setBirthDate(String birthDate) {
            BirthDate = birthDate;
        }

        public String getHobbies() {
            return Hobbies;
        }

        public void setHobbies(String hobbies) {
            Hobbies = hobbies;
        }
    }

   /* @SerializedName("id")
    public int id;*/

    @SerializedName("type")
    public String type;

    @SerializedName("notification_type")
    public int notification_type;

    @SerializedName("notifiable_type")
    public String notifiable_type;

    @SerializedName("notifiable_id")
    public int notifiable_id;

    @SerializedName("data")
    public String data;

    @SerializedName("sender_id")
    public int sender_id;

    @SerializedName("receiver_id")
    public int receiver_id;

    @SerializedName("SenderProfilePicUrl")
    public String SenderProfilePicUrl;

    @SerializedName("postUrl")
    public String postUrl;

    @SerializedName("sender")
    private UserData userdata;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public int getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(int notification_type) {
        this.notification_type = notification_type;
    }

    public String getNotifiable_type() {
        return notifiable_type;
    }

    public void setNotifiable_type(String notifiable_type) {
        this.notifiable_type = notifiable_type;
    }

    public int getNotifiable_id() {
        return notifiable_id;
    }

    public void setNotifiable_id(int notifiable_id) {
        this.notifiable_id = notifiable_id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getSender_id() {
        return sender_id;
    }

    public void setSender_id(int sender_id) {
        this.sender_id = sender_id;
    }

    public int getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(int receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getSenderProfilePicUrl() {
        return SenderProfilePicUrl;
    }

    public void setSenderProfilePicUrl(String senderProfilePicUrl) {
        SenderProfilePicUrl = senderProfilePicUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public UserData getUserdata() {
        return userdata;
    }

    public void setUserdata(UserData userdata) {
        this.userdata = userdata;
    }

    public static class UserData {

        @SerializedName("id")
        public int Id;

        @SerializedName("FirstName")
        public String FirstName;

        @SerializedName("LastName")
        public String LastName;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }
    }
}

//-----------for notification

      /*  {
        "id": 0,
        "type": "App\\Notifications\\CommentPost",
        "notification_type": 2,
        "notifiable_type": "App\\User",
        "notifiable_id": 72,
        "data": "{\"type\":\"comment\",\"notification_type\":\"2\",\"post_id\":\"71\",\"CommentMessage\":\"testing notification test test\",\"created_by\":\"Chirag Gohel\",\"sender_id\":75,\"receiver_id\":72}",
        "sender_id": 75,
        "receiver_id": 72,
        "is_read": 0,
        "read_at": "",
        "created_at": "2020-08-07 11:22:16",
        "updated_at": "2020-08-07 11:22:16",
        "SenderProfilePicUrl": "http://popinloop.clientworkstore.us/uploads/ProfilePic/1595826929_1595826929.png",
        "postUrl": "http://popinloop.clientworkstore.us/uploads/Post/1595655084IMG-20180516-WA0085.jpg"
        }*/

//---------for Pending follow request
/*[{
        "Id": 44,
        "UserId": 71,
        "FriendsId": 72,
        "IsFollowing": 0,
        "IsActive": 1,
        "created_at": "2020-07-25 05:57:57",
        "updated_at": "2020-07-25 05:57:57",
        "follow_user": {
        "id": 71,
        "FirstName": "yogi",
        "LastName": "patel",
        "email": "yogi123@mailinator.com",
        "email_verified_at": "2020-07-25 05:23:09",
        "Age": null,
        "Gender": "Female",
        "Upi": "9876541230",
        "ProfilePic": "1595654557cropped654218822060584064.jpg",
        "created_at": "2020-07-25 05:22:37",
        "updated_at": "2020-07-25 05:24:16",
        "ProfileVid": null,
        "MobileNumber": "9874563210",
        "BirthDate": "1996-07-25",
        "WorkAt": "Rajkot",
        "Relationship": "Single",
        "Hobbies": "Dange",
        "Address": "Rajkot",
        "Status": "Good",
        "Biodata": "Good",
        "EmailVerificationToken": "",
        "IsActive": 0,
        "Roles": 2,
        "SocialId": null,
        "SocialFlag": 0
        }
        }]*/
