package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FollowersModel implements Serializable {
    @SerializedName("Id")
    public int Id;

    @SerializedName("UserId")
    public int UserId;

    @SerializedName("FriendsId")
    public int FriendsId;

    @SerializedName("IsFollowing")
    public int IsFollowing;

    @SerializedName("IsActive")
    public int IsActive;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("follow_user")
    private FollowerUser following_user;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getFriendsId() {
        return FriendsId;
    }

    public void setFriendsId(int friendsId) {
        FriendsId = friendsId;
    }

    public int getIsFollowing() {
        return IsFollowing;
    }

    public void setIsFollowing(int isFollowing) {
        IsFollowing = isFollowing;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public FollowerUser getFollowing_user() {
        return following_user;
    }

    public void setFollowing_user(FollowerUser following_user) {
        this.following_user = following_user;
    }

    public static class FollowerUser {
        @SerializedName("id")
        public int Id;

        @SerializedName("FirstName")
        public String FirstName;

        @SerializedName("LastName")
        public String LastName;

        @SerializedName("email")
        public String email;

        @SerializedName("ProfilePic")
        public String ProfilePic;

        @SerializedName("MobileNumber")
        public String MobileNumber;

        @SerializedName("BirthDate")
        public String BirthDate;

        @SerializedName("Hobbies")
        public String Hobbies;

        @SerializedName("Address")
        public String Address;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfilePic() {
            return ProfilePic;
        }

        public void setProfilePic(String profilePic) {
            ProfilePic = profilePic;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            MobileNumber = mobileNumber;
        }

        public String getBirthDate() {
            return BirthDate;
        }

        public void setBirthDate(String birthDate) {
            BirthDate = birthDate;
        }

        public String getHobbies() {
            return Hobbies;
        }

        public void setHobbies(String hobbies) {
            Hobbies = hobbies;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }
    }
}
/*
[{
        "Id": 44,
        "UserId": 71,
        "FriendsId": 72,
        "IsFollowing": 1,
        "IsActive": 1,
        "created_at": "2020-07-25 05:57:57",
        "updated_at": "2020-07-25 05:57:57",
        "following_user": {
        "id": 72,
        "FirstName": "Yogita",
        "LastName": "patel",
        "email": "yogi456@mailinator.com",
        "email_verified_at": "2020-07-25 05:27:37",
        "Age": null,
        "Gender": "Female",
        "Upi": "9807654089",
        "ProfilePic": "1595654837cropped1025334412.jpg",
        "created_at": "2020-07-25 05:27:17",
        "updated_at": "2020-07-25 05:38:09",
        "ProfileVid": null,
        "MobileNumber": "9807650890",
        "BirthDate": "1995-07-25",
        "WorkAt": "Rajkot",
        "Relationship": "Single",
        "Hobbies": "Dance",
        "Address": "Rajkot",
        "Status": "Good",
        "Biodata": "Good",
        "EmailVerificationToken": "",
        "IsActive": 0,
        "Roles": 2,
        "SocialId": null,
        "SocialFlag": 0
        }
        }]*/
