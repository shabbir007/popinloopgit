package com.example.talentogram.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {

    @SerializedName("Success")
    private boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("user_data")

    private UserData userData;
    @SerializedName("expires_at")

    private String expiresAt;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }

    public class UserData {

        @SerializedName("id")
        private Integer id;

        @SerializedName("FirstName")
        private String firstName;

        @SerializedName("LastName")
        private String lastName;

        @SerializedName("email")
        private String email;

        @SerializedName("email_verified_at")
        private String emailVerifiedAt;

        @SerializedName("Age")
        private Object age;

        @SerializedName("Gender")
        private String gender;

        @SerializedName("Upi")
        private String upi;

        @SerializedName("ProfilePic")
        private String profilePic;

        @SerializedName("created_at")
        private String createdAt;

        @SerializedName("updated_at")
        private String updatedAt;

        @SerializedName("ProfileVid")
        private Object profileVid;

        @SerializedName("MobileNumber")
        private String mobileNumber;

        @SerializedName("BirthDate")
        private String birthDate;

        @SerializedName("WorkAt")
        private String workAt;

        @SerializedName("Relationship")
        private Object relationship;

        @SerializedName("Hobbies")
        private Object hobbies;

        @SerializedName("Address")
        private Object address;

        @SerializedName("Status")
        private String status;

        @SerializedName("Biodata")
        private Object biodata;

        @SerializedName("EmailVerificationToken")
        private String emailVerificationToken;

        @SerializedName("IsActive")
        private Integer isActive;

        @SerializedName("Roles")
        private Integer roles;

        @SerializedName("SocialId")
        private Object socialId;

        @SerializedName("SocialFlag")
        private Integer socialFlag;

        @SerializedName("is_delete")
        private Integer isDelete;

        @SerializedName("bank_account")
        private BankAccount bankAccount;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public void setEmailVerifiedAt(String emailVerifiedAt) {
            this.emailVerifiedAt = emailVerifiedAt;
        }

        public Object getAge() {
            return age;
        }

        public void setAge(Object age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getUpi() {
            return upi;
        }

        public void setUpi(String upi) {
            this.upi = upi;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getProfileVid() {
            return profileVid;
        }

        public void setProfileVid(Object profileVid) {
            this.profileVid = profileVid;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getBirthDate() {
            return birthDate;
        }

        public void setBirthDate(String birthDate) {
            this.birthDate = birthDate;
        }

        public String getWorkAt() {
            return workAt;
        }

        public void setWorkAt(String workAt) {
            this.workAt = workAt;
        }

        public Object getRelationship() {
            return relationship;
        }

        public void setRelationship(Object relationship) {
            this.relationship = relationship;
        }

        public Object getHobbies() {
            return hobbies;
        }

        public void setHobbies(Object hobbies) {
            this.hobbies = hobbies;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getBiodata() {
            return biodata;
        }

        public void setBiodata(Object biodata) {
            this.biodata = biodata;
        }

        public String getEmailVerificationToken() {
            return emailVerificationToken;
        }

        public void setEmailVerificationToken(String emailVerificationToken) {
            this.emailVerificationToken = emailVerificationToken;
        }

        public Integer getIsActive() {
            return isActive;
        }

        public void setIsActive(Integer isActive) {
            this.isActive = isActive;
        }

        public Integer getRoles() {
            return roles;
        }

        public void setRoles(Integer roles) {
            this.roles = roles;
        }

        public Object getSocialId() {
            return socialId;
        }

        public void setSocialId(Object socialId) {
            this.socialId = socialId;
        }

        public Integer getSocialFlag() {
            return socialFlag;
        }

        public void setSocialFlag(Integer socialFlag) {
            this.socialFlag = socialFlag;
        }

        public Integer getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(Integer isDelete) {
            this.isDelete = isDelete;
        }

        public BankAccount getBankAccount() {
            return bankAccount;
        }

        public void setBankAccount(BankAccount bankAccount) {
            this.bankAccount = bankAccount;
        }

        public class BankAccount {
            String bank_holder_name, ifsc_code, bank_account, branch_name;

            public String getBank_holder_name() {
                return bank_holder_name;
            }

            public void setBank_holder_name(String bank_holder_name) {
                this.bank_holder_name = bank_holder_name;
            }

            public String getIfsc_code() {
                return ifsc_code;
            }

            public void setIfsc_code(String ifsc_code) {
                this.ifsc_code = ifsc_code;
            }

            public String getBank_account() {
                return bank_account;
            }

            public void setBank_account(String bank_account) {
                this.bank_account = bank_account;
            }

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }
        }

    }
}





