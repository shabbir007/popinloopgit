package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserModel implements Serializable {

    @SerializedName("id")
    public int Id;

    @SerializedName("FirstName")
    public String FirstName;

    @SerializedName("LastName")
    public String LastName;

    @SerializedName("email")
    public String email;

    @SerializedName("Age")
    public String Age;

    @SerializedName("Gender")
    public String Gender;

    @SerializedName("Upi")
    public String Upi;

    @SerializedName("ProfilePics")
    public String ProfilePics;

    @SerializedName("ProfilePic")
    public String ProfilePic;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("ProfileVid")
    public String ProfileVid;

    @SerializedName("MobileNumber")
    public String MobileNumber;

    @SerializedName("BirthDate")
    public String BirthDate;

    @SerializedName("WorkAt")
    public String WorkAt;

    @SerializedName("Relationship")
    public String Relationship;

    @SerializedName("Hobbies")
    public String Hobbies;

    @SerializedName("Address")
    public String Address;

    @SerializedName("Status")
    public String Status;

    @SerializedName("Biodata")
    public String Biodata;

    @SerializedName("IsActive")
    public int IsActive;

    @SerializedName("Roles")
    public int Roles;

    @SerializedName("SocialId")
    public String SocialId;

    @SerializedName("SocialFlag")
    public int SocialFlag;

    public int getId() {
        return Id;
    }
    public void setId(int id) {
        Id = id;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String firstName) {
        FirstName = firstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String lastName) {
        LastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getUpi() {
        return Upi;
    }

    public void setUpi(String upi) {
        Upi = upi;
    }

    public String getProfilePics() {
        return ProfilePics;
    }

    public void setProfilePics(String profilePic) {
        ProfilePics = profilePic;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getProfileVid() {
        return ProfileVid;
    }

    public void setProfileVid(String profileVid) {
        ProfileVid = profileVid;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getWorkAt() {
        return WorkAt;
    }

    public void setWorkAt(String workAt) {
        WorkAt = workAt;
    }

    public String getRelationship() {
        return Relationship;
    }

    public void setRelationship(String relationship) {
        Relationship = relationship;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getBiodata() {
        return Biodata;
    }

    public void setBiodata(String biodata) {
        Biodata = biodata;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public int getRoles() {
        return Roles;
    }

    public void setRoles(int roles) {
        Roles = roles;
    }

    public String getSocialId() {
        return SocialId;
    }

    public void setSocialId(String socialId) {
        SocialId = socialId;
    }

    public int getSocialFlag() {
        return SocialFlag;
    }

    public void setSocialFlag(int socialFlag) {
        SocialFlag = socialFlag;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

/* "user_data": {
        "id": 5,
                "FirstName": "Nitesh",
                "LastName": "Parmar",
                "email": "parmarnitesh0123@gmail.com",
                "email_verified_at": "2019-11-25 05:25:53",
                "Age": 32,
                "Gender": "Male",
                "Upi": "446464545",
                "ProfilePic": "",
                "created_at": "2019-11-25 05:25:45",
                "updated_at": "2020-02-07 12:39:22",
                "ProfileVid": "",
                "MobileNumber": "7984731639",
                "BirthDate": "1890-08-21",
                "WorkAt": "",
                "Relationship": "",
                "Hobbies": "",
                "Address": "",
                "Status": "",
                "Biodata": "",
                "EmailVerificationToken": "",
                "IsActive": 0,
                "Roles": 2,
                "SocialId": "",
                "SocialFlag": 0
    }*/
}
