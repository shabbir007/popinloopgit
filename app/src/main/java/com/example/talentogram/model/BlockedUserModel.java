package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BlockedUserModel implements Serializable
{
    @SerializedName("id")
    public  int id;

    @SerializedName("UserId")
    public int UserId;

    @SerializedName("BlockUserId")
    public int BlockUserId;

    @SerializedName("block_user")
    public Block_User block_user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getBlockUserId() {
        return BlockUserId;
    }

    public void setBlockUserId(int blockUserId) {
        BlockUserId = blockUserId;
    }

    public Block_User getBlock_user() {
        return block_user;
    }

    public void setBlock_user(Block_User block_user) {
        this.block_user = block_user;
    }

    public static class Block_User
    {
        @SerializedName("id")
        public  int id;

        @SerializedName("FirstName")
        public String FirstName;

        @SerializedName("LastName")
        public String LastName;

        @SerializedName("ProfilePic")
        public String ProfilePic;

        @SerializedName("Address")
        public String Address;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getProfilePic() {
            return ProfilePic;
        }

        public void setProfilePic(String profilePic) {
            ProfilePic = profilePic;
        }


        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }
    }
}
/*
{
        "id": 4,
        "UserId": 72,
        "BlockUserId": 71,
        "created_at": "2020-08-06 11:22:02",
        "updated_at": "2020-08-06 11:22:02",
        "block_user": {
        "id": 71,
        "FirstName": "yogi",
        "LastName": "patel",
        "email": "yogi123@mailinator.com",
        "Gender": "Female",
        "ProfilePic": "http://popinloop.clientworkstore.us/uploads/ProfilePic/1595654557cropped654218822060584064.jpg",
        "Relationship": "Single",
        "WorkAt": "Rajkot",
        "BirthDate": "1996-07-25",
        "Hobbies": "Dange",
        "Address": "Rajkot",
        "Status": "Good",
        "Biodata": "Good"
        }
        }*/
