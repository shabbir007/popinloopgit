package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MyPostModel {

    String PostImageUrl = "";

    public String getPostImageUrl() {
        return PostImageUrl;
    }

    public void setPostImageUrl(String postImageUrl) {
        PostImageUrl = postImageUrl;
    }

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("PostType")
    public int PostType;

    @SerializedName("post_detail")
    private PostDetail post_detail;

    @SerializedName("userdata")
    public PostModel.userData userdata;

    @SerializedName("VideoUrl")
    private ArrayList<VideoUrl> videoUrl;

    @SerializedName("ImageUrl")
    public ArrayList<ImageUrl> imageUrl;

    @SerializedName("user_own_post")
    public ArrayList<UserOwnPost> userOwnPosts;

    @SerializedName("user_own_like_post")
    public ArrayList<UserOwnLikeVideo> userOwnLikeVideos;


    public ArrayList<UserOwnPost> getUserOwnPosts() {
        return userOwnPosts;
    }

    public void setUserOwnPosts(ArrayList<UserOwnPost> userOwnPosts) {
        this.userOwnPosts = userOwnPosts;
    }

    public ArrayList<UserOwnLikeVideo> getUserOwnLikeVideos() {
        return userOwnLikeVideos;
    }

    public void setUserOwnLikeVideos(ArrayList<UserOwnLikeVideo> userOwnLikeVideos) {
        this.userOwnLikeVideos = userOwnLikeVideos;
    }

    public class UserOwnPost {
        int PostType;
        @SerializedName("VideoUrl")
        private ArrayList<VideoUrl> videoUrl;

        @SerializedName("ImageUrl")
        public ArrayList<ImageUrl> imageUrl;

        @SerializedName("post_detail")
        private PostDetail post_detail;

        @SerializedName("IsInContest")
        private int IsInContest;

        @SerializedName("Font")
        public String Font;

        @SerializedName("Color")
        public String Color;

        public String getFont() {
            return Font;
        }

        public void setFont(String font) {
            Font = font;
        }

        public String getColor() {
            return Color;
        }

        public void setColor(String color) {
            Color = color;
        }

        public int getPostType() {
            return PostType;
        }

        public void setPostType(int postType) {
            PostType = postType;
        }

        public ArrayList<VideoUrl> getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(ArrayList<VideoUrl> videoUrl) {
            this.videoUrl = videoUrl;
        }

        public ArrayList<ImageUrl> getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(ArrayList<ImageUrl> imageUrl) {
            this.imageUrl = imageUrl;
        }

        public PostDetail getPost_detail() {
            return post_detail;
        }

        public void setPost_detail(PostDetail post_detail) {
            this.post_detail = post_detail;
        }

        public int getIsInContest() {
            return IsInContest;
        }

        public void setIsInContest(int isInContest) {
            IsInContest = isInContest;
        }
    }

    public class UserOwnLikeVideo {
        int PostType;
        @SerializedName("VideoUrl")
        private ArrayList<VideoUrl> videoUrl;

        @SerializedName("ImageUrl")
        public ArrayList<ImageUrl> imageUrl;

        @SerializedName("post_detail")
        private PostDetail post_detail;

        @SerializedName("IsInContest")
        private int IsInContest;
        @SerializedName("Font")
        public String Font;

        @SerializedName("Color")
        public String Color;

        public String getFont() {
            return Font;
        }

        public void setFont(String font) {
            Font = font;
        }

        public String getColor() {
            return Color;
        }

        public void setColor(String color) {
            Color = color;
        }

        public int getPostType() {
            return PostType;
        }

        public void setPostType(int postType) {
            PostType = postType;
        }

        public ArrayList<VideoUrl> getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(ArrayList<VideoUrl> videoUrl) {
            this.videoUrl = videoUrl;
        }

        public ArrayList<ImageUrl> getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(ArrayList<ImageUrl> imageUrl) {
            this.imageUrl = imageUrl;
        }

        public PostDetail getPost_detail() {
            return post_detail;
        }

        public void setPost_detail(PostDetail post_detail) {
            this.post_detail = post_detail;
        }

        public int getIsInContest() {
            return IsInContest;
        }

        public void setIsInContest(int isInContest) {
            IsInContest = isInContest;
        }
    }

    public class VideoUrl {

        @SerializedName("VideoUrl")
        private String VideoUrl;

        public String getVideoUrl() {
            return VideoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            VideoUrl = videoUrl;
        }
    }

    public class ImageUrl {

        @SerializedName("ImageUrl")
        public String imageUrl;

        @SerializedName("VideoUrl")
        private String videoUrl;

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

    }

    public static class AudioUrl {

    }

    /*public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getContestId() {
        return ContestId;
    }

    public void setContestId(String contestId) {
        ContestId = contestId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getPostVisibleType() {
        return PostVisibleType;
    }

    public void setPostVisibleType(int postVisibleType) {
        PostVisibleType = postVisibleType;
    }

    public String getIsInContest() {
        return IsInContest;
    }

    public void setIsInContest(String isInContest) {
        IsInContest = isInContest;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }
*/
    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getPostType() {
        return PostType;
    }

    public void setPostType(int postType) {
        PostType = postType;
    }

    public PostDetail getPost_detail() {
        return post_detail;
    }

    public void setPost_detail(PostDetail post_detail) {
        this.post_detail = post_detail;
    }

    public PostModel.userData getUserdata() {
        return userdata;
    }

    public void setUserdata(PostModel.userData userdata) {
        this.userdata = userdata;
    }

    public ArrayList<VideoUrl> getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(ArrayList<VideoUrl> videoUrl) {
        this.videoUrl = videoUrl;
    }

    public ArrayList<ImageUrl> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(ArrayList<ImageUrl> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public static class userData {

        @SerializedName("ProfileVid")
        public String ProfileVid;

        @SerializedName("ProfilePics")
        public String ProfilePics;

        @SerializedName("FirstName")
        public String FirstName;

        @SerializedName("LastName")
        public String LastName;

        public String getProfileVid() {
            return ProfileVid;
        }

        public void setProfileVid(String profileVid) {
            ProfileVid = profileVid;
        }

        public String getProfilePics() {
            return ProfilePics;
        }

        public void setProfilePics(String profilePics) {
            ProfilePics = profilePics;
        }


        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }
    }


}


