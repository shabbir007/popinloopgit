package com.example.talentogram.model;

public class ChatModel {
    private String ImageUrl = "";
    private String UserName;
    private String UserDesc;

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserDesc() {
        return UserDesc;
    }

    public void setUserDesc(String userDesc) {
        UserDesc = userDesc;
    }
}
