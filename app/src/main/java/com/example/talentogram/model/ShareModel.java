package com.example.talentogram.model;

import android.graphics.drawable.Drawable;

public class ShareModel {
    private String AppName;
    private String PackageName;
    private Drawable AppIcon;

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    public Drawable getAppIcon() {
        return AppIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        AppIcon = appIcon;
    }


}
