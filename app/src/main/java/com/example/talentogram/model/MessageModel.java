package com.example.talentogram.model;

import java.io.Serializable;

public class MessageModel implements Serializable {

    public static int TYPE_AUDIO = 21;
    public static int TYPE_TEXT = 1;

    public String sender_message, receiver_message;

    public String getSender_message() {
        return sender_message;
    }

    public void setSender_message(String sender_message) {
        this.sender_message = sender_message;
    }

    public String getReceiver_message() {
        return receiver_message;
    }

    public void setReceiver_message(String receiver_message) {
        this.receiver_message = receiver_message;
    }
    public String text,audioPath;
    public int type;
    public int time;

    public MessageModel(int time) {
        this.time = time;
        //this.audioPath = audioPath;
        this.type = TYPE_AUDIO;
    }
/*
    public MessageModel() {
        this.type = TYPE_AUDIO;
    }*/

    public MessageModel(String text) {
        this.text = text;
        this.type = TYPE_TEXT;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }
}
