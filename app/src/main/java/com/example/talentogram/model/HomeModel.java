package com.example.talentogram.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class HomeModel implements Serializable {

    @SerializedName("Id")
    public int Id;

    @SerializedName("ContestId")
    public String ContestId;

    @SerializedName("UserId")
    public int UserId;

    @SerializedName("ispostliked")
    public int ispostliked;

    @SerializedName("PostType")
    public int PostType;

    @SerializedName("PostVisibleType")
    public int PostVisibleType;

    @SerializedName("IsInContest")
    public String IsInContest;

    @SerializedName("IsActive")
    public int IsActive;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("userdata")
    public userData userdata;

    @SerializedName("post_detail")
    public postdetail post_detail;

    public int getIspostliked() {
        return ispostliked;
    }

    public void setIspostliked(int ispostliked) {
        this.ispostliked = ispostliked;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getContestId() {
        return ContestId;
    }

    public void setContestId(String contestId) {
        ContestId = contestId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getPostType() {
        return PostType;
    }

    public void setPostType(int postType) {
        PostType = postType;
    }

    public int getPostVisibleType() {
        return PostVisibleType;
    }

    public void setPostVisibleType(int postVisibleType) {
        PostVisibleType = postVisibleType;
    }

    public String getIsInContest() {
        return IsInContest;
    }

    public void setIsInContest(String isInContest) {
        IsInContest = isInContest;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public userData getUserdata() {
        return userdata;
    }

    public void setUserdata(userData userdata) {
        this.userdata = userdata;
    }

    public postdetail getPost_detail() {
        return post_detail;
    }

    public void setPost_detail(postdetail post_detail) {
        this.post_detail = post_detail;
    }

    public static class userData
    {
        @SerializedName("id")
        public int id;

        @SerializedName("FirstName")
        public String FirstName;

        @SerializedName("LastName")
        public String LastName;

        @SerializedName("Gender")
        public String Gender;

        @SerializedName("Upi")
        public String Upi;

        @SerializedName("ProfilePics")
        public String ProfilePic;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getGender() {
            return Gender;
        }

        public void setGender(String gender) {
            Gender = gender;
        }

        public String getUpi() {
            return Upi;
        }

        public void setUpi(String upi) {
            Upi = upi;
        }

        public String getProfilePic() {
            return ProfilePic;
        }

        public void setProfilePic(String profilePic) {
            ProfilePic = profilePic;
        }
    }
    public static class postdetail
    {
        @SerializedName("Id")
        public int Id;

        @SerializedName("Title")
        public String Title;

        @SerializedName("PostId")
        public int PostId;

        @SerializedName("Description")
        public String Description;

        @SerializedName("LikeCount")
         public int LikeCount;

        @SerializedName("CommentCount")
        public int CommentCount;

        @SerializedName("PostiVideoId")
        public String PostiVideoId;

        @SerializedName("PostVideo")
        private ArrayList<PostVideo> PostVideo;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int postId) {
            PostId = postId;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int likeCount) {
            LikeCount = likeCount;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int commentCount) {
            CommentCount = commentCount;
        }

        public String getPostiVideoId() {
            return PostiVideoId;
        }

        public void setPostiVideoId(String postiVideoId) {
            PostiVideoId = postiVideoId;
        }

        public ArrayList<postdetail.PostVideo> getPostVideo() {
            return PostVideo;
        }

        public void setPostVideo(ArrayList<postdetail.PostVideo> postVideo) {
            PostVideo = postVideo;
        }

        public  static class  PostVideo
        {
            @SerializedName("Id")
            public int Id;

            @SerializedName("UserId")
            public int UserId;

            @SerializedName("AdminSongId")
            public int AdminSongId;

            @SerializedName("VideoUrl")
            public String VideoUrl;

            @SerializedName("IsActive")
            public int IsActive;

            @SerializedName("created_at")
            public String created_at;

            @SerializedName("updated_at")
            public String updated_at;


            public int getId() {
                return Id;
            }

            public void setId(int id) {
                Id = id;
            }

            public int getUserId() {
                return UserId;
            }

            public void setUserId(int userId) {
                UserId = userId;
            }

            public int getAdminSongId() {
                return AdminSongId;
            }

            public void setAdminSongId(int adminSongId) {
                AdminSongId = adminSongId;
            }

            public String getVideoUrl() {
                return VideoUrl;
            }

            public void setVideoUrl(String videoUrl) {
                VideoUrl = videoUrl;
            }

            public int getIsActive() {
                return IsActive;
            }

            public void setIsActive(int isActive) {
                IsActive = isActive;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }


    }
}
 /*{
         "Id": 74,
         "ContestId": "",
         "UserId": 71,
         "PostType": 1,
         "PostVisibleType": 0,
         "IsInContest": "",
         "IsActive": 1,
         "created_at": "2020-07-25 05:41:50",
         "updated_at": "2020-07-25 05:41:50",
         "userdata": {
         "id": 71,
         "FirstName": "yogi",
         "LastName": "patel",
         "Gender": "Female",
         "Upi": "9876541230",
         "ProfilePic": "http://popinloop.clientworkstore.us/uploads/ProfilePic/http://popinloop.clientworkstore.us/uploads/ProfilePic/1595654557cropped654218822060584064.jpg"
         },
         "post_detail": {
         "Id": 62,
         "Title": "abc",
         "PostId": 74,
         "Description": "sunset vibes",
         "LikeCount": 0,
         "CommentCount": 0,
         "PostiVideoId": "32",
         "PostVideo": [
         {
         "Id": 32,
         "UserId": 71,
         "AdminSongId": 1,
         "VideoUrl": "http://popinloop.clientworkstore.us/uploads/Post/15956557101b2f8313b390d6e540db8b8129db84a8.mp4",
         "IsActive": 1,
         "created_at": "2020-07-25 05:41:50",
         "updated_at": "2020-07-25 05:41:50"
         }
         ]
         }
         },*/