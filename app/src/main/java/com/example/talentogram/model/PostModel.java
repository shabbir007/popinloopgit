package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PostModel implements Serializable {

    @SerializedName("Id")
    public int Id;

    @SerializedName("UserId")
    public int UserId;

    @SerializedName("PostVisibleType")
    public int PostVisibleType;

    @SerializedName("is_download")
    public int is_download;

    @SerializedName("PostType")
    public int PostType;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("IsInContest")
    public String IsInContest;

    @SerializedName("Font")
    public String Font;

    @SerializedName("ispostliked")
    public int ispostliked;

    @SerializedName("Color")
    public String Color;

    @SerializedName("EncryptedId")
    public String EncryptedId;

    @SerializedName("post_detail")
    private PostDetail post_detail;

   @SerializedName("userdata")
    public userData userdata;

    @SerializedName("VideoUrl")
    private ArrayList<VideoUrl> videoUrl;

    @SerializedName("ImageUrl")
    public ArrayList<ImageUrl> imageUrl;

    @SerializedName("AudioUrl")
    public ArrayList<AudioUrl> audioUrls;


    public String getFont() {
        return Font;
    }

    public void setFont(String font) {
        Font = font;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public int getIspostliked() {
        return ispostliked;
    }

    public void setIspostliked(int ispostliked) {
        this.ispostliked = ispostliked;
    }

    public int getPostVisibleType() {
        return PostVisibleType;
    }

    public void setPostVisibleType(int postVisibleType) {
        PostVisibleType = postVisibleType;
    }

    public int getIs_download() {
        return is_download;
    }

    public void setIs_download(int is_download) {
        this.is_download = is_download;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) { UserId = userId; }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getPostType() {
        return PostType;
    }

    public void setPostType(int postType) {
        PostType = postType;
    }

    public String getEncryptedId() {
        return EncryptedId;
    }

    public void setEncryptedId(String encryptedId) {
        EncryptedId = encryptedId;
    }

    public PostDetail getPost_detail() {
        return post_detail;
    }

    public void setPost_detail(PostDetail post_detail) {
        this.post_detail = post_detail;
    }

    public String getIsInContest() {
        return IsInContest;
    }

    public void setIsInContest(String isInContest) {
        IsInContest = isInContest;
    }

    public userData getUserdata() {
        return userdata;
    }

    public void setUserdata(userData userdata) {
        this.userdata = userdata;
    }

    public ArrayList<VideoUrl> getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(ArrayList<VideoUrl> videoUrl) {
        this.videoUrl = videoUrl;
    }

    public ArrayList<ImageUrl> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(ArrayList<ImageUrl> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<AudioUrl> getAudioUrls() {
        return audioUrls;
    }

    public void setAudioUrls(ArrayList<AudioUrl> audioUrls) {
        this.audioUrls = audioUrls;
    }

    public static class userData {

        @SerializedName("FirstName")
        public String FirstName;

        @SerializedName("LastName")
        public String LastName;

        @SerializedName("ProfileVid")
        public String ProfileVid;

        @SerializedName("ProfilePics")
        public String ProfilePics;


        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }
        public String getProfileVid() {
            return ProfileVid;
        }

        public void setProfileVid(String profileVid) {
            ProfileVid = profileVid;
        }

        public String getProfilePics() {
            return ProfilePics;
        }

        public void setProfilePics(String profilePics) {
            ProfilePics = profilePics;
        }

    }

    public static class VideoUrl {

        @SerializedName("VideoUrl")
        private String VideoUrl;

        public String getVideoUrl() {
            return VideoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            VideoUrl = videoUrl;
        }
    }

    public static class ImageUrl {

        @SerializedName("ImageUrl")
        public String imageUrl;

        @SerializedName("VideoUrl")
        private String videoUrl;

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

    }
    public static class AudioUrl {

        @SerializedName("Id")
        private int Id;

        @SerializedName("AudioUrl")
        private String AudioUrl;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getAudioUrl() {
            return AudioUrl;
        }

        public void setAudioUrl(String audioUrl) {
            AudioUrl = audioUrl;
        }
    }
}
/*
{
            "Id": 80,
            "ContestId": "",
            "UserId": 72,
            "PostType": 1,
            "PostVisibleType": 0,
            "IsInContest": "",
            "IsActive": 1,
            "created_at": "2020-08-04 12:14:13",
            "updated_at": "2020-08-04 12:14:13",
            "ImageUrl": [],
            "AudioUrl": [],
            "VideoUrl": [
                {
                    "Id": 36,
                    "UserId": 72,
                    "AdminSongId": 1,
                    "VideoUrl": "http://popinloop.clientworkstore.us/uploads/Post/1596543253116723509_1214612662210982_956871965144849725_n.mp4",
                    "IsActive": 1,
                    "created_at": "2020-08-04 12:14:13",
                    "updated_at": "2020-08-04 12:14:13"
                }
            ],
            "userdata": {
                "id": 72,
                "FirstName": "Yogita",
                "LastName": "patel",
                "email": "yogi456@mailinator.com",
                "email_verified_at": "2020-07-25 05:27:37",
                "Age": "",
                "Gender": "Female",
                "Upi": "9807654089",
                "ProfilePic": "1595654837cropped1025334412.jpg",
                "created_at": "2020-07-25 05:27:17",
                "updated_at": "2020-07-28 04:44:33",
                "ProfileVid": "",
                "MobileNumber": "9807650890",
                "BirthDate": "1995-07-25",
                "WorkAt": "Rajkot",
                "Relationship": "Single",
                "Hobbies": "Dance",
                "Address": "Rajkot",
                "Status": "Good",
                "Biodata": "Good",
                "EmailVerificationToken": "",
                "IsActive": 1,
                "Roles": 2,
                "SocialId": "",
                "SocialFlag": 0,
                "ProfilePics": "http://popinloop.clientworkstore.us/uploads/ProfilePic/1595654837cropped1025334412.jpg"
            },
            "post_detail": {
                "Id": 68,
                "PostId": 80,
                "Title": "abc",
                "Description": "monsoon vibes",
                "PostPath": "",
                "LikeCount": 0,
                "CommentCount": 0,
                "PostSongsId": "",
                "PostiVideoId": "36",
                "PostImageId": "",
                "created_at": "2020-08-04 12:14:13",
                "updated_at": "2020-08-04 12:14:13"
            }
        }*/
