package com.example.talentogram.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class OtherUsersProfileModel implements Serializable {

    @SerializedName("id")
    public int Id;

    @SerializedName("IsFriend")
    public int IsFriend;

    @SerializedName("FollowingCount")
    public int FollowingCount;

    @SerializedName("FollowerCount")
    public int FollowerCount;

    @SerializedName("FirstName")
    public String FirstName;

    @SerializedName("LastName")
    public String LastName;

    @SerializedName("email")
    public String email;

    @SerializedName("Age")
    public String Age;

    @SerializedName("Gender")
    public String Gender;

    @SerializedName("Upi")
    public String Upi;

    @SerializedName("ProfilePic")
    public String ProfilePic;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("ProfileVid")
    public String ProfileVid;

    @SerializedName("MobileNumber")
    public String MobileNumber;

    @SerializedName("BirthDate")
    public String BirthDate;

    @SerializedName("WorkAt")
    public String WorkAt;

    @SerializedName("Relationship")
    public String Relationship;

    @SerializedName("Hobbies")
    public String Hobbies;

    @SerializedName("Address")
    public String Address;

    @SerializedName("Status")
    public String Status;

    @SerializedName("Biodata")
    public String Biodata;

    @SerializedName("IsActive")
    public int IsActive;

    @SerializedName("Roles")
    public int Roles;

    @SerializedName("SocialId")
    public String SocialId;

    @SerializedName("SocialFlag")
    public int SocialFlag;

    @SerializedName("user_like_video")
    private ArrayList<userLikeVideo> user_like_video;

    @SerializedName("user_post_video")
    private ArrayList<userPostVideo> user_post_video;


    public int getIsFriend() {
        return IsFriend;
    }

    public void setIsFriend(int isFriend) {
        IsFriend = isFriend;
    }

    public int getFollowingCount() {
        return FollowingCount;
    }

    public void setFollowingCount(int followingCount) {
        FollowingCount = followingCount;
    }

    public int getFollowerCount() {
        return FollowerCount;
    }

    public void setFollowerCount(int followerCount) {
        FollowerCount = followerCount;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getUpi() {
        return Upi;
    }

    public void setUpi(String upi) {
        Upi = upi;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getProfileVid() {
        return ProfileVid;
    }

    public void setProfileVid(String profileVid) {
        ProfileVid = profileVid;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getWorkAt() {
        return WorkAt;
    }

    public void setWorkAt(String workAt) {
        WorkAt = workAt;
    }

    public String getRelationship() {
        return Relationship;
    }

    public void setRelationship(String relationship) {
        Relationship = relationship;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getBiodata() {
        return Biodata;
    }

    public void setBiodata(String biodata) {
        Biodata = biodata;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public int getRoles() {
        return Roles;
    }

    public void setRoles(int roles) {
        Roles = roles;
    }

    public String getSocialId() {
        return SocialId;
    }

    public void setSocialId(String socialId) {
        SocialId = socialId;
    }

    public int getSocialFlag() {
        return SocialFlag;
    }

    public void setSocialFlag(int socialFlag) {
        SocialFlag = socialFlag;
    }

    public ArrayList<userLikeVideo> getUser_like_video() {
        return user_like_video;
    }

    public void setUser_like_video(ArrayList<userLikeVideo> user_like_video) {
        this.user_like_video = user_like_video;
    }

    public ArrayList<userPostVideo> getUser_post_video() {
        return user_post_video;
    }

    public void setUser_post_video(ArrayList<userPostVideo> user_post_video) {
        this.user_post_video = user_post_video;
    }

    public static class userLikeVideo {

        @SerializedName("Id")
        public int Id;

        @SerializedName("UserId")
        public int UserId;

        @SerializedName("VideoUrl")
        public String VideoUrl;

        @SerializedName("IsActive")
        public int IsActive;

        @SerializedName("created_at")
        public String created_at;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getVideoUrl() {
            return VideoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            VideoUrl = videoUrl;
        }

        public int getIsActive() {
            return IsActive;
        }

        public void setIsActive(int isActive) {
            IsActive = isActive;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }

    public static class userPostVideo {

        @SerializedName("Id")
        public int Id;

        @SerializedName("UserId")
        public int UserId;

        @SerializedName("AdminSongId")
        public int AdminSongId;

        @SerializedName("VideoUrl")
        public String VideoUrl;

        @SerializedName("IsActive")
        public int IsActive;

        @SerializedName("created_at")
        public String created_at;

        @SerializedName("updated_at")
        public String updated_at;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getAdminSongId() {
            return AdminSongId;
        }

        public void setAdminSongId(int adminSongId) {
            AdminSongId = adminSongId;
        }

        public String getVideoUrl() {
            return VideoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            VideoUrl = videoUrl;
        }

        public int getIsActive() {
            return IsActive;
        }

        public void setIsActive(int isActive) {
            IsActive = isActive;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }


        /*"Id": 32,
        "UserId": 71,
        "AdminSongId": 1,
        "VideoUrl": "http://popinloop.clientworkstore.us/uploads/Post/15956557101b2f8313b390d6e540db8b8129db84a8.mp4",
        "IsActive": 1,
        "created_at": "2020-07-25 05:41:50",
        "updated_at": "2020-07-25 05:41:50"*/
    }
}


/*
"user_data": {
        "id": 71,
        "FirstName": "yogi",
        "LastName": "patel",
        "email": "yogi123@mailinator.com",
        "email_verified_at": "2020-07-25 05:23:09",
        "Age": "",
        "Gender": "Female",
        "Upi": "9876541230",
        "ProfilePic": "http://popinloop.clientworkstore.us/uploads/ProfilePic/1595654557cropped654218822060584064.jpg",
        "created_at": "2020-07-25 05:22:37",
        "updated_at": "2020-08-04 10:35:40",
        "ProfileVid": "",
        "MobileNumber": "9874563210",
        "BirthDate": "1996-07-25",
        "WorkAt": "Rajkot",
        "Relationship": "Single",
        "Hobbies": "Dange",
        "Address": "Rajkot",
        "Status": "Good",
        "Biodata": "Good",
        "EmailVerificationToken": "",
        "IsActive": 1,
        "Roles": 2,
        "SocialId": "",
        "SocialFlag": 0,
        "user_like_video": [
        {
        "Id": 34,
        "UserId": 72,
        "VideoUrl": "http://popinloop.clientworkstore.us/uploads/Post/1596542279116723509_1214612662210982_956871965144849725_n.mp4",
        "IsActive": 1,
        "created_at": "2020-08-04 11:57:59"
        }
        ],
        "user_post_video": [
        {
        "Id": 32,
        "UserId": 71,
        "AdminSongId": 1,
        "VideoUrl": "http://popinloop.clientworkstore.us/uploads/Post/15956557101b2f8313b390d6e540db8b8129db84a8.mp4",
        "IsActive": 1,
        "created_at": "2020-07-25 05:41:50",
        "updated_at": "2020-07-25 05:41:50"
        },
        {
        "Id": 33,
        "UserId": 71,
        "AdminSongId": 1,
        "VideoUrl": "http://popinloop.clientworkstore.us/uploads/Post/15956688721595668802682.mp4",
        "IsActive": 1,
        "created_at": "2020-07-25 09:21:12",
        "updated_at": "2020-07-25 09:21:12"
        }
        ]
        }*/
