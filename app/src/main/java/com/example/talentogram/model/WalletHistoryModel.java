package com.example.talentogram.model;

import java.util.ArrayList;

public class WalletHistoryModel {
    boolean Success ;
    String avail_balance ;
    ArrayList<History> history;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean success) {
        Success = success;
    }

    public String getAvail_balance() {
        return avail_balance;
    }

    public void setAvail_balance(String avail_balance) {
        this.avail_balance = avail_balance;
    }

    public ArrayList<History> getHistory() {
        return history;
    }

    public void setHistory(ArrayList<History> history) {
        this.history = history;
    }
    public class History{
        int Id,UserId,TransferUserId,ContestPaymentRefrenceId,BankId,ContestId;
        String TransactionsType,TransferType,Amount,ContestTitle,RazorPayTxnId,BankTxnId,TransferRefrenceId,created_at;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getTransferUserId() {
            return TransferUserId;
        }

        public void setTransferUserId(int transferUserId) {
            TransferUserId = transferUserId;
        }

        public int getContestPaymentRefrenceId() {
            return ContestPaymentRefrenceId;
        }

        public void setContestPaymentRefrenceId(int contestPaymentRefrenceId) {
            ContestPaymentRefrenceId = contestPaymentRefrenceId;
        }
        public int getBankId() {
            return BankId;
        }

        public void setBankId(int bankId) {
            BankId = bankId;
        }

        public int getContestId() {
            return ContestId;
        }

        public void setContestId(int contestId) {
            ContestId = contestId;
        }

        public String getTransactionsType() {
            return TransactionsType;
        }

        public void setTransactionsType(String transactionsType) {
            TransactionsType = transactionsType;
        }

        public String getTransferType() {
            return TransferType;
        }

        public void setTransferType(String transferType) {
            TransferType = transferType;
        }

        public String getAmount() {
            return Amount;
        }

        public void setAmount(String amount) {
            Amount = amount;
        }

        public String getContestTitle() {
            return ContestTitle;
        }

        public void setContestTitle(String contestTitle) {
            ContestTitle = contestTitle;
        }

        public String getRazorPayTxnId() {
            return RazorPayTxnId;
        }

        public void setRazorPayTxnId(String razorPayTxnId) {
            RazorPayTxnId = razorPayTxnId;
        }

        public String getBankTxnId() {
            return BankTxnId;
        }

        public void setBankTxnId(String bankTxnId) {
            BankTxnId = bankTxnId;
        }

        public String getTransferRefrenceId() {
            return TransferRefrenceId;
        }

        public void setTransferRefrenceId(String transferRefrenceId) {
            TransferRefrenceId = transferRefrenceId;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }

/*
    {
        "Success": true,
            "avail_balance": 1799,
            "history": [
        {
            "Id": 12,
                "UserId": 75,
                "TransactionsType": "debit",
                "TransferType": "withdrawal",
                "Amount": "1.00",
                "TransferUserId": null,
                "TransferRefrenceId": null,
                "ContestPaymentRefrenceId": null,
                "RazorPayTxnId": null,
                "BankId": null,
                "BankTxnId": null,
                "created_at": "2021-04-12 12:07:02",
                "updated_at": "2021-04-12 12:07:02",
                "ContestTitle": "Audio Contest",
                "ContestId": 5
        },
    ]
    }*/
}
