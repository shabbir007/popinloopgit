package com.example.talentogram.Video_recording.video_trimmer;

import android.app.Application;
import android.content.Context;

import com.danikula.videocache.HttpProxyCacheServer;
import com.example.talentogram.SegmentProgress.Variables;
import com.google.firebase.FirebaseApp;



public class TicTic extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Variables.CATCH_DIR = getCacheDir().getAbsolutePath();
        FirebaseApp.initializeApp(this);


    }

    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        TicTic app = (TicTic) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this)
                .maxCacheSize(1024 * 1024 * 1024)
                .maxCacheFilesCount(20)
                .build();
    }
}
