package com.example.talentogram.Video_recording.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.example.talentogram.R;
import com.example.talentogram.SegmentProgress.Variables;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.custom_views.CustomEditText;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.others.MyConstants;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.exoplayer2.Player;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostVideoActivity extends AppCompatActivity implements Player.EventListener {

    private String fileName, description;
    private Dialog dialog;
    private int postTypeId = 0;

    @BindView(R.id.etPostDescription)
    CustomEditText etDescription;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.llChoice)
    LinearLayout llChoice;

    @BindView(R.id.tvPostType)
    CustomTextView tvPostType;

    @BindView(R.id.swSwitch)
    SwitchCompat swSwitch;

    @BindView(R.id.btnPost)
    Button btnPost;

    @BindView(R.id.video_thumbnail)
    ImageView video_thumbnail;

    @BindView(R.id.chAllowDownload)
    CheckBox chAllowDownload;

    String video_path;
    ProgressDialog progressDialog;
    int IsChecked = 1;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_description);
        ButterKnife.bind(this);

        fileName = getIntent().getExtras().getString("filename", "");
        description = getIntent().getExtras().getString("description", "");

        video_path = Variables.output_filter_file;
        video_thumbnail = findViewById(R.id.video_thumbnail);

        if (description.equals("")) {
            etDescription.setText(description);
        }

        Bitmap bmThumbnail;
        bmThumbnail = ThumbnailUtils.createVideoThumbnail(video_path,
                MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);

        if (bmThumbnail != null) {
            video_thumbnail.setImageBitmap(bmThumbnail);
        } else {
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);

        chAllowDownload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsChecked = 1;
                } else {
                    IsChecked = 0;
                }
            }
        });

        bindClick();
    }

    private void bindClick() {
        ivBack.setOnClickListener(view -> startActivity(new Intent(PostVideoActivity.this, PreviewActivity.class).putExtra("filename", fileName)));

        btnPost.setOnClickListener(view -> uploadPost());

        llChoice.setOnClickListener(view -> {
            showMenuPopup(view);

        });

        swSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    new DownloadingTask().execute();
                } else {
                    // The toggle is disabled
                }
            }
        });

    }

    private void uploadPost() {

        String Description = etDescription.getText().toString().trim();
        String PostType = tvPostType.getText().toString().trim();

        switch (PostType) {
            case "Public":
                postTypeId = 0;
                break;
            case "Private":
                postTypeId = 1;
                break;
            case "Followers":
                postTypeId = 2;
                break;
        }
        if (TextUtils.isEmpty(Description)) {
            etDescription.requestFocus();
            etDescription.setError("Please add description.");
        }else{
        HashMap<String, String> userdata = new HashMap<>();
        userdata.put("Title", "abc");
        userdata.put("PostType", String.valueOf(1));
        userdata.put("PostHashTagIds", "");
        userdata.put("NewHashtags", "Video");
        userdata.put("PostVisibleType", String.valueOf(postTypeId));
        userdata.put("Description", Description);
        userdata.put("AdminSongId", String.valueOf(1));
        userdata.put("isDownload", String.valueOf(IsChecked));

        HashMap<String, File> postfile = new HashMap<>();
        File video = new File(video_path);
        postfile.put("PostUrl[]", video);

        System.out.println(userdata + "userdata");
        new NetworkCall(PostVideoActivity.this, Services.ADDPOST,
                userdata, postfile, true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                System.out.println(response + "PostAdded");
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        String message = obj.getString("message");
                        if (Success) {
                            new AlertDialog.Builder(PostVideoActivity.this)
                                    .setMessage(message)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            startActivity(new Intent(PostVideoActivity.this, DashBoardActivity.class));
                                            finish();
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        } else {
                            Utils.showWarn(PostVideoActivity.this, message);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });}
    }

    private void showMenuPopup(View v) {
        int gravity = Gravity.CENTER;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.dialog_who_view, null);
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });
        popupView.findViewById(R.id.tvPrivate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPostType.setText("Public");
                popupWindow.dismiss();
            }
        });
        popupView.findViewById(R.id.tvfollower).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPostType.setText("Followers");
                popupWindow.dismiss();
            }
        });
        popupView.findViewById(R.id.tvPublic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPostType.setText("Private");
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(v, gravity, 0, 0);
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {
        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PostVideoActivity.this);
            progressDialog.setMessage("Downloading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (fileName != null) {
                    progressDialog.dismiss();
                    ContextThemeWrapper ctw = new ContextThemeWrapper(PostVideoActivity.this, R.style.AppTheme);
                    final androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ctw);
                    alertDialogBuilder.setTitle("File ");
                    alertDialogBuilder.setMessage("File Downloaded Successfully");
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
                    alertDialogBuilder.show();
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                        }
                    }, 500);

                    Log.e(MyConstants.TAG, "Download Failed");

                }
            } catch (Exception e) {
                e.printStackTrace();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 500);
                Log.e(MyConstants.TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }
            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "Talentogram/");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }
                String time = String.valueOf(System.currentTimeMillis());
                File newFile = new File(folder.getAbsolutePath() + File.separator + time + ".mp4");
                try {
                    copyFile(new File(fileName), newFile);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(MyConstants.TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }
}
