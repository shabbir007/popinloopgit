package com.example.talentogram.Video_recording.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.talentogram.R;
import com.example.talentogram.activity.DashBoardActivity;
import com.example.talentogram.activity.UploadFromGalleryActivity;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.others.MyConstants;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class AddNewPostActivity extends AppCompatActivity implements CameraKitEventListener {

    private RoundedHorizontalProgressBar progressBar;
    private LinearLayout llAudioText;
    private ImageView ivCloseAddAudio;
    private LinearLayout llSpeed;
    private LinearLayout llFilter;
    private LinearLayout llFlash;
    private LinearLayout llSpeedShow;
    private LinearLayout llPointThreeX;
    private TextView tvPointThreeX, tvAddSound;
    private LinearLayout llPointFiveX;
    private TextView tvPointFiveX;
    private LinearLayout ll1X;
    private TextView tv1X;
    private LinearLayout ll2X;
    private TextView tv2X;
    private LinearLayout ll3X;
    private TextView tv3X;
    private RelativeLayout rlBottom;
    private RelativeLayout rlFunction;
    private ImageView ivRecord;
    private LinearLayout ll60sec;
    private ImageView iv60Sec, ivFlashOff, ivFlashOn;
    private LinearLayout ll15Sec;
    private ImageView iv15Sec;
    private LinearLayout llUpload, llRotateCamera;
    //  private LinearLayout llEffect;
    private CameraView cameraKitView;
    CircularProgressBar circularProgressBar;
    public boolean speedOnOff = false;
    public boolean fifteenSec = true;
    SharedPreferences prefs;
    String prefName = "MyPref";
    String audioID, artist_name, artist_band;
    Uri MyUri = Uri.EMPTY;
    MediaPlayer mPlayer;
    String video;
    String TAG = "TALENTOGRAM";

    boolean is_flash_on = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_post);
        bindReference();
        bindClick();
        mPlayer = new MediaPlayer();

        Intent intent = getIntent();
        video = intent.getStringExtra("videoUrl");


        cameraKitView.start();
        cameraKitView.addCameraKitListener(new CameraKitEventListener() {
            @Override
            public void onEvent(CameraKitEvent cameraKitEvent) {
            }

            @Override
            public void onError(CameraKitError cameraKitError) {
            }

            @Override
            public void onImage(CameraKitImage cameraKitImage) {
            }

            @Override
            public void onVideo(CameraKitVideo cameraKitVideo) {

            }
        });


    }

    private void bindReference() {
        cameraKitView = findViewById(R.id.cameraView);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        progressBar = (RoundedHorizontalProgressBar) findViewById(R.id.progressBar);
        llAudioText = (LinearLayout) findViewById(R.id.llAudioText);
        ivCloseAddAudio = (ImageView) findViewById(R.id.ivCloseAddAudio);
        ivFlashOff = (ImageView) findViewById(R.id.ivFlashOff);
        ivFlashOn = (ImageView) findViewById(R.id.ivFlashOn);
        llSpeed = (LinearLayout) findViewById(R.id.llSpeed);
        llFilter = (LinearLayout) findViewById(R.id.llFilter);
        llFlash = (LinearLayout) findViewById(R.id.llFlash);
        llSpeedShow = (LinearLayout) findViewById(R.id.llSpeedShow);
        llPointThreeX = (LinearLayout) findViewById(R.id.llPointThreeX);
        tvPointThreeX = (TextView) findViewById(R.id.tvPointThreeX);
        llPointFiveX = (LinearLayout) findViewById(R.id.llPointFiveX);
        tvPointFiveX = (TextView) findViewById(R.id.tvPointFiveX);
        ll1X = (LinearLayout) findViewById(R.id.ll1X);
        tv1X = (TextView) findViewById(R.id.tv1X);
        ll2X = (LinearLayout) findViewById(R.id.ll2X);
        tv2X = (TextView) findViewById(R.id.tv2X);
        ll3X = (LinearLayout) findViewById(R.id.ll3X);
        tv3X = (TextView) findViewById(R.id.tv3X);
        rlBottom = (RelativeLayout) findViewById(R.id.rlBottom);
        rlFunction = (RelativeLayout) findViewById(R.id.rlFunction);
        ivRecord = (ImageView) findViewById(R.id.ivRecord);
        ll60sec = (LinearLayout) findViewById(R.id.ll60sec);
        iv60Sec = (ImageView) findViewById(R.id.iv60Sec);
        ll15Sec = (LinearLayout) findViewById(R.id.ll15Sec);
        iv15Sec = (ImageView) findViewById(R.id.iv15Sec);
        llUpload = (LinearLayout) findViewById(R.id.llUpload);
        //llEffect = (LinearLayout) findViewById(R.id.llEffect);
        tvAddSound = (CustomTextView) findViewById(R.id.tvAddSound);
        llRotateCamera = (LinearLayout) findViewById(R.id.llRotateCamera);


    }

    boolean flash = false;

    private void bindClick() {

        llSpeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (speedOnOff) {
                    speedOnOff = false;
                    llSpeedShow.setVisibility(View.GONE);
                } else {
                    speedOnOff = true;
                    llSpeedShow.setVisibility(View.VISIBLE);
                }
            }
        });
        ll2X.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                ll1X.setBackgroundColor(Color.parseColor("#000000"));
                tv1X.setTextColor(Color.parseColor("#ffffff"));
                ll3X.setBackgroundColor(Color.parseColor("#000000"));
                tv3X.setTextColor(Color.parseColor("#ffffff"));
                llPointThreeX.setBackgroundColor(Color.parseColor("#000000"));
                tvPointThreeX.setTextColor(Color.parseColor("#ffffff"));
                llPointFiveX.setBackgroundColor(Color.parseColor("#000000"));
                tvPointFiveX.setTextColor(Color.parseColor("#ffffff"));
                ll2X.setBackgroundColor(Color.parseColor("#ffffff"));
                tv2X.setTextColor(Color.parseColor("#000000"));
            }
        });

        ll3X.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                ll1X.setBackgroundColor(Color.parseColor("#000000"));
                tv1X.setTextColor(Color.parseColor("#ffffff"));
                ll2X.setBackgroundColor(Color.parseColor("#000000"));
                tv2X.setTextColor(Color.parseColor("#ffffff"));
                llPointThreeX.setBackgroundColor(Color.parseColor("#000000"));
                tvPointThreeX.setTextColor(Color.parseColor("#ffffff"));
                llPointFiveX.setBackgroundColor(Color.parseColor("#000000"));
                tvPointFiveX.setTextColor(Color.parseColor("#ffffff"));
                ll3X.setBackgroundColor(Color.parseColor("#ffffff"));
                tv3X.setTextColor(Color.parseColor("#000000"));
            }
        });
        llPointFiveX.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                ll1X.setBackgroundColor(Color.parseColor("#000000"));
                tv1X.setTextColor(Color.parseColor("#ffffff"));
                ll2X.setBackgroundColor(Color.parseColor("#000000"));
                tv2X.setTextColor(Color.parseColor("#ffffff"));
                llPointThreeX.setBackgroundColor(Color.parseColor("#000000"));
                tvPointThreeX.setTextColor(Color.parseColor("#ffffff"));
                ll3X.setBackgroundColor(Color.parseColor("#000000"));
                tv3X.setTextColor(Color.parseColor("#ffffff"));
                llPointFiveX.setBackgroundColor(Color.parseColor("#ffffff"));
                tvPointFiveX.setTextColor(Color.parseColor("#000000"));
            }
        });
        llPointThreeX.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                ll1X.setBackgroundColor(Color.parseColor("#000000"));
                tv1X.setTextColor(Color.parseColor("#ffffff"));
                ll2X.setBackgroundColor(Color.parseColor("#000000"));
                tv2X.setTextColor(Color.parseColor("#ffffff"));
                ll3X.setBackgroundColor(Color.parseColor("#000000"));
                tv3X.setTextColor(Color.parseColor("#ffffff"));
                llPointFiveX.setBackgroundColor(Color.parseColor("#000000"));
                tvPointFiveX.setTextColor(Color.parseColor("#ffffff"));
                llPointThreeX.setBackgroundColor(Color.parseColor("#ffffff"));
                tvPointThreeX.setTextColor(Color.parseColor("#000000"));
            }
        });
        ll1X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll1X.setBackgroundColor(Color.parseColor("#ffffff"));
                tv1X.setTextColor(Color.parseColor("#000000"));
                ll2X.setBackgroundColor(Color.parseColor("#000000"));
                tv2X.setTextColor(Color.parseColor("#ffffff"));
                ll3X.setBackgroundColor(Color.parseColor("#000000"));
                tv3X.setTextColor(Color.parseColor("#ffffff"));
                llPointThreeX.setBackgroundColor(Color.parseColor("#000000"));
                tvPointThreeX.setTextColor(Color.parseColor("#ffffff"));
                llPointFiveX.setBackgroundColor(Color.parseColor("#000000"));
                tvPointFiveX.setTextColor(Color.parseColor("#ffffff"));
            }
        });

        ll60sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iv60Sec.setVisibility(View.VISIBLE);
                iv15Sec.setVisibility(View.GONE);
                fifteenSec = false;
            }
        });

        ll15Sec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iv60Sec.setVisibility(View.GONE);
                iv15Sec.setVisibility(View.VISIBLE);
                fifteenSec = true;
            }
        });

        ivCloseAddAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraKitView.stop();
                startActivity(new Intent(AddNewPostActivity.this, DashBoardActivity.class));
                finish();
            }
        });


        llFlash.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (is_flash_on) {
                    is_flash_on = false;
                    cameraKitView.setFlash(0);
                    ivFlashOff.setVisibility(View.VISIBLE);
                    ivFlashOn.setVisibility(View.GONE);

                } else {
                    is_flash_on = true;
                    cameraKitView.setFlash(CameraKit.Constants.FLASH_TORCH);
                    ivFlashOff.setVisibility(View.GONE);
                    ivFlashOn.setVisibility(View.VISIBLE);
                }

            }
        });

        ivRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivRecord.setVisibility(View.GONE);
                circularProgressBar.setVisibility(View.VISIBLE);
                llSpeedShow.setVisibility(View.GONE);
                rlFunction.setVisibility(View.GONE);
                llUpload.setVisibility(View.GONE);

                if (MyUri != null || MyUri != Uri.EMPTY) {
                    try {
                        playAudio(MyUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                File folder = new File(Environment.getExternalStorageDirectory() + File.separator + getResources().getString(R.string.app_name));
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }
                String time = String.valueOf(System.currentTimeMillis());

                if (success) {
                    cameraKitView.captureVideo(new CameraKitEventCallback<CameraKitVideo>() {
                        @Override
                        public void callback(CameraKitVideo cameraKitVideo) {
                            cameraKitVideo.getVideoFile();
                            File newFile = new File(folder.getAbsolutePath() + File.separator + time + ".mp4");
                            FileChannel outputChannel = null;
                            FileChannel inputChannel = null;
                            try {
                                outputChannel = new FileOutputStream(newFile).getChannel();
                                inputChannel = new FileInputStream(cameraKitVideo.getVideoFile()).getChannel();
                                inputChannel.transferTo(0, inputChannel.size(), outputChannel);
                                inputChannel.close();
                                cameraKitVideo.getVideoFile().delete();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

                if (fifteenSec) {
                    progressBar.animateProgress(15000, 0, 100);
                    progressBar.setProgressColors(Color.parseColor("#FF8A65"), Color.parseColor("#FD5622"));
                    circularProgressBar.setProgress(9.9f);
                    circularProgressBar.setProgressWithAnimation(0f, Long.valueOf(15000)); // =1s
                    circularProgressBar.setProgressMax(10f);
                    circularProgressBar.setProgressDirection(CircularProgressBar.ProgressDirection.TO_LEFT);

                    cameraKitView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cameraKitView.stopVideo();
                            String path = folder.getAbsolutePath().toString() + File.separator + time + ".mp4";

                            startActivity(new Intent(AddNewPostActivity.this, PreviewActivity.class)
                                    .putExtra("filename", path));
                            finish();
                        }
                    }, 15000);

                } else {

                    progressBar.animateProgress(60000, 0, 100);
                    progressBar.setProgressColors(Color.parseColor("#FF8A65"), Color.parseColor("#FD5622"));
                    circularProgressBar.setProgress(60000);
                    circularProgressBar.setProgress(9.9f);
                    circularProgressBar.setProgressWithAnimation(0f, Long.valueOf(60000)); // =1s
                    circularProgressBar.setProgressMax(10f);
                    circularProgressBar.setProgressDirection(CircularProgressBar.ProgressDirection.TO_LEFT);

                    cameraKitView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cameraKitView.stopVideo();
                            String path = folder.getAbsolutePath().toString() + File.separator + time + ".mp4";
                            Log.d("path-->", path);
                            Log.d(TAG, "stopped-->");
                            startActivity(new Intent(AddNewPostActivity.this, PreviewActivity.class).putExtra("filename", path));
                            finish();
                        }
                    }, 60000);
                }

            }
        });

        llUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AddNewPostActivity.this, UploadFromGalleryActivity.class);
                startActivity(i);
            }
        });

        tvAddSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, MyConstants.REQUEST_TAKE_AUDIO);
            }
        });
        llRotateCamera.setOnClickListener(v -> {
            RotateCamera();
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void flashLightOn(View view) {
        Camera camera = Camera.open();
        Camera.Parameters parameters = camera.getParameters();
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(parameters);
        camera.startPreview();
    }

    public void flashLightOff(View view) {
        Camera camera = Camera.open();
        Camera.Parameters parameters = camera.getParameters();
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        camera.setParameters(parameters);
        camera.stopPreview();
        camera.release();

    }

    @Override
    protected void onStart() {
        super.onStart();
        cameraKitView.start();

    }

    public void RotateCamera() {
        cameraKitView.toggleFacing();
    }

    @Override
    public void onBackPressed() {
        cameraKitView.stopVideo();
        super.onBackPressed();
    }

    @Override
    public void onPause() {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        super.onPause();
    }

    @Override
    public void onEvent(CameraKitEvent cameraKitEvent) {

    }

    @Override
    public void onError(CameraKitError cameraKitError) {
        Log.d("errorr-->", String.valueOf(cameraKitError));
    }

    @Override
    public void onImage(CameraKitImage cameraKitImage) {

    }

    @Override
    public void onVideo(CameraKitVideo cameraKitVideo) {
        Log.d(TAG, "captured-->");
        Log.d("taken-->", String.valueOf(cameraKitVideo));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MyConstants.REQUEST_TAKE_AUDIO && resultCode == Activity.RESULT_OK) {
            if ((data != null) && (data.getData() != null)) {
                audioID = data.getDataString();
                MyUri = Uri.parse(audioID);
                String[] proj = {MediaStore.Audio.Media._ID,
                        MediaStore.Audio.Media.DATA,
                        MediaStore.Audio.Media.TITLE,
                        MediaStore.Audio.Artists.ARTIST};

                Cursor tempCursor = managedQuery(MyUri,
                        proj, null, null, null);

                tempCursor.moveToFirst(); //reset the cursor
                int col_index = -1;
                int numSongs = tempCursor.getCount();
                int currentNum = 0;
                do {
                    col_index = tempCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
                    artist_name = tempCursor.getString(col_index);
                    col_index = tempCursor.getColumnIndexOrThrow(MediaStore.Audio.Artists.ARTIST);
                    artist_band = tempCursor.getString(col_index);
                    //do something with artist name here
                    //we can also move into different columns to fetch the other values

                    currentNum++;
                } while (tempCursor.moveToNext());

                prefs = getSharedPreferences(prefName, MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("audio_selected", audioID);
                editor.putString("audio_name", artist_name);
                editor.commit();
                tvAddSound.setText(prefs.getString("audio_name", ""));

            }
        }
    }

    private void playAudio(Uri audioFileUri) throws IOException {
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setDataSource(AddNewPostActivity.this, audioFileUri);
        mPlayer.prepare();
        mPlayer.start();

    }

    @Override
    public void onDestroy() {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        super.onDestroy();

    }

    public void onResume() {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        super.onResume();
    }

}
