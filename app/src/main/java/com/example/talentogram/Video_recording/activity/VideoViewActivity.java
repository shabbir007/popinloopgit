package com.example.talentogram.Video_recording.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.talentogram.R;

import static com.example.talentogram.others.Utils.timeConversion;

public class VideoViewActivity extends Activity {

    private String str_video;
    private VideoView vv_video;
    private ImageView ivBack,pause;
    private Button btnNext;

    private SeekBar seekBar;
    double current_pos, total_duration;
    private TextView current, total;
    private LinearLayout showProgress;
    private Handler mHandler,handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoview);
        init();
        bindWidget();
    }

    private void init() {

        vv_video = (VideoView) findViewById(R.id.vv_video);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        btnNext = findViewById(R.id.btnNext);

        pause = (ImageView) findViewById(R.id.pause);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        current = (TextView) findViewById(R.id.current);
        total = (TextView) findViewById(R.id.total);
        showProgress = (LinearLayout) findViewById(R.id.showProgress);

        str_video = getIntent().getStringExtra("video");
        mHandler = new Handler();
        handler = new Handler();

        vv_video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                setVideoProgress();
            }
        });

        playVideo(str_video);
        setPause();

    }

    private void bindWidget() {

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(VideoViewActivity.this, PreviewActivity.class).
                        putExtra("filename", str_video));
            }
        });
    }
    public void setVideoProgress() {
        //get the video duration
        current_pos = vv_video.getCurrentPosition();
        total_duration = vv_video.getDuration();

        //display video duration
        total.setText(timeConversion((long) total_duration));
        current.setText(timeConversion((long) current_pos));
        seekBar.setMax((int) total_duration);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    current_pos = vv_video.getCurrentPosition();
                    current.setText(timeConversion((long) current_pos));
                    seekBar.setProgress((int) current_pos);
                    handler.postDelayed(this, 1000);
                } catch (IllegalStateException ed){
                    ed.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 1000);
        //seekbar change listner
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                current_pos = seekBar.getProgress();
                vv_video.seekTo((int) current_pos);
            }
        });
    }

    public void playVideo(String str_video) {
        try  {
            vv_video.setVideoURI(Uri.parse(str_video));
            vv_video.start();
            pause.setImageResource(R.drawable.d_ic_pause_circle_filled_black_24dp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //pause video
    public void setPause() {
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vv_video.isPlaying()) {
                    vv_video.pause();
                    pause.setImageResource(R.drawable.d_ic_play_circle_filled_black_24dp);
                } else {
                    vv_video.start();
                    pause.setImageResource(R.drawable.d_ic_pause_circle_filled_black_24dp);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        playVideo(str_video);
    }
}
