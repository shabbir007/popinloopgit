package com.example.talentogram.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPref {
    public static String USER_ID = "Id";
    public static String USER_EMAIL = "Email";
    public static String USER_FIRSTNAME = "FirstName";
    public static String USER_LASTNAME = "LastName";
    public static String USER_AUTH = "Auth";
    public static String FCM_TOKEN = "FCM_TOKEN";
    public static String USER_ACCESS_TOKEN = "accessToken";
    public static String USER_PROFILE_PIC = "Profile";
    public static String USER_GENDER = "gender";
    public static String USER_PROFILE_VID = "Profile_video";
    public static String isFirst = "isFirstTime";
    public static String imagePath = "imagePath";
    public static SharedPreferences shared;
    public static SharedPreferences.Editor et;
    public static final String DEVICE_TOKEN = "device_token";
    public static final String BANK_ACCOUNT_NAME = "BANK_ACCOUNT_NAME";
    public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    public static final String IFSCCODE = "IFSCCODE";
    public static final String BRANCH_NAME = "BRANCH_NAME";


    public String password = "pass";
    public String isLogin = "isLoggedIn";

    public static final String MY_PREF_NAME = "Talentogram";
    private Context context;

    public MySharedPref(Context context) {
        super();
        this.context = context;
    }

    public static boolean getIsFirstTime() {
        return shared.contains(isFirst) ? shared.getBoolean(isFirst, false) : false;
    }
    public static void setIsFirstTime(boolean isfirst) {
        et.putBoolean(isFirst, isfirst);
        et.commit();
    }
    public String getPassword() {
        return shared.contains(password) ? shared.getString(password, "") : "";
    }
    public void setPassword(String passwordname) {
        et.putString(password, passwordname);
        et.commit();
    }
    public boolean isLogin() {
        return !new MySharedPref(context)
                .getString(context,MySharedPref.USER_ID, "").equals("");
    }
    public static void MySharedPref(Context ct) {
        shared = ct.getSharedPreferences(MySharedPref.MY_PREF_NAME, 0);
        et = shared.edit();
    }

    public String getLoggedInUserID() {
        return getString(context,MySharedPref.USER_ID, "");
    }
    public String getAuth() {
        return getString(context,MySharedPref.USER_AUTH, "");
    }
    public String getLoggedInUserName() {
        return getString(context,MySharedPref.USER_FIRSTNAME, "");
    }

    public static String getString(Context context, final String key, final String value) {
        SharedPreferences sh = context.getSharedPreferences(MySharedPref.MY_PREF_NAME, 0);
        return sh.getString(key, value);
    }

    public static Boolean getBoolean(Context context, final String key, final Boolean value) {
        SharedPreferences sh = context.getSharedPreferences(MySharedPref.MY_PREF_NAME, 0);
        return sh.getBoolean(key, value);
    }

    public static void sharedPrefClear(final Context context) {
        SharedPreferences.Editor sha = context.getSharedPreferences(MySharedPref.MY_PREF_NAME, 0).edit();
        sha.clear();
        sha.apply();
        sha.commit();
    }

    public static void setString(Context context, final String key, final String value) {
        SharedPreferences.Editor sha = context.getSharedPreferences(MySharedPref.MY_PREF_NAME, 0).edit();
        sha.putString(key, value);
        sha.apply();
    }

    public static void setBoolean(Context context, final String key, final Boolean value) {
        SharedPreferences.Editor sha = context.getSharedPreferences(MySharedPref.MY_PREF_NAME, 0).edit();
        sha.putBoolean(key, value);
        sha.apply();
    }

    public void setInt (Context context,final String key, final int value) {
        SharedPreferences.Editor sha;
        sha = context.getSharedPreferences(MY_PREF_NAME, 0).edit();
        sha.putInt(key, value);
        sha.apply();
    }

}
