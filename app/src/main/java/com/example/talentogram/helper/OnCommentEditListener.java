package com.example.talentogram.helper;

public interface OnCommentEditListener {
    public void onClick(int id, String comment, int position);
}
