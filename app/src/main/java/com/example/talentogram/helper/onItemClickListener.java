package com.example.talentogram.helper;

import android.view.View;

public interface onItemClickListener {

    public void onClick(View view, int index);
}
