package com.example.talentogram.helper;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import com.example.talentogram.model.AudioPlayModel;
import com.example.talentogram.others.Utils;
import java.util.ArrayList;

public class CommonSDAsynch extends AsyncTask<Void, Void, ArrayList<AudioPlayModel>>{

    private ArrayList<AudioPlayModel> tempArray = new ArrayList<AudioPlayModel>();
    public Context ct;
    public SDcardAsynchInterfaces aInterface;
    public String dataType = "";
    public int lastRecordId = 0;

    public CommonSDAsynch(Fragment ft, Context ct, String dataType, int lastId) {
        this.ct = ct;
        this.aInterface = (SDcardAsynchInterfaces) ft;
        this.dataType = dataType;
        this.lastRecordId = lastId;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        try {
        } catch (Exception e) {
            e.printStackTrace();
        }

        Utils.showPopup(ct);
    }

    @Override
    protected ArrayList<AudioPlayModel> doInBackground(Void... voids) {
        tempArray = getPlayList("/sdcard/");

        return tempArray;
    }

    @Override
    protected void onPostExecute(ArrayList<AudioPlayModel> hashMaps) {
        super.onPostExecute(hashMaps);

        this.aInterface.onAsynchReturn(hashMaps);

        Utils.hidePopup();
    }

    public ArrayList<AudioPlayModel> getPlayList(String rootPath) {
        ArrayList<AudioPlayModel> fileList = new ArrayList<>();

        Cursor c = null;

        if ((dataType.equalsIgnoreCase("AudioModel"))) {
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

            String[] projection = {
                    MediaStore.Audio.AudioColumns._ID,
                    MediaStore.Audio.AudioColumns.DATA,
                    MediaStore.Audio.AudioColumns.TITLE,
                    MediaStore.Audio.AudioColumns.ALBUM,
                    MediaStore.Audio.ArtistColumns.ARTIST,
            };
            c = ct.getContentResolver().query(
                    uri,
                    projection,
                    MediaStore.Audio.AudioColumns._ID + " > " + String.valueOf(lastRecordId),
                    new String[]{}, "");
        } else {
            Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

            String[] projection = {
                    MediaStore.Video.VideoColumns._ID,
                    MediaStore.Video.VideoColumns.DATA,
                    MediaStore.Video.VideoColumns.TITLE,
                    MediaStore.Video.VideoColumns.ALBUM,
                    MediaStore.Video.VideoColumns.ARTIST,
            };

            c = ct.getContentResolver().query(
                    uri,
                    projection,
                    MediaStore.Video.VideoColumns._ID + " > " + String.valueOf(lastRecordId),
                    new String[]{}, "");
        }


        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    int songId = c.getInt(0);
                    String path = c.getString(1);
                    String name = c.getString(2);
                    String album = c.getString(3);
                    String artist = c.getString(4);


                    AudioPlayModel vModel = new AudioPlayModel();
                    vModel.setId(songId);
                    vModel.setName(name);
                    vModel.setPath(path);
                    fileList.add(vModel);
                }
                c.close();
            }
        }

        return fileList;

    }


}