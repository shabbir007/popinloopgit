package com.example.talentogram.helper;


import com.example.talentogram.model.AudioPlayModel;

import java.util.ArrayList;

public interface SDcardAsynchInterfaces {

    public void onAsynchReturn(ArrayList<AudioPlayModel> arrayList);
    public void onSongFound(AudioPlayModel songHashMap);

}