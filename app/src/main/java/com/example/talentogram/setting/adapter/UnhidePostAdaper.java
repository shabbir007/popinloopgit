package com.example.talentogram.setting.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.contest.AudioDialogActivity;
import com.example.talentogram.contest.activity.ViewWritingContestDetailActivity;
import com.example.talentogram.model.PostModel;
import com.example.talentogram.others.Utils;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.talentogram.others.Utils.getTimeDiff;

public class UnhidePostAdaper extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<PostModel> postModelList;
    public static final int VIDEOVIEW = 0;
    public static final int RESTVIEW = 1;


    @Override
    public int getItemViewType(int position) {
        if (postModelList.get(position).getPostType() == 1) {
            return VIDEOVIEW;
        } else {
            return RESTVIEW;
        }
    }

    public UnhidePostAdaper(Context context, ArrayList<PostModel> postModelList) {
        this.context = context;
        this.postModelList = postModelList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (VIDEOVIEW == viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_unhide_video, parent, false);
            return new VideoViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_unhide_post, parent, false);
            return new MyViewHolder(itemView);
        }
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderMain, int position) {
        PostModel postModel = postModelList.get(position);

        if (VIDEOVIEW == holderMain.getItemViewType()) {
        VideoViewHolder holder = (VideoViewHolder) holderMain;
            holder.setIsRecyclable(false);
            try {
                Glide.with(context)
                        .load(postModel.getUserdata().getProfilePics())
                        .thumbnail(Glide.with(context).load(R.drawable.noimage))
                        .into(holder.ivUserProfile);
            } catch (Exception e) {
                e.printStackTrace();

            }
            holder.tvUserName.setText(postModel.getUserdata().getFirstName() + " " + postModel.getUserdata().getLastName());

            holder.tvTime.setText(Utils.getdate(postModel.getCreated_at()));

            holder.ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu(v,context,postModel.getId(),position);
                }
            });

        }
        else {
           MyViewHolder holder = (MyViewHolder) holderMain;
            try {
                Glide.with(context)
                        .load(postModel.getUserdata().getProfilePics())
                        .thumbnail(Glide.with(context).load(R.drawable.noimage))
                        .into(holder.ivUserProfile);
            } catch (Exception e) {
                e.printStackTrace();

            }//audio View
            if (postModel.getPostType() == 0) {
                holder.flImage.setVisibility(View.GONE);
                holder.flAudio.setVisibility(View.VISIBLE);
                holder.flWriting.setVisibility(View.GONE);
                try {
                    Glide.with(context)
                            .load(R.drawable.audio_visualizer)
                            .thumbnail(Glide.with(context).load(R.drawable.audio_visualizer))
                            .into(holder.ivPostAudio);

                } catch (Exception e) {
                    e.printStackTrace();

                }
                holder.flAudio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = postModel.getAudioUrls().get(0).getAudioUrl();
                        String title = postModel.getPost_detail().getTitle();
                        Intent i = new Intent(context, AudioDialogActivity.class);
                        i.putExtra("AudioUrl", url);
                        i.putExtra("title", title);
                        context.startActivity(i);
                    }
                });

            }
            //imageView
            else if (postModel.getPostType() == 2 && postModel.getImageUrl().size() > 0) {
                holder.flImage.setVisibility(View.VISIBLE);
                holder.flAudio.setVisibility(View.GONE);
                holder.flWriting.setVisibility(View.GONE);
                try {
                    Glide.with(context)
                            .load(postModel.getImageUrl().get(0).getVideoUrl())
                            .thumbnail(Glide.with(context).load(R.drawable.noimage))
                            .error(R.drawable.noimage)
                            .into(holder.ivPostImage);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } //Writing view
            else if (postModel.getPostType() == 3) {
                holder.flImage.setVisibility(View.GONE);
                holder.flAudio.setVisibility(View.GONE);
                holder.flWriting.setVisibility(View.VISIBLE);
                holder.tvWriting.setText(postModel.getPost_detail().getDescription());

                if (!TextUtils.isEmpty(postModel.getColor())) {
                    String color_ ="#"+postModel.getColor();
                    holder.llWritingView.setBackgroundColor(Color.parseColor(color_));
                } else {
                    holder.llWritingView.setBackgroundColor(R.color.DarkGray);
                }
                if (!TextUtils.isEmpty(postModel.getFont())) {
                    String fontName = postModel.getFont();
                    Typeface font = Typeface.createFromAsset(context.getAssets(),
                            "fonts/" + fontName);
                    holder.tvWriting.setTypeface(font);
                }


                holder.tvWriting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, ViewWritingContestDetailActivity.class);
                        i.putExtra("description", postModel.getPost_detail().getDescription());
                        i.putExtra("title", postModel.getPost_detail().getTitle());
                        i.putExtra("color",postModel.getColor());
                        i.putExtra("font",postModel.getFont());
                        context.startActivity(i);
                    }
                });
            }

            getTimeDiff(postModel.getCreated_at());

            holder.tvUserName.setText(postModel.getUserdata().getFirstName() + " " + postModel.getUserdata().getLastName());
            holder.tvtime.setText(Utils.getdate(postModel.getCreated_at()));

            holder.ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu(v,context,postModel.getId(),position);
                }
            });

        }
    }
    public void showMenu(View v,Context context, int postId, int position) {
        PopupMenu popup = new PopupMenu(context, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_unhide:
                       unHidePost(context,postId,position);
                        break;

                }
                return false;
            }

        });// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.unhide_menu, popup.getMenu());
        popup.show();
    }

    @Override
    public int getItemCount() {
        return postModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircularImageView ivUserProfile;
        private TextView tvUserName,tvtime,tvWriting;
        private ImageView ivMore,ivPostImage,ivPostAudio;
        private FrameLayout flImage,flAudio,flWriting;
        private LinearLayout llWritingView;
        public MyViewHolder(@NonNull View view) {
            super(view);
            ivUserProfile = view.findViewById(R.id.ivUserProfile);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvtime = view.findViewById(R.id.tvTime);
            ivMore = view.findViewById(R.id.ivMore);
            ivPostImage = view.findViewById(R.id.ivPostImage);
            ivPostAudio = view.findViewById(R.id.ivPostAudio);
            llWritingView = view.findViewById(R.id.llWritingView);
            flImage = view.findViewById(R.id.flImage);
            flAudio = view.findViewById(R.id.flAudio);
            flWriting = view.findViewById(R.id.flWriting);
            tvWriting = view.findViewById(R.id.tvWriting);

        }
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
       ImageView ivMore;
       CircularImageView ivUserProfile;
       TextView tvUserName,tvTime;
        public VideoViewHolder(@NonNull View view) {
            super(view);
            ivUserProfile = view.findViewById(R.id.ivUserProfile);
            tvUserName = view.findViewById(R.id.tvUserName);
            ivUserProfile = view.findViewById(R.id.ivUserProfile);
            tvTime = view.findViewById(R.id.tvTime);
            ivMore = view.findViewById(R.id.ivMore);

        }
    }
    private void unHidePost(Context context, int postId, int position) {
        HashMap<String, String> userData = new HashMap<>();
        userData.put("PostId", String.valueOf(postId));

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(R.string.blank_warning_unhidepost);

        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
            new NetworkCall(context, Services.UNHIDE_POST, userData,
                    true, NetworkCall.REQ_POST,
                    true, new NetworkCall.MultiPartRequest() {
                @Override
                public void myResponseMethod(String response) {
                    try {
                        if (!response.isEmpty()) {
                            JSONObject obj = new JSONObject(response);
                            boolean Success = obj.getBoolean("Success");
                            String message = obj.getString("message");
                            if (Success) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle("Success");
                                alertDialog.setMessage(message);
                                alertDialog.setPositiveButton("ok", (dialog, which) -> {
                                    postModelList.remove(position);
                                    notifyDataSetChanged();
                                });
                                alertDialog.show();

                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
        });
        alertDialog.setNegativeButton("No", (dialog, which) -> {
            dialog.cancel();
        });
        alertDialog.show();
    }
}
