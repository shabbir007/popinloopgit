package com.example.talentogram.setting.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.talentogram.R;
import com.example.talentogram.setting.adapter.BlockUserListAdapter;
import com.example.talentogram.model.BlockedUserModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BlockedUserListActivity extends AppCompatActivity {

    private ArrayList<BlockedUserModel> blockedUserModels = new ArrayList<>();
    private BlockUserListAdapter blockUserListAdapter;
    private RecyclerView rvBlockedUser;
    private ImageView ivBack;
    private LinearLayout llNoUserBlocked;
    private LinearLayout llback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_user_list);

        bindWidgetReference();
        blockUserListView(); }

    private void bindWidgetReference() {
        rvBlockedUser = findViewById(R.id.rvBlockedUser);
        llNoUserBlocked = findViewById(R.id.llNoUserBlocked);
        ivBack = findViewById(R.id.ivBack);
        llback = findViewById(R.id.llBack);
        llback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void blockUserListView() {

        new NetworkCall(this, Services.BLOCK_USER_LIST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");
                    if (Success) {
                        JSONArray jblockList = jsonObject.getJSONArray("data");
                        blockedUserModels.clear();
                        blockedUserModels = (ArrayList<BlockedUserModel>)
                                new Gson().fromJson(jblockList.toString(),
                                        new TypeToken<ArrayList<BlockedUserModel>>() {
                                        }.getType());

                        blockUserListAdapter = new BlockUserListAdapter(BlockedUserListActivity.this, blockedUserModels);
                        rvBlockedUser.setAdapter(blockUserListAdapter);
                        rvBlockedUser.setLayoutManager(new LinearLayoutManager(BlockedUserListActivity.this,
                                LinearLayoutManager.VERTICAL, false));
                        if (blockedUserModels.size() == 0) {
                            llNoUserBlocked.setVisibility(View.VISIBLE);
                            rvBlockedUser.setVisibility(View.GONE);
                        }
                    } else {llNoUserBlocked.setVisibility(View.GONE);
                        rvBlockedUser.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

    }
}