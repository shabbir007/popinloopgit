package com.example.talentogram.setting.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.talentogram.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingActivity extends AppCompatActivity {
    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.llBlockedUser)
    LinearLayout llBlockedUser;

    @BindView(R.id.llUnhidePost)
    LinearLayout llUnhidePost;

    @BindView(R.id.llSavedPost)
    LinearLayout llSavedPost;

    @BindView(R.id.llReportedPost)
    LinearLayout llReportedPost;

    @BindView(R.id.llall_request_sent)
    LinearLayout llall_request_sent;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        bindWidget();
        tvTitle.setText(getResources().getString(R.string.setting));
    }

    private void bindWidget() {
        llBack.setOnClickListener(v -> onBackPressed());
        llBlockedUser.setOnClickListener(v -> {
            Intent i = new Intent(SettingActivity.this, BlockedUserListActivity.class);
            startActivity(i);

        });
        llUnhidePost.setOnClickListener(v -> {
            Intent i = new Intent(SettingActivity.this, UnhidePostActivity.class);
            startActivity(i);

        });
        llSavedPost.setOnClickListener(v -> {
            Intent i = new Intent(SettingActivity.this, UnSavePostActivity.class);
            startActivity(i);

        });
        llReportedPost.setOnClickListener(v -> {
            Intent i = new Intent(SettingActivity.this, ReportedPostActivity.class);
            startActivity(i);

        });
        llall_request_sent.setOnClickListener(v -> {
            Intent i = new Intent(SettingActivity.this, AllRequestActivity.class);
            startActivity(i);

        });

    }
}