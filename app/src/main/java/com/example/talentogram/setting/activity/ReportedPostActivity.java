package com.example.talentogram.setting.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.danikula.videocache.HttpProxyCacheServer;
import com.example.talentogram.R;
import com.example.talentogram.SegmentProgress.Variables;
import com.example.talentogram.setting.adapter.UnReportPostAdaper;
import com.example.talentogram.model.PostModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReportedPostActivity extends AppCompatActivity implements Player.EventListener {
    private ArrayList<PostModel> postList;
    private RecyclerView rvPost;
    private UnReportPostAdaper unReportPostAdaper;
    int currentPage = -1;
    private LinearLayout llBack,llNoData;
    private TextView tvTitle;
    private LinearLayoutManager layoutManager;
    boolean is_user_stop_video = false, isReloadList = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unhide_post);
        initView();
        getPost();
        tvTitle.setText(getResources().getString(R.string.txt_reported_post));   }

    private void initView() {
        layoutManager = new LinearLayoutManager(ReportedPostActivity.this);
        rvPost = findViewById(R.id.rv_post);
        llBack = findViewById(R.id.llBack);
        tvTitle = findViewById(R.id.tvTitle);
        llNoData = findViewById(R.id.llNoData);

        rvPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (isReloadList && newState == 0) {
                    isReloadList = false;
                    currentPage = -1;
                    setData();
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int page_no = layoutManager.findLastCompletelyVisibleItemPosition();

                if (page_no != currentPage && page_no >0) {
                    currentPage = page_no;
                    releasePreviousPlayer();
                    if (postList.get(currentPage).getPostType() == 1) {
                        Set_Player(currentPage);
                    }
                }
                System.out.println("page_no-----" + page_no);
                System.out.println("currentPage-----" + currentPage);
            }
        });
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setData() {
        rvPost.setLayoutManager(layoutManager);
        unReportPostAdaper = new UnReportPostAdaper(ReportedPostActivity.this, postList);
        rvPost.setAdapter(unReportPostAdaper);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (postList.get(0).getPostType() == 1) {
                    currentPage = 0;
                    Set_Player(0);
                }
            }
        }, 1000);
    }

    private void getPost() {
        new NetworkCall(ReportedPostActivity.this, Services.GET_REPORTED_POST,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {
                try {
                    if (!response.isEmpty()) {
                        JSONObject obj = new JSONObject(response);
                        boolean Success = obj.getBoolean("Success");
                        if (Success) {
                            JSONArray jPostData = obj.getJSONArray("data");
                            postList = new ArrayList<>();
                            postList = (ArrayList<PostModel>) new Gson().fromJson(jPostData.toString(),
                                    new TypeToken<ArrayList<PostModel>>() {
                                    }.getType());
                            postList.size();
                            llNoData.setVisibility(View.GONE);
                            rvPost.setVisibility(View.VISIBLE);
                            setData();
                        } else {
                            llNoData.setVisibility(View.VISIBLE);
                            rvPost.setVisibility(View.GONE);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    public void Call_cache() {
        if ((currentPage) < postList.size()) {
            HttpProxyCacheServer proxy = getProxy(ReportedPostActivity.this);
            proxy.getProxyUrl(postList.get(currentPage).getVideoUrl().get(0).getVideoUrl());
        }
    }

    private HttpProxyCacheServer proxy;

    public HttpProxyCacheServer getProxy(Context context) {
        return proxy == null ? (proxy = newProxy()) : proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(ReportedPostActivity.this)
                .maxCacheSize(1024 * 1024 * 1024)
                .maxCacheFilesCount(20)
                .build();
    }

    public SimpleExoPlayer privious_player;

    public void releasePreviousPlayer() {
        if (privious_player != null) {
            privious_player.removeListener(ReportedPostActivity.this);
            privious_player.release();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (privious_player != null) {
            privious_player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (privious_player != null) {
            privious_player.setPlayWhenReady(false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (privious_player != null) {
            privious_player.release();
        }
    }

    boolean is_visible_to_user;

    @Override
    protected void onResume() {
        super.onResume();
    }

    // this will call when swipe for another video and
    // this function will set the player to the current video
    public void Set_Player(final int currentPage) {

        final PostModel item = postList.get(currentPage);
        Call_cache();

        HttpProxyCacheServer proxy = getProxy(ReportedPostActivity.this);
        String proxyUrl = proxy.getProxyUrl(item.getVideoUrl().get(0).getVideoUrl());

        DefaultLoadControl loadControl = new DefaultLoadControl.Builder().
                setBufferDurationsMs(1 * 1024, 1 * 1024,
                        500, 1024).createDefaultLoadControl();

        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(ReportedPostActivity.this, trackSelector, loadControl);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(ReportedPostActivity.this, Util.getUserAgent(ReportedPostActivity.this, getResources().getString(R.string.app_name)));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(proxyUrl));

        Log.d(Variables.tag, item.getVideoUrl().get(0).getVideoUrl());
        Log.d(Variables.tag, proxyUrl);

        player.prepare(videoSource);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.addListener(ReportedPostActivity.this);
        final PlayerView playerView;
        if (layoutManager != null) {
            View layout = layoutManager.findViewByPosition(currentPage);
            if (layout != null) {
                playerView = layout.findViewById(R.id.playerview);
                playerView.setPlayer(player);
                player.setPlayWhenReady(is_visible_to_user);
                privious_player = player;

                final RelativeLayout mainlayout = layout.findViewById(R.id.mainlayout);

                playerView.setOnTouchListener(new View.OnTouchListener() {
                    private GestureDetector gestureDetector = new GestureDetector(ReportedPostActivity.this, new GestureDetector.SimpleOnGestureListener() {
                        @Override
                        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                            super.onFling(e1, e2, velocityX, velocityY);
                            float diffY = e2.getY() - e1.getY();
                            float diffX = e2.getX() - e1.getX();
                            if (Math.abs(diffX) > Math.abs(diffY)) {
                                if (Math.abs(diffX) > 100 && Math.abs(velocityX) > 1000) {
                                    if (diffX > 0) {
                                        if (player.getPlayWhenReady()) {
                                            is_user_stop_video = true;
                                            privious_player.setPlayWhenReady(false);
                                        }
                                    }
                                }
                            }
                            return true;
                        }

                        @Override
                        public boolean onSingleTapConfirmed(MotionEvent e) {
                            if (!player.getPlayWhenReady()) {
                                is_user_stop_video = false;
                                privious_player.setPlayWhenReady(true);

                            } else {
                                is_user_stop_video = true;
                                privious_player.setPlayWhenReady(false);
                            }
                            return super.onSingleTapConfirmed(e);
                        }

                        @Override
                        public boolean onSingleTapUp(MotionEvent e) {
                            super.onSingleTapUp(e);
                            return true;
                        }

                        @Override
                        public void onLongPress(MotionEvent e) {
                            super.onLongPress(e);

                        }

                    });

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        gestureDetector.onTouchEvent(event);
                        return true;
                    }
                });

            }
        }
    }
}