package com.example.talentogram.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.talentogram.R;
import com.example.talentogram.custom_views.CustomBoldTextView;
import com.example.talentogram.custom_views.CustomTextView;
import com.example.talentogram.model.BlockedUserModel;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class BlockUserListAdapter extends RecyclerView.Adapter<BlockUserListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<BlockedUserModel> blockUserList;

    public BlockUserListAdapter(Context context, ArrayList<BlockedUserModel> blockuserList) {
        this.context = context;
        this.blockUserList = blockuserList;
    }

    @NonNull
    @Override
    public BlockUserListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_followers, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BlockUserListAdapter.MyViewHolder holder, int position) {
        BlockedUserModel blockedUserModel = blockUserList.get(position);

        holder.tvUserName.setText(blockedUserModel.getBlock_user().getFirstName()+" "+blockedUserModel.getBlock_user().getLastName());

        Glide.with(context)
                .load(blockedUserModel.getBlock_user().getProfilePic())
                .thumbnail(Glide.with(context).load(R.drawable.user))
                .into(holder.ivProfile);

        holder.tvAddress.setText(blockedUserModel.getBlock_user().getAddress());
        holder.btnFollow.setText("Blocked");
    }

    @Override
    public int getItemCount() {
        return blockUserList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivProfile;
        private CustomBoldTextView tvUserName;
        private CustomTextView tvAddress;
        private Button btnFollow;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(context, itemView);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            btnFollow = itemView.findViewById(R.id.btnFollow);
        }
    }
}
