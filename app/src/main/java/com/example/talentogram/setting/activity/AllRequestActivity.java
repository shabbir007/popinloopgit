package com.example.talentogram.setting.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.talentogram.R;
import com.example.talentogram.adapter.AllRequestSentAdapter;
import com.example.talentogram.model.AllRequestSentModel;
import com.example.talentogram.network.NetworkCall;
import com.example.talentogram.network.Services;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AllRequestActivity extends AppCompatActivity {
    private RecyclerView rvData;
    private ArrayList<AllRequestSentModel> allRequestSentModels = new ArrayList<>();
    private ImageView ivBack;
    private AllRequestSentAdapter followingAdapter;
    private LinearLayout llNoData;
    private LinearLayout llback;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_request);
        bindWidgetReference();
        followingListView();
    }

    private void bindWidgetReference() {
        rvData = findViewById(R.id.rvData);
        llNoData = findViewById(R.id.llNoData);
        ivBack = findViewById(R.id.ivBack);
        llback = findViewById(R.id.llBack);
        tvTitle = findViewById(R.id.tvTitle);

        tvTitle.setText("All Request Sent");
        llback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void followingListView() {

        new NetworkCall(this, Services.ALL_REQUEST_SENT,
                true, NetworkCall.REQ_POST,
                true, new NetworkCall.MultiPartRequest() {
            @Override
            public void myResponseMethod(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean Success = jsonObject.getBoolean("Success");
                    if (Success) {
                        JSONArray jContestList = jsonObject.getJSONArray("data");
                        allRequestSentModels.clear();
                        allRequestSentModels = (ArrayList<AllRequestSentModel>)
                                new Gson().fromJson(jsonObject.get("data").toString(),
                                        new TypeToken<ArrayList<AllRequestSentModel>>() {
                                        }.getType());

                        followingAdapter = new AllRequestSentAdapter(AllRequestActivity.this, allRequestSentModels);
                        rvData.setAdapter(followingAdapter);
                        rvData.setLayoutManager(new LinearLayoutManager(AllRequestActivity.this,
                                LinearLayoutManager.VERTICAL, false));

                        if (allRequestSentModels.size() == 0) {
                            llNoData.setVisibility(View.VISIBLE);
                            rvData.setVisibility(View.GONE);
                        }
                    } else {
                        llNoData.setVisibility(View.VISIBLE);
                        rvData.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}